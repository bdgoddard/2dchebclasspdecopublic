function g = Gaussian(x)

    g = exp(-x.^2);

end