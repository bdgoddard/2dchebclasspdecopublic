function output = NumericalExperimentsExact2DPDECO(iNum,jNum,kNum)
    Tols2 = 10^-4; 
    Tols1 = 10^-8; 

    betaVec = [10^-5,10^-3, 10^-1, 10, 10^3];
    gammaVec = [0, 1, -1];
    bcVec = [(0.25*pi)^2,0,0.25,0,0];
    n = 21;
    N1 = 30;
    N2 = 30;

    testFunVec{1} = 'AD_Flow_DirichletInt_2D_1'; 
    testFunVec{2} = 'AD_Flow_NeumannInt_2D_1';
    testFunVec{3} = 'AD_Force_DirichletInt_2D_2'; 
    testFunVec{4} = 'AD_Force_NeumannInt_2D_2'; 
    testFunVec{5} = 'AD_Flow_NeumannInt_Exact_2D_0';

    BCStrVec{1} = 'ComputeDirichletConstantBCs';
    BCStrVec{2} = 'ComputeNeumannBCs';    
    BCStrVec{3} = 'ComputeDirichletConstantBCs';
    BCStrVec{4} = 'ComputeNeumannBCs';
    BCStrVec{5} = 'ComputeNeumannBCs';
    
    PDERHSStrVec{1} = 'AD_FlowfVext';
    PDERHSStrVec{2} = 'AD_FlowfVext';
    PDERHSStrVec{3} = 'AD_Force';
    PDERHSStrVec{4} = 'AD_Force';
    PDERHSStrVec{5} = 'AD_FlowfVext';

for i = iNum
    BCFunStr = BCStrVec{i};
    testFun = testFunVec{i};
    PDERHSStr = PDERHSStrVec{i};
    bc = bcVec(i);
for j = jNum
    beta = betaVec(j);
for k = kNum
    gamma = gammaVec(k);
    Phys_Area = struct('shape','Box','y1Min',-1,'y1Max',1,'N',[N1,N2], 'y2Min',-1,'y2Max',1);
    Plot_Area = struct('N1',100,'N2',100','y1Min', -1, 'y2Min', -1, 'y1Max', 1, 'y2Max', 1);
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGaussian2D');
    ProbSpecs = struct('BCFunStr', BCFunStr, 'PDERHSStr', PDERHSStr, 'ComputeNormStr', 'ComputeRelL2LinfNorm2D');
    DataInput = struct('testFun', testFun,'DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', beta,'gamma',gamma,'D0',1,'other',0, 'bc',bc);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('SolverFlag', 'FixPt', 'AdaSolver','ArmijoWolfe', 'lambda', 0.01, 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG','rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt, 'V2Num',V2Num);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_2D(opts);
    output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;
    text.JFW = output1a.Errors.ForwardError.JFW2D;
    text.JOpt = output1a.Errors.OptimizationError.JOpti2D;
    text.Iters = output1a.OptimizationResult.Iterations;
    text.gamma = gamma;
    iStr = num2str(i);
    jStr = num2str(j);
    kStr = num2str(k);
    ti = strcat("Test",iStr,jStr,kStr);
    ttext.(ti) = text;
end
end
    output.ttext = ttext;
    output.out = output1a;
end