function output = TableAppendixFixptArmijoWolfe()
 %%% Compares fixed-point with Armijo-Wolfe results to an analytical
 %%% solution for a flow control problem with no-flux boundary condition

 for i =1:5
    output = NumericalExperimentsExact2DPDECO(5,i:i,1:1);   % 2D Example 1
    output1a = output.out;
    
    beta = output1a.optsPhys.optsPhysFW.Params.beta;

    rhoErrOpt = output1a.Errors.OptimizationError.rhoExactErr2D;
    pErrOpt =  output1a.Errors.OptimizationError.pExactErr2D;
    disp(strcat('For beta =', num2str(beta), ' rho error = ',num2str(rhoErrOpt)))
    disp(strcat('For beta =', num2str(beta), ' p error = ',num2str(pErrOpt)))
   
 end    
    



end

      