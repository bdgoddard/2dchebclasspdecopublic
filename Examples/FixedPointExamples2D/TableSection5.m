function output = TableSection5()
%%%This compares Fixpt with Armijo-Wolfe to Newton Krylov for test problem
%%%in section 5.1
    texFile = 'FCNComparison2D.tex';
    tableCaption = 'Flow Control, No-Flux: Comparison of $\rho$ and $\vec{w}$ obtained from Newton--Krylov and fixed-point--Armijo--Wolfe solvers, for a range of $\kappa$ and $\beta$.';
    tableLabel = 'TabFCNCompare';

    fout=fopen(texFile, 'wt');
    fprintf(fout, '%s\n', '\begin{table}'); 
    fprintf(fout, '%s\n', '\centering'); 
    fprintf(fout, '%s\n', '\begin{tabular}{ | c | c || c | c | c | c | c ||}');
    fprintf(fout, '%s\n', '\hline');
    fprintf(fout, '\\multicolumn{2}{|c||}{}& %s & %s & %s & %s & %s  \\\\\n', '$\beta = 10^{-5}$', '$\beta = 10^{-3}$', '$\beta = 10^{-1}$', '$\beta = 10^{1}$', '$\beta = 10^{3}$'); 
    fprintf(fout, '%s\n', '\hline');
    fprintf(fout, '%s\n', '\hline');
    
    for i = 1:3 %kappa
        for k = 1:5 %beta
            %%% Solving examples with fixed point and armijo-wolfe
            output = NumericalExperimentsSection5PDECO(2,k:k,i:i);   % 2D Example 1
            output1a = output.out;
            output1a.OptimizationResult.CompTimeOpti
            output1a.OptimizationResult.Iterations
            
            %%% Extracting results for Newton-Krylov solver
            beta = output1a.optsPhys.optsPhysFW.Params.beta;
        
            IDC = output1a.IDC;
            FP_w = output1a.OptimizationResult.Control;
            FP_rho = output1a.OptimizationResult.rhoNum; 
            kappa = output1a.optsPhys.optsPhysFW.Params.gamma;
        
            %%% Comparing Newton-Krylov results to fixed-point with Armijo-Wolfe
            %%% using error measure in paper
            opts.beta = beta;
            opts.kappa = kappa;
            opts2 = struct;
            nk_one = DataStorage(['NewtonKrylovData' filesep 'FCNLarge'] ,@FCNExampleLargeN,opts,opts2); 
%             nk_one.tout
            nk_rho = nk_one.rho;
            nk_w = nk_one.w;
          
            rho_difference = ComputeRelL2LinfNorm2D(FP_rho, nk_rho', IDC.Int, []);
            w_difference = ComputeRelL2LinfNorm2D(FP_w, nk_w', IDC.Int, []);
            
            disp(strcat('For beta = ', num2str(beta), ', Kappa = ' , num2str(kappa),' rho error = ',num2str(rho_difference)))
            disp(strcat('For beta = ', num2str(beta), ', Kappa = ' , num2str(kappa),' w error = ',num2str(w_difference)))
            

            rho_difference = sprintf('%0.2e',rho_difference);
            rho_difference = strrep(rho_difference, 'e-0','e-');
            w_difference = sprintf('%0.2e',w_difference);
            w_difference = strrep(w_difference, 'e-0','e-');

            rhoOut = strcat("rho",num2str(k));
            wOut = strcat("w",num2str(k));
            resk.(rhoOut) = rho_difference;
            resk.(wOut) =w_difference;
        end
        kappa = sprintf('%g',kappa);

        fprintf(fout,'%s%s%s  & %s & %s%s%s & %s%s%s & %s%s%s & %s%s%s & %s%s%s\\\\\n', ...
                '\multirow{2}{*}{$\kappa= \numprint{',kappa,'}$}','$\mathcal{E}_{\rho}$','$\numprint{',resk.rho1,'}$','$\numprint{',...
                resk.rho2,'}$','$\numprint{',resk.rho3,'}$','$\numprint{',resk.rho4,'}$','$\numprint{',resk.rho5,'}$'); 
        fprintf(fout,' & %s & %s%s%s & %s%s%s & %s%s%s & %s%s%s & %s%s%s\\\\\n', ...
                '$\mathcal{E}_{\vec w}$','$\numprint{',resk.w1,'}$','$\numprint{',...
                resk.w2,'}$','$\numprint{',resk.w3,'}$','$\numprint{',resk.w4,'}$','$\numprint{',resk.w5,'}$');  
        fprintf(fout, '%s\n', '\hline');
    end
    fprintf(fout, '%s\n', '\end{tabular}');
    fprintf(fout, '%s%s%s\n', '\caption{', tableCaption, '}');
    fprintf(fout, '%s%s%s\n', '\label{', tableLabel, '}');
    fprintf(fout, '%s', '\end{table}'); % end the table
    fclose(fout)
end

      