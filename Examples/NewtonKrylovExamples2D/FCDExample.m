function output = FCDExample(opts,opts2)
%%%% needs imput specifying opts.kappa, opts.beta and opts2 = []

kappa = opts.kappa;
bc = (0.25*pi)^2;
%% set up domain
geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];
nx = 20;   
n = 10; % necessary to achieve 1e-10 acc.
geom.N = [nx;nx]; 

aBox = Box(geom); % make a Box object
aBox.ComputeAll;  % compute differentiation, integration, etc
aBox.ComputeInterpolationMatrix((-1:0.01:1)',(-1:0.01:1)',true,true);
x1 = aBox.Pts.y1_kv; x2 = aBox.Pts.y2_kv; % points from box
bound = aBox.Ind.bound;

L = aBox.Diff.Lap;
Lap = L;
grad = aBox.Diff.grad;
Dx1 = aBox.Diff.Dy1; 
Dx2 = aBox.Diff.Dy2;

IB = eye(nx^2); IB = sparse(IB(bound,:));
ZB = sparse(zeros(size(IB)));

% need to specify V2 and parameters
optsV2 = struct;
Conv = aBox.ComputeConvolutionMatrix(@GaussianNK,optsV2);

% computes dot product with the normal
N = aBox.Ind.normal;
Ngrad = sparse(N*grad);

T = 1;         % time interval of integration [0,T]
bet = opts.beta; % penalisation parameter

%% Initial/final values 
u0 = (0.25*pi)^2*cos(pi *x1/2).*cos(pi* x2/2) + (0.25*pi)^2;
vT = zeros(nx^2,1);    

%% Define nonlinear ODE system 
%
%   u' = K_1(t,u,v)*u - K_2(t,u,v)*v + f(t,u,v),
%   v' = K_3(t,u,v)*u - K_4(t,u,v)*v + g(t,u,v),
%
% where K_j = K_j(t,u,v).

I = speye(nx^2);
M = eye(nx^2);
M(bound,:) = 0;
M1 = sparse(M);
M2 = sparse(M);

%%

tstype = 2;
opts.tolValIn = 101;
opts.JFu = @JFu;
opts.JFv = @JFv;
opts.JGu = @JGu;
opts.JGv = @JGv;
opts.tol = 1e-16;
opts.verbose = 0;
opts.waitbar = 0;
method = 3;         % iterative solve with FD approx of Jacobian+dyn forcing
%iters = 11;        % max number of Newton iterations
iters = 10;         % max number of Newton iterations

    

%% INITAL CALL TO THE SOLVER
tic
%[t,u,v,out] = solver_old(@F, @G, M1, M2, u0, vT, T, n, tstype, method, iters, opts);
[t,u,v,out] = solver(@F, @G, M1, M2, u0, vT, T, n, method, iters, opts);
tout = toc;

 
w = -(1/bet)*[u;u].*(grad*v);

 for i = 1:n+1
     Vout(i,:) = Vext(t(i));
     gout(i,:) = g(t(i),0,0);
 end

output.out = out;
output.rho = u;
output.v = v;
output.w = w;
output.g = gout';
output.Vout = Vout';
output.Mass = aBox.Int*u;
output.t = t;
output.tout = tout;
  
    %% external potential 
    function sol = Vext(t)
      sol = 2*sin(pi*x2/3 - pi/2).*sin(pi*x1/2);
    end

    %% Construct F and G
    function F_out = F(t,u,v) 
        F_out = K1(t,u,v)*u - K2(t,u,v)*v + f(t,u,v);
    end
    function G_out = G(t,u,v) 
        G_out = K3(t,u,v)*u - K4(t,u,v)*v + g(t,u,v);
    end

    function K1_out = K1(t,u,v)
        K1_out = L  ...
             + 2/bet * scalarOperator(dotVectors(grad*u,grad*v)) ...
             + dotVectorOperator(grad*Vext,grad) ...
             + scalarOperator(Lap*Vext)...
             + kappa * ( dotVectorOperator(grad*(Conv*u),grad) ...
                              + scalarOperator(L*(Conv*u)) );  
                            
        K1_out(bound,:) = IB;
    end

    function K2_out = K2(t,u,v)
        K2_out = -1/bet*scalarOperator(u.^2)*L;
        K2_out(bound,:) = ZB;
    end

    function K3_out = K3(t,u,v)
        K3_out = -I + 1/bet * scalarOperator(dotVectors(grad*v,grad*v))...
            - kappa * ( Dx1*Conv*scalarOperator(Dx1*v) + Dx2*Conv*scalarOperator(Dx2*v) );
        K3_out(bound,:) = ZB;
    end

    function K4_out = K4(t,u,v)
        K4_out = L  - dotVectorOperator(grad*Vext,grad) ...
            - kappa * dotVectorOperator(grad*(Conv*u),grad);
        K4_out(bound,:) = IB;
    end

    function f_out = f(t,u,v)
        f_out = zeros(size(x1));
        f_out(bound) = - bc;
    end

    function g_out = g(t,u,v)
        g_out = (1 - t)*((0.25*pi)^2*cos(pi *x1/2).*cos(pi* x2/2) + (0.25*pi)^2)...
            + t*(((0.25*pi)^2*cos(pi *x1/2).*cos(3*pi* x2/2) + (0.25*pi)^2));
        g_out(bound) = 0;
    end

    %% Jacobian
    
    function J = JFu(t,u,v)        
        J =  L ...
             + 2/bet * scalarOperator(u)*scalarOperator(L*v)  ...
             + 2/bet * scalarOperator(dotVectors(grad*u,grad*v)) ...
             + 2/bet * scalarOperator(u) * dotVectorOperator(grad*v,grad) ...
             + dotVectorOperator(grad*Vext,grad) + scalarOperator(Lap*Vext)...
             + kappa* ( dotVectorOperator(grad*(Conv*u),grad) ...
                              + scalarOperator(L*(Conv*u)) ...
                              + dotVectorOperator((grad*u),grad)*Conv ...
                              + scalarOperator(u)*L*Conv );
          J(bound,:) = IB;
    end

    function J = JFv(t,u,v)
        J = 1/bet * scalarOperator(u.^2) * L ...
            + 2/bet * scalarOperator(u) * dotVectorOperator(grad*u,grad);
        J(bound,:) = ZB;
    end

    function J = JGu(t,u,v)
        J = -I + 1/bet * scalarOperator(dotVectors(grad*v,grad*v))...
            + kappa * ( dotVectorOperator(grad*v,grad*Conv) ...
                              - Dx1*Conv*scalarOperator(Dx1*v) - Dx2*Conv*scalarOperator(Dx2*v) );
        J(bound,:) = ZB;
    end

    function J = JGv(t,u,v)
        J = -L + dotVectorOperator(grad*Vext,grad)...
            + 2/bet * scalarOperator(u) * dotVectorOperator(grad*v,grad) ...
            + kappa * ( dotVectorOperator(grad*Conv*u,grad) ...
                              - Dx1 * Conv * scalarOperator(u) * Dx1 ...
                              - Dx2 * Conv * scalarOperator(u) * Dx2 );
        J(bound,:) = IB;
    end


end