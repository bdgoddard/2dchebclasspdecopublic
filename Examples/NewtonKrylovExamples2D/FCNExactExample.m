function output = FCNExactExample(opts,opts2)
%%%% needs imput specifying opts.kappa, opts.beta and opts2 = []

%% Set up domain
geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];

nx = 20;    
n = 10;
geom.N = [nx;nx]; 

aBox = Box(geom); % make a Box object
aBox.ComputeAll;  % compute differentiation, integration, etc

aBox.ComputeInterpolationMatrix((-1:0.01:1)',(-1:0.01:1)',true,true);

x1 = aBox.Pts.y1_kv; x2 = aBox.Pts.y2_kv; % points from box

bound = aBox.Ind.bound;

L = aBox.Diff.Lap;
Lap = L;
grad = aBox.Diff.grad;
Dx1 = aBox.Diff.Dy1; 
Dx2 = aBox.Diff.Dy2;

IB = eye(nx^2); IB = sparse(IB(bound,:));
ZB = sparse(zeros(size(IB)));

% computes dot product with the normal
N = aBox.Ind.normal;
Ngrad = sparse(N*grad);
T = 1;         % time interval of integration [0,T]
bet = opts.beta; % penalisation parameter

c1 = 0.25;
c2 = 0.25;

%% Initial/final values 
u0 = uex(0);
vT = zeros(nx^2,1);    

%% Define nonlinear ODE system 
%
%   u' = K_1(t,u,v)*u - K_2(t,u,v)*v + f(t,u,v),
%   v' = K_3(t,u,v)*u - K_4(t,u,v)*v + g(t,u,v),
%
% where K_j = K_j(t,u,v).

I = speye(nx^2);
M = eye(nx^2);
M(bound,:) = 0;
M1 = sparse(M);
M2 = sparse(M);

%%

opts.tolValIn = 201;
opts.uex = @uex;
opts.vex = @vex;
opts.JFu = @JFu;
opts.JFv = @JFv;
opts.JGu = @JGu;
opts.JGv = @JGv;
opts.tol = 1e-16;
opts.verbose = 0;
opts.waitbar = 0;
method = 3;         % iterative solve with FD approx of Jacobian+dyn forcing
iters = 10;         % max number of Newton iterations

%     s = 0.3;
%     U = uex(s);
%     V = vex(s);
%         
%     dtU = bet^(1/2)*c1/T*( exp(s/T))*(cos(pi*x) + 1);
%     dtV = bet^(1/2)*c2/T*( - exp(s/T))*(cos(pi*x) + 1);%/1.16068; 
%         
%     Ftemp = F(s,U,V);
%     Gtemp = G(s,U,V);
%     uErr = Ftemp-dtU
%     vErr = Gtemp-dtV
%     return

%% INITAL CALL TO THE SOLVER
tic
%[t,u,v,out] = solver_old(@F, @G, M1, M2, u0, vT, T, n, 2, method, iters, opts);
[t,u,v,out] = solver(@F, @G, M1, M2, u0, vT, T, n, method, iters, opts);
tout = toc;

for j = 1:length(t)
    U(:,j) = uex(t(j));
    V(:,j) = vex(t(j));
end 
%output.w = -(1/bet)*(grad*v).*u;
output.out = out;
output.rhoEx = U;
output.vEX = V;
output.rho = u;
output.v = v;
output.tout = tout;
output.Int = aBox.Int;



    %% external potential

    function sol = Vext(t)
        sol = cos(pi*x1).*cos(pi*x2); %0.1*(x1.^2).*(x2.^2);
    end


    %% exact solution
    function sol = uex(t)
        Crho = bet^(1/2);
        Trho = c1*exp(t);
        Yrho = (cos(pi*x1) + 1).*(cos(pi*x2) + 1);
        sol  = Crho*Trho*Yrho;
       
    end
    
    function sol = vex(t)
        Cp   = bet^(1/2);
        Tp   = c2*(exp(T) - exp(t));
        Yp   = (cos(pi*x1) + 1).*(cos(pi*x2) + 1);
        sol  = Cp*Tp*Yp;
    end

    %% Construct F and G
    function F_out = F(t,u,v) 
        F_out = K1(t,u,v)*u - K2(t,u,v)*v + f(t,u,v);
    end
    function G_out = G(t,u,v) 
        G_out = K3(t,u,v)*u - K4(t,u,v)*v + g(t,u,v);
    end


      function K1_out = K1(t,u,v)
        K1_out = L  ...
             + 2/bet * scalarOperator(dotVectors(grad*u,grad*v)) ...
             + dotVectorOperator(grad*Vext,grad) ...
             + scalarOperator(Lap*Vext);  
        temp3 = zeros(size(K1_out(bound,:)));
        temp3(:,bound) = scalarOperator(Ngrad*Vext);
        
        K1_out(bound,:) = Ngrad + sparse(temp3);
    end

    function K2_out = K2(t,u,v)
        K2_out = -1/bet*scalarOperator(u.^2)*L;
        K2_out(bound,:) = -bet^(-1)*scalarOperator(u(bound).^2)*Ngrad; 
    end

    function K3_out = K3(t,u,v)
        K3_out = -I + 1/bet * scalarOperator(dotVectors(grad*v,grad*v));
        K3_out(bound,:) = ZB;
    end

    function K4_out = K4(t,u,v)
        K4_out = L - dotVectorOperator(grad*Vext,grad);
        K4_out(bound,:) = Ngrad;
    end

    function f_out = f(t,u,v)
%         Crho = bet^(1/2);
%         Cp   = bet^(1/2);
%         
%         Trho = c1*exp(t);
%         dTrho = c1*exp(t);
%         Tp   = c2*(exp(T) - exp(t));
%         
%         Yrho = (cos(pi*x1) + 1).*(cos(pi*x2) + 1);
%         dY1rho = -pi*sin(pi*x1).*(cos(pi*x2) + 1);
%         dY2rho = -pi*sin(pi*x2).*(cos(pi*x1) + 1);
%         ddY1rho = -(pi)^2*cos(pi*x1).*(cos(pi*x2) + 1);
%         ddY2rho = -(pi)^2*cos(pi*x2).*(cos(pi*x1) + 1);
%         dY1p  = dY1rho;
%         ddY1p = ddY1rho;
%         dY2p  = dY2rho;
%         ddY2p = ddY2rho;
% 
%         rho = Crho*Trho*Yrho;
%         
%         rho_t = Crho*dTrho*Yrho;
%         rho_y1 = Crho*Trho*dY1rho;
%         rho_y2 = Crho*Trho*dY2rho;
%         rho_yy1 = Crho*Trho*ddY1rho;
%         rho_yy2 = Crho*Trho*ddY2rho;
%        
%         p_y1  = Cp*Tp*dY1p;
%         p_yy1 = Cp*Tp*ddY1p;
%         p_y2  = Cp*Tp*dY2p;
%         p_yy2 = Cp*Tp*ddY2p;
%         
%         
%         w1 = -1/bet * rho .* p_y1;
%         w2 = -1/bet * rho .* p_y2;
%         
%         w_y1 = -1/bet * ( rho_y1 .* p_y1 + rho .* p_yy1 );
%         w_y2 = -1/bet * ( rho_y2 .* p_y2 + rho .* p_yy2 );
%         
%         f_out = rho_t - rho_yy1 - rho_yy2 + w_y1.*rho + w1.*rho_y1 + w_y2.*rho + w2.*rho_y2...
%             - dotVectors((grad*Vext),[rho_y1; rho_y2]) - (Lap*Vext).*rho;
        
        f_out = 0.25*bet^(1/2)*exp(t)*(cos(pi*x1) +1).*(cos(pi*x2) +1) ...
            + 0.25*bet^(1/2)*pi^2*exp(t)*(cos(pi*x1).*(2*cos(pi*x2) + 1) + cos(pi*x2))...
            - pi^2*bet^(1/2)*exp(t)*((cos(pi*x1/2)).^2).*((cos(pi*x2/2)).^2).*(cos(pi*x1).*(1-4*cos(pi*x2)) + cos(pi*x2))...
            +0.25*pi^2*bet^(1/2)*exp(2*t)*(exp(T) - exp(t))*((cos(pi*x1/2)).^4).*((cos(pi*x2/2)).^4).*(cos(pi*x1).*(6*cos(pi*x2) + 1) + cos(pi*x2) - 4);
        
        f_out(bound) = 0;
    end

    function g_out = g(t,u,v)
%         Crho = bet^(1/2);
%         Cp   = bet^(1/2);
%         
%         Trho = c1*exp(t);%/2.5;
%         Tp   = c2*(exp(T) - exp(t));%/1.16068; 
%         dTp  = - c2*exp(t);%/1.16068; 
%         
%         Yrho = (cos(pi*x1) + 1).*(cos(pi*x2) + 1);
%         dY1rho = -pi*sin(pi*x1).*(cos(pi*x2) + 1);
%         dY2rho = -pi*sin(pi*x2).*(cos(pi*x1) + 1);
%         ddY1rho = -(pi)^2*cos(pi*x1).*(cos(pi*x2) + 1);
%         ddY2rho = -(pi)^2*cos(pi*x2).*(cos(pi*x1) + 1);
%         dY1p  = dY1rho;
%         ddY1p = ddY1rho;
%         dY2p  = dY2rho;
%         ddY2p = ddY2rho;
% 
%         rho = Crho*Trho*Yrho;
%        
%         p_t = Cp*dTp*Yrho;
%         p_y1  = Cp*Tp*dY1p;
%         p_yy1 = Cp*Tp*ddY1p;
%         p_y2  = Cp*Tp*dY2p;
%         p_yy2 = Cp*Tp*ddY2p;
%         
%         w = -1/bet * [rho .* p_y1; rho .* p_y2];
%         
%         g_out = p_t + p_yy1 + p_yy2 + rho + dotVectors(w,[p_y1;p_y2]) - dotVectors((grad*Vext),[p_y1;p_y2]);
        
        g_out = - 0.25*pi^2*bet^(1/2)*(exp(T) - exp(t))*...
            (cos(pi*x1).*(cos(pi*x2) + 1) + cos(pi*x2).*(cos(pi*x1) + 1)...
            +((sin(pi*x1)).^2).*cos(pi*x2).*(cos(pi*x2) + 1) + ((sin(pi*x2)).^2).*cos(pi*x1).*(cos(pi*x1) + 1))...
            + 0.5*pi^2*bet^(1/2)*exp(t)*((exp(T) - exp(t))^2)*((cos(pi*x1/2)).^4).*((cos(pi*x2/2)).^4).*(cos(pi*x1).*cos(pi*x2) - 1);
        g_out(bound) = 0;
    end

    %% Jacobian
   
    function J = JFu(t,u,v)        
        J =  L ...
             + 2/bet * scalarOperator(u)*scalarOperator(L*v)  ...
             + 2/bet * scalarOperator(dotVectors(grad*u,grad*v)) ...
             + 2/bet * scalarOperator(u) * dotVectorOperator(grad*v,grad) ...
             + dotVectorOperator(grad*Vext,grad) + scalarOperator(Lap*Vext);
          temp = zeros(size(J(bound,:)));
          temp(:,bound) = scalarOperator(u(bound).*(Ngrad*v));  
          temp3 = zeros(size(J(bound,:)));
          temp3(:,bound) = scalarOperator(Ngrad*Vext);
          J(bound,:) = Ngrad + 2/bet*sparse(temp) + sparse(temp3);
    end

    function J = JFv(t,u,v)
        J = 1/bet * scalarOperator(u.^2) * L ...
            + 2/bet * scalarOperator(u) * dotVectorOperator(grad*u,grad);
        J(bound,:) = 1/bet*scalarOperator(u(bound).^2)*Ngrad;
    end

    function J = JGu(t,u,v)
        J = -I + 1/bet * scalarOperator(dotVectors(grad*v,grad*v));
        J(bound,:) = ZB;
    end

    function J = JGv(t,u,v)
        J = -L ...
            + 2/bet * scalarOperator(u) * dotVectorOperator(grad*v,grad) ...
            + dotVectorOperator(grad*Vext,grad);
        J(bound,:) = Ngrad;
    end

end