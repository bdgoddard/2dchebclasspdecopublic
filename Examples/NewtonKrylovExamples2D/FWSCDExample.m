function results = FWSCDExample(opts,opts2)
%%%% needs imput specifying opts.kappa and opts2 = []

kappa = opts.kappa;
%--------------------------------------------------------------------------
% Make multishape and shorthands
%--------------------------------------------------------------------------
    bc = 0.25;
    
    N = 20;
    n = 10;
        
    geom.y1Min = -1;
    geom.y1Max = 1;
    geom.y2Min = -1;
    geom.y2Max = 1;
    geom.N = [N,N];
    aBox = Box(geom);
    
    aBox.ComputeAll(); 
    
    Pts = aBox.Pts;
    
    x1 = Pts.y1_kv;
    x2 = Pts.y2_kv;
    
    Ind = aBox.Ind;
    bound = Ind.bound;
    
    normal = Ind.normal;
    
    Diff = aBox.Diff;
    div = Diff.div;
    grad = Diff.grad;
    
    Int = aBox.Int;
    
    optsV2 = struct;
    Conv = aBox.ComputeConvolutionMatrix(@GaussianNK,optsV2);
    
    ge.yMin = 0;
    ge.yMax = 1;
    ge.N = n + 1;
    
    InterLine = SpectralLine(ge);
    
    times = InterLine.Pts.y;

%--------------------------------------------------------------------------
% Initial condition
%--------------------------------------------------------------------------
    
    rho_ic = 0.25*cos(pi* x1/2).*cos(pi*x2/2) + 0.25;

%--------------------------------------------------------------------------
% ODE setup
%--------------------------------------------------------------------------
    mM = ones(size(x1));
    mM(bound) = 0;  
    
    opts = odeset('RelTol',10^-9,'AbsTol',10^-9,'Mass',diag(mM));
    
    [outTimes, rho_t] = ode15s(@drho_dt,times,rho_ic,opts);  
    
    
    
     for i = 1:n+1
        gout(i,:) = g(times(i),0,0);
     end
    
    results.rho = rho_t';
    results.g = gout';
    results.bound = bound;
    results.w = zeros(size(rho_t'));
    
    function drhodt = drho_dt(t,rho)
        
        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(t));
        Interp=InterpT.InterPol;   

        rho2= [rho;rho];
        gradVext = grad*Vext(t); 

        ConvV2FW = rho2.*(grad*(Conv*rho));

        rhoflux =  -(grad*rho + rho2.*gradVext + kappa*ConvV2FW);       
        drhodt  = -div*rhoflux;
        
        drhodt(bound) = rho(bound) - bc;

        
    end

     function sol = Vext(t)
      sol = 0.75*(1-t)*( - cos(pi* x1/2).*sin(pi* x2/2) + 1);
     end
 
     function g_out = g(t,u,v)
        g_out = (0.25*cos(pi* x1/2).*cos(pi* x2/2) + 0.25)*(1 - t) + t*(0.25*sin(pi*x1).*sin(pi*x2/2 - pi/2) + 0.25);
     end

end