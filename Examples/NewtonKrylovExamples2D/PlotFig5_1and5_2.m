function PlotFig5_1and5_2()

optsa.beta = 10^-3;
optsa.kappa = -1;
optsb.beta = 10^-3;
optsb.kappa = 0;
optsc.beta = 10^-3;
optsc.kappa = 1;


FCNkn1 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,optsa,[]); 
FCNk0 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,optsb,[]); 
FCNk1 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,optsc,[]); 
opts3.ma = max([max(max(FCNkn1.rho)),max(max(FCNk0.rho)), max(max(FCNk1.rho))]);
opts3.mi = min([min(min(FCNkn1.rho)),min(min(FCNk0.rho)), min(min(FCNk1.rho))]);
opts3.me = (opts3.ma + opts3.mi)/2;
opts3.flnorm = max([max(max(abs(FCNkn1.w))),max(max(abs(FCNk0.w))), max(max(abs(FCNk1.w)))]);
opts3.Va = max([max(max(FCNkn1.Vout)),max(max(FCNk0.Vout)), max(max(FCNk1.Vout))]);
opts3.Vi = min([min(min(FCNkn1.Vout)),min(min(FCNk0.Vout)), min(min(FCNk1.Vout))]);
opts3.Ve = (opts3.Va + opts3.Vi)/2;

OptimalStatePlot(FCNkn1,20,opts3,'FCNkn1.png')
OptimalStatePlot(FCNk0,20,opts3,'FCNk0.png')
OptimalStatePlot(FCNk1,20,opts3,'FCNk1.png')
FlowControlPlot(FCNkn1,20,opts3,'FCNkn1c.png')
FlowControlPlot(FCNk0,20,opts3,'FCNk0c.png')
FlowControlPlot(FCNk1,20,opts3,'FCNk1c.png')

end