function PlotFig5_3()
maxNumCompThreads(10)

ComputeTab() 

function ComputeTab()
    kappaVec = [0, 1, -1];
f1 = figure( 'Position', [1,1,1400, 800]);
    for j = 1:3
        
        kappa = kappaVec(j);
        opts1.kappa = kappa;
        opts2.kappa = kappa;
        opts3.kappa = kappa;
        opts4.kappa = kappa;
        opts5.kappa = kappa;

        opts1.beta  = 10^-5;
        opts2.beta  = 10^-3;
        opts3.beta  = 10^-1;
        opts4.beta  = 10;
        opts5.beta  = 10^3;

        data1 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,opts1,opts1); 
        data2 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,opts2,opts2); 
        data3 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,opts3,opts3); 
        data4 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,opts4,opts4); 
        data5 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,opts5,opts5); 
     

        
        colorOrder = {'g','r' ,'b' ,'m' ,'y'};
        markers = { ':o','-^','--*','-+','-.d' };
        
      
       
        hold on
        subplot(2,3,j)
        semilogy(0:length(data1.out.nrmresf)-1,data1.out.nrmresf,markers{1},'Color',colorOrder{1});
        hold on
        semilogy(0:length(data2.out.nrmresf)-1,data2.out.nrmresf,markers{2},'Color',colorOrder{2});
        hold on
        semilogy(0:length(data3.out.nrmresf)-1,data3.out.nrmresf,markers{3},'Color',colorOrder{3});
        hold on
        semilogy(0:length(data4.out.nrmresf)-1,data4.out.nrmresf,markers{4},'Color',colorOrder{4});
        hold on
        semilogy(0:length(data5.out.nrmresf)-1,data5.out.nrmresf,markers{5},'Color',colorOrder{5});
        hold on
        
        
        xlabel('Newton Iteration','interpreter', 'latex')
        ylabel('Residual Error','interpreter', 'latex')
        set(gca,'FontSize',15);  
        
        subplot(2,3,3 + j)
              
        semilogy(0:length(data1.out.nrmresg)-1,data1.out.nrmresg,markers{1},'Color',colorOrder{1});
        hold on
        semilogy(0:length(data2.out.nrmresg)-1,data2.out.nrmresg,markers{2},'Color',colorOrder{2});
        hold on
        semilogy(0:length(data3.out.nrmresg)-1,data3.out.nrmresg,markers{3},'Color',colorOrder{3});
        hold on
        semilogy(0:length(data4.out.nrmresg)-1,data4.out.nrmresg,markers{4},'Color',colorOrder{4});
        hold on
        semilogy(0:length(data5.out.nrmresg)-1,data5.out.nrmresg,markers{5},'Color',colorOrder{5});
        hold on
        xlabel('Newton Iteration','interpreter', 'latex')
        ylabel('Residual Error','interpreter', 'latex')
        set(gca,'FontSize',15);  
        hold off

       
 
    end
        subplot(2,3,1)
        title('$\kappa = 0$','interpreter', 'latex')
        subplot(2,3,2)
        title('$\kappa = 1$','interpreter', 'latex')
        subplot(2,3,3)
        title('$\kappa = -1$','interpreter', 'latex')
        subplot(2,3,4)
        title('$\kappa = 0$','interpreter', 'latex')
        subplot(2,3,5)
        title('$\kappa = 1$','interpreter', 'latex')
        subplot(2,3,6)
        title('$\kappa = -1$','interpreter', 'latex')       
        legend( '$\beta = 10^{-5}$','$\beta = 10^{-3}$','$\beta = 10^{-1}$','$\beta = 10^{1}$','$\beta = 10^{3}$','interpreter', 'latex')

    
        save2png('FCNConvergence.png',gcf,300)

    
end

end
