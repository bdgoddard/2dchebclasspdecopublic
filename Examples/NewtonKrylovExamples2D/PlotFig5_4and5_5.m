function PlotFig5_4and5_5()

optsa.beta = 10^-3;
optsa.kappa = -1;
optsb.beta = 10^-3;
optsb.kappa = 0;
optsc.beta = 10^-3;
optsc.kappa = 1;


FCDkn1 = DataStorage(['NewtonKrylovData' filesep 'nFCD'] ,@FCDExample,optsa,[]); 
FCDk0 = DataStorage(['NewtonKrylovData' filesep 'nFCD'] ,@FCDExample,optsb,[]); 
FCDk1 = DataStorage(['NewtonKrylovData' filesep 'nFCD'] ,@FCDExample,optsc,[]); 
opts4.ma = max([max(max(FCDkn1.rho)),max(max(FCDk0.rho)), max(max(FCDk1.rho))]);
opts4.mi = min([min(min(FCDkn1.rho)),min(min(FCDk0.rho)), min(min(FCDk1.rho))]);
opts4.me = (opts4.ma + opts4.mi)/2;
opts4.flnorm = max([max(max(abs(FCDkn1.w))),max(max(abs(FCDk0.w))), max(max(abs(FCDk1.w)))]);
opts4.Va = max([max(max(FCDkn1.Vout)),max(max(FCDk0.Vout)), max(max(FCDk1.Vout))]);
opts4.Vi = min([min(min(FCDkn1.Vout)),min(min(FCDk0.Vout)), min(min(FCDk1.Vout))]);
opts4.Ve = (opts4.Va + opts4.Vi)/2;

OptimalStatePlot(FCDkn1,20,opts4,'FCDkn1.png')
OptimalStatePlot(FCDk0,20,opts4,'FCDk0.png')
OptimalStatePlot(FCDk1,20,opts4,'FCDk1.png')
FlowControlPlot(FCDkn1,20,opts4,'FCDkn1c.png')
FlowControlPlot(FCDk0,20,opts4,'FCDk0c.png')
FlowControlPlot(FCDk1,20,opts4,'FCDk1c.png')


end