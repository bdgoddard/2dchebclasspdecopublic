function PlotFig5_6and5_7()

optsa.beta = 10^-3;
optsa.kappa = -1;
optsb.beta = 10^-3;
optsb.kappa = 0;
optsc.beta = 10^-3;
optsc.kappa = 1;



SCNkn1 = DataStorage(['NewtonKrylovData' filesep 'nSCN'] ,@SCNExample,optsa,[]); 
SCNk0 = DataStorage(['NewtonKrylovData' filesep 'nSCN'] ,@SCNExample,optsb,[]); 
SCNk1 = DataStorage(['NewtonKrylovData' filesep 'nSCN'] ,@SCNExample,optsc,[]); 
opts2.ma = max([max(max((SCNkn1.rho))),max(max((SCNk0.rho))), max(max((SCNk1.rho)))]);
opts2.mi = min([min(min((SCNkn1.rho))),min(min((SCNk0.rho))), min(min((SCNk1.rho)))]);
opts2.me = (opts2.ma + opts2.mi)/2;
opts2.mac = max([max(max((SCNkn1.w))),max(max((SCNk0.w))), max(max((SCNk1.w)))]);
opts2.mic = min([min(min((SCNkn1.w))),min(min((SCNk0.w))), min(min((SCNk1.w)))]);
opts2.mec = (opts2.mac + opts2.mic)/2;
opts2.Va = max([max(max((SCNkn1.Vout))),max(max((SCNk0.Vout))), max(max((SCNk1.Vout)))]);
opts2.Vi = min([min(min((SCNkn1.Vout))),min(min((SCNk0.Vout))), min(min((SCNk1.Vout)))]);
opts2.Ve = (opts2.Va + opts2.Vi)/2;
 
OptimalStatePlot(SCNkn1,20,opts2,'SCNkn1.png')
OptimalStatePlot(SCNk0,20,opts2,'SCNk0.png')
OptimalStatePlot(SCNk1,20,opts2,'SCNk1.png')
SourceControlPlot(SCNkn1,20,opts2,'SCNkn1c.png')
SourceControlPlot(SCNk0,20,opts2,'SCNk0c.png')
SourceControlPlot(SCNk1,20,opts2,'SCNk1c.png')

end