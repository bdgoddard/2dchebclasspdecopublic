function PlotFig5_8and5_9()

optsa.beta = 10^-3;
optsa.kappa = -1;
optsb.beta = 10^-3;
optsb.kappa = 0;
optsc.beta = 10^-3;
optsc.kappa = 1;


SCDkn1 = DataStorage(['NewtonKrylovData' filesep 'nSCD'] ,@SCDExample,optsa,[]); 
SCDk0 = DataStorage(['NewtonKrylovData' filesep 'nSCD'] ,@SCDExample,optsb,[]); 
SCDk1 = DataStorage(['NewtonKrylovData' filesep 'nSCD'] ,@SCDExample,optsc,[]); 
opts1.ma = max([max(max((SCDkn1.rho))),max(max((SCDk0.rho))), max(max((SCDk1.rho)))]);
opts1.mi = min([min(min((SCDkn1.rho))),min(min((SCDk0.rho))), min(min((SCDk1.rho)))]);
opts1.me = (opts1.ma + opts1.mi)/2;
opts1.mac = max([max(max((SCDkn1.w))),max(max((SCDk0.w))), max(max((SCDk1.w)))]);
opts1.mic = min([min(min((SCDkn1.w))),min(min((SCDk0.w))), min(min((SCDk1.w)))]);
opts1.mec = (opts1.mac + opts1.mic)/2;
opts1.Va = max([max(max((SCDkn1.Vout))),max(max((SCDk0.Vout))), max(max((SCDk1.Vout)))]);
opts1.Vi = min([min(min((SCDkn1.Vout))),min(min((SCDk0.Vout))), min(min((SCDk1.Vout)))]);
opts1.Ve = (opts1.Va + opts1.Vi)/2;
 
OptimalStatePlot(SCDkn1,20,opts1,'SCDkn1.png')
OptimalStatePlot(SCDk0,20,opts1,'SCDk0.png')
OptimalStatePlot(SCDk1,20,opts1,'SCDk1.png')
SourceControlPlot(SCDkn1,20,opts1,'SCDkn1c.png')
SourceControlPlot(SCDk0,20,opts1,'SCDk0c.png')
SourceControlPlot(SCDk1,20,opts1,'SCDk1c.png')


end