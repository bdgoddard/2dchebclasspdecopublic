function data = RunAll2DNewtonKrylovOCPs()

opts = struct;
opts2 = struct;


betVec = [10^-5, 10^-3, 10^-1, 10, 10^3];
kappaVec = [0, 1, -1];


for i = 1:5
    for j = 1:3
        bet = betVec(i);
        kappa = kappaVec(j);
        opts.beta = bet;
        opts.kappa = kappa;
        % Example 5.1.2
        data1 = DataStorage(['NewtonKrylovData' filesep 'aFCD'] ,@FCDExample,opts,opts2); 
        % Example 5.1.1
        data2 = DataStorage(['NewtonKrylovData' filesep 'aFCN'] ,@FCNExample,opts,opts2); 
        % Example 5.1.4
        data3 = DataStorage(['NewtonKrylovData' filesep 'aSCD'] ,@SCDExample,opts,opts2); 
        % Example 5.1.3
        data4 = DataStorage(['NewtonKrylovData' filesep 'aSCN'] ,@SCNExample,opts,opts2); 
    end
end

data.f1 = data1;
data.f2 = data2;
data.f3 = data3;
data.f4 = data4;
end