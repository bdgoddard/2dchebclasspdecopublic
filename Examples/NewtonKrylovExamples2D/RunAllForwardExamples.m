function data = RunAllForwardExamples()
% Running all uncontrolled examples, which are used for baseline cost
opts = struct;
opts2 = struct;

kappaVec = [0, 1, -1];


for j = 1:3
    kappa = kappaVec(j);
    opts.kappa = kappa;
    % Example 5.1.2
    data1 = DataStorage(['NewtonKrylovData' filesep 'FWFCD'] ,@FWFCDExample,opts,opts2); 
    % Example 5.1.1
    data2 = DataStorage(['NewtonKrylovData' filesep 'FWFCN'] ,@FWFCNExample,opts,opts2); 
    % Example 5.1.4
    data3 = DataStorage(['NewtonKrylovData' filesep 'FWSCD'] ,@FWSCDExample,opts,opts2); 
    % Example 5.1.3
    data4 = DataStorage(['NewtonKrylovData' filesep 'FWSCN'] ,@FWSCNExample,opts,opts2); 
end


data.f1 = data1;
data.f2 = data2;
data.f3 = data3;
data.f4 = data4;
end