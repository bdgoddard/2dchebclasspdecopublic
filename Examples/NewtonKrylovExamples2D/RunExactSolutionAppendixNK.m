function data = RunExactSolutionAppendixNK()
opts = struct;
opts2 = struct;


betVec = [10^-5, 10^-3, 10^-1, 10, 10^3];



for i = 1:5
    bet = betVec(i);
    opts.beta = bet;
    % Exact Solution for validation in appendix
    data1 = DataStorage(['NewtonKrylovData' filesep 'FCNExact'] ,@FCNExactExample,opts,opts2); 

end

data.f1 = data1;

end