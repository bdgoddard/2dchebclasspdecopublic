function dataOut = RunFCNExampleLargeN()
    % Runs Example 5.1.1 with larger N and n to compare to fixed point method
    opts = struct;
    opts2 = struct;
       
    betVec = [10^-5, 10^-3, 10^-1, 10, 10^3];
    kappaVec = [0, 1, -1];
    
    for i = 1:5
        for j = 1:3
            bet = betVec(i);
            kappa = kappaVec(j);
            opts.beta = bet;
            opts.kappa = kappa;
            data = DataStorage(['NewtonKrylovData' filesep 'aFCNLargeM'] ,@FCNExampleLargeN,opts,opts2); 
        end
    end
    
    dataOut = data;
end