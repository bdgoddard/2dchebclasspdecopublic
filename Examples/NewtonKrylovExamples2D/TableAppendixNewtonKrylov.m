function TableAppendixNewtonKrylov()
maxNumCompThreads(10)

tableCaption1 = 'Exact solution for a flow-control problem with no-flux boundary conditions. Errors in the state $\rho$ and the adjoint $\adj$ are displayed for different values of $\beta$.';

% Computes Table A.1, Newton-Krylov Result
ComputeTab('FCNExact2D.tex', tableCaption1, 'TabFCNExact') 
 
function ComputeTab(texFile, tableCaption,tableLabel)

    fout=fopen(texFile, 'wt');
    fprintf(fout, '%s\n', '\begin{table}'); 
    fprintf(fout, '%s\n', '\centering'); 
    fprintf(fout, '%s\n', '\begin{tabular}{ | c || c | c | c | c | c ||}');
    fprintf(fout, '%s\n', '\hline');
    fprintf(fout, '& %s & %s & %s & %s & %s  \\\\\n', '$\beta = 10^{-5}$', '$\beta = 10^{-3}$', '$\beta = 10^{-1}$', '$\beta = 10^{1}$', '$\beta = 10^{3}$'); 
    fprintf(fout, '%s\n', '\hline');
    fprintf(fout, '%s\n', '\hline');


    opts1.beta  = 10^-5;
    opts2.beta  = 10^-3;
    opts3.beta  = 10^-1;
    opts4.beta  = 10;
    opts5.beta  = 10^3;

    data1 = DataStorage(['NewtonKrylovData' filesep 'FCNExact'] ,@FCNExactExample,opts1,opts2); 
    data2 = DataStorage(['NewtonKrylovData' filesep 'FCNExact'] ,@FCNExactExample,opts2,opts2); 
    data3 = DataStorage(['NewtonKrylovData' filesep 'FCNExact'] ,@FCNExactExample,opts3,opts2); 
    data4 = DataStorage(['NewtonKrylovData' filesep 'FCNExact'] ,@FCNExactExample,opts4,opts2); 
    data5 = DataStorage(['NewtonKrylovData' filesep 'FCNExact'] ,@FCNExactExample,opts5,opts2); 

    
    ComputeRelL2LinfNorm2D(data1.rho', data1.rhoEx', data1.Int, [])
    UErr1 = ComputeRelL2LinfNorm2D(data1.rho', data1.rhoEx', data1.Int, []);
    UErr2 = ComputeRelL2LinfNorm2D(data2.rho', data2.rhoEx', data2.Int, []);
    UErr3 = ComputeRelL2LinfNorm2D(data3.rho', data3.rhoEx', data3.Int, []);
    UErr4 = ComputeRelL2LinfNorm2D(data4.rho', data4.rhoEx', data4.Int, []);
    UErr5 = ComputeRelL2LinfNorm2D(data5.rho', data5.rhoEx', data5.Int, []);
    
    VErr1 = ComputeRelL2LinfNorm2D(data1.v', data1.vEX', data1.Int, []);
    VErr2 = ComputeRelL2LinfNorm2D(data2.v', data2.vEX', data2.Int, []);
    VErr3 = ComputeRelL2LinfNorm2D(data3.v', data3.vEX', data3.Int, []);
    VErr4 = ComputeRelL2LinfNorm2D(data4.v', data4.vEX', data4.Int, []);
    VErr5 = ComputeRelL2LinfNorm2D(data5.v', data5.vEX', data5.Int, []);

    UErr1 = sprintf('%0.2e',UErr1);
    UErr1 = strrep(UErr1, 'e-0','e-');
    UErr2 = sprintf('%0.2e',UErr2);
    UErr2 = strrep(UErr2, 'e-0','e-');
    UErr3 = sprintf('%0.2e',UErr3);
    UErr3 = strrep(UErr3, 'e-0','e-');
    UErr4 = sprintf('%0.2e',UErr4);
    UErr4 = strrep(UErr4, 'e-0','e-');
    UErr5 = sprintf('%0.2e',UErr5);
    UErr5 = strrep(UErr5, 'e-0','e-');

    VErr1 = sprintf('%0.2e',VErr1);
    VErr1 = strrep(VErr1, 'e-0','e-');
    VErr2 = sprintf('%0.2e',VErr2);
    VErr2 = strrep(VErr2, 'e-0','e-');
    VErr3 = sprintf('%0.2e',VErr3);
    VErr3 = strrep(VErr3, 'e-0','e-');
    VErr4 = sprintf('%0.2e',VErr4);
    VErr4 = strrep(VErr4, 'e-0','e-');
    VErr5 = sprintf('%0.2e',VErr5);
    VErr5 = strrep(VErr5, 'e-0','e-');

    fprintf(fout,' %s & %s%s%s & %s%s%s & %s%s%s & %s%s%s & %s%s%s\\\\\n', ...
            '$\mathcal{E}_{\rho}$','$\numprint{',UErr1,'}$','$\numprint{',...
            UErr2,'}$','$\numprint{',UErr3,'}$','$\numprint{',UErr4,'}$','$\numprint{',UErr5,'}$'); 
    fprintf(fout,' %s & %s%s%s & %s%s%s & %s%s%s & %s%s%s & %s%s%s\\\\\n', ...
            '$\mathcal{E}_{\adj}$','$\numprint{',VErr1,'}$','$\numprint{',...
            VErr2,'}$','$\numprint{',VErr3,'}$','$\numprint{',VErr4,'}$','$\numprint{',VErr5,'}$');  
    fprintf(fout, '%s\n', '\hline');


    fprintf(fout, '%s\n', '\end{tabular}');
    fprintf(fout, '%s%s%s\n', '\caption{', tableCaption, '}');
    fprintf(fout, '%s%s%s\n', '\label{', tableLabel, '}');
    fprintf(fout, '%s', '\end{table}'); 
    fclose(fout)

    end
end
