function TablesSection5()
maxNumCompThreads(10)

tableCaption1 = 'Source Control Dirichlet Problem: Cost $\mathcal{J}_{uc}$ of applying no control (i.e., $\vec{w} = \vec{0}$)and optimal control cost $\mathcal{J}_{c}$ for a range of values of the interaction strength $\kappa$ and regularization parameter $\beta$. The value of $\mathcal J_{c}$ for $\beta = 10^{-5}$ is of order $10^{-5}$.';
tableCaption2 = 'Flow Control No-Flux Problem: Cost when $\vec{w}=\vec{0}$ and optimal control cost for a range of $\kappa$, $\beta$.';
tableCaption3 = 'Flow Control Dirichlet Problem: Cost when $w=0$ and optimal control cost for a range of $\kappa$, $\beta$.';
tableCaption4 = 'Source Control No-Flux Problem: Cost when $w=0$ and optimal control cost for a range of $\kappa$, $\beta$.';


 % Computes Table 5.5
 ComputeTab(1, 'SCDExample2D.tex', tableCaption1, 'TabSCD') 
 % Computes Table 5.1
 ComputeTab(2, 'FCNExample2D.tex', tableCaption2, 'TabFCN') 
 % Computes Table 5.3
 ComputeTab(3, 'FCDExample2D.tex', tableCaption3, 'TabFCD') 
 % Computes Table 5.4
 ComputeTab(4, 'SCNExample2D.tex', tableCaption4, 'TabSCN') 
    
 
function ComputeTab(num, texFile, tableCaption,tableLabel)

    kappaVec = [0, 1, -1];

    fout=fopen(texFile, 'wt');
    fprintf(fout, '%s\n', '\begin{table}'); 
    fprintf(fout, '%s\n', '\centering'); 
    fprintf(fout, '%s\n', '\begin{tabular}{ | c | c || c | c | c | c | c ||}');
    fprintf(fout, '%s\n', '\hline');
    fprintf(fout, '\\multicolumn{2}{|c||}{}& %s & %s & %s & %s & %s  \\\\\n', '$\beta = 10^{-5}$', '$\beta = 10^{-3}$', '$\beta = 10^{-1}$', '$\beta = 10^{1}$', '$\beta = 10^{3}$'); 
    fprintf(fout, '%s\n', '\hline');
    fprintf(fout, '%s\n', '\hline');


    for j = 1:3
        kappa = kappaVec(j);
        opts1.kappa = kappa;
        opts2.kappa = kappa;
        opts3.kappa = kappa;
        opts4.kappa = kappa;
        opts5.kappa = kappa;

        opts1.beta  = 10^-5;
        opts2.beta  = 10^-3;
        opts3.beta  = 10^-1;
        opts4.beta  = 10;
        opts5.beta  = 10^3;

        if num ==1     
            FWdata = DataStorage(['NewtonKrylovData' filesep 'FWSCD'] ,@FWSCDExample,opts1,opts1); 

            data1 = DataStorage(['NewtonKrylovData' filesep 'aSCD'] ,@SCDExample,opts1,opts1); 
            data2 = DataStorage(['NewtonKrylovData' filesep 'aSCD'] ,@SCDExample,opts2,opts2); 
            data3 = DataStorage(['NewtonKrylovData' filesep 'aSCD'] ,@SCDExample,opts3,opts3); 
            data4 = DataStorage(['NewtonKrylovData' filesep 'aSCD'] ,@SCDExample,opts4,opts4); 
            data5 = DataStorage(['NewtonKrylovData' filesep 'aSCD'] ,@SCDExample,opts5,opts5); 
        elseif num ==2 
            FWdata = DataStorage(['NewtonKrylovData' filesep 'FWFCN'] ,@FWFCNExample,opts1,opts1); 

            data1 = DataStorage(['NewtonKrylovData' filesep 'aFCN'] ,@FCNExample,opts1,opts1); 
            data2 = DataStorage(['NewtonKrylovData' filesep 'aFCN'] ,@FCNExample,opts2,opts2); 
            data3 = DataStorage(['NewtonKrylovData' filesep 'aFCN'] ,@FCNExample,opts3,opts3); 
            data4 = DataStorage(['NewtonKrylovData' filesep 'aFCN'] ,@FCNExample,opts4,opts4); 
            data5 = DataStorage(['NewtonKrylovData' filesep 'aFCN'] ,@FCNExample,opts5,opts5); 
        elseif num == 3
            FWdata = DataStorage(['NewtonKrylovData' filesep 'FWFCD'] ,@FWFCDExample,opts1,opts1); 

            data1 = DataStorage(['NewtonKrylovData' filesep 'aFCD'] ,@FCDExample,opts1,opts1); 
            data2 = DataStorage(['NewtonKrylovData' filesep 'aFCD'] ,@FCDExample,opts2,opts2); 
            data3 = DataStorage(['NewtonKrylovData' filesep 'aFCD'] ,@FCDExample,opts3,opts3); 
            data4 = DataStorage(['NewtonKrylovData' filesep 'aFCD'] ,@FCDExample,opts4,opts4); 
            data5 = DataStorage(['NewtonKrylovData' filesep 'aFCD'] ,@FCDExample,opts5,opts5); 
        elseif num ==4
            FWdata = DataStorage(['NewtonKrylovData' filesep 'FWSCN'] ,@FWSCNExample,opts1,opts1); 

            data1 = DataStorage(['NewtonKrylovData' filesep 'aSCN'] ,@SCNExample,opts1,opts1); 
            data2 = DataStorage(['NewtonKrylovData' filesep 'aSCN'] ,@SCNExample,opts2,opts2); 
            data3 = DataStorage(['NewtonKrylovData' filesep 'aSCN'] ,@SCNExample,opts3,opts3); 
            data4 = DataStorage(['NewtonKrylovData' filesep 'aSCN'] ,@SCNExample,opts4,opts4); 
            data5 = DataStorage(['NewtonKrylovData' filesep 'aSCN'] ,@SCNExample,opts5,opts5); 
        end

        data1.tout
        data2.tout
        data3.tout
        data4.tout
        data5.tout
        
        data1.g = FWdata.g;
        data2.g = FWdata.g;
        data3.g = FWdata.g;
        data4.g = FWdata.g;
        data5.g = FWdata.g;
        
        JFW1 =   CostFunctional(FWdata,opts1.beta); 
        JFW2 =   CostFunctional(FWdata,opts2.beta);   
        JFW3 =   CostFunctional(FWdata,opts3.beta);   
        JFW4 =   CostFunctional(FWdata,opts4.beta);   
        JFW5 =   CostFunctional(FWdata,opts5.beta);

        JOpt1 =   CostFunctional(data1,opts1.beta); 
        JOpt2 =   CostFunctional(data2,opts2.beta);   
        JOpt3 =   CostFunctional(data3,opts3.beta);   
        JOpt4 =   CostFunctional(data4,opts4.beta);   
        JOpt5 =   CostFunctional(data5,opts5.beta);   
       
        
        JFW1 = sprintf('%0.2e',JFW1);
        JFW1 = strrep(JFW1, 'e-0','e-');
        JFW2 = sprintf('%0.2e',JFW2);
        JFW2 = strrep(JFW2, 'e-0','e-');
        JFW3 = sprintf('%0.2e',JFW3);
        JFW3 = strrep(JFW3, 'e-0','e-');
        JFW4 = sprintf('%0.2e',JFW4);
        JFW4 = strrep(JFW4, 'e-0','e-');
        JFW5 = sprintf('%0.2e',JFW5);
        JFW5 = strrep(JFW5, 'e-0','e-');
       
        JOpt1 = sprintf('%0.2e',JOpt1);
        JOpt1 = strrep(JOpt1, 'e-0','e-');
        JOpt2 = sprintf('%0.2e',JOpt2);
        JOpt2 = strrep(JOpt2, 'e-0','e-');
        JOpt3 = sprintf('%0.2e',JOpt3);
        JOpt3 = strrep(JOpt3, 'e-0','e-');
        JOpt4 = sprintf('%0.2e',JOpt4);
        JOpt4 = strrep(JOpt4, 'e-0','e-');
        JOpt5 = sprintf('%0.2e',JOpt5);
        JOpt5 = strrep(JOpt5, 'e-0','e-');
        
        kappa = sprintf('%g',kappa);
        
        fprintf(fout,'%s%s%s  & %s & %s%s%s & %s%s%s & %s%s%s & %s%s%s & %s%s%s\\\\\n', ...
                '\multirow{2}{*}{$\kappa= \numprint{',kappa,'}$}','$\mathcal{J}_{uc}$','$\numprint{',JFW1,'}$','$\numprint{',...
                JFW2,'}$','$\numprint{',JFW3,'}$','$\numprint{',JFW4,'}$','$\numprint{',JFW5,'}$'); 
        fprintf(fout,' & %s & %s%s%s & %s%s%s & %s%s%s & %s%s%s & %s%s%s\\\\\n', ...
                '$\mathcal{J}_c$','$\numprint{',JOpt1,'}$','$\numprint{',...
                JOpt2,'}$','$\numprint{',JOpt3,'}$','$\numprint{',JOpt4,'}$','$\numprint{',JOpt5,'}$');  
        fprintf(fout, '%s\n', '\hline');

    end

    fprintf(fout, '%s\n', '\end{tabular}');
    fprintf(fout, '%s%s%s\n', '\caption{', tableCaption, '}');
    fprintf(fout, '%s%s%s\n', '\label{', tableLabel, '}');
    fprintf(fout, '%s', '\end{table}'); % end the table
    fclose(fout)

    end
end
