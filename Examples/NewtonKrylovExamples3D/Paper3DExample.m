function output = Paper3DExample(opts, opts2)
%%%% needs imput specifying opts.kappa, opts.beta and opts2 = []

kappa = opts.kappa;
%% Set up domain
geom.y1Min = -1; geom.y1Max = 1; 
geom.y2Min = -1; geom.y2Max = 1; 
geom.y3Min = -1; geom.y3Max = 1; 

nx = 20;    
n = 10;
geom.N = [nx;nx;nx]; 

aBox = Box3(geom); % make a Box object
aBox.ComputeAll;  % compute differentiation, integration, etc

aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);

x1 = aBox.Pts.y1_kv; x2 = aBox.Pts.y2_kv; x3 = aBox.Pts.y3_kv; 

bound = aBox.Ind.bound;

L = aBox.Diff.Lap;
Lap = L;
grad = aBox.Diff.grad;
Dx1 = aBox.Diff.Dy1; 
Dx2 = aBox.Diff.Dy2;
Dx3 = aBox.Diff.Dy3;

IB = eye(nx^3); IB = sparse(IB(bound,:));
ZB = sparse(zeros(size(IB)));

% need to specify V2 and parameters
optsV2 = struct;
Conv = aBox.ComputeConvolutionMatrix(@GaussianNK,optsV2);

% computes dot product with the normal
N = aBox.Ind.normal;
Ngrad = sparse(N*grad);
T = 1;         % time interval of integration [0,T]
bet = opts.bet; % penalisation parameter


%% Initial/final values 
u0 = (zeros(size(x1)) + 0.125); 
vT = zeros(nx^3,1);       

%% Define nonlinear ODE system 
%
%   u' = K_1(t,u,v)*u - K_2(t,u,v)*v + f(t,u,v),
%   v' = K_3(t,u,v)*u - K_4(t,u,v)*v + g(t,u,v),
%
% where K_j = K_j(t,u,v).

I = speye(nx^3);
M = eye(nx^3);
M(bound,:) = 0;
M1 = sparse(M);
M2 = sparse(M);

%%

opts.tolValIn = 201;
opts.JFu = @JFu;
opts.JFv = @JFv;
opts.JGu = @JGu;
opts.JGv = @JGv;
opts.tol = 1e-16;
opts.verbose = 0;
opts.waitbar = 0;
method = 3;         % iterative solve with FD approx of Jacobian+dyn forcing
iters = 10;         % max number of Newton iterations

%% INITAL CALL TO THE SOLVER
tic
[t,u,v,out] = solver(@F, @G, M1, M2, u0, vT, T, n, method, iters, opts);
tout = toc;
 
%output.w = -(1/bet)*(grad*v).*u;
output.out = out;
output.rho = u;
output.v = v;
output.tout = tout;



    %% external potential

    function sol = Vext(t)
        sol = ((x1 + 0.3).^2 - 1).*((x1-0.4).^2 - 0.5).*...
                            ((x2 + 0.3).^2 - 1).*((x2-0.4).^2 - 0.5).*...
                            ((x3 + 0.3).^2 - 1).*((x3-0.4).^2 - 0.5);
    end


    %% Construct F and G
    function F_out = F(t,u,v) 
        F_out = K1(t,u,v)*u - K2(t,u,v)*v;
    end
    function G_out = G(t,u,v) 
        G_out = K3(t,u,v)*u - K4(t,u,v)*v + g(t,u,v);
    end


      function K1_out = K1(t,u,v)
        K1_out = L  ...
             + 2/bet * scalarOperator(dotVectors3(grad*u,grad*v)) ...
             + dotVectorOperator3(grad*Vext,grad) ...
             + scalarOperator(Lap*Vext)...
             + kappa * ( dotVectorOperator3(grad*(Conv*u),grad) ...
                              + scalarOperator(L*(Conv*u)) );  
        
        temp2 = zeros(size(K1_out(bound,:)));
        temp2(:,bound) = scalarOperator(Ngrad*Conv*u);
        temp3 = zeros(size(K1_out(bound,:)));
        temp3(:,bound) = scalarOperator(Ngrad*Vext);
        
        K1_out(bound,:) = Ngrad + sparse(temp3) + kappa*sparse(temp2);
    end

    function K2_out = K2(t,u,v)
        K2_out = -1/bet*scalarOperator(u.^2)*L;
        K2_out(bound,:) = -bet^(-1)*scalarOperator(u(bound).^2)*Ngrad; 
    end

    function K3_out = K3(t,u,v)
        K3_out = -I + 1/bet * scalarOperator(dotVectors3(grad*v,grad*v))...
            - kappa * ( Dx1*Conv*scalarOperator(Dx1*v) + Dx2*Conv*scalarOperator(Dx2*v) ...
            + Dx3*Conv*scalarOperator(Dx3*v));
        K3_out(bound,:) = ZB;
    end

    function K4_out = K4(t,u,v)
        K4_out = L - dotVectorOperator3(grad*Vext,grad) - kappa * dotVectorOperator3(grad*(Conv*u),grad);
        K4_out(bound,:) = Ngrad;
    end

   
    function g_out = g(t,u,v)
        g_out = 0.125*(1-t) ...
            + t*(pi/4)^3*cos(pi*x1/2).*cos(pi*x2/2).*cos(pi*x3/2);
        g_out(bound) = 0;
    end

    %% Jacobian
   
    function J = JFu(t,u,v)        
        J =  L ...
             + 2/bet * scalarOperator(u)*scalarOperator(L*v)  ...
             + 2/bet * scalarOperator(dotVectors3(grad*u,grad*v)) ...
             + 2/bet * scalarOperator(u) * dotVectorOperator3(grad*v,grad) ...
             + dotVectorOperator3(grad*Vext,grad) + scalarOperator(Lap*Vext)...
             + kappa* ( dotVectorOperator3(grad*(Conv*u),grad) ...
                              + scalarOperator(L*(Conv*u)) ...
                              + dotVectorOperator3((grad*u),grad)*Conv ...
                              + scalarOperator(u)*L*Conv );
          temp = zeros(size(J(bound,:)));
          temp(:,bound) = scalarOperator(u(bound).*(Ngrad*v));  
          temp2 = zeros(size(J(bound,:)));
          temp2(:,bound) = scalarOperator(Ngrad*Conv*u); 
          temp3 = zeros(size(J(bound,:)));
          temp3(:,bound) = scalarOperator(Ngrad*Vext);
          J(bound,:) = Ngrad + 2/bet*sparse(temp) + sparse(temp3)...
              + kappa*(sparse(temp2) + scalarOperator(u(bound))*(Ngrad*Conv));
    end

    function J = JFv(t,u,v)
        J = 1/bet * scalarOperator(u.^2) * L ...
            + 2/bet * scalarOperator(u) * dotVectorOperator3(grad*u,grad);
        J(bound,:) = 1/bet*scalarOperator(u(bound).^2)*Ngrad;
    end

    function J = JGu(t,u,v)
        J = -I + 1/bet * scalarOperator(dotVectors3(grad*v,grad*v))...
            + kappa * ( dotVectorOperator3(grad*v,grad*Conv) ...
                              - Dx1*Conv*scalarOperator(Dx1*v) - Dx2*Conv*scalarOperator(Dx2*v) ...
                              - Dx3*Conv*scalarOperator(Dx3*v) );
        J(bound,:) = ZB;
    end

    function J = JGv(t,u,v)
        J = -L ...
            + 2/bet * scalarOperator(u) * dotVectorOperator3(grad*v,grad) ...
            + dotVectorOperator3(grad*Vext,grad) ...
            + kappa * ( dotVectorOperator3(grad*Conv*u,grad) ...
                              - Dx1 * Conv * scalarOperator(u) * Dx1 ...
                              - Dx2 * Conv * scalarOperator(u) * Dx2  ...
                              - Dx3 * Conv * scalarOperator(u) * Dx3 );
        J(bound,:) = Ngrad;
    end

end