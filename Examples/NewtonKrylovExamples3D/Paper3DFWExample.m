function out = Paper3DFWExample(opts, opts2)
%%%% needs imput specifying opts.kappa, opts.beta and opts2 = []

kappa = opts.kappa;
%% Set up domain
geom.y1Min = -1; geom.y1Max = 1; 
geom.y2Min = -1; geom.y2Max = 1; 
geom.y3Min = -1; geom.y3Max = 1; 

nx = 20;    
n = 11;
geom.N = [nx;nx;nx]; 

aBox = Box3(geom); % make a Box object
aBox.ComputeAll;  % compute differentiation, integration, etc

aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);

x1 = aBox.Pts.y1_kv; x2 = aBox.Pts.y2_kv; x3 = aBox.Pts.y3_kv; 

bound = aBox.Ind.bound;

L = aBox.Diff.Lap;
Lap = L;
grad = aBox.Diff.grad;
div = aBox.Diff.div;
Dx1 = aBox.Diff.Dy1; 
Dx2 = aBox.Diff.Dy2;
Dx3 = aBox.Diff.Dy3;

IB = eye(nx^3); IB = sparse(IB(bound,:));
ZB = sparse(zeros(size(IB)));

% need to specify V2 and parameters
optsV2 = struct;
Conv = aBox.ComputeConvolutionMatrix(@GaussianNK,optsV2);

% computes dot product with the normal
normal = aBox.Ind.normal;
T = 1;         % time interval of integration [0,T]
bet = opts.bet; % penalisation parameter

tgeom.yMin = 0;
tgeom.yMax = 1;
tgeom.N = n;
aLine = SpectralLine(tgeom);
ti = aLine.Pts.y;

%% Initial/final values 
rho_0 = (zeros(size(x1)) + 0.125);     

for i = 1:n
    gOut(:,i) = g(ti(i));
end

    %---------------------------------------------------------------------%
    % Solve the ODE                                                       %
    %---------------------------------------------------------------------%
    
    mM  = ones(size(x1));
    mM(bound) = 0;
    opts = odeset('RelTol',10^-9,'AbsTol',10^-9,'Mass',diag(mM));
    [outTimes,Rho_t] = ode15s(@rhs,ti,rho_0,opts);


    out.rho = Rho_t;
    out.g = gOut;

    function dydt = rhs(t,rho)
        rho3 = [rho;rho;rho];
        ConvTerm = div*(rho3.*(grad*(Conv*rho)));
        
        dydt = Lap*rho + div*(rho3.*(grad*Vext)) + kappa*ConvTerm;
   
        
        % Neumann
        dydt(bound)  = normal*(grad*rho);
          
    end 


    function sol = Vext()
        sol = ((x1 + 0.3).^2 - 1).*((x1-0.4).^2 - 0.5).*...
                            ((x2 + 0.3).^2 - 1).*((x2-0.4).^2 - 0.5).*...
                            ((x3 + 0.3).^2 - 1).*((x3-0.4).^2 - 0.5);
    end


    function g_out = g(t)
        g_out = 0.125*(1-t) ...
            + t*(pi/4)^3*cos(pi*x1/2).*cos(pi*x2/2).*cos(pi*x3/2);
        g_out(bound) = 0;
    end


end