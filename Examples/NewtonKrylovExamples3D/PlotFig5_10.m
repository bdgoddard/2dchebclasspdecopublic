function PlotFig5_10()

opts1.bet = 10^-3;
opts1.kappa = 1;
opts0.bet = 10^-3;
opts0.kappa = 0;
optsn1.bet = 10^-3;
optsn1.kappa = -1;

dataOpt1 = DataStorage(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,opts1,[]); 
dataOpt0 = DataStorage(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,opts0,[]); 
dataOptn1 = DataStorage(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,optsn1,[]); 

Plotting3Drho(dataOpt1,'rhok1.png')
Plotting3Drho(dataOpt0,'rhok0.png')
Plotting3Drho(dataOptn1,'rhokn1.png')

end