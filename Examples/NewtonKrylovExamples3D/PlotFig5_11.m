function PlotFig5_11()

opts1.bet = 10^-3;
opts1.kappa = 1;
opts0.bet = 10^-3;
opts0.kappa = 0;
optsn1.bet = 10^-3;
optsn1.kappa = -1;

dataOpt1 = DataStorage2(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,opts1,[]); 
dataOpt0 = DataStorage2(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,opts0,[]); 
dataOptn1 = DataStorage2(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,optsn1,[]); 

geom.y1Min = -1; geom.y1Max = 1; 
geom.y2Min = -1; geom.y2Max = 1; 
geom.y3Min = -1; geom.y3Max = 1; 

nx = 20;    
geom.N = [nx;nx;nx]; 

aBox = Box3(geom); % make a Box object
aBox.ComputeAll;  % compute differentiation, integration, etc

aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);

grad = aBox.Diff.grad;

w1 = -(1/opts1.bet)*(grad*dataOpt1.v).*[dataOpt1.rho;dataOpt1.rho;dataOpt1.rho];
w0 = -(1/opts0.bet)*(grad*dataOpt0.v).*[dataOpt0.rho;dataOpt0.rho;dataOpt0.rho];
wn1 = -(1/optsn1.bet)*(grad*dataOptn1.v).*[dataOptn1.rho;dataOptn1.rho;dataOptn1.rho];

flnorm = max(max(abs(w1)));
flmax = max(max(w0));
flmin = min(min(wn1));

Plotting3DControl(w1, flnorm, 0.3,true, flmax, flmin,'Controlk1.png')
Plotting3DControl(w0, flnorm, 0.3,true, flmax, flmin,'Controlk0.png')
Plotting3DControl(wn1, flnorm, 0.3,true, flmax, flmin,'Controlkn1.png')



end