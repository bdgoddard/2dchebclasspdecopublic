function data = Run3DExamples()
opts = struct;
opts2 = struct;

kappaVec = [1,-1,0];
betVec = [10^-3, 10^-1, 10];

for j = 1:3
    for i = 1:3
        bet = betVec(i);
        kappa = kappaVec(j);
        opts.bet = bet;
        opts.kappa = kappa;
        % Example 5.2. optimal control problem
        dataOpt = DataStorage(['NewtonKrylovData' filesep 'Ex13D'] ,@Paper3DExample,opts,opts2); 
    end
    % Example 5.2 - uncontrolled case to evaluate baseline cost
    % (independent of beta)
    dataFW = DataStorage(['NewtonKrylovData' filesep 'Ex13DFW'] ,@Paper3DFWExample,opts,opts2); 
end
data.FW = dataFW;
data.Opt = dataOpt;


end






