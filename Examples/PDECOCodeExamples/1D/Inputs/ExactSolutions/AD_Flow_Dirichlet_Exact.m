function ADFlowExactD = AD_Flow_Dirichlet_Exact(this,y,t,T,beta)

%1/0.8588
%1/1.6872
c1 = 1;
c2 = 2;

        Crho = beta^(1/2);
        Cp   = beta^(1/2);
        
        Trho = c1*exp(t);%/2.5;
        dTrho = c1*exp(t);%/2.5;
        Tp   = c2*(exp(T) - exp(t));%/1.16068; 
        dTp  = - c2*exp(t);%/1.16068; 
        
        Yrho = cos(pi*y/2);
        dYrho = -pi/2*sin(pi*y/2);
        ddYrho = -(pi/2)^2*cos(pi*y/2);
        Yp   = Yrho;
        dYp  = dYrho;
        ddYp = ddYrho;

        rho = Crho*Trho*Yrho;
        
        rho_t = Crho*dTrho*Yrho;
        rho_y = Crho*Trho*dYrho;
        rho_yy = Crho*Trho*ddYrho;
        
        p   = Cp*Tp*Yp;
        p_t  = Cp*dTp*Yp;
        p_y  = Cp*Tp*dYp;
        p_yy = Cp*Tp*ddYp;
        
        w = -1/beta * rho .* p_y;
        w_y = -1/beta * ( rho_y .* p_y + rho .* p_yy );
        
        f = rho_t - rho_yy + w_y.*rho + w.*rho_y;
        rhoHat = p_t + p_yy + rho + w.*p_y;
               
        ADFlowExactD.rhoIC = rho;
        ADFlowExactD.OptirhoIG = rho;
        
        ADFlowExactD.pIC = p;
        ADFlowExactD.OptipIG = p;
        
        ADFlowExactD.rhoTarget = rhoHat;
           
        ADFlowExactD.wForce = zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = f;
        
        ADFlowExactD.wFlow= w;
        
        ADFlowExactD.Flow = zeros(size(y));
end