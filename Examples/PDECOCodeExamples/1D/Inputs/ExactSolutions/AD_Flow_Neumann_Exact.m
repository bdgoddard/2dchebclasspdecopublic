function ADFlowExactD = AD_Flow_Neumann_Exact(this,y,t,T,beta)

%         c1 = other.scalerho;
%         c2 = other.scalep;
        c1 = 1;
        c2 = 1;

        Crho = beta^(1/2);
        Cp   = beta^(1/2);
        
        Trho = c1*exp(t);
        dTrho = c1*exp(t);
        Tp   = c2*(exp(T) - exp(t));
        dTp  = - c2*exp(t);
        
        Yrho = cos(pi*y)+2;
        dYrho = -pi*sin(pi*y);
        ddYrho = -(pi)^2*cos(pi*y);
        Yp   = cos(pi*y);
        dYp  = dYrho;
        ddYp = ddYrho;

        rho = Crho*Trho*Yrho;
        
        rho_t = Crho*dTrho*Yrho;
        rho_y = Crho*Trho*dYrho;
        rho_yy = Crho*Trho*ddYrho;
        
        p   = Cp*Tp*Yp;
        p_t  = Cp*dTp*Yp;
        p_y  = Cp*Tp*dYp;
        p_yy = Cp*Tp*ddYp;


        w = -(1/beta) * rho .* p_y ;
        w_y = -(1/beta) * ( rho_y .* p_y + rho .* p_yy );
        
        f = rho_t - rho_yy + w_y.*rho + w.*rho_y;
        rhoHat = p_t + p_yy + rho + w.*p_y;
               
        ADFlowExactD.rhoIC = rho;
        ADFlowExactD.OptirhoIG = rho;
        
        ADFlowExactD.pIC = p;
        ADFlowExactD.OptipIG = p;
        
        ADFlowExactD.rhoTarget = rhoHat;
           
        ADFlowExactD.wForce = zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = f;
        
        ADFlowExactD.wFlow = w;
        
        ADFlowExactD.Flow = zeros(size(y));
end