function ADForceExactD = AD_Force_Dirichlet_Exact(this,y,t,T,beta)
    
    
        ADForceExactD.rhoIC = 2*exp(t).*(cos(pi*y/2));
        ADForceExactD.OptirhoIG = 2*exp(t).*cos(pi*y/2);
        
        ADForceExactD.pIC = (exp(T)- exp(t)).*cos(pi*y/2);
        ADForceExactD.OptipIG = (exp(T)- exp(t)).*cos(pi*y/2);
        
        ADForceExactD.rhoTarget = 2*exp(t).*cos(pi*y/2) - exp(t).*cos(pi*y/2) - (pi^2/4)*(exp(T)-exp(t)).*...
            cos(pi*y/2) - (pi^2*(exp(T)-exp(t))/ 4).* sin(pi*y/2).^2 ;
           
        ADForceExactD.wForce= -((exp(T)-exp(t))/beta ).*cos(pi*(y)/2);
         %ADForceExactD.wForce= zeros(size(y));
        
        ADForceExactD.Vext = cos(pi*y/2);
        
        ADForceExactD.Force = 2*exp(t).*cos(pi*y/2)+ (pi^2/2)*exp(t).*cos(pi*y/2) + (exp(T)-exp(t))/beta.*...
            cos(pi*y/2)- (pi^2*exp(t)/2)*(sin(pi*y/2).^2 - cos(pi*y/2).^2);
        
        ADForceExactD.wFlow = zeros(size(y));
        
        ADForceExactD.Flow = zeros(size(y));
        ADForceExactD.rhoAIG = -16*ones(size(y));
        ADForceExactD.wForceIG = -((T-t)/T)*16*ones(size(y));
        mask = (-beta*16*ones(size(y)) -((T-t)/T)*16*ones(size(y)))<0;
        muIG = -beta*16*ones(size(y)) -((T-t)/T)*16*ones(size(y));
        muIG(mask)=0;
        ADForceExactD.muIG = muIG;
end