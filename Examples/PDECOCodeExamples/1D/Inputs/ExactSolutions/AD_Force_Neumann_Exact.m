function ADForceExactN = AD_Force_Neumann_Exact(this,y,t,T,beta)
    %cw=1, cForce=1, cExt=1, gamma=1, cFlow=0
    
        ADForceExactN.rhoIC = 2*exp(t).*(cos(pi*y));
        ADForceExactN.OptirhoIG = 2*exp(t).*cos(pi*y);
        
        ADForceExactN.pIC = (exp(T)- exp(t)).*cos(pi*y);
        ADForceExactN.OptipIG = (exp(T)- exp(t)).*cos(pi*y);
        
        ADForceExactN.rhoTarget =  2*exp(t).*cos(pi*y) - exp(t).*cos(pi*y) - pi^2*(exp(T)-exp(t)).*...
            cos(pi*y) - pi^2*(exp(T)-exp(t)).* sin(pi*y).^2 ;
           
        ADForceExactN.wForce = -((exp(T)-exp(t))/beta).*cos(pi*y);
        
        ADForceExactN.Vext =  cos(pi*y);
        
        ADForceExactN.Force = (2*exp(t)+ 2*pi^2*exp(t)).*cos(pi*y) + ((exp(T)-exp(t))/beta).*...
            cos(pi*y)- 2*pi^2*exp(t)*(sin(pi*y).^2 - cos(pi*y).^2);
        
        ADForceExactN.wFlow = zeros(size(y));
        
        ADForceExactN.Flow = zeros(size(y));
end

