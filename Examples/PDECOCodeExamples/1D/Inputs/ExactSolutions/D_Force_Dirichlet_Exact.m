function DExactD = D_Force_Dirichlet_Exact(this,x,t,T,beta)
   %cw=1, cForce=0, cExt=0, gamma=0, cFlow=0
    
    DExactD.rhoIC = ( 4/(pi^2*beta)*exp(T) - 4/((4+pi^2)*beta)*exp(t) )*(cos(pi*x/2));
    DExactD.OptirhoIG = ( 4/(pi^2*beta)*exp(T) - 4/((4+pi^2)*beta)*exp(t) )*cos(pi*x/2);
    
    DExactD.pIC = -(exp(T) - exp(t))*cos(pi*x/2);
    DExactD.OptipIG = -(exp(T) - exp(t))*cos(pi*x/2);

    DExactD.rhoTarget = ( ( pi^2/4 + 4/(pi^2*beta) )*exp(T) + ( 1 - pi^2/4 - 4/((4+pi^2)*beta))*exp(t) ) *cos(pi*x/2); %Needed?
    
    DExactD.wForce = ((exp(T)-exp(t))/beta ).*cos(pi*x/2);

    DExactD.Vext = zeros(size(x));

    DExactD.Force = zeros(size(x));
    
    DExactD.wFlow = zeros(size(x));
    
    DExactD.Flow = zeros(size(x));
    
    
end  
