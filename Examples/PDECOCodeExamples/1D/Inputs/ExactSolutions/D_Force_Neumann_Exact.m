    function DExactN = D_Force_Neumann_Exact(this,x,t,T,beta)
    
    DExactN.rhoIC = ( 1/(pi^2*beta)*exp(T) - 1/((1+pi^2)*beta)*exp(t) )*(cos(pi*x));
    DExactN.OptirhoIG = ( 1/(pi^2*beta)*exp(T) - 1/((1+pi^2)*beta)*exp(t) )*cos(pi*x);
    
    DExactN.pIC = -(exp(T) - exp(t))*cos(pi*x);
    DExactN.OptipIG = -(exp(T) - exp(t))*cos(pi*x);

    DExactN.rhoTarget = ( ( pi^2 + 1/(pi^2*beta) )*exp(T) ...
               + ( 1 - pi^2 - 1/((1+pi^2)*beta))*exp(t) ) *cos(pi*x); %Needed?

    DExactN.wForce = ((exp(T)-exp(t))/beta).*cos(pi*x);  

    DExactN.Vext= zeros(size(x));

    DExactN.Force = zeros(size(x));
    
    DExactN.wFlow = zeros(size(x));
    
    DExactN.Flow = zeros(size(x));
   
    end
    
