function ADFlowExactD = AD_Flow_DirichletInt1(this,y,t,T,beta)


        Crho = beta^(1/2);
        Cp   = beta^(1/2);
        
        Trho = exp(t);%/2.5;
        dTrho = exp(t);%/2.5;
        Tp   = (exp(T) - exp(t));%/1.16068; 
        dTp  = - exp(t);%/1.16068; 
        
        Yrho = cos(pi*y/2);
        dYrho = -pi/2*sin(pi*y/2);
        ddYrho = -(pi/2)^2*cos(pi*y/2);
        Yp   = Yrho;
        dYp  = dYrho;
        ddYp = ddYrho;

        rho = Crho*Trho*Yrho;
        
        rho_t = Crho*dTrho*Yrho;
        rho_y = Crho*Trho*dYrho;
        rho_yy = Crho*Trho*ddYrho;
        
        p   = Cp*Tp*Yp;
        p_t  = Cp*dTp*Yp;
        p_y  = Cp*Tp*dYp;
        p_yy = Cp*Tp*ddYp;
        

        ADFlowExactD.rhoIC =  (1/2)*(cos(pi*y) ) + 1/2;
        ADFlowExactD.OptirhoIG = (1/2)*(cos(pi*y) ) + 1/2;
        
        ADFlowExactD.pIC = p;
        ADFlowExactD.OptipIG = p;
        
        ADFlowExactD.rhoTarget = (1-t)*((1/2)*(cos(pi*y) ) + 1/2) + t*(-(1/2)*(cos(2*pi*y) ) + 1/2);
           
        ADFlowExactD.wForce = zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = zeros(size(y));
        
        ADFlowExactD.wFlow= zeros(size(y));
        
        ADFlowExactD.Flow = zeros(size(y));
end