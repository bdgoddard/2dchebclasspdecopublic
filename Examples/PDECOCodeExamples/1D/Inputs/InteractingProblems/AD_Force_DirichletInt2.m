function ADForceExactD = AD_Force_DirichletInt2(this,y,t,T,beta)
    
    
        ADForceExactD.rhoIC = (1/2)*cos(pi*y) + 1/2;
        ADForceExactD.OptirhoIG = (1/2)*cos(pi*y) + 1/2;
        
        ADForceExactD.pIC = (exp(T)- exp(t)).*cos(pi*y/2);
        ADForceExactD.OptipIG = (exp(T)- exp(t)).*cos(pi*y/2);
        
        ADForceExactD.rhoTarget = (1-t)*((1/2)*cos(pi*y) + 1/2) + t*(-(1/2)*cos(2*pi*y) + (1/2));
           
        ADForceExactD.wForce= zeros(size(y));
        
        ADForceExactD.Vext =  0.5*((((y + 0.3).^2 - 0.2).*((y-0.4).^2 - 0.3)));
        
        ADForceExactD.Force =  zeros(size(y));
        
        ADForceExactD.wFlow = zeros(size(y));
        
        ADForceExactD.Flow = zeros(size(y));
end