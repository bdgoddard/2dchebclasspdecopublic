function ADForceExactN = AD_Force_NeumannInt2(this,y,t,T,beta)
    %cw=1, cForce=1, cExt=1, gamma=1, cFlow=0
    
        ADForceExactN.rhoIC = zeros(size(y)) + 0.5;
        ADForceExactN.OptirhoIG = zeros(size(y)) + 0.5;
        
        ADForceExactN.pIC = (exp(T)- exp(t)).*cos(pi*y);
        ADForceExactN.OptipIG = (exp(T)- exp(t)).*cos(pi*y);
        
        ADForceExactN.rhoTarget =  (1-t)*0.5 + t*(1/2)*(-cos(pi*y)+1);
           
        ADForceExactN.wForce = zeros(size(y));
        
        ADForceExactN.Vext =  zeros(size(y));
        
        ADForceExactN.Force = zeros(size(y));
        
        ADForceExactN.wFlow = zeros(size(y));
        
        ADForceExactN.Flow = zeros(size(y));
end

