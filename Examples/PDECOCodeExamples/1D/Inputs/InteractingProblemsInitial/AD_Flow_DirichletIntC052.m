function ADFlowExactD = AD_Flow_DirichletIntC052(this,y,t,T,beta)

        ADFlowExactD.rhoIC =   zeros(size(y)) + 0.5;
        ADFlowExactD.OptirhoIG =  zeros(size(y)) + 0.5;
        
        ADFlowExactD.pIC = beta*(exp(T)-exp(t))*(cos(pi*y/2));      
        ADFlowExactD.OptipIG = beta*(exp(T)-exp(t))*(cos(pi*y/2));
        
        ADFlowExactD.rhoTarget = (1-t)*0.5 + t*((1/2)*(sin(pi*y ) +1)); 
        
        ADFlowExactD.wForce =  zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = zeros(size(y));
        
        ADFlowExactD.wFlow= zeros(size(y));
        
        ADFlowExactD.Flow = zeros(size(y));
end