function ADFlowExactD = AD_Flow_NeumannInt3(this,y,t,T,beta)

        Cp   = beta;
        Tp   = (exp(T) - exp(t));
        Yp   = cos(pi*y);

        p   = Cp*Tp*Yp;

        rhoHat = (1-t)*0.5 + t*(1/4)*(cos(pi*y)+2);
               
        ADFlowExactD.rhoIC = zeros(size(y)) + 0.5; %rho;
        ADFlowExactD.OptirhoIG = zeros(size(y)) + 0.5; %rho;
        
        ADFlowExactD.pIC = p;
        ADFlowExactD.OptipIG = p;
        
        ADFlowExactD.rhoTarget = rhoHat;
           
        ADFlowExactD.wForce = zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = zeros(size(y));
        
        ADFlowExactD.wFlow = zeros(size(y));
        
        ADFlowExactD.Flow = zeros(size(y));
end