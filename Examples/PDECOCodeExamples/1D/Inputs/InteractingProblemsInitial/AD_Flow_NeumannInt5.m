function ADFlowExactD = AD_Flow_NeumannInt5(this,y,t,T,beta)

        Cp   = beta;
        
        Tp   = (exp(T) - exp(t));
        Yp   = cos(pi*y);

        p   = Cp*Tp*Yp;

        ADFlowExactD.rhoIC = zeros(size(y)) + 0.5; %rho;
        ADFlowExactD.OptirhoIG = zeros(size(y)) + 0.5; %rho;
        
        ADFlowExactD.pIC = p;
        ADFlowExactD.OptipIG = p;
        
        ADFlowExactD.rhoTarget = 0.5*(1-t) + t*((1/2)*sin(pi*(y - 2)/2) + 1/2);
           
        ADFlowExactD.wForce = zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = zeros(size(y));
        
        ADFlowExactD.wFlow = zeros(size(y));
        
        ADFlowExactD.Flow = zeros(size(y));
end