function ADFlowExactD = AD_Flow_NeumannInt8(this,y,t,T,beta)

        Cp   = beta;

        Tp   = (exp(T) - exp(t));
        Yp   = cos(pi*y);
        p   = Cp*Tp*Yp;
               
        ADFlowExactD.rhoIC = (1/2)*cos(pi*y) + (1/2); %rho;
        ADFlowExactD.OptirhoIG = (1/2)*cos(pi*y) + (1/2); %rho;
        
        ADFlowExactD.pIC = p;
        ADFlowExactD.OptipIG = p;
        
        ADFlowExactD.rhoTarget = ((1/2)*cos(pi*y) + (1/2))*(1-t) + t*(-(1/2)*cos(2*pi*y) + (1/2));
           
        ADFlowExactD.wForce = zeros(size(y));
        
        ADFlowExactD.Vext = zeros(size(y));
        
        ADFlowExactD.Force = zeros(size(y));
        
        ADFlowExactD.wFlow = zeros(size(y));
        
        ADFlowExactD.Flow = zeros(size(y));
end