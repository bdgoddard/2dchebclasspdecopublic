function output = SingleTestExact2PDECO1()
 % one exact solution test 
 %    rhoError: 1.2487e-07
 %    pError:   1.0564e-07
 %    wError:   1.0349e-05
  
    Tols2 = 10^-3; 
    Tols1 = 10^-5; 

    n = 12; 
    N = 100;
    
    Phys_Area = struct('shape', 'SpectralLine', 'N', N, 'yMin', -1, 'yMax', 1) ;
    Plot_Area = struct('N',100,'yMin',-1,'yMax',1); 
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGradGaussian');
    
    ProbSpecs = struct('BCFunStr', 'ComputeDirichletBCs', 'PDERHSStr', 'AD_Force', 'ComputeNormStr', 'ComputeRelL2LinfNorm');
    DataInput = struct('testFun', 'AD_Force_Dirichlet_Exact','DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', 10^-1,'gamma',0,'other',[],'D0', 1);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('SolverFlag', 'FixPt', 'AdaSolverStr', [], 'lambda', 0.01, 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG',,'rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    VarStr = struct('rhoOpt',1,'pOpt',2,'wFW',3,'wOpt',4);%,'Force',1,'wOpt',2);%,'rhoOpt',5,'rhoHat',1,'rhoFW',2,'wFW',7);
    optsPlot = struct('VarStr',VarStr,'plotTimes',linspace(0,1,10));
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_1D(opts);
    output1a.getExactSolution;
    output1a.getRhoTarget;
    output1a.getOptimizationIG;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;  
    output1a.plotResults(optsPlot);

    %output.out = output1a;
    
    output.rhoFW = output1a.ForwardResult.rho_t;
    output.rhoNum =output1a.OptimizationResult.rhoNum ;
    output.pNum =output1a.OptimizationResult.pNum ;
%     output.rhoError = output1a.Errors.OptimizationError.rhoExactErr;
%     output.pError = output1a.Errors.OptimizationError.pExactErr;
%     output.wError = output1a.Errors.OptimizationError.wExactErr;
end