function output = TestAllExactSolsPDECO1()
% Test for all exact solutions with different n,N, beta.  
% for below configurations, the solutions are:
% 1. rhoErr = 0.0000001248
% 2. rhoErr = 0.00000004283
% 3. rhoErr = 0.000000014448
% 4. rhoErr = 0.00000001575
% 5. rhoErr = 0.00000002667
% 6. rhoErr = 0.00000004663


iVec = 1:6;  % Choose which exact solutions to run in loop
jVec = 1; %1:4;  % Choose which beta to run in loop
kVec = 2; %1:4;  % Choose which number of points to run in loop 
betaVec = [10^-3, 10^-1, 10, 10^3]; 

nVec = [10, 20, 30, 40];
NVec = [20, 30, 30, 40];

Tols2 = 10^-4;
Tols1 = 10^-8;

SolverFlag = 'FixPt'; %Alternatively do 'Picard' of 'fsolve'

% Exact Solutions to run
testFunVec{1} = 'AD_Flow_Neumann_Exact';
testFunVec{2} = 'AD_Flow_Dirichlet_Exact';
testFunVec{3} = 'AD_Force_Neumann_Exact';
testFunVec{4} = 'AD_Force_Dirichlet_Exact';
testFunVec{5} = 'D_Force_Neumann_Exact';
testFunVec{6} = 'D_Force_Dirichlet_Exact';

BCStrVec{1} = 'ComputeNeumannBCs';
BCStrVec{2} = 'ComputeDirichletBCs';
BCStrVec{3} = 'ComputeNeumannBCs';
BCStrVec{4} = 'ComputeDirichletBCs';
BCStrVec{5} = 'ComputeNeumannBCs';
BCStrVec{6} = 'ComputeDirichletBCs';

PDERHSStrVec{1} = 'AD_Flowf';
PDERHSStrVec{2} = 'AD_Flowf';
PDERHSStrVec{3} = 'AD_Force';
PDERHSStrVec{4} = 'AD_Force';
PDERHSStrVec{5} = 'D_Force';
PDERHSStrVec{6} = 'D_Force';
for i = iVec
    BCFunStr = BCStrVec{i};
    testFun = testFunVec{i};
    PDERHSStr = PDERHSStrVec{i};
for j = jVec
    beta = betaVec(j);
for k = kVec
    n = nVec(k); 
    N = NVec(k);

    other.scalerho = 1;  
    other.scalep = 1;  
    other.D0 = 1;
    Phys_Area = struct('shape', 'SpectralLine', 'N', N, 'yMin', -1, 'yMax', 1) ;
    Plot_Area = struct('N',100,'yMin',-1,'yMax',1); 
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGradGaussian');
    
    ProbSpecs = struct('BCFunStr', BCFunStr, 'PDERHSStr', PDERHSStr, 'ComputeNormStr', 'ComputeRelL2LinfNorm');
    DataInput = struct('testFun', testFun,'DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', beta,'gamma',0,'other',other,'D0', 1);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('lambda', 0.1, 'SolverFlag', SolverFlag, 'AdaSolverStr', [], 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG',,'rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
     
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_1D(opts);
    output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;  

    rhoErrFW = output1a.Errors.ForwardError.rhoExactErr;
    rhoErrOpt = output1a.Errors.OptimizationError.rhoExactErr;
    Iters = output1a.OptimizationResult.Iterations;
    text1 = sprintf(testFun);
    text = sprintf('\n ODETols: %.10f, \n OptiTols: %.8f, \n beta: %0.3f \n Iterations: %d,\n Forward Error: %.12f,\n Optimization Error: %.20f',...
                   [Tols1; Tols2; beta; Iters; rhoErrFW; rhoErrOpt]);
    texti = [text1, text];
    iStr = num2str(i);
    jStr = num2str(j);
    kStr = num2str(k);
    ti = strcat("Test",iStr,jStr,kStr);
    ttext.(ti) = texti;
end
end
end
    output = ttext;
end