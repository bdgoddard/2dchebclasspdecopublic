function TestConvolutionMatrices()

   %%%%--------------------------------------------------------------------
   % This code tests the convolution matrices from the pseudospectral and
   % finite difference discretization.
   % It compares output of the numerical and analytical
   % convolution for (x-y)Sin(alpha y) on the interval /x-y/< R or [x-R,x+R]
   %
   % Inputs:
   % Parameters for shape(Fourier/FiniteDifferencePeriodic for periodic bcs and Spectral/FIniteDifference for no flux and dirichlet bcs)
   % Constants for R,alpha
   %
   % Output:
   % Plot of graphs and display of error between numerical and exact convolutions
   %%%---------------------------------------------------------------------
   
    AddPaths();
    
    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------

    PhysArea_Fourier = struct('shape','FourierLine','N',100,'yMin',0,'yMax',1);
    PhysArea_Spectral = struct('shape','SpectralLine','N',100,'yMin',0,'yMax',1);
    PhysArea_FD = struct('shape','FiniteDifferenceLine','N',100,'yMin',0,'yMax',1);
    PhysArea_FDP = struct('shape','FiniteDifferencePeriodicLine','N',100,'yMin',0,'yMax',1);
    
    PlotArea = struct('N',200,'yMin',0,'yMax',1);
      
    shapeClass_Fourier = str2func(PhysArea_Fourier.shape);
    shapeClass_Spectral = str2func(PhysArea_Spectral.shape);
    shapeClass_FD = str2func(PhysArea_FD.shape);
    shapeClass_FDP = str2func(PhysArea_FDP.shape);
    
    fLine = shapeClass_Fourier(PhysArea_Fourier);
    aLine = shapeClass_Spectral(PhysArea_Spectral);
    dLine = shapeClass_FD(PhysArea_FD);
    pLine = shapeClass_FDP(PhysArea_FDP);
    
    [Pts_Fourier,~,~,~,~] = fLine.ComputeAll(PlotArea); 
    [Pts_Spectral,~,~,~,~] = aLine.ComputeAll(PlotArea); 
    [Pts_FD,~,~,~,~] = dLine.ComputeAll(PlotArea); 
    [Pts_FDP,~,~,~,~] = pLine.ComputeAll(PlotArea);
    
    %----------------------------------------------------------------------
    % Compute Convolution Matrices
    %----------------------------------------------------------------------
    R = 0.2;
    shapeParams.yMax = R;
    shapeParams.N = 50;
    
    Conv_Fourier = fLine.ComputeConvolutionMatrix(@kernel,shapeParams);
    Conv_Spectral = aLine.ComputeConvolutionMatrix_Pointwise(@kernel,shapeParams);
    Conv_FD = dLine.ComputeConvolutionMatrix_Pointwise(@kernel,shapeParams);
    Conv_FDP = pLine.ComputeConvolutionMatrix(@kernel,shapeParams);
    
    y_Fourier = Pts_Fourier.y;
    y_Spectral = Pts_Spectral.y;
    y_FD = Pts_FD.y;
    y_FDP = Pts_FDP.y;
    
    %----------------------------------------------------------------------
    % Numerical convolution
    %----------------------------------------------------------------------
    
    alpha = 2*pi;
    
    ConvN_Fourier = Conv_Fourier*sin(alpha*y_Fourier);
    ConvN_Spectral = Conv_Spectral*sin(alpha*y_Spectral);
    ConvN_FD = Conv_FD*sin(alpha*y_FD);
    ConvN_FDP = Conv_FDP*sin(alpha*y_FDP);
    
%     f = cos(alpha*y_FDP);
%     f(f<0)=0;
%     
%     newcheck =Conv_FDP*f;
%     
%     figure()
%     pLine.plot(newcheck);
%     return
    

    %----------------------------------------------------------------------
    % Exact convolution
    %----------------------------------------------------------------------
    
    ymax_Fourier = y_Fourier+R;
    ymin_Fourier = y_Fourier-R;
    
    ymax_Spectral = min(y_Spectral +R,1);
    ymin_Spectral = max(y_Spectral -R,0);
    
    ymax_FD = min(y_FD +R,1);
    ymin_FD = max(y_FD -R,0);
    
    ymax_FDP = y_FDP+R;
    ymin_FDP = y_FDP-R;
    
    %yp = y+R;
    %ym = y-R;
    
    Conv_Fourier_Exact = -( sin(alpha*ymax_Fourier)/alpha^2 - R.*cos(alpha*ymax_Fourier)/alpha ) ...
            + ( sin(alpha*ymin_Fourier)/alpha^2 + R.*cos(alpha*ymin_Fourier)/alpha ) ;
        
    Conv_Spectral_Exact = -( sin(alpha*ymax_Spectral ))/alpha^2 +(-y_Spectral + ymax_Spectral).*cos(alpha*ymax_Spectral )/alpha  ...
            + ( sin(alpha*ymin_Spectral ))/alpha^2 -(-y_Spectral + ymin_Spectral).*cos(alpha*ymin_Spectral )/alpha  ;
        
    Conv_FD_Exact = -( sin(alpha*ymax_FD ))/alpha^2 +(-y_FD + ymax_FD).*cos(alpha*ymax_FD )/alpha  ...
            + ( sin(alpha*ymin_FD ))/alpha^2 -(-y_FD + ymin_FD).*cos(alpha*ymin_FD )/alpha  ;
        
    Conv_FDP_Exact = -( sin(alpha*ymax_FDP)/alpha^2 - R.*cos(alpha*ymax_FDP)/alpha ) ...
            + ( sin(alpha*ymin_FDP)/alpha^2 + R.*cos(alpha*ymin_FDP)/alpha ) ;
        
        
    %Conv_Exact = -( sin(alpha*yp)/alpha^2 - R.*cos(alpha*yp)/alpha ) ...
            %+ ( sin(alpha*ym)/alpha^2 + R.*cos(alpha*ym)/alpha ) ;

    %----------------------------------------------------------------------
    % Plotting and Error Comparison
    %----------------------------------------------------------------------
    figure()
    subplot(2,2,1);    
	opts.plain = true;
    opts.linewidth = 2;
    %opts.colour = 'k';
    fLine.plot(ConvN_Fourier,opts);
    hold on
    opts.linestyle = '--';
    %opts.color = 'b';
    fLine.plot(Conv_Fourier_Exact,opts);
    title('Fourier');
    ylim([-0.03,0.03])
    legend('numerical', 'exact');
    hold off
    
    subplot(2,2,2); 
    opts_S.plain = true;
    opts_S.linewidth = 2;
    %opts_S.color = 'r';
    aLine.plot(ConvN_Spectral,opts_S);
    hold on
    opts_S.linestyle = '--';
    %opts_S.color = 'b';
    aLine.plot(Conv_Spectral_Exact,opts_S);
    title('Spectral')
    ylim([-0.03,0.03])
    legend('numerical', 'exact');
    hold off
    
    subplot(2,2,3); 
    opts_P.plain = true;
    opts_P.linewidth = 2;
    %opts_S.color = 'r';
    pLine.plot(ConvN_FDP,opts_P);
    %plot(y_FDP,ConvN_FDP);
    hold on
    opts_P.linestyle = '--';
    %opts_S.color = 'b';
    pLine.plot(Conv_FDP_Exact,opts_P);
    %plot(y_FDP,Conv_FDP_Exact,'--');
    title('Finite Difference Periodic')
    ylim([-0.03,0.03])
    legend('numerical', 'exact');
    hold off
    
    subplot(2,2,4); 
    opts_D.plain = true;
    opts_D.linewidth = 2;
    %opts_S.color = 'r';
    dLine.plot(ConvN_FD,opts_D);
    hold on
    opts_D.linestyle = '--';
    %opts_S.color = 'b';
    dLine.plot(Conv_FD_Exact,opts_D);
    title('Finite Difference')
    ylim([-0.03,0.03])
    legend('numerical', 'exact');
    hold off
    
    
    
    Fourier_Convolution_Error = max(abs(ConvN_Fourier- Conv_Fourier_Exact))
    if (Fourier_Convolution_Error <1e-4)
        disp('Fourier test passed')
    else
        disp('Fourier test failed')
    end
    Spectral_Convolution_Error = max(abs(ConvN_Spectral- Conv_Spectral_Exact))
    if (Spectral_Convolution_Error <1e-4)
        disp('Spectral test passed')
    else
        disp('Spectral test failed')
    end
    
    FD_Convolution_Error = max(abs(ConvN_FD- Conv_FD_Exact))
    if (FD_Convolution_Error <1e-4)
        disp('Finite Difference test passed')
    else
        disp('Finite Difference test failed')
    end
    
    FDP_Convolution_Error = max(abs(ConvN_FDP- Conv_FDP_Exact))
    if (FDP_Convolution_Error <1e-4)
        disp('Finite Difference Periodic test passed')
    else
        disp('Finite Difference Periodic test failed')
    end
    %----------------------------------------------------------------------
    % Kernel function
    %----------------------------------------------------------------------

    function K = kernel(y)
        K = y;
    end
end