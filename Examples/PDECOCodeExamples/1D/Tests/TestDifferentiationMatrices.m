 function TestDifferentiationMatrices()

   %%%%--------------------------------------------------------------------
   % This code tests the differentiation and intergration matrices from the pseudospectral and
   % finite difference discretization.
   % It compares output of the numerical and analytical
   % differentiation and integration forSin(alpha y) on the interval [0,1]
   %
   % Inputs:
   % Parameters for shape(Fourier/FiniteDifferencePeriodic for periodic bcs and Spectral/FIniteDifference for no flux and dirichlet bcs)
   % Constants for R,alpha
   %
   % Output:
   % Plot of graphs and display of error between numerical and exact
   % solutions
   %%%---------------------------------------------------------------------
   
    AddPaths();
    clear all;
    
    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------

    PhysArea_Fourier = struct('shape','FourierLine','N',100,'yMin',0,'yMax',1);
    PhysArea_Spectral = struct('shape','SpectralLine','N',100,'yMin',0,'yMax',1);
    PhysArea_FD = struct('shape','FiniteDifferenceLine','N',100,'yMin',0,'yMax',1);
    PhysArea_FDP = struct('shape','FiniteDifferencePeriodicLine','N',100,'yMin',0,'yMax',1);
    
    PlotArea = struct('N',200,'yMin',0,'yMax',1);
      
    shapeClass_Fourier = str2func(PhysArea_Fourier.shape);
    shapeClass_Spectral = str2func(PhysArea_Spectral.shape);
    shapeClass_FD = str2func(PhysArea_FD.shape);
    shapeClass_FDP = str2func(PhysArea_FDP.shape);
    
    fLine = shapeClass_Fourier(PhysArea_Fourier);
    aLine = shapeClass_Spectral(PhysArea_Spectral);
    dLine = shapeClass_FD(PhysArea_FD);
    pLine = shapeClass_FDP(PhysArea_FDP);
     
    [Pts_Fourier,~,~,~,~] = fLine.ComputeAll(PlotArea); 
    [Pts_Spectral,~,~,~,~] = aLine.ComputeAll(PlotArea); 
    [Pts_FD,~,~,~,~] = dLine.ComputeAll(PlotArea); 
    [Pts_FDP,~,~,~,~] = pLine.ComputeAll(PlotArea);
    
    %----------------------------------------------------------------------
    % Compute Differentiation and Integration Matrices
    %----------------------------------------------------------------------

    %----First Derivative----------------------------  
    Diff_Fourier = fLine.Diff.Dy;
    Diff_Spectral = aLine.Diff.Dy;
    Diff_FD = dLine.Diff.Dy;
    Diff_FDP = pLine.Diff.Dy;
    
    %----Second Derivative ---------------------------
    Diff2_Fourier =fLine.Diff.DDy;
    Diff2_Spectral = aLine.Diff.DDy;
    Diff2_FD = dLine.Diff.DDy;
    Diff2_FDP = pLine.Diff.DDy;
    
    %----Integration----------------------------------
    Int_Fourier = fLine.Int;
    Int_Spectral = aLine.Int;
    Int_FD = dLine.Int;
    Int_FDP = pLine.Int ;  
    
    %----Space points---------------------------------    
    y_Fourier = Pts_Fourier.y;
    y_Spectral = Pts_Spectral.y;
    y_FD = Pts_FD.y;
    y_FDP = Pts_FDP.y;
    
    
    
    
    %----------------------------------------------------------------------
    % Numerical Differentiation and Integration
    %----------------------------------------------------------------------
    
     alpha = 2*pi;
     DiffN_Fourier = Diff_Fourier*sin(alpha*y_Fourier);
     DiffN_Spectral = Diff_Spectral*sin(alpha*y_Spectral);
     DiffN_FD = Diff_FD*sin(alpha*y_FD);
     DiffN_FDP = Diff_FDP*sin(alpha*y_FDP);
     
     Diff2N_Fourier = Diff2_Fourier*sin(alpha*y_Fourier);
     Diff2N_Spectral = Diff2_Spectral*sin(alpha*y_Spectral);
     Diff2N_FD = Diff2_FD*sin(alpha*y_FD);
     Diff2N_FDP = Diff2_FDP*sin(alpha*y_FDP);
     
     
     IntN_Fourier = Int_Fourier*sin(alpha*y_Fourier);
     IntN_Spectral = Int_Spectral*sin(alpha*y_Spectral);
     IntN_FD = Int_FD*sin(alpha*y_FD);
     IntN_FDP = Int_FDP*sin(alpha*y_FDP);

    %----------------------------------------------------------------------
    % Exact Differentiation and Integration
    %----------------------------------------------------------------------
  
    Diff_Exact_Fourier = alpha*cos(alpha* y_Fourier);
    Diff_Exact_Spectral = alpha*cos(alpha* y_Spectral);
    Diff_Exact_FD = alpha*cos(alpha* y_FD);
    Diff_Exact_FDP = alpha*cos(alpha* y_FDP);
    
    Diff2_Exact_Fourier = -alpha^2*sin(alpha* y_Fourier);
    Diff2_Exact_Spectral = -alpha^2*sin(alpha* y_Spectral);
    Diff2_Exact_FD = -alpha^2*sin(alpha* y_FD);
    Diff2_Exact_FDP = -alpha^2*sin(alpha* y_FDP);
    
    Int_Exact = - cos(alpha*1)/(alpha) + cos(alpha*0)/(alpha);

    %----------------------------------------------------------------------
    % Plotting and Error Comparison
    %----------------------------------------------------------------------
    
    figure(); 
    subplot(2,4,1);    
	optsF.plain = true;
    optsF.linewidth = 2;
    fLine.plot(DiffN_Fourier,optsF);
    hold on
    optsF.linestyle = '--';
    fLine.plot(Diff_Exact_Fourier,optsF);
    title('1st derivative: Fourier');
    %ylim([-0.03,0.03])
    lgd = legend('numerical', 'exact');
    lgd.FontSize = 7;
    hold off
    
    subplot(2,4,2);    
	optsS.plain = true;
    optsS.linewidth = 2;
    aLine.plot(DiffN_Spectral,optsS);
    hold on
    optsS.linestyle = '--';
    aLine.plot(Diff_Exact_Spectral,optsS);
    title('Spectral');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    
    subplot(2,4,3);    
	optsFD.plain = true;
    optsFD.linewidth = 2;
    dLine.plot(DiffN_FD,optsFD);
    hold on
    optsFD.linestyle = '--';
    dLine.plot(Diff_Exact_FD,optsFD);
    title('FD');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    
    subplot(2,4,4);    
	optsFDP.plain = true;
    optsFDP.linewidth = 2;
    pLine.plot(DiffN_FDP,optsFDP);
    hold on
    optsFDP.linestyle = '--';
    pLine.plot(Diff_Exact_FDP,optsFDP);
    title('FDP');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    
    subplot(2,4,5);    
	optsF2.plain = true;
    optsF2.linewidth = 2;
    fLine.plot(Diff2N_Fourier,optsF2);
    hold on
    optsF2.linestyle = '--';
    fLine.plot(Diff2_Exact_Fourier,optsF2);
    title('2nd derivative: Fourier');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    
    subplot(2,4,6);    
	optsS2.plain = true;
    optsS2.linewidth = 2;
    aLine.plot(Diff2N_Spectral,optsS2);
    hold on
    optsS2.linestyle = '--';
    aLine.plot(Diff2_Exact_Spectral,optsS2);
    title('Spectral');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    
    subplot(2,4,7);    
	optsFD2.plain = true;
    optsFD2.linewidth = 2;
    dLine.plot(Diff2N_FD,optsFD2);
    hold on
    optsFD2.linestyle = '--';
    dLine.plot(Diff2_Exact_FD,optsFD2);
    title('FD');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    
    subplot(2,4,8);    
	optsFDP2.plain = true;
    optsFDP2.linewidth = 2;
    pLine.plot(Diff2N_FDP,optsFDP2);
    hold on
    optsFDP2.linestyle = '--';
    pLine.plot(Diff2_Exact_FDP,optsFDP2);
    title('FDP');
    %ylim([-0.03,0.03])
    %legend('numerical', 'exact');
    hold off
    

    
    
       
    Fourier_FirstDerivative_Error = max(abs(DiffN_Fourier- Diff_Exact_Fourier))
    if (Fourier_FirstDerivative_Error <1e-4)
        disp('Fourier First Derivative test passed')
    else
        disp('Fourier First Derivative test failed')
    end
    Fourier_SecondDerivative_Error = max(abs(Diff2N_Fourier- Diff2_Exact_Fourier))
    if (Fourier_SecondDerivative_Error <1e-4)
        disp('Fourier Second Derivative test passed')
    else
        disp('Fourier Second Derivative test failed')
    end    
    Fourier_Integration_Error = max(abs(IntN_Fourier- Int_Exact))
    if (Fourier_Integration_Error <1e-4)
        disp('Fourier Integration test passed')
    else
        disp('Fourier Integration test failed')
    end
    
    Spectral_FirstDerivative_Error = max(abs(DiffN_Spectral- Diff_Exact_Spectral))
    if (Spectral_FirstDerivative_Error <1e-4)
        disp('Spectral First Derivative test passed')
    else
        disp('Spectral First Derivative test failed')
    end
    Spectral_SecondDerivative_Error = max(abs(Diff2N_Spectral- Diff2_Exact_Spectral))
    if (Spectral_SecondDerivative_Error <1e-4)
        disp('Spectral Second Derivative test passed')
    else
        disp('Spectral Second Derivative test failed')
    end    
    Spectral_Integration_Error = max(abs(IntN_Spectral- Int_Exact))
    if (Spectral_Integration_Error <1e-4)
        disp('Spectral Integration test passed')
    else
        disp('Spectral Integration test failed')
    end
    
    FD_FirstDerivative_Error = max(abs(DiffN_FD- Diff_Exact_FD))
    if (FD_FirstDerivative_Error <1e-4)
        disp('Finite Difference First Derivative test passed')
    else
        disp('Finite Difference First Derivative test failed')
    end
    FD_SecondDerivative_Error = max(abs(Diff2N_FD- Diff2_Exact_FD))
    if (FD_SecondDerivative_Error <1e-4)
        disp('Finite Difference Second Derivative test passed')
    else
        disp('Finite Difference Second Derivative test failed')
    end    
    FD_Integration_Error = max(abs(IntN_FD- Int_Exact))
    if (FD_Integration_Error <1e-4)
        disp('Finite Difference Integration test passed')
    else
        disp('Finite Difference Integration test failed')
    end
    
    FDP_FirstDerivative_Error = max(abs(DiffN_FDP- Diff_Exact_FDP))
    if (FDP_FirstDerivative_Error <1e-4)
        disp('Finite Difference Periodic First Derivative test passed')
    else
        disp('Finite Difference Periodic First Derivative test failed')
    end
    FDP_SecondDerivative_Error = max(abs(Diff2N_FDP- Diff2_Exact_FDP))
    if (FDP_SecondDerivative_Error <1e-4)
        disp('Finite Difference Periodic Second Derivative test passed')
    else
        disp('Finite Difference Periodic Second Derivative test failed')
    end    
    FDP_Integration_Error = max(abs(IntN_FDP- Int_Exact))
    if (FDP_Integration_Error <1e-4)
        disp('Finite Difference Periodic Integration test passed')
    else
        disp('Finite Difference Periodic Integration test failed')
    end
   
end