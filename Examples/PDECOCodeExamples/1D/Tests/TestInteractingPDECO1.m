function output = TestInteractingPDECO1()
% testing a few examples of interacting problems
% need to change lambda to 0.001 for problem 5!!
% solutions for current configuration (beta = 10^-3, gamma =-1):
% 1. 667 Iters, ~4 min, JFW = 0.0438, JOpt = 0.0011
% 2. 715 Iters, ~4 min, JFW = 0.0536, JOpt = 0.0097
% 3. 668 Iters, ~3 min, JFW = 0.0510, JOpt = 0.0026
% 4. 800 Iters, ~3 min, JFW = 0.1417, JOpt = 0.0203
% 5. 7030 Iters, ~35 min, JFW = 0.0606, JOpt = 0.0060 !!lambda changed!!
% 6. 606 Iters, ~3 min, JFW = 0.0041, JOpt = 0.0002
% 7. 641 Iters, ~3 min, JFW = 0.0209, JOpt = 0.0009

iVec = 1:2;  % problems to solve
jVec = 2; %1:3; % gamma values
kVec = 1; %1:4; % beta values

SolverFlag = 'FixPt';

betaVec = [10^-3, 10^-1, 10, 10^3];  
gammaVec = [0, -1, 1];

testFunVec{1} = 'AD_Flow_NeumannInt5'; % Asymmetric with rhoIC =0.5 
testFunVec{2} = 'AD_Flow_NeumannInt8'; % One peak to two peaks 
testFunVec{3} = 'AD_Flow_DirichletIntC052'; %0.5 BC
testFunVec{4} = 'AD_Force_DirichletInt2'; % one peak to two peaks % force control
testFunVec{5} = 'AD_Force_NeumannInt2'; %%%% Change lambda!!! % force control
testFunVec{6} = 'AD_Flow_NeumannInt3'; %symmetric, flow control
testFunVec{7} = 'AD_Flow_NeumannInt4'; %symmetric, flow control 

BCFunVec{1} = 'ComputeNeumannBCs';
BCFunVec{2} = 'ComputeNeumannBCs';
BCFunVec{3} = 'ComputeDirichletC05BCs';
BCFunVec{4} = 'ComputeDirichletBCs';
BCFunVec{5} = 'ComputeNeumannBCs';
BCFunVec{6} = 'ComputeNeumannBCs';
BCFunVec{7} = 'ComputeNeumannBCs';

PDERHSVec{1} = 'AD_Flowf';
PDERHSVec{2} = 'AD_Flowf';
PDERHSVec{3} = 'AD_Flowf';
PDERHSVec{4} = 'AD_Force';
PDERHSVec{5} = 'AD_Force';
PDERHSVec{6} = 'AD_Flowf';
PDERHSVec{7} = 'AD_Flowf';

BCOptVec{1} = [];
BCOptVec{2} = [];
BCOptVec{3} = 'ComputeDirichletBCs';
BCOptVec{4} = [];
BCOptVec{5} = [];
BCOptVec{6} = [];
BCOptVec{7} = [];

for i = iVec
    BCFunStr = BCFunVec{i};
    testFun = testFunVec{i};
    PDERHSStr = PDERHSVec{i};
    BCOpt = BCOptVec{i};
for j = jVec
    gamma = gammaVec(j);
for k = kVec
    beta = betaVec(k);
    
    Tols2 = 10^-4; 
    Tols1 = 10^-8; 

    n = 30; 
    N = 40;

    Phys_Area = struct('shape', 'SpectralLine', 'N', N, 'yMin', -1, 'yMax', 1) ;
    Plot_Area = struct('N',100,'yMin',-1,'yMax',1); 
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGradGaussian');
    
    ProbSpecs = struct('BCFunStr', BCFunStr, 'PDERHSStr', PDERHSStr, 'ComputeNormStr', 'ComputeRelL2LinfNorm');
    DataInput = struct('testFun', testFun,'OptirhoIG','FWrho','DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', beta,'gamma',gamma,'other',[],'D0', 1);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('lambda', 0.01, 'SolverFlag', SolverFlag, 'AdaSolverStr', [], 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG',,'rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput,'BCFunOptStr',BCOpt);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_1D(opts);
    %output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;  

    JFW = output1a.Errors.ForwardError.JFW;
    JOpt = output1a.Errors.OptimizationError.JOpti;
    Iters = output1a.OptimizationResult.Iterations;
    text1 = sprintf(testFun);
    text = sprintf('\n gamma: %.1f,\n beta: %0.3f,\n Iterations: %d,\n J FW: %.8f,\n J Opt: %.8f', [gamma; beta; Iters; JFW; JOpt]);
    texti = [text1, text];
    iStr = num2str(i);
    jStr = num2str(j);
    kStr = num2str(k);
    ti = strcat("Test",iStr,jStr,kStr);
    ttext.(ti) = texti; 
end
end
end
output = ttext;

end