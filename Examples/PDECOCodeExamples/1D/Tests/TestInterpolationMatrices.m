function TestInterpolationMatrices()

   %%%%--------------------------------------------------------------------
   % This code tests the interpolation matrices from the pseudospectral and
   % finite difference discretization.
   % It compares output of the numerical and analytical
   % interpolation for (x-y)Sin(alpha y) on the interval /x-y/< R or [x-R,x+R]
   %
   % Inputs:
   % Parameters for shape(Fourier/FiniteDifferencePeriodic for periodic bcs and Spectral/FIniteDifference for no flux and dirichlet bcs)
   % Constants for R,alpha
   %
   % Output:
   % Plot of graphs and display of error between numerical and exact convolutions
   %%%---------------------------------------------------------------------
   
    AddPaths();
    
    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------

    PhysArea_Fourier = struct('shape','FourierLine','N',20,'yMin',0,'yMax',1);
    PhysArea_Spectral = struct('shape','SpectralLine','N',20,'yMin',0,'yMax',1);
    PhysArea_FD = struct('shape','FiniteDifferenceLine','N',20,'yMin',0,'yMax',1);
    PhysArea_FDP = struct('shape','FiniteDifferencePeriodicLine','N',20,'yMin',0,'yMax',1);
    
    PlotArea = struct('N',200,'yMin',0,'yMax',1);
      
    shapeClass_Fourier = str2func(PhysArea_Fourier.shape);
    shapeClass_Spectral = str2func(PhysArea_Spectral.shape);
    shapeClass_FD = str2func(PhysArea_FD.shape);
    shapeClass_FDP = str2func(PhysArea_FDP.shape);
    
    fLine = shapeClass_Fourier(PhysArea_Fourier);
    aLine = shapeClass_Spectral(PhysArea_Spectral);
    dLine = shapeClass_FD(PhysArea_FD);
    pLine = shapeClass_FDP(PhysArea_FDP);
    
    [Pts_Fourier,~,~,~,~] = fLine.ComputeAll(PlotArea); 
    [Pts_Spectral,~,~,~,~] = aLine.ComputeAll(PlotArea); 
    [Pts_FD,~,~,~,~] = dLine.ComputeAll(PlotArea); 
    [Pts_FDP,~,~,~,~] = pLine.ComputeAll(PlotArea);
    
    %----------------------------------------------------------------------
    % Compute Interpolation Matrices
    %----------------------------------------------------------------------
    plotpoints = linspace(0,1,1000);
    %ComputeInterpolationMatrix(this,interp,saveBool)
    InterpSpace_Fourier = fLine.ComputeInterpolationMatrix(fLine.CompSpace(plotpoints'),true);
    InterpSpace_Spectral = aLine.ComputeInterpolationMatrix(aLine.CompSpace(plotpoints'),true);
    InterpSpace_FD = dLine.ComputeInterpolationMatrix(dLine.CompSpace(plotpoints'),true);
    InterpSpace_FDP = pLine.ComputeInterpolationMatrix(pLine.CompSpace(plotpoints'),true);      %pLine.InterpolationPlot(PlotArea,true);
    
    
    y_Fourier = Pts_Fourier.y;
    y_Spectral = Pts_Spectral.y;
    y_FD = Pts_FD.y;
    y_FDP = Pts_FDP.y;
    
    
    alpha = 4*pi;
    rho_Fourier = sin(alpha*y_Fourier);
    rho_Spectral = sin(alpha*y_Spectral);
    rho_FD = sin(alpha*y_FD);
    rho_FDP = sin(alpha*y_FDP);
    
    rho_exact = sin(alpha*plotpoints);
    
    var_Fourier = InterpSpace_Fourier.InterPol*rho_Fourier;
    var_Spectral = InterpSpace_Spectral.InterPol*rho_Spectral;
    var_FD = InterpSpace_FD.InterPol*rho_FD;
    var_FDP = InterpSpace_FDP.InterPol*rho_FDP;
    
   

    %----------------------------------------------------------------------
    % Plotting and Error Comparison
    %----------------------------------------------------------------------
    figure()
    subplot(2,2,1);    
	opts.plain = true;
    opts.linewidth = 2;
    %opts.colour = 'k';
    plot(plotpoints,rho_exact);
    hold on
    opts.linestyle = '--';
    %opts.color = 'b';
    plot(plotpoints,var_Fourier);
     title('Fourier');
%     ylim([-0.03,0.03])
     legend('exact', 'interp');
     hold off
    
    subplot(2,2,2); 
    opts_S.plain = true;
    opts_S.linewidth = 2;
    %opts_S.color = 'r';
    plot(plotpoints,rho_exact);
    hold on
    opts_S.linestyle = '--';
    %opts_S.color = 'b';
    plot(plotpoints,var_Spectral);
    title('Spectral')
    %ylim([-0.03,0.03])
    legend('exact', 'interp');
    hold off
    
    subplot(2,2,3); 
    opts_P.plain = true;
    opts_P.linewidth = 2;
    %opts_S.color = 'r';
    plot(plotpoints,rho_exact);
    hold on
    opts_P.linestyle = '--';
    %opts_S.color = 'b';
    plot(plotpoints,var_FDP);
    title('Finite Difference Periodic')
    %ylim([-0.03,0.03])
    legend('exact', 'interp');
    hold off
    
    subplot(2,2,4); 
    opts_D.plain = true;
    opts_D.linewidth = 2;
    %opts_S.color = 'r';
    plot(plotpoints,rho_exact);
    hold on
    opts_D.linestyle = '--';
    %opts_S.color = 'b';
    plot(plotpoints,var_FD);
    title('Finite Difference')
    %ylim([-0.03,0.03])
    legend('exact', 'interp');
    hold off
    
    
    
    Fourier_Interpolation_Error = max(abs(var_Fourier- rho_exact))
    if (Fourier_Interpolation_Error <1e-4)
        disp('Fourier test passed')
    else
        disp('Fourier test failed')
    end
    Spectral_Interpolation_Error = max(abs(var_Spectral- rho_exact))
    if (Spectral_Interpolation_Error <1e-4)
        disp('Spectral test passed')
    else
        disp('Spectral test failed')
    end
    
    FD_Interpolation_Error = max(abs(var_FD- rho_exact))
    if (FD_Interpolation_Error <1e-4)
        disp('Finite Difference test passed')
    else
        disp('Finite Difference test failed')
    end
    
    FDP_Interpolation_Error = max(abs(var_FDP- rho_exact))
    if (FDP_Interpolation_Error <1e-4)
        disp('Finite Difference Periodic test passed')
    else
        disp('Finite Difference Periodic test failed')
    end
    
end