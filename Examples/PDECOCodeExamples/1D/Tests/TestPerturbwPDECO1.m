function output = TestPerturbwPDECO1()
  % Perturbing an exact solution by 10% or 50% in time (or time and space)
  % and check the final error against the exact solution
  % Currently t perturbed by 10%, takes ~2 min and solution is:
  %    InitialwErr: 0.1000
  %    FinalwErr: 6.2681e-05
  %    rhoErr: 1.1667e-05
  %    pErr: 2.8562e-05
  
  
    Tols2 = 10^-4; 
    Tols1 = 10^-8; 

    n = 30; 
    N = 40;
      
    beta = 10^-3;
    testFunStr = 'AD_Flow_Neumann_Exact';
    
    Phys_Area = struct('shape', 'SpectralLine', 'N', N, 'yMin', -1, 'yMax', 1) ;
    Plot_Area = struct('N',100,'yMin',-1,'yMax',1); 
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGradGaussian');
     
    %wFlowEx = wFlowExact(N,-1,1,100,beta,n,0,1,testFunStr,[]);
    wFlow = wFlowperturbt(N,-1,1,100,beta,n,0,1,testFunStr,[]);         %  %perturb t by 10%
    %wFlow = wFlowperturbt2(N,-1,1,100,beta,n,0,1,testFunStr,[]);       %  %perturb t by 50%
    %wFlow = wFlowperturbtandx(N,-1,1,100,beta,n,0,1,testFunStr,[]);    %  %perturb t and x by 10%
    %wFlow = wFlowperturbtandx2(N,-1,1,100,beta,n,0,1,testFunStr,[]);   %  %perturb t and x by 50%

    ProbSpecs = struct('BCFunStr', 'ComputeNeumannBCs', 'PDERHSStr', 'AD_Flowf', 'ComputeNormStr', 'ComputeRelL2LinfNorm');
    DataInput = struct('testFun', testFunStr , 'wFlow', wFlow,'OptirhoIG','FWrho','DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', beta,'gamma',0,'other',[],'D0', 1);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('SolverFlag', 'FixPt', 'AdaSolverStr', [], 'lambda', 0.01, 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_1D(opts);
    output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;  
    
    output.InitialwErr = ComputeRelL2LinfNorm(wFlow, output1a.ExactSolution.wFlow, output1a.IDC.Int,0);
    output.FinalwErr = ComputeRelL2LinfNorm(output1a.OptimizationResult.Control', output1a.ExactSolution.wFlow, output1a.IDC.Int,0);
    output.rhoErr = ComputeRelL2LinfNorm(output1a.OptimizationResult.rhoNum, output1a.ExactSolution.rhoIC, output1a.IDC.Int,0);
    output.pErr = ComputeRelL2LinfNorm(output1a.OptimizationResult.pNum, output1a.ExactSolution.pIC, output1a.IDC.Int,0);
        
end