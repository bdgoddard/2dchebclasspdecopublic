
function ADFlowExactD = AD_Flow_Dirichlet_Exact2D(this,y1,y2,t,T,beta)
    %cw=0, cForce=1, cExt=0, gamma=0, cFlow=1
    
        ADFlowExactD.rhoIC = 2*beta^(1/2)*exp(t)*(cos(pi*y1/2).*cos(pi*y2/2));
        ADFlowExactD.OptirhoIG = 2*beta^(1/2)*exp(t)*(cos(pi*y1/2).*cos(pi*y2/2));
        
        ADFlowExactD.pIC = beta^(1/2)*(exp(T)- exp(t))*(cos(pi*y1/2).*cos(pi*y2/2));
        ADFlowExactD.OptipIG = beta^(1/2)*(exp(T)- exp(t))*(cos(pi*y1/2).*cos(pi*y2/2));
        
        ADFlowExactD.rhoTarget = beta^(1/2)*exp(t).*(cos(pi*y1/2).*cos(pi*y2/2)) - beta^(1/2)*(pi^2/2)*(exp(T)-exp(t))*...
            (cos(pi*y1/2).*cos(pi*y2/2)) - beta^(1/2)*((pi^2*exp(t)*(exp(T)-exp(t))^2)/ (2))*(cos(pi*y1/2).*cos(pi*y2/2)).*(((sin(pi*y1/2).^2).*(cos(pi*y2/2).^2)) +((cos(pi*y1/2).^2).*(sin(pi*y2/2).^2)) );
           
        ADFlowExactD.wForce = zeros(size(y1));
        
        ADFlowExactD.Vext = zeros(size(y1));
        
        ADFlowExactD.Force = beta^(1/2)*(2*exp(t)+ pi^2*exp(t))*(cos(pi*y1/2).*cos(pi*y2/2)) + (beta^(1/2)*(2*pi.^2*exp(2*t)*(exp(T)-exp(t))))*...
            ((cos(pi*y1/2).^3).*(cos(pi*y2/2).^3) - (cos(pi*y1/2).^3).*(sin(pi*y2/2).^2).*(cos(pi*y2/2)) -(cos(pi*y2/2).^3).*(sin(pi*y1/2).^2).*(cos(pi*y1/2)) );
        
        ADFlowExactD.wFlow=(pi*exp(t)*(exp(T)-exp(t)) )*(cos(pi*y1/2).*cos(pi*y2/2)).*[sin(pi*y1/2).*cos(pi*y2/2), cos(pi*y1/2).*sin(pi*y2/2)];
        
        ADFlowExactD.Flow = zeros(size([y1,y1]));
end

