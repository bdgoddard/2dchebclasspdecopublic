function ADFlowExactN = AD_Flow_Neumann_Exact2D(this,y1,y2,t,T,beta)
        
        c=1;
        ADFlowExactN.rhoIC= c^(1/2)*beta^(1/2)*exp(t)*(cos(pi*y1).*cos(pi*y2));
        ADFlowExactN.OptirhoIG = c^(1/2)*beta^(1/2)*exp(t)*(cos(pi*y1).*cos(pi*y2));
        
        ADFlowExactN.pIC = c^(1/2)*beta^(1/2)*(exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        ADFlowExactN.OptipIG = c^(1/2)*beta^(1/2)*(exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        
        ADFlowExactN.rhoTarget =  - c^(1/2)*beta^(1/2)*2*pi^2*(exp(T)-exp(t))*...
             (cos(pi*y1).*cos(pi*y2)) - c^(3/2)*beta^(1/2)*((pi^2*exp(t)*(exp(T)-exp(t))^2))*(cos(pi*y1).*cos(pi*y2)).*...
             ( (sin(pi*y1).^2).*(cos(pi*y2).^2) +(cos(pi*y1).^2).*(sin(pi*y2).^2)  );
           
        ADFlowExactN.wForce =  zeros(size(y1));
        
        ADFlowExactN.Vext = zeros(size(y1));
        
        ADFlowExactN.Force = c^(1/2)*beta^(1/2)*(exp(t)+ 2*pi^2*exp(t))*(cos(pi*y1).*cos(pi*y2)) + ((c^(3/2)*beta^(1/2)*2*pi^2*exp(2*t)*(exp(T)-exp(t))))*...
            ((cos(pi*y1).^3).*(cos(pi*y2).^3) - (cos(pi*y1).^3).*(sin(pi*y2).^2) .* cos(pi*y2)- (cos(pi*y2).^3).*(sin(pi*y1).^2) .* cos(pi*y1));
        
        ADFlowExactN.wFlow=(c*pi*exp(t)*(exp(T)-exp(t)))*(cos(pi*y1).*cos(pi*y2)).*[sin(pi*y1).*cos(pi*y2), sin(pi*y2).*cos(pi*y1)];
        ADFlowExactN.Flow = zeros(size([y1,y1]));
end

