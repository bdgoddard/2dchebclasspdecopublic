function ADForceExactD = AD_Force_Dirichlet_Exact2D(this,y1,y2,t,T,beta)
    %cw=1, cForce=1, cExt=1, gamma=1, cFlow=0
    
        ADForceExactD.rhoIC = 2*exp(t)*(cos(pi*y1/2)).*(cos(pi*y2/2));
        ADForceExactD.OptirhoIG = 2*exp(t)*(cos(pi*y1/2)).*(cos(pi*y2/2));
        
        ADForceExactD.pIC = (exp(T)- exp(t))*(cos(pi*y1/2)).*(cos(pi*y2/2));
        ADForceExactD.OptipIG = (exp(T)- exp(t))*(cos(pi*y1/2)).*(cos(pi*y2/2));
        
        ADForceExactD.rhoTarget = (exp(t)-(pi^2/2)*(exp(T) -exp(t)))*(cos(pi*y1/2)).*(cos(pi*y2/2)) - ...
           (pi^2/4)*(exp(T) -exp(t))*((sin(pi*y1/2).^2).*(cos(pi*y2/2).^2) +(cos(pi*y1/2).^2).*(sin(pi*y2/2).^2) );
           
        ADForceExactD.wForce = -((exp(T)-exp(t))/beta )*(cos(pi*y1/2)).*(cos(pi*y2/2));
        
        ADForceExactD.Vext = (cos(pi*y1/2)).*(cos(pi*y2/2));
        
        ADForceExactD.Force = (2*exp(t) + pi^2*exp(t) +(1/beta)*(exp(T)-exp(t)))* cos(pi*y1/2).*cos(pi*y2/2) ...
            + (pi^2/2)*exp(t)*( 2* (cos(pi*y1/2).^2 ) .*(cos(pi*y2/2).^2) ...
            - (sin(pi*y1/2).^2).*(cos(pi*y2/2).^2) - (sin(pi*y2/2).^2).*(cos(pi*y1/2).^2)  );
        
        ADForceExactD.wFlow = zeros(size([y1,y1]));
        ADForceExactD.Flow = zeros(size([y1,y1]));
end

