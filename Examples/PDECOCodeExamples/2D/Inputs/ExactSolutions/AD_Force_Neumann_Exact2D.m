function ADForceExactN = AD_Force_Neumann_Exact2D(this,y1,y2,t,T,beta)
    %cw=1, cForce=1, cExt=1, gamma=1, cFlow=0
    
        ADForceExactN.rhoIC = 2*exp(t)*(cos(pi*y1).*cos(pi*y2));
        ADForceExactN.OptirhoIG = 2*exp(t)*(cos(pi*y1).*cos(pi*y2));
       
        ADForceExactN.pIC = (exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        ADForceExactN.OptipIG = (exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        
        ADForceExactN.rhoTarget =  exp(t)*(cos(pi*y1).*cos(pi*y2)) - 2*pi^2*(exp(T)-exp(t))*...
            (cos(pi*y1).*cos(pi*y2)) - pi^2*(exp(T)-exp(t))*( (sin(pi*y1).^2).*(cos(pi*y2).^2) + (cos(pi*y1).^2).*(sin(pi*y2).^2) ) ;
           
        ADForceExactN.wForce = -((exp(T)-exp(t))/beta)*(cos(pi*y1).*cos(pi*y2));
        
        ADForceExactN.Vext =  (cos(pi*y1).*cos(pi*y2));
        
        ADForceExactN.Force = (2*exp(t)+ 4*pi^2*exp(t))*(cos(pi*y1).*cos(pi*y2)) + ((exp(T)-exp(t))/beta)*...
            (cos(pi*y1).*cos(pi*y2)) + 2*pi^2*exp(t)*( 2*(cos(pi*y1).^2).*(cos(pi*y2).^2) -...
            (sin(pi*y1).^2).*(cos(pi*y2).^2) - (cos(pi*y1).^2).*(sin(pi*y2).^2) );
        
        ADForceExactN.wFlow=zeros(size([y1,y1]));
        
        ADForceExactN.Flow=zeros(size([y1,y1]));
end

