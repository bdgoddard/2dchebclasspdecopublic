function DExactD = D_Force_Dirichlet_Exact2D(this,x1,x2,t,T,beta)
   %cw=1, cForce=0, cExt=0, gamma=0, cFlow=0
    
    DExactD.rhoIC = ( (4/(2* pi^2*beta))*exp(T) - (4/((4+2*pi^2)*beta))*exp(t))*cos(pi*x1/2).*cos(pi*x2/2);
 
    DExactD.OptirhoIG = ( (4/(2* pi^2*beta))*exp(T) - (4/((4+2*pi^2)*beta))*exp(t))*cos(pi*x1/2).*cos(pi*x2/2);
    
    DExactD.pIC = - (exp(T) - exp(t))*cos(pi*x1/2).*cos(pi*x2/2);
    DExactD.OptipIG = - (exp(T) - exp(t))*cos(pi*x1/2).*cos(pi*x2/2);

    DExactD.rhoTarget =( (( (2*pi^2)/4) +(4/(2*pi^2*beta)))*exp(T) + (1 - ((2*pi^2)/4) - (4/((4+2*pi^2)*beta)))*exp(t) )*cos(pi*x1/2).*cos(pi*x2/2);
   
    DExactD.wForce = ((exp(T)-exp(t))/beta )*cos(pi*x1/2).*cos(pi*x2/2);

    DExactD.Vext = zeros(size(x1));

    DExactD.Force = zeros(size(x1));
    
    DExactD.wFlow = zeros(size([x1,x1]));
    
    DExactD.Flow = zeros(size([x1,x1]));
end  
