function DExactN = D_Force_Neumann_Exact2D(this,x1,x2,t,T,beta)
    %cw=1, cForce=0, cExt=0, gamma=0, cFlow=0
    
    DExactN.rhoIC = ( 1/(2*pi^2*beta)*exp(T) - 1/((1+2*pi^2)*beta)*exp(t) )*(cos(pi*x1).*cos(pi*x2));
   
    DExactN.OptirhoIG = ( 1/(2*pi^2*beta)*exp(T) - 1/((1+2*pi^2)*beta)*exp(t) )*cos(pi*x1).*cos(pi*x2);
   
    
    DExactN.pIC = - (exp(T) - exp(t))*cos(pi*x1).*cos(pi*x2);
    DExactN.OptipIG = -(exp(T) - exp(t))*cos(pi*x1).*cos(pi*x2);

    DExactN.rhoTarget = ( ( 2*pi^2 + 1/(2*pi^2*beta) )*exp(T) ...
               + ( 1 - 2*pi^2 - 1/((1+2*pi^2)*beta))*exp(t) ) *cos(pi*x1).*cos(pi*x2);

    DExactN.wForce = ((exp(T)-exp(t))/beta )*cos(pi*x1).*cos(pi*x2);     

    DExactN.Vext= zeros(size(x1));

    DExactN.Force = zeros(size(x1));
    
    DExactN.wFlow = zeros(size([x1,x1]));
    
    DExactN.Flow = zeros(size([x1,x1]));
   
    end
    
