function ADFlowExactN = AD_Flow_NeumannInt62D(this,y1,y2,t,T,beta)
        
        c=0.5;
        ADFlowExactN.rhoIC= zeros(size(y1)) + 0.25; 
        ADFlowExactN.OptirhoIG = zeros(size(y1)) + 0.25; 
        
        ADFlowExactN.pIC = c^(1/2)*beta*(exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        ADFlowExactN.OptipIG = c^(1/2)*beta*(exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        
        ADFlowExactN.rhoTarget = 0.25*(1-t) + t*((1/4)*((sin(pi*(y1 - 2)/2)).*sin(pi*(y2 - 2)/2)) + 1/4);
           
        ADFlowExactN.wForce = zeros(size(y1));
        
        ADFlowExactN.Vext = zeros(size(y1));
        
        ADFlowExactN.Force = zeros(size(y1));
        
        ADFlowExactN.wFlow= zeros(size([y1,y1]));
        
        ADFlowExactN.Flow = zeros(size([y1,y1]));
end

