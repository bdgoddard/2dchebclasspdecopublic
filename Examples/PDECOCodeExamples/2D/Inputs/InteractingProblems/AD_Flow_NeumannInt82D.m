function ADFlowExactN = AD_Flow_NeumannInt82D(this,y1,y2,t,T,beta)
        
        c=0.5;
        ADFlowExactN.rhoIC= zeros(size(y1)) + 0.25; 
        ADFlowExactN.OptirhoIG = zeros(size(y1)) + 0.25; 
        
        ADFlowExactN.pIC = c^(1/2)*beta*(exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        ADFlowExactN.OptipIG = c^(1/2)*beta*(exp(T)- exp(t))*(cos(pi*y1).*cos(pi*y2));
        
        ADFlowExactN.rhoTarget = (1-t)*0.25 + t*(1/0.9921)*exp(-3*((y1+0.2).^2 + (y2+0.2).^2));
           
        ADFlowExactN.wForce = zeros(size(y1));
        
        ADFlowExactN.Vext = zeros(size(y1));
        
        ADFlowExactN.Force = zeros(size(y1));
        
        ADFlowExactN.wFlow= zeros(size([y1,y1]));
        
        ADFlowExactN.Flow = zeros(size([y1,y1]));
end

