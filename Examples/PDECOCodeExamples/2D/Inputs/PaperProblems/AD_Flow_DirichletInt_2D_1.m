function ADFlowExactD = AD_Flow_DirichletInt_2D_1(this,y1,y2,t,T,beta)

        

        ADFlowExactD.rhoIC =  (pi/4)^2 * cos(pi*y1/2).*cos(pi*y2/2) + (pi/4)^2;
        ADFlowExactD.OptirhoIG = (pi/4)^2 * cos(pi*y1/2).*cos(pi*y2/2) + (pi/4)^2;
        
        ADFlowExactD.pIC = zeros(size(y1));
        ADFlowExactD.OptipIG = zeros(size(y1));
        
        ADFlowExactD.rhoTarget = (1-t)*((pi/4)^2 * cos(pi*y1/2).*cos(pi*y2/2) + (pi/4)^2) + t*((pi/4)^2 * cos(pi*y1/2).*cos(3*pi*y2/2) + (pi/4)^2);
           
        ADFlowExactD.wForce = zeros(size(y1));
        
        ADFlowExactD.Vext = 2*sin(pi*y1/2).*sin((pi*y2/3) - (pi/2));
        
        ADFlowExactD.Force = zeros(size(y1));
        
        ADFlowExactD.wFlow= zeros(size([y1,y1]));
        
        ADFlowExactD.Flow = zeros(size([y1,y1]));
end