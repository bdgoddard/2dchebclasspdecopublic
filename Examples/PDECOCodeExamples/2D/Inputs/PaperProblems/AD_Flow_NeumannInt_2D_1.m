function ADFlowExactD = AD_Flow_NeumannInt_2D_1(this,y1,y2,t,T,beta)
      
        

        ADFlowExactD.rhoIC =  (1/4)* ones(size(y1));
        ADFlowExactD.OptirhoIG = (1/4)* ones(size(y1));
        
        ADFlowExactD.pIC = zeros(size(y1));
        ADFlowExactD.OptipIG = zeros(size(y1));
        
        ADFlowExactD.rhoTarget = (1/4)*(1-t) + (t/1.3791)*exp(-2*((y1+0.2).^2 + (y2+0.2).^2));
           
        ADFlowExactD.wForce = zeros(size(y1));
        
        ADFlowExactD.Vext = ((y1+0.3).^2 -1).*((y1-0.4).^2 -0.5).*((y2+0.3).^2 -1).*((y2-0.4).^2 -0.5);
        
        ADFlowExactD.Force = zeros(size(y1));
        
        ADFlowExactD.wFlow= zeros(size([y1,y1]));
        
        ADFlowExactD.Flow = zeros(size([y1,y1]));
end