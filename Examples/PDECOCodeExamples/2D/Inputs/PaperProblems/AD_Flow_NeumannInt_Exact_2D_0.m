function ADFlowExactD = AD_Flow_NeumannInt_Exact_2D_0(this,y1,y2,t,T,beta)
      %Test problem for Appendix, Exact solution example
        

        ADFlowExactD.rhoIC =  (1/4)*beta^(1/2)*exp(t)*(cos(pi*y1)+1).*(cos(pi*y2)+1);
        ADFlowExactD.OptirhoIG =  (1/4)*beta^(1/2)*exp(t)*(cos(pi*y1)+1).*(cos(pi*y2)+1);
        
        ADFlowExactD.pIC =  (1/4)*beta^(1/2)*(exp(T)-exp(t))*(cos(pi*y1)+1).*(cos(pi*y2)+1);
        ADFlowExactD.OptipIG = (1/4)*beta^(1/2)*(exp(T) - exp(t))*(cos(pi*y1)+1).*(cos(pi*y2)+1);
        
        ADFlowExactD.rhoTarget = 0*beta^(1/2)*exp(t)*(cos(pi*y1) +1).*(cos(pi*y2) + 1)...
                                    - 0.25*pi^2*beta^(1/2)*(exp(T) - exp(t))*...
                                        (cos(pi*y1).*(cos(pi*y2) + 1) + cos(pi*y2).*(cos(pi*y1) + 1)...
                                    +((sin(pi*y1)).^2).*cos(pi*y2).*(cos(pi*y2) + 1) + ((sin(pi*y2)).^2).*cos(pi*y1).*(cos(pi*y1) + 1))...
                                    + 0.5*pi^2*beta^(1/2)*exp(t)*((exp(T) - exp(t))^2)*((cos(pi*y1/2)).^4).*((cos(pi*y2/2)).^4).*(cos(pi*y1).*cos(pi*y2) - 1);%- (pi^2/4)* beta^(1/2)*(exp(T) - exp(t))*((cos(pi*y1)).*(cos(pi*y2)+1) + (cos(pi*y2)).*(cos(pi*y1)+1))...
                               
        
        ADFlowExactD.wForce = zeros(size(y1));
        
        ADFlowExactD.Vext = cos(pi*y1).*cos(pi*y2);
        
        ADFlowExactD.Force = 0.25*beta^(1/2)*exp(t)*(cos(pi*y1) +1).*(cos(pi*y2) +1) ...
                                + 0.25*beta^(1/2)*pi^2*exp(t)*(cos(pi*y1).*(2*cos(pi*y2) + 1) + cos(pi*y2))...
                                - pi^2*beta^(1/2)*exp(t)*((cos(pi*y1/2)).^2).*((cos(pi*y2/2)).^2).*(cos(pi*y1).*(1-4*cos(pi*y2)) + cos(pi*y2))...
                                +0.25*pi^2*beta^(1/2)*exp(2*t)*(exp(T) - exp(t))*((cos(pi*y1/2)).^4).*((cos(pi*y2/2)).^4).*(cos(pi*y1).*(6*cos(pi*y2) + 1) + cos(pi*y2) - 4);

        
        ADFlowExactD.wFlow= [(1/16)*exp(t)*(exp(T)-exp(t))*pi*sin(pi*y1).* (cos(pi*y1)+1).*((cos(pi*y2)+1).^2),...
                            (1/16)*exp(t)*(exp(T)-exp(t))*pi*sin(pi*y2).*((cos(pi*y1)+1).^2).*(cos(pi*y2)+1)];
        
        ADFlowExactD.Flow = zeros(size([y1,y1]));
        
    function out = dotVectors(u,v)
        
        [m,n] =size(u);
        pd = zeros(m,n);
        out = zeros(1,n);
        for i=1:m
            pd(i) = u(i).* v(i);
            out = out+ pd(i);
        end
    end
end