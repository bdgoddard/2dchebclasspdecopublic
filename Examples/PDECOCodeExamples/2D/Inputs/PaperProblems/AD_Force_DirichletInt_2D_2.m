function ADForceExactD = AD_Force_DirichletInt_2D_2(this,y1,y2,t,T,beta)
    
    
        ADForceExactD.rhoIC = (1/4)*cos(pi*y1/2).*cos(pi*y2/2) + 1/4;
        ADForceExactD.OptirhoIG = (1/4)*cos(pi*y1/2).*cos(pi*y2/2) + 1/4;
        
        ADForceExactD.pIC = zeros(size(y1));%(exp(T)- exp(t)).*cos(pi*y/2);
        ADForceExactD.OptipIG = zeros(size(y1));%(exp(T)- exp(t)).*cos(pi*y/2);
        
        ADForceExactD.rhoTarget = (1-t)*((1/4)*cos(pi*y1/2).*cos(pi*y2/2) + 1/4) -t*((1/4)*sin(pi*y1/2).*sin((pi*y2/2)-(pi/2)) + (1/4));
           
        ADForceExactD.wForce= zeros(size(y1));
        
        ADForceExactD.Vext =  (3/4)*(1-t)*(-cos(pi*y1/2).*sin(pi*y2/2)+1);
        
        ADForceExactD.Force =  zeros(size(y1));
        
        ADForceExactD.wFlow = zeros(size([y1,y1]));
        
        ADForceExactD.Flow = zeros(size([y1,y1]));
end