function ADForceExactD = AD_Force_NeumannInt_2D_2(this,y1,y2,t,T,beta)
    
    
        ADForceExactD.rhoIC = (1/4)*ones(size(y1)); 
        ADForceExactD.OptirhoIG = (1/4)*ones(size(y1));
        
        ADForceExactD.pIC = zeros(size(y1));
        ADForceExactD.OptipIG = zeros(size(y1));
        
        ADForceExactD.rhoTarget = (1/4)*(1-t)+ t*((1/4)*sin((pi*(y1-2))/2).*sin((pi*(y2-2))/2) + (1/4)); 
           
        ADForceExactD.wForce= zeros(size(y1));
        
        ADForceExactD.Vext =  cos((pi*y1/5) - (pi/5)).*sin((pi*y2/5));
        
        ADForceExactD.Force =  zeros(size(y1));
        
        ADForceExactD.wFlow = zeros(size([y1,y1]));
        
        ADForceExactD.Flow = zeros(size([y1,y1]));
end