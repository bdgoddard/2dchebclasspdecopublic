function output = SingleTestExactPDECO2D()
% testing one exact solution
% with below configurations get the following results:
%    rhoErr: 3.0956e-08
%    pErr:   2.4141e-08
%    wErr:   5.6082e-06

    Tols2 = 10^-4; 
    Tols1 = 10^-8; 

    n = 20;
    N1 = 30;
    N2 = 30;
    
    Phys_Area = struct('shape','Box','y1Min',-1,'y1Max',1,'N',[N1,N2], 'y2Min',-1,'y2Max',1);
    Plot_Area = struct('N1',100,'N2',100','y1Min', -1, 'y2Min', -1, 'y1Max', 1, 'y2Max', 1);
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGaussian2D');
    ProbSpecs = struct('BCFunStr', 'ComputeNeumannBCs', 'PDERHSStr', 'AD_Flowf', 'ComputeNormStr', 'ComputeRelL2LinfNorm2D');
    DataInput = struct('testFun', 'AD_Flow_Neumann_Exact2D' ,'OptirhoIG','FWrho','DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', 10^-3,'gamma',0,'D0',1,'other',0);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('SolverFlag', 'FixPt', 'AdaSolverStr', [], 'lambda', 0.01, 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG','rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt, 'V2Num',V2Num);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_2D(opts);
    output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;
    
    output.out = output1a;
    output.rhoErr = output1a.Errors.OptimizationError.rhoExactErr2D;
    output.pErr = output1a.Errors.OptimizationError.pExactErr2D;
    output.wErr = output1a.Errors.OptimizationError.wExactErr2D;
end