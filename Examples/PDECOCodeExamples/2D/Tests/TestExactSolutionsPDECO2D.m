function output = TestExactSolutionsPDECO2D()
% testing all 2D exact solutions 
% with the current configurations, the computation time is <1 min.
% for the below configurations (beta =10^-3, n = N =30), we get the following results:
% 1. rhoError = 0.00000002767
% 2. rhoError = 0.00000003818
% 3. rhoError = 0.000000009115
% 4. rhoError = 0.00000001406
% 5. rhoError = 0.00000001216
% 6. rhoError = 0.00000003856

    Tols2 = 10^-4; 
    Tols1 = 10^-8; 
    
    Vecn = [10, 20, 30, 40];
    VecN1 = [10, 20, 30, 40];
    VecN2 = [10, 20, 30, 40];
    betaVec = [10^-3, 10^-1, 10, 10^3];
    
    testFunVec{1} = 'AD_Flow_Neumann_Exact2D';
    testFunVec{2} = 'AD_Flow_Dirichlet_Exact2D';
    testFunVec{3} = 'AD_Force_Neumann_Exact2D';
    testFunVec{4} = 'AD_Force_Dirichlet_Exact2D';
    testFunVec{5} = 'D_Force_Neumann_Exact2D';
    testFunVec{6} = 'D_Force_Dirichlet_Exact2D';

    BCStrVec{1} = 'ComputeNeumannBCs';
    BCStrVec{2} = 'ComputeDirichletBCs';
    BCStrVec{3} = 'ComputeNeumannBCs';
    BCStrVec{4} = 'ComputeDirichletBCs';
    BCStrVec{5} = 'ComputeNeumannBCs';
    BCStrVec{6} = 'ComputeDirichletBCs';

    PDERHSStrVec{1} = 'AD_Flowf';
    PDERHSStrVec{2} = 'AD_Flowf';
    PDERHSStrVec{3} = 'AD_Force';
    PDERHSStrVec{4} = 'AD_Force';
    PDERHSStrVec{5} = 'D_Force';
    PDERHSStrVec{6} = 'D_Force';
for i = 1:6
    BCFunStr = BCStrVec{i};
    testFun = testFunVec{i}
    PDERHSStr = PDERHSStrVec{i};
for j = 1:1
    beta = betaVec(j)
for k = 3:3
    n = Vecn(k);
    N1 = VecN1(k);
    N2 = VecN2(k);
    Phys_Area = struct('shape','Box','y1Min',-1,'y1Max',1,'N',[N1,N2], 'y2Min',-1,'y2Max',1);
    Plot_Area = struct('N1',100,'N2',100','y1Min', -1, 'y2Min', -1, 'y1Max', 1, 'y2Max', 1);
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGaussian2D');
    ProbSpecs = struct('BCFunStr', BCFunStr, 'PDERHSStr', PDERHSStr, 'ComputeNormStr', 'ComputeRelL2LinfNorm2D');
    DataInput = struct('testFun', testFun,'DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', beta,'gamma',0,'D0',1,'other',0);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('SolverFlag', 'FixPt', 'AdaSolverStr', [], 'lambda', 0.01, 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG','rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
   
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt, 'V2Num',V2Num);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_2D(opts);
    output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;
    rhoErrFW = output1a.Errors.ForwardError.rhoExactErr2D;
    rhoErrOpt = output1a.Errors.OptimizationError.rhoExactErr2D;
    Iters = output1a.OptimizationResult.Iterations;
    text1 = sprintf(testFun);
    %text = sprintf('\n ODETols: %.10f, \n OptiTols: %.8f, \n beta: %0.3f \n Iterations: %d,\n Forward Error: %.12f,\n Optimization Error: %.12f',...
                  % [Tols1; Tols2; beta; Iters; rhoErrFW; rhoErrOpt]);
    text = sprintf('\n n: %0.3f \n N: %0.3f \n beta: %0.3f \n Iterations: %d,\n Forward Error: %.12f,\n Optimization Error: %.30f',...
                   [n; N1; beta; Iters; rhoErrFW; rhoErrOpt]);
    texti = [text1, text];
    iStr = num2str(i);
    jStr = num2str(j);
    kStr = num2str(k);
    ti = strcat("Test",iStr,jStr,kStr);
    ttext.(ti) = texti;
end
end
    output = ttext;
    
end