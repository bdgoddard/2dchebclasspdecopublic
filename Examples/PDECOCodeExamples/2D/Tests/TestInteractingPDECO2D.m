function output = TestInteractingPDECO2D()
% Interacting 2D problems with Neumann Flow control
% note the below configurations are a bit low in terms of number of points
% at below configurations we have the following results:
% 1. Iters = 674, JFW = 0.0334, JOpt = 0.0020
% 2. Iters = 646, JFW = 0.0130, JOpt = 0.0007
% 3. Iters = 665, JFW = 0.0329, JOpt = 0.0014
  
    Tols2 = 10^-4; 
    Tols1 = 10^-8; 

    betaVec = [10^-3, 10^-1, 10, 10^3];
    gammaVec = [-1, 0, 1];
    n = 10;
    N1 = 20;
    N2 = 20;

    testFunVec{1} = 'AD_Flow_NeumannInt52D';
    testFunVec{2} = 'AD_Flow_NeumannInt62D'; 
    testFunVec{3} = 'AD_Flow_NeumannInt82D';

    BCStrVec{1} = 'ComputeNeumannBCs';
    BCStrVec{2} = 'ComputeNeumannBCs';
    BCStrVec{3} = 'ComputeNeumannBCs';
    
    PDERHSStrVec{1} = 'AD_Flowf';
    PDERHSStrVec{2} = 'AD_Flowf';
    PDERHSStrVec{3} = 'AD_Flowf';

for i = 1:3
    BCFunStr = BCStrVec{i};
    testFun = testFunVec{i}
    PDERHSStr = PDERHSStrVec{i};
for j = 1:1
    beta = betaVec(j)
for k = 1:1
    gamma = gammaVec(k)
    Phys_Area = struct('shape','Box','y1Min',-1,'y1Max',1,'N',[N1,N2], 'y2Min',-1,'y2Max',1);
    Plot_Area = struct('N1',100,'N2',100','y1Min', -1, 'y2Min', -1, 'y1Max', 1, 'y2Max', 1);
    Time_Area = struct ('t0', 0, 'TMax',1, 'n', n);
    Tols = struct('AbsTol', Tols1,'RelTol', Tols1);
    V2Num = struct('alpha',1,'V2','ComputeGaussian2D');
    ProbSpecs = struct('BCFunStr', BCFunStr, 'PDERHSStr', PDERHSStr, 'ComputeNormStr', 'ComputeRelL2LinfNorm2D');
    DataInput = struct('testFun', testFun,'DataRecompFW', false,'DataRecompConv', false); % and also rhoIC, pIC,...
    Params = struct('beta', beta,'gamma',gamma,'D0',1,'other',0);
    
    OptTols = struct('FunTol', Tols2,'OptiTol', Tols2,'StepTol', Tols2,'ConsTol', Tols2);
    OptSolver = struct('SolverFlag', 'FixPt', 'AdaSolverStr', [], 'lambda', 0.01, 'OptTols', OptTols);
    OptDataInput = struct('DataRecompOpt', false);%('OptirhoIG', 'OptipIG','rhoTarget')
         
    optsPhysOpt = struct('OptSolver',OptSolver, 'OptDataInput', OptDataInput);
    optsPhysFW = struct('ProbSpecs', ProbSpecs, 'DataInput', DataInput, 'Params', Params, 'V2Num',V2Num);  
  
    
    optsPhys = struct('optsPhysFW', optsPhysFW, 'optsPhysOpt', optsPhysOpt, 'V2Num',V2Num);
    optsNum = struct('PhysArea', Phys_Area, 'PlotArea',Plot_Area,'TimeArea',Time_Area, 'V2Num',V2Num, 'Tols', Tols);
    
    opts = struct('optsPhys', optsPhys, 'optsNum', optsNum);
    output1a = PDECO_2D(opts);
    output1a.getExactSolution;
    output1a.ForwardDynamics;
    output1a.OptimizationDynamics;
    output1a.ComputeErrors;
    rhoErrFW = output1a.Errors.ForwardError.JFW2D;
    rhoErrOpt = output1a.Errors.OptimizationError.JOpti2D;
    Iters = output1a.OptimizationResult.Iterations;
    text1 = sprintf(testFun);
    %text = sprintf('\n ODETols: %.10f, \n OptiTols: %.8f, \n beta: %0.3f \n Iterations: %d,\n Forward Error: %.12f,\n Optimization Error: %.12f',...
                  % [Tols1; Tols2; beta; Iters; rhoErrFW; rhoErrOpt]);
    text = sprintf('\n gamma: %0.1f \n beta: %0.3f \n Iterations: %d,\n JFW: %.12f,\n JOpt: %.30f',...
                   [gamma; beta; Iters; rhoErrFW; rhoErrOpt]);
    texti = [text1, text];
    iStr = num2str(i);
    jStr = num2str(j);
    kStr = num2str(k);
    ti = strcat("Test",iStr,jStr,kStr);
    ttext.(ti) = texti;
end
end
    output = ttext;
end