classdef (Abstract) FiniteDifference < Interval
    
    %**********************************************
    %************   Constructor   *****************
    %**********************************************
    methods 
        function this = FiniteDifference(N)
             this@Interval(N);             
             this.Pts.x = FDPointsWeights(N); 
        end      
    end
        
    
    methods (Access = public)
        
        function Int =  ComputeIntegrationVector(this)
            [~,wInt] = FDPointsWeights(this.N);
            [~,J] = PhysSpace(this,this.Pts.x);
                        
            Int = wInt.*(J.');
            this.Int = Int;
        end 
        
        function Diff = ComputeDifferentiationMatrix(this)
        
            CompDiff  = FDDiff(this.N);   
            Diff = PhysicalDerivatives_1D(@this.PhysSpace,this.Pts.x,CompDiff);            
            this.Diff = Diff;
        
        end
        
        function Interp = ComputeInterpolationMatrix(this,interp,saveBool)
            
            InterPol = FDInterpolation(this.Pts.x,interp);
            Interp = struct('InterPol',InterPol,...
                            'Nplot',length(interp),...
                            'N',this.N,...
                            'pts',PhysSpace(this,interp));

            if((nargin >= 3) && saveBool)
                this.Interp = Interp;
            end  
        end      
        
        function Ind = ComputeIndices(this)
            Ind = GetIndicesInterval(this);
            this.Ind = Ind;
        end       
        
        function M_conv = ComputeConvolutionMatrix(this,f,saveBool)

            %error('Finite difference convolution not implemented yet');
            N  = this.N; 
            Pts = this.Pts;
            Int = this.Int;  % 1 x N

            % find size of function matrix by making a dummy version
            fPTemp = f(Pts.y-Pts.y);
            fDim   = size(fPTemp);
            nElts  = prod(fDim(2:end)); % first dimension stores values
            
            IntT = Int.';  % N x 1
            
            IntT = IntT(:,ones(1,nElts)); % N x nElts
            IntT = reshape(IntT,fDim);    % size(f)
            
            M_conv = zeros([N,N,fDim(2:end)]);
            
            Mmask = repmat({':'},[1,fDim]);
            
            for i=1:N 
                fP          = f(Pts.y(i)-Pts.y);  
                Mmask{1} = i;
                M_conv(Mmask{:}) = IntT.*fP;
            end
            M_conv(isnan(M_conv)) = 0;
            
            if((nargin == 3) && islogical(saveBool) && saveBool)
                this.Conv = M_conv;
            end
                           

        end
    end
    
end