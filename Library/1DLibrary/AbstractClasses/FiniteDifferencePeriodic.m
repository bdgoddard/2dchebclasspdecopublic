classdef (Abstract) FiniteDifferencePeriodic < Interval
    
    %**********************************************
    %************   Constructor   *****************
    %**********************************************
    methods 
        function this = FiniteDifferencePeriodic(N)
            
            if(mod(N,2) == 1)              
                msgID = 'FiniteDifferencePeriodicClass:Constructor';
                msgtext = 'Number of points, N, must be even.';
                ME = MException(msgID,msgtext,N); 
                throwAsCaller(ME);
            end
            
            this@Interval(N);
            this.Pts.x = (0:N-1)'/N; % computational domain
            %this.Pts.y = (2*pi)*this.Pts.x; % physical domain
            
        end      
    end
    
    methods (Access = public)
        
        function Diff = ComputeDifferentiationMatrix(this)        
            CompDiff = FDPDiff(this.N);             
            Diff = PhysicalDerivatives_1D(@this.PhysSpace,this.Pts.x,CompDiff);   
            
            this.Diff = Diff;              
        end 
        
        function Int =  ComputeIntegrationVector(this)
            [~,wInt] = FDPInt(this.N);
            [~,J] = PhysSpace(this,this.Pts.x);
            
            J(J==inf)  = 0;  
            J(J==-inf)  = 0;  
            J(isnan(J))= 0;  
            
            Int = wInt.*(J.');
            this.Int = Int;
        end
        
        function Interp = ComputeInterpolationMatrix(this,interp,saveBool)                       
            
            InterPol = FDPInterpolation(this.Pts.x,interp);               
            Interp = struct('InterPol',InterPol,...
                            'Nplot',length(interp),...
                            'N',this.N,...
                            'pts',this.PhysSpace(interp),...
                            'xPlot',interp,...
                            'yPlot',this.PhysSpace(interp));

            if((nargin >= 3) && saveBool)
                this.Interp = Interp;
            end  
            
        end  
                
        function Ind    = ComputeIndices(this)
            Ind = GetIndicesFourier(this);
            this.Ind = Ind;
        end
        
        function M_conv = ComputeConvolutionMatrix(this,f,saveBool)
            %M_conv = [];
            
            N  = this.N; 
            y = this.Pts.y;
            Int = this.Int;
            
            M_conv = zeros(N);

            for i=1:N 
                M_conv(i,:)  = Int.*f(GetDistance(this,y(i)-y)).';
            end
            
            this.Conv = M_conv;
            
            if((nargin == 3) && islogical(saveBool) && saveBool)
                this.Conv = M_conv;
            end
            
        end
        
        function d  = GetDistance(this,y)
            d = y;
            mask = ( abs(d) > this.L/2 );
            d(mask) = d(mask) - sign(d(mask))*this.L;
            %d = mod(y,this.L);
        end

        
        
    end
    
end