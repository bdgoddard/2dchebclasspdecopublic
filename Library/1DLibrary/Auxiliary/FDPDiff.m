function Diff = FDPDiff(N)
    
    h=1/(N); % on interval [0,1]
    
    e = ones(N,1);
    D = sparse(1:N,[2:N 1], 2*e/3, N,N)...
        - sparse(1:N,[3:N 1 2], e/12, N,N);
    D = (D-D')/h;
    
    DD = sparse(1:N,[2:N 1], 4*e/3, N,N)...
        - sparse(1:N,[3:N 1 2], e/12, N,N);
    DD = (DD+DD');
    DD = DD + sparse(1:N,[(1:N)],-5*e/2,N,N); 
    DD =DD/(h^2);
        
    Diff.Dx = sparse(D);
    Diff.DDx = sparse(DD);
    %Diff.DDx = sparse(D^2);
    
end