function [x,Int] = FDPInt( N )
    x    =  FDPSeq(N);
    Int  =  ones(size(x))/N;
    Int  =  Int';
end