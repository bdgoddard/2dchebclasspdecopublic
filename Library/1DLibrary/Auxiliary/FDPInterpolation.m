function Interpol = FDPInterpolation(x,xPlot)

nPlot = length(xPlot);
N = length(x);

Interpol = zeros(nPlot,N);

for iPlot = 1:nPlot
    
    [isElement,pos] = ismember(x,xPlot(iPlot));
   
    
    if(any(isElement))
        
        pos = find(pos,1,'first');
        Interpol(iPlot,pos) = 1;
        
    else
        
        xPrevPos = find(x-xPlot(iPlot)<0,1,'last');
        xNextPos = find(x-xPlot(iPlot)>0,1,'first');
         
        if isempty(xNextPos)
            %xNextPos = find(x-xPlot(iPlot)<0,1,'first');
            xNextPos = 1;
            xPrev = x(xPrevPos);
            xNext = 1;
        
            
            alpha = (xPlot(iPlot) - xPrev)/(xNext - xPrev);
        elseif isempty(xPrevPos)
            xPrevPos = find(x-xPlot(iPlot)>0,1,'last');
            xPrev = x(xPrevPos);
            xNext = x(xNextPos);
        
            
            alpha = (xPlot(iPlot) - xPrev)/(xNext - xPrev);
        else
        
            xPrev = x(xPrevPos);
            xNext = x(xNextPos);
        
            
            alpha = (xPlot(iPlot) - xPrev)/(xNext - xPrev);
        end
        
        Interpol(iPlot,xPrevPos) = 1 - alpha;
        Interpol(iPlot,xNextPos) = alpha;
        
    end
    
end



end