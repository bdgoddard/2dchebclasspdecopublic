function x = FDPSeq( N )
x = (0:N-1)'/(N);
end