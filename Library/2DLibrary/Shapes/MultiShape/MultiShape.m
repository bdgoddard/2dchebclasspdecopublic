classdef MultiShape < handle
    
    properties (Access = public)                

        Shapes
        
        Pts
        Diff
        Ind
        Int
        Conv
        
        Interp
        
        Intersections
        nShapes
        M

        y1Mask
        y2Mask
        
    end
   
    methods
        
        function this = MultiShape(shapes)

            nShapes = length(shapes);
            this.nShapes = nShapes;

            for iShape = 1:nShapes
                shapeFn = str2func(shapes(iShape).shape);
                this.Shapes(iShape).Shape = shapeFn(shapes(iShape).geom);
                this.Shapes(iShape).Geom = shapes(iShape).geom;
            end
            
            GetPoints(this);
            GetShapeMasks(this);

            ComputeDifferentiationMatrices(this);
            ComputeIndices(this);
            ComputeIntegrationVectors(this);
            ComputeInterpolationMatrices(this);
        end
        
        
        function GetPoints(this)
            ptsFields = {'x1_kv','x2_kv','y1_kv','y2_kv'};
            for iField = 1:length(ptsFields)
                allPts = [];
                for iShape = 1:this.nShapes
                    shapePts = this.Shapes(iShape).Shape.Pts.(ptsFields{iField});
                    allPts = [allPts; shapePts]; %#ok
                end
                this.Pts.(ptsFields{iField}) = allPts;
            end
                
            this.Pts.y1Min = min(min(this.Pts.y1_kv));
            this.Pts.y1Max = max(max(this.Pts.y1_kv));
            this.Pts.y2Min = min(min(this.Pts.y2_kv));
            this.Pts.y2Max = max(max(this.Pts.y2_kv));

            
        end
        
        function GetShapeMasks(this)
            
            N1 = zeros(this.nShapes,1);
            N2 = zeros(this.nShapes,1);
            
            for iShape = 1:this.nShapes
                N1(iShape) = this.Shapes(iShape).Shape.Pts.N1;
                N2(iShape) = this.Shapes(iShape).Shape.Pts.N2;
            end
            
            Mi = N1.*N2;
            this.M = sum(Mi);
            
            ptsCount = 0;
            ptsCount2 = 0;
            
            for iShape = 1:this.nShapes
                mask = false(this.M,1);
                mask(ptsCount + 1 : ptsCount + Mi(iShape)) = true;
                this.Shapes(iShape).PtsMask = mask;
                ptsCount = ptsCount + Mi(iShape);
                
                mask2 = false(2*this.M,1);
                mask2(ptsCount2 + 1 : ptsCount2 + 2*Mi(iShape)) = true;
                this.Shapes(iShape).PtsMask2 = mask2;
                ptsCount2 = ptsCount2 + 2*Mi(iShape);
            end
            
            % Compute vector masks for e.g. grad as it looks like
            % [Shape1.Dy1; Shape1.Dy2; Shape2.Dy1; Shape2.Dy2; ...
            % ShapeN.Dy1; ShapeN.Dy2]            
            ptsCount = 0;
            mask1 = false(2*this.M,1);
            mask2 = false(2*this.M,1);

            for iShape = 1:this.nShapes
                mask1(ptsCount + 1 : ptsCount + Mi(iShape)) = true;
                ptsCount = ptsCount + Mi(iShape);
                mask2(ptsCount + 1 : ptsCount + Mi(iShape)) = true;
                ptsCount = ptsCount + Mi(iShape);
                
            end

            this.y1Mask = mask1;
            this.y2Mask = mask2;

        end
        
        
        function ComputeDifferentiationMatrices(this)

            for iShape = 1:this.nShapes
                this.Shapes(iShape).Shape.ComputeDifferentiationMatrix;
            end
            
            diffFields = {'Dy1','Dy2','DDy1','DDy2','Dy1Dy2','Lap','grad','div'};
            
            for iField = 1:length(diffFields)
                allD = [];
                for iShape = 1:this.nShapes
                    shapeD = this.Shapes(iShape).Shape.Diff.(diffFields{iField});
                    allD = blkdiag(allD, shapeD);
                end
                this.Diff.(diffFields{iField}) = allD;
            end
                            
        end
        
        function ComputeInterpolationMatrices(this,interpGrid)
            
            if(nargin<2)
                interpGrid = (-1:0.01:1)';
            end
                 
            InterPol = [];
            pts1 = [];
            pts2 = [];
            
            for iShape = 1:this.nShapes
                IPi = this.Shapes(iShape).Shape.ComputeInterpolationMatrix(interpGrid,interpGrid,true,true); 
                InterPoli = IPi.InterPol;
                pts1i = IPi.pts1;
                pts2i = IPi.pts2;
                
                InterPol = blkdiag(InterPol,InterPoli);
                pts1 = [pts1;pts1i]; %#ok
                pts2 = [pts2;pts2i]; %#ok
            end
            
            this.Interp.InterPol = InterPol;
            this.Interp.pts1 = pts1;
            this.Interp.pts2 = pts2;
            
        end
        
        function IPU = ComputeInterpolationMatricesUniform(this,dy)
            
            if(~isfield(this.Pts,'Uniform'))
                if(nargin<2)
                    this.ComputeUniformPts;
                else
                    this.ComputeUniformPts(dy);
                end
            end
    
            
            IPU = [];
            
            for iShape = 1:this.nShapes
                if(nargin<2)
                    IPUi = this.Shapes(iShape).Shape.InterpolationPlotUniform();
                else
                    IPUi = this.Shapes(iShape).Shape.InterpolationPlotUniform(dy);
                end
                
                IPU = blkdiag(IPU,IPUi);
                
            end
            
            this.Interp.InterPolUniform = IPU;
            
        end
            
            

        function ComputeIndices(this)
            
            for iShape = 1:this.nShapes
                this.Shapes(iShape).Shape.ComputeIndices;
            end
                                                
            % Compute intersections of left, right, top and bottom for each
            % of the shapes
            
            lrtbFields = {'left','right','top','bottom'};            
            nFields = length(lrtbFields);
            
            for iShape = 1:this.nShapes
                                
                iInd = this.Shapes(iShape).Shape.Ind;
                
                y1i = this.Shapes(iShape).Shape.Pts.y1_kv;
                y2i = this.Shapes(iShape).Shape.Pts.y2_kv;
                
                for jShape = 1:this.nShapes
                    
                    if(iShape ~= jShape)

                        maskij = false(this.M,1);

                        jInd = this.Shapes(jShape).Shape.Ind;

                        y1j = this.Shapes(jShape).Shape.Pts.y1_kv;
                        y2j = this.Shapes(jShape).Shape.Pts.y2_kv;

                        for iField = 1:nFields

                            mask1 = iInd.(lrtbFields{iField});
                            y11 = y1i(mask1);
                            y21 = y2i(mask1);

                            for jField = 1:nFields

                                mask2 = jInd.(lrtbFields{jField});
                                y12 = y1j(mask2);
                                y22 = y2j(mask2);
                                                                
                                [matches,flip] = this.CheckIntersection(y11,y21,y12,y22);

                                if(matches)
                                    
                                    maskij(this.Shapes(iShape).PtsMask) = mask1;

                                    this.Intersections(iShape,jShape).PtsMask = maskij;

                                    this.Intersections(iShape,jShape).Side = lrtbFields{iField};

                                    this.Intersections(iShape,jShape).Flip = flip;
                                    
                                    cornersij = false(this.M,1);
                                    cornersij(this.Shapes(iShape).PtsMask) = iInd.corners;

                                    this.Intersections(iShape,jShape).Corners = cornersij;

                                end

                            end
                                                        
                        end
                        
                    else % iShape = jShape
                    
                        this.Intersections(iShape,jShape).PtsMask = false(this.M,1);
                        this.Intersections(iShape,jShape).Corners = false(this.M,1);
                        this.Intersections(iShape,jShape).Side = [];
                    end
 
                end
                
            end
            
            % Find boundaries of MultiShape by taking the union of all of
            % the corresponding boundaries
            
            boundaries = [];

            for iShape = 1:this.nShapes

                % Start with all edges being on the boundary of the
                % MultiShape
                boundariesTemp = lrtbFields;
                for jShape = 1:this.nShapes

                    % Find edges that aren't intersections with other
                    % shapes
                    intersectSide = this.Intersections(iShape,jShape).Side;
                    notIntersect = ~strcmp(intersectSide,boundariesTemp);

                    % Keep only these edges
                    boundariesTemp = boundariesTemp(notIntersect);

                end

                % Construct contribution to the boundary from
                % Shape(iShape)
                boundaries = cat(2,boundaries,{boundariesTemp});

            end
            
            % Loop through left, right, top and bottom and combine these
            % edges that come from each of Shape(iShape) into the
            % correseponding edge for the MultiShape
            for iField = 1:nFields
               
                boundary = false(this.M,1);
                
                % the current edge
                field = lrtbFields{iField};
                
                for iShape = 1:this.nShapes
                    
                    % If the edge of Shape(iShape) is in the boundary then
                    % add that to the correseponding boundary of MultiShape
                    if(any(strcmp(field,boundaries{iShape})))
                        
                        % need to ensure that the edge mask goes into the
                        % corresponding part of the MultiShape pts by using
                        % the PtsMask for Shapes(iShape)
                        boundary(this.Shapes(iShape).PtsMask) = this.Shapes(iShape).Shape.Ind.(field);
                        
                    end
                    
                end
                
                % Save mask for MultiShape
                this.Ind.(field) = boundary;
            end
            
            % Compute entire boundary
            this.Ind.bound = this.Ind.left | this.Ind.right | this.Ind.top | this.Ind.bottom;

            
            % Easy indexing of subshape boundaries
            boundFields = {'left','right','top','bottom','bound'};
            normalFields = {'normalLeft','normalRight','normalTop','normalBottom','normal'};

            nFields = length(boundFields);
            
            this.Ind.Shape = struct;
            for iField = 1:nFields
               
                % the current edge
                field = boundFields{iField};
                
                for iShape = 1:this.nShapes
                    % need to ensure that the edge mask goes into the
                    % corresponding part of the MultiShape pts by using
                    % the PtsMask for Shapes(iShape)
                    edge = false(this.M,1);
                    edge(this.Shapes(iShape).PtsMask) = this.Shapes(iShape).Shape.Ind.(field);
                    this.Ind.Shape(iShape).(field) = edge;
                end
                                
            end
            
            nFields = length(normalFields);
            for iField = 1:nFields
               
                % the current edge
                normalField = normalFields{iField};
                boundField = boundFields{iField};
                
                for iShape = 1:this.nShapes
                    % need to ensure that the normal mask goes into the
                    % corresponding part of the MultiShape pts by using
                    % the PtsMask2 for Shapes(iShape)
                    normal = zeros(this.M,2*this.M);
                    maski1 = this.Ind.Shape(iShape).(boundField);
                    maski2 = this.Shapes(iShape).PtsMask2;                    
                    normal(maski1,maski2) = this.Shapes(iShape).Shape.Ind.(normalField);
                    normal(~maski1,:) = [];                        
                    this.Ind.Shape(iShape).(normalField) = sparse(normal);
                end
                                
            end
                                    
            % Construct intersection mask
            intersections = false(this.M,1);
            for iShape = 1:this.nShapes
                for jShape = 1:this.nShapes
                    intersections(this.Intersections(iShape,jShape).PtsMask) = true;
                end
            end
            this.Ind.intersections = intersections;
                            
            % Construct normals for the MultiShape
            nFields = length(boundFields);
            
            for iField = 1:nFields

                allNormals = [];

                for iShape = 1:this.nShapes

                   N1i = this.Shapes(iShape).Shape.Pts.N1;
                   N2i = this.Shapes(iShape).Shape.Pts.N2;
                   Mi = N1i*N2i;

                   bound = this.Shapes(iShape).Shape.Ind.(boundFields{iField}); 

                   normal = this.Shapes(iShape).Shape.Ind.(normalFields{iField}); 

                   temp = zeros(Mi,2*Mi);
                   temp(bound,:) = normal;

                   allNormals = blkdiag(allNormals,temp);

                end

                allNormals(~this.Ind.(boundFields{iField}),:) = []; %#ok
                this.Ind.(normalFields{iField}) = sparse(allNormals);
            
            end
           
            % Construct normals for intersections
            for iField = 1:nFields
                
                for iShape = 1:this.nShapes
                    for jShape = 1:this.nShapes
                        
                        if(iShape ~= jShape)
                        
                            sidei = this.Intersections(iShape,jShape).Side;
                            iPos = find(strcmp(sidei,boundFields));

                            if(~isempty(iPos))

                                temp = zeros(this.M,2*this.M);

                                maski1 = this.Intersections(iShape,jShape).PtsMask;
                                maski2 = this.Shapes(iShape).PtsMask2;
                                normali = this.Shapes(iShape).Shape.Ind.(normalFields{iPos});

                                temp(maski1,maski2) = normali;
                                temp(~maski1,:) = [];                        
                                this.Intersections(iShape,jShape).Normal = sparse(temp);
                            end

                        end
                        
                    end
                end
            end
            
        end
        
        function dydt = ApplyIntersectionBCs(this,dydt,data,BCs)
        
            for iShape = 1:this.nShapes
                for jShape = iShape+1:this.nShapes

                    if(~isempty(BCs(iShape,jShape).function))
                        BCfn = str2func(BCs(iShape,jShape).function);
                        [BC,mask] = BCfn(data,this.Intersections,iShape,jShape);
                        dydt(mask) = BC(mask);
                    end                
                end
            end            
            
        end
        
        function ComputeIntegrationVectors(this)
            
            this.Int = [];
            
            for iShape = 1:this.nShapes
                Inti = this.Shapes(iShape).Shape.ComputeIntegrationVector;
                this.Int = [this.Int, Inti];
            end
        end
        
        function v = MakeVector(this,v1,v2)
            v = zeros(2*length(v1),1);
            v(this.y1Mask) = v1;
            v(this.y2Mask) = v2;
        end
        
        function [v1,v2] = SplitVector(this,v)
            v1 = v(this.y1Mask);
            v2 = v(this.y2Mask);
        end
        
        function M_conv = ComputeConvolutionMatrix(this,f,saveBool)
            
            if(nargin(f)==1)
                useDistance = true;
            else
                useDistance = false;
            end

            y1 = this.Pts.y1_kv;
            y2 = this.Pts.y2_kv;
            
            if(useDistance)
                fPTemp = f(GetDistance(this,y1,y2));
            else
                fPTemp = f(y1,y2);
            end
            
            fDim = size(fPTemp);
            
            nElts = prod(fDim(2:end));
            
            IntT = (this.Int).';  % M x 1
            
            IntT = IntT(:,ones(1,nElts)); % M x nElts
            IntT = reshape(IntT,fDim);    % size(f)
            
            M_conv = zeros([this.M,this.M,fDim(2:end)]);
            
            Mmask = repmat({':'},[1,fDim]);
            
            for i=1:this.M 
                if(useDistance)
                    fP          = f(GetDistance(this,y1(i) - y1,y2(i) - y2));
                else
                    fP          = f(y1(i) - y1,y2(i) - y2);
                end
                Mmask{1} = i;
                M_conv(Mmask{:}) = IntT.*fP;
            end
            M_conv(isnan(M_conv)) = 0;
            
            if((nargin >= 3) && islogical(saveBool) && saveBool)
                this.Conv = M_conv;
            end
            
        end                

        function d = GetDistance(this,y1,y2) %#ok
            d = sqrt( y1.^2 + y2.^2 );
        end
        
        %------------------------------------------------------------------
        % Plotting
        %------------------------------------------------------------------
        
        function PlotGrid(this)
            for iShape = 1:this.nShapes
                thisShape = this.Shapes(iShape).Shape;
                thisShape.PlotGrid;
                hold on
            end 
        end
    
        function PlotBound(this,boundList)
            if(nargin==1)
                boundList = {'bound'};
            end
            
            colourList = {'r','b','m','c'};
            
            y1 = this.Pts.y1_kv;
            y2 = this.Pts.y2_kv;
            
            nFields = length(boundList);
            
            for iField = 1:nFields
                mask = this.Ind.(boundList{iField});
                scatter(y1(mask),y2(mask),'o','MarkerEdgeColor',colourList{iField},'MarkerFaceColor',colourList{iField});
                hold on
            end
            legend(boundList);
        end
        
        function PlotIntersections(this,plotIJ,opts)
            
            y1 = this.Pts.y1_kv;
            y2 = this.Pts.y2_kv;
            
            if(nargin<2 || isempty(plotIJ))
                plotIJ = true(this.nShapes);
            end
            
            if(nargin<3)
                opts = [];
            end
            
            if(isfield(opts,'colour'))
                colour = opts.colour;
            else
                colour = 'b';
            end

            if(isfield(opts,'linewidth'))
                linewidth = opts.linewidth;
            else
                linewidth = 2;
            end

            if(isfield(opts,'plotPoints'))
                plotPoints = opts.plotPoints;
            else
                plotPoints = false;
            end
            
            for iShape = 1:this.nShapes
                for jShape = iShape+1:this.nShapes
                    if(plotIJ(iShape,jShape))
                        PtsMask = this.Intersections(iShape,jShape).PtsMask;
                        if(plotPoints)
                            scatter(y1(PtsMask),y2(PtsMask),'x',colour);
                            hold on
                        end
                        plot(y1(PtsMask),y2(PtsMask),'Color',colour,'Linewidth',linewidth);
                    end
                end
            end
            
            hold off
              
            this.SetAxes;
                       
        end
            
        function PlotNormalsShapes(this)
            
            for iShape = 1:this.nShapes
                y1 = this.Shapes(iShape).Shape.Pts.y1_kv;
                y2 = this.Shapes(iShape).Shape.Pts.y2_kv;
                
                dir1 = [ones(size(y1)); zeros(size(y2))];
                dir2 = [zeros(size(y1)); ones(size(y2))];
                
                normal = this.Shapes(iShape).Shape.Ind.normal;
                bound = this.Shapes(iShape).Shape.Ind.bound;
                
                v1 = normal * dir1;
                v2 = normal * dir2;
                
                vnorm = sqrt(v1.^2 + v2.^2);
                v1 = v1./vnorm;
                v2 = v2./vnorm;
                
                y1 = y1(bound);
                y2 = y2(bound);
                
                quiver(y1, y2, v1, v2, 'AutoScale','off');
                
                hold on
                
            end
       
            axis equal
            
        end

        
        function PlotNormals(this,boundList)
            
            boundListFull = {'left','right','top','bottom','bound'};
            normalListFull = {'normalLeft','normalRight','normalTop', ...
                                'normalBottom','normal'};
            
            if(nargin==1)
                boundList = {'bound'};
            end
            
            colourList = {'r','b','m','c','k'};

            y1 = this.Pts.y1_kv;
            y2 = this.Pts.y2_kv;
            
            dir1 = zeros(2*this.M,1);
            dir2 = zeros(2*this.M,1);
            
            dir1(this.y1Mask) = 1;
            dir2(this.y2Mask) = 1;
            
            nFields = length(boundList);
            
            for iField = 1:nFields

                iPos = find(strcmp(boundList{iField},boundListFull));
                
                normal = this.Ind.(normalListFull{iPos});
                bound = this.Ind.(boundListFull{iPos});

                v1 = normal * dir1;
                v2 = normal * dir2;

                vnorm = sqrt(v1.^2 + v2.^2);
                v1 = v1./vnorm;
                v2 = v2./vnorm;

                y1Plot = y1(bound);
                y2Plot = y2(bound);         

                quiver(y1Plot, y2Plot, v1, v2, 'AutoScale','off','Color',colourList{iPos});                
                
                hold on
            end

            axis equal
            
        end
     
        function PlotNormalsIntersections(this)
            
            y1 = this.Pts.y1_kv;
            y2 = this.Pts.y2_kv;
            
            dir1 = zeros(2*this.M,1);
            dir2 = zeros(2*this.M,1);
            
            dir1(this.y1Mask) = 1;
            dir2(this.y2Mask) = 1;
            
            for iShape = 1:this.nShapes
                for jShape = 1:this.nShapes
               
                    if(any(this.Intersections(iShape,jShape).PtsMask))
                        normal = this.Intersections(iShape,jShape).Normal;
                        bound = this.Intersections(iShape,jShape).PtsMask;

                        v1 = normal * dir1;
                        v2 = normal * dir2;

                        vnorm = sqrt(v1.^2 + v2.^2);
                        v1 = v1./vnorm;
                        v2 = v2./vnorm;
                        
                        y1Plot = y1(bound);
                        y2Plot = y2(bound);         

                        quiver(y1Plot, y2Plot, v1, v2, 'AutoScale','off');

                        hold on
                    end
                    
                end
                
            end
            
            axis equal
            
        end
        
        function Plot(this,V,opts,optsDetail)

            if(nargin<4)
                optsDetail = [];
            end
            if(nargin<3)
                opts = {};
            end
            
            optsDetail.sigma = false;
            optsDetail.edgecolor = 'none';
            
            for iShape = 1:this.nShapes
                Vi = V(this.Shapes(iShape).PtsMask);
                this.Shapes(iShape).Shape.plot(Vi,opts,optsDetail);
                hold on
            end
            
            this.SetAxes;
            
        end
        
        function PlotVectorField(this,v,opts)

            if(nargin<3)
                opts = [];
            end

            if(isfield(opts,'colour'))
                colour = opts.colour;
            else
                colour = 'b';
            end
            
            if(isfield(opts,'lineWidth'))
                lineWidth = opts.lineWidth;
            else
                lineWidth = 2;
            end
            
            if(~isfield(this.Interp,'InterPolUniform'))
                if(isfield(opts,'dy'))
                    IPU = this.ComputeInterpolationMatricesUniform(opts.dy);
                else
                    IPU = this.ComputeInterpolationMatricesUniform;
                end
            else
                IPU = this.Interp.InterPolUniform;
            end

            
            PtsU = this.Pts.Uniform;
            
            [v1,v2] = this.SplitVector(v);
     
            v1U = IPU*v1;
            v2U = IPU*v2;
            quiver(PtsU.y1_kv,PtsU.y2_kv,v1U,v2U,'color',colour,'linewidth',lineWidth);
            
            this.SetAxes;
            
        end

        function SetAxes(this,ha)
            
            if (nargin < 2)
                ha = gca;
            end
            
            xMin = this.Pts.y1Min;
            xMax = this.Pts.y1Max;
            yMin = this.Pts.y2Min;
            yMax = this.Pts.y2Max;
            
            xlim([xMin, xMax]);
            ylim([yMin, yMax]);
            
            pbaspect(ha,[(xMax-xMin) (yMax-yMin) 1/2*min((xMax-xMin),(yMax-yMin))]);
        end

        function UniformPts = ComputeUniformPts(this,dy)
            
            y1 = [];
            y2 = [];
            
            for iShape = 1:this.nShapes
                if(nargin<2)
                    Ptsi = this.Shapes(iShape).Shape.ComputeUniformPts;
                else
                    Ptsi = this.Shapes(iShape).Shape.ComputeUniformPts(dy);
                end
                
                y1 = [y1; Ptsi.y1_kv]; 
                y2 = [y2; Ptsi.y2_kv]; 
            end
                
            UniformPts.y1_kv = y1;
            UniformPts.y2_kv = y2;
            
            this.Pts.Uniform = UniformPts;
            
        end
        
        function matches = myisequal(this,y1,y2)
            
            matches = 0;
            
            if(size(y1) == size(y2))
                d = abs(y1-y2);
                if max(d)< 10^(-14)
                    matches = 1;
                end
            end
        end
        
        function [matches,flip] = CheckIntersection(this,y11,y12,y21,y22)
            
            matches = false;
            flip = false;
            
            y1 = [y11 y12];
            y2 = [y21 y22];
            
            matches1 = this.myisequal(y1,y2);
            if(matches1)
                matches = true;
            else
                matches2 = this.myisequal(y1,flipud(y2));
                if(matches2)
                    matches = true;
                    flip = true;
                end
            end
        end
        
    end % end methods
    
end