classdef (Abstract) SpectralSpectralFourier < Volume
    %SPECTRALSPECTRALFOURIER 
    
    methods
        function this = SpectralSpectralFourier(N1,N2,N3)
            this@Volume(N1,N2,N3);
            
            this.Pts.x1 = ClenCurtFlip(N1-1);
            this.Pts.x2 = ClenCurtFlip(N2-1);
            this.Pts.x3 = FourierSeq(N3)/(2*pi);            
        end               
    end
    
    methods (Access = protected)
        function [I1,I2,I3] = computeInterpolationMatrices(this,interp1,interp2,interp3)
            %%% Interp1, Interp2, Interp3 must be the computational grids:
            %%% [-1,1],[-1,1],[0,1]; 
            I1 = barychebevalMatrix(this.Pts.x1,interp1);
            I2 = barychebevalMatrix(this.Pts.x2,interp2);
            I3 = fftInterpMatrix(this.Pts.x3,interp3);
        end        
    end
        
    methods (Access = public) 
        function [Int,Trace] = ComputeIntegrationVector(this)            
            [~,dy1] = this.PhysSpace1(this.Pts.x1);
            [~,dy2] = this.PhysSpace2(this.Pts.x2);   
            [~,dy3] = this.PhysSpace3(this.Pts.x3);
            
            [~,wInt1] = ClenCurtFlip(this.N1-1);
            [~,wInt2] = ClenCurtFlip(this.N2-1);            
            [~,wInt3] = FourierInt(this.N3); 
            
            dy1(dy1 == inf) = 0;
            dy2(dy2 == inf) = 0;
            
            Int1 = dy1'.*wInt1;
            Int2 = dy2'.*wInt2;
            Int3 = dy3'.*wInt3;
                        
            Int      = kron(kron(Int1,Int2),Int3);
            this.Int = Int;
            
            Trace.Int1 = Int1;
            Trace.Int2 = Int2;
            Trace.Int12 = kron(Int1,Int2);
            Trace.Int3 = Int3;
            
            ones11 = ones(this.N1,1);
            ones21 = ones(this.N2,1);
            
            Trace.x1_kv  = kron(this.Pts.x1,ones21);
            Trace.x2_kv  = kron(ones11,this.Pts.x2);
            Trace.y1_kv  = PhysSpace1(this,Trace.x1_kv);
            Trace.y2_kv  = PhysSpace2(this,Trace.x2_kv);
                        
            Trace.N1 = this.N1;
            Trace.N2 = this.N2;
            Trace.N3 = this.N3;

            this.Trace = Trace;
            
        end        
        function Diff   = ComputeDifferentiationMatrix(this)               
            if(nargin == 1)
                pts = this.Pts;                
            end
                        
            x1 = pts.x1;
            x2 = pts.x2;
            x3 = pts.x3;
            
            Diff1 = barychebdiff(x1,3);   
            Diff2 = barychebdiff(x2,3);   
            Diff3 = FourierDiff(x3,3);

            Sel = {'Dy1' ;'Dy2' ; 'Dy3'; 'DDy1'; 'DDy2'; 'DDy3' ; 
                  'Dy1Dy2'; 'Dy1Dy3'; 'Dy2Dy3';
                  'Lap' ;'grad' ;'div';...
                  'gradDiv'; 'LapVec'}; 

            Diff = PhysicalDerivatives3(this,pts,Sel,Diff1,Diff2,Diff3,2);
            this.Diff = Diff;
        end        
        function [Interp,Interp1,Interp2,Interp3] = ComputeInterpolationMatrix(this,interp1,interp2,interp3,fullInterpGrid,saveBool)            
            
            [Interp1,Interp2,Interp3] = computeInterpolationMatrices(this,interp1,interp2,interp3);
            
            % kron of the resultant interpolation matrices
            InterPol = kron(kron(Interp1,Interp2),Interp3);
            
            % interpolation after trace over Fourier
            InterPol12 = kron(Interp1,Interp2);
            InterPol3  = Interp3;
            y1 = this.PhysSpace1(interp1);
            y2 = this.PhysSpace2(interp2);
            y3 = this.PhysSpace3(interp3);
            
            Interp = struct('InterPol',InterPol,...
                            'InterPol12',InterPol12,...
                            'InterPol3',InterPol3,...
                            'Nplot1',length(interp1),...
                            'Nplot2',length(interp2),...
                            'Nplot3',length(interp3),...
                            'x1',interp1,'x2',interp2,'x3',interp3,...
                            'y1',y1,'y2',y2,'y3',y3,...
                            'N1',this.N1,'N2',this.N2,'N3',this.N3);
                                                
            if((nargin >= 5) && fullInterpGrid)
                
                ones1 = ones(size(interp1));
                ones2 = ones(size(interp2));
                ones3 = ones(size(interp3));
                
                Interp.x1plot = kron(kron(interp1,ones2),ones3);
                Interp.x2plot = kron(kron(ones1,interp2),ones3);
                Interp.x3plot = kron(kron(ones1,ones2),interp3);
                
                [Interp.y1plot,Interp.y2plot,Interp.y3plot] = PhysSpace(this,Interp.x1plot,Interp.x2plot,Interp.x3plot);
            end
            if((nargin >= 6) && saveBool)
                this.Interp = Interp;
            end                        
        end                
        
        function [y1_kv,y2_kv,y3_kv,J] = PhysSpace(this,x1,x2,x3)
            [y1_kv,dy1] = PhysSpace1(this,x1);
            [y2_kv,dy2] = PhysSpace2(this,x2);
            [y3_kv,dy3] = PhysSpace3(this,x3);
            
            if(nargout > 2)
                n        = length(x1);
                J        = zeros(n,3,3);                
                J(:,1,1) = dy1;
                J(:,2,2) = dy2;
                J(:,3,3) = dy3;
            end                        
        end        
        function [x1,x2,x3] = CompSpace(this,y1,y2,y3)
            x1 = CompSpace1(this,y1);
            x2 = CompSpace2(this,y2);
            x3 = CompSpace3(this,y3);
        end  
        
        function M_conv = ComputeConvolutionMatrix(this,f,saveBool)
            %%% Not implemented yet
            M_conv = [];
        end        
        function Ind    = ComputeIndices(this)
            Ind      = GetIndicesBoxFourier(this);
            this.Ind = Ind;
        end        
         
    end
    
    methods (Access = public)
        function [y,dy,dx,ddx,dddx,ddddx] = PhysSpace3(this,x)
               [y,dy,dx,ddx,dddx,ddddx] = LinearMap01(x,0,2*pi);
        end        
        function xf = CompSpace3(this,y)
            xf  = y/(2*pi);
        end           
        
        function PlotSpatialPlane(this,func)
            this.PlotSlice(3,0,func);
            xlabel('x');
            ylabel('y');
            view(H,[0,90]);
        end
        
        function PlotThetaPlane(this,func)
            this.PlotSlice(1,0,func);
            xlabel('$x$','Interpreter','Latex');
            zlabel('$\theta$','Interpreter','Latex');
            view(H,[90,0]);
        end
        
    end

    
    
    %**********************************************
    %************   Mapping functions *************
    %**********************************************
    
    % Phys/CompSpace3 are hard-coded to Fourier
    methods (Abstract = true,Access = public)         
         [y,dy,dx,ddx,dddx,ddddx] = PhysSpace1(x);
         [y,dy,dx,ddx,dddx,ddddx] = PhysSpace2(x);
         x = CompSpace1(y);
         x = CompSpace2(y);
    end  
        
end


