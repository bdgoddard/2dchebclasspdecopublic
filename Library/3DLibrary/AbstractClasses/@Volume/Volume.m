classdef (Abstract) Volume < handle
	
    methods 
        function this = Volume(N1,N2,N3)
            this.NVec = [N1,N2,N3];
            this.M    = N1*N2*N3;
        end
    end
    
    properties (Access = public)
        Pts
        Diff
        Int
        Trace
        Ind
        Interp
        InterpFlux = []
        Conv
        Origin = [0,0,0]
        
        BoundaryPaths 
        %vector with [Path_right; PathUpper ; PathLeft ; PathBottom]
        
        NVec       
        M        
        polar = 'undefined';        
    end
            
    methods (Abstract = true, Access = public)         
         [y1_kv,y2_kv,y3_kv,J]      = PhysSpace(x1,x2,x3);         
         [x1,x2,x3]                 = CompSpace(y1,y2,y3);
         
         Ind    = ComputeIndices(this);
         Diff   = ComputeDifferentiationMatrix(this);
         Interp = ComputeInterpolationMatrix(this,interp1,interp2,interp3,fullInterpGrid,saveBool);           
         Int    = ComputeIntegrationVector(this);
         M_conv = ComputeConvolutionMatrix(this,f,saveBool);
    end 
    
    
    methods (Access = public)
        % Getters: just to be consistent with previous notation
        function n1 = N1(this)
            n1 = this.NVec(1);
        end
        function n2 = N2(this)
            n2 = this.NVec(2);
        end 
        function n3 = N3(this)
            n3 = this.NVec(3);
        end
                
        function this = InitializationPts(this)
            ones11 = ones(this.N1,1);
            ones21 = ones(this.N2,1);
            ones31 = ones(this.N3,1);
            
            this.Pts.x1_kv  = kron(kron(this.Pts.x1,ones21),ones31);
            this.Pts.x2_kv  = kron(kron(ones11,this.Pts.x2),ones31);
            this.Pts.x3_kv  = kron(kron(ones11,ones21),this.Pts.x3);
            
            [this.Pts.y1_kv,this.Pts.y2_kv,this.Pts.y3_kv] = ...
                PhysSpace(this,this.Pts.x1_kv,this.Pts.x2_kv,this.Pts.x3_kv);
            
            this.Pts.y1 = this.PhysSpace1(this.Pts.x1);
            this.Pts.y2 = this.PhysSpace2(this.Pts.x2);
            this.Pts.y3 = this.PhysSpace3(this.Pts.x3);

            this.Pts.N1 = this.N1;
            this.Pts.N2 = this.N2;
            this.Pts.N3 = this.N3;
        end
        function d  = GetDistance(this,pts_y1,pts_y2,pts_y3)             
            if(nargin == 2)
                pts_y3 = pts_y1.y3_kv;
                pts_y2 = pts_y1.y2_kv;
                pts_y1 = pts_y1.y1_kv;
            end
            
            %ptsCart = GetCartPts(this,pts_y1,pts_y2,pts_y3);
            %d       = sqrt(ptsCart.y1_kv.^2 + ptsCart.y2_kv.^2); 
            d       = sqrt(pts_y1.^2 + pts_y2.^2 + pts_y3.^2);
        end
        function [Pts,Diff,Int,Ind] = ComputeAll(this)
            Diff = ComputeDifferentiationMatrix(this);
            Int  = ComputeIntegrationVector(this);                                    
            Pts  = this.Pts;       
            % need to compute indices after integration and points so we
            % can e.g. find theta==0 boundary
            Ind  = ComputeIndices(this);
        end        
        
        %-----------------------------------------------------------------%
        % PLOTS:
        %-----------------------------------------------------------------%
        function PlotGrid(this)
            global PersonalUserOutput
            if(~PersonalUserOutput)
                return;
            end
            
            h = scatter3(this.Pts.y1_kv,this.Pts.y2_kv,this.Pts.y3_kv,'ob');              
            set(h,'MarkerEdgeColor','k','MarkerFaceColor','g'); 
            hold on
            PlotGridMedians(this)
            hold off
        end                           
        function PlotGridCorners(this)
            y1 = this.Pts.y1_kv(this.Ind.corners);
            y2 = this.Pts.y2_kv(this.Ind.corners);
            y3 = this.Pts.y3_kv(this.Ind.corners);
            hs = scatter3(y1,y2,y3,'or');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','r');
        end        
        function PlotGridBound(this)
            y1 = this.Pts.y1_kv(this.Ind.bound);
            y2 = this.Pts.y2_kv(this.Ind.bound);
            y3 = this.Pts.y3_kv(this.Ind.bound);
            hs = scatter3(y1,y2,y3,'o');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','b');
        end        
        function PlotGridTop(this)
            y1 = this.Pts.y1_kv(this.Ind.top);
            y2 = this.Pts.y2_kv(this.Ind.top);
            y3 = this.Pts.y3_kv(this.Ind.top);
            hs = scatter3(y1,y2,y3,'o');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','b');
        end
        function PlotGridBottom(this)
            y1 = this.Pts.y1_kv(this.Ind.bottom);
            y2 = this.Pts.y2_kv(this.Ind.bottom);
            y3 = this.Pts.y3_kv(this.Ind.bottom);
            hs = scatter3(y1,y2,y3,'o');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','b');
        end    
        function PlotGridMedians(this)
            y1 = this.Pts.y1_kv(this.Ind.median1);
            y2 = this.Pts.y2_kv(this.Ind.median1);
            y3 = this.Pts.y3_kv(this.Ind.median1);
            
            hs = scatter3(y1,y2,y3,'o');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','r');
            
            y1 = this.Pts.y1_kv(this.Ind.median2);
            y2 = this.Pts.y2_kv(this.Ind.median2);
            y3 = this.Pts.y3_kv(this.Ind.median2);
            hs = scatter3(y1,y2,y3,'o');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','r');
            
            y1 = this.Pts.y1_kv(this.Ind.median3);
            y2 = this.Pts.y2_kv(this.Ind.median3);
            y3 = this.Pts.y3_kv(this.Ind.median3);
            hs = scatter3(y1,y2,y3,'o');
            set(hs,'MarkerEdgeColor','k','MarkerFaceColor','b');
            hold off
        end
        function PlotNormalVectors(this)
            % Not yet implemented!
        end
        
        function [hx] = PlotSlice(this,k,y_0,funcVals,opts)
            if(nargin < 5)
                opts = [];
            end
            colormap(jet)

            x1     = this.Pts.x1;
            x2     = this.Pts.x2;
            x3     = this.Pts.x3;
                       
            x1int  = linspace(min(x1),max(x2),this.N1*2)';
            x2int  = linspace(min(x2),max(x2),this.N2*2)';
            x3int  = linspace(min(x3),max(x3),this.N3*2)';
                        
            I = this.ComputeInterpolationMatrix(x1int,x2int,x3int,true,false);

            y1p    = I.y1plot;
            y2p    = I.y2plot;
            y3p    = I.y3plot;
            
            N1p    = I.Nplot1;
            N2p    = I.Nplot2;
            N3p    = I.Nplot3;

%             [X1,X2,X3] = meshgrid(x1int,x2int,x3int);
            
            y1plot     = permute(reshape(y1p,N3p,N2p,N1p),[2,3,1]);
            y2plot     = permute(reshape(y2p,N3p,N2p,N1p),[2,3,1]);
            y3plot     = permute(reshape(y3p,N3p,N2p,N1p),[2,3,1]);
            
            Zint    = I.InterPol*funcVals;
            Zplot   = permute(reshape(Zint,N3p,N2p,N1p),[2,3,1]);
            
            Slice       = struct('y1',[],'y2',[],'y3',[]);
            SliceNames = fieldnames(Slice); 
            Slice.(SliceNames{k}) = y_0;
                                             
            hx = slice(y1plot,y2plot,y3plot,Zplot,Slice.y1,Slice.y2,Slice.y3);
            hx.FaceColor = 'interp';
            hx.EdgeColor = 'none';
            set(gcf,'color','w');
            
        end

        
        %%%%%%%%%%%%%%%%%
        %%% Jonna Adds
        %%%%%%%%%%%%%%%%%
        
        function InterpolationPlotFlux(this,PlotArea)   
            if(isfield(PlotArea,'NFlux'))
                N = PlotArea.NFlux;
            else
                N = 30;
                cprintf('m','!!PlotArea.NFlux is not set!!\n 30 is used as default in function "InterpolationPlotFlux" of "Shape" class.\n');
            end
            
            dy1     = (PlotArea.y1Max-PlotArea.y1Min);
            dy2     = (PlotArea.y2Max-PlotArea.y2Min);
            dy3     = (PlotArea.y3Max-PlotArea.y3Min);
                        
            deltaY  = (max([dy1,dy2,dy3])/N);            
            
            PlotArea.N1 = round(dy1/deltaY);            
            PlotArea.N2 = round(dy2/deltaY);
            PlotArea.N3 = round(dy3/deltaY);
            
            this.InterpFlux = InterpolationPlotCart(this,PlotArea);
        end
        
        function IP = InterpolationPlotCart(this,PlotArea,saveBool)
            
            y1C = PlotArea.y1Min + (PlotArea.y1Max-PlotArea.y1Min)*(0:(PlotArea.N1-1))'/(PlotArea.N1-1);
            y2C = PlotArea.y2Min + (PlotArea.y2Max-PlotArea.y2Min)*(0:(PlotArea.N2-1))'/(PlotArea.N2-1);
            y3C = PlotArea.y3Min + (PlotArea.y3Max-PlotArea.y3Min)*(0:(PlotArea.N3-1))'/(PlotArea.N3-1);
            
            
            ones11 = ones(this.N1,1);
            ones21 = ones(this.N2,1);
            ones31 = ones(this.N3,1);
            
            y1C_kv  = kron(kron(y1C,ones21),ones31);
            y2C_kv  = kron(kron(ones11,y2C),ones31);
            y3C_kv  = kron(kron(ones11,ones21),y3C);
            
            
            %pts         = GetInvCartPts(this,y1C_kv,y2C_kv);
            
            IP.InterPol = InterpolationMatrix_Pointwise(this,y1C_kv,y2C_kv,y3C_kv);
            IP.pts1     = y1C_kv;
            IP.pts2     = y2C_kv;
            IP.pts3     = y3C_kv;
            IP.Nplot1   = PlotArea.N1;
            IP.Nplot2   = PlotArea.N2;
            IP.Nplot3   = PlotArea.N3;
            
            if((nargin == 3) && saveBool)
                this.Interp = IP;
            end
            
        end            
        
        function IP = InterpolationMatrix_Pointwise(this,y1P,y2P,y3P)            
            IP = zeros(length(y1P),this.N1*this.N2*this.N3);            
            for i =1:length(y1P)
                [x1,x2,x3] = CompSpace(this,y1P(i),y2P(i),y3P(i));
                h       = ComputeInterpolationMatrix(this,x1,x2,x3);
                IP(i,:) = h.InterPol;
            end                                
        end    
        
        function [y1_s,y2_s,y3_s,fl_y1,fl_y2,fl_y3] = plotFlux(this,flux,maskAdd,fl_norm,lw,c,scale_factor,vField3,y2CartShift)
                        
            flux = flux(:);
            
            nSpecies=size(flux,3);

            if(nSpecies == 1)
                nCol = 1;
            else
                nCol = 2;
            end
            
            nRows  = ceil(nSpecies/nCol);
            
            if(isempty(this.InterpFlux))
                pts = this.Pts;         
                IP  = diag(length(this.Pts.y1_kv));
                mask    = ((this.Pts.y1_kv <= max(this.Interp.y1plot)) & ...
                           (this.Pts.y1_kv >= min(this.Interp.y1plot)) & ...
                           (this.Pts.y2_kv <= max(this.Interp.y2plot)) & ...
                           (this.Pts.y2_kv >= min(this.Interp.y2plot)) & ...
                           (this.Pts.y3_kv <= max(this.Interp.y3plot)) & ...
                           (this.Pts.y3_kv >= min(this.Interp.y3plot)));                                                           
                if(exist('maskAdd','var') && ~isempty(maskAdd))                           
                    mask   = (maskAdd & mask);                
                end
            else
                pts.y1_kv = this.InterpFlux.pts1;
                pts.y2_kv = this.InterpFlux.pts2;
                pts.y3_kv = this.InterpFlux.pts3;
                IP        = this.InterpFlux.InterPol;
                mask      = true(size(this.InterpFlux.pts1));
            end
            
            if(sum(mask)==0)
                return;
            end                     
            
            %yCart  = GetCartPts(this,pts.y1_kv,pts.y2_kv,pts.y3_kv);
            y1_s   = pts.y1_kv(mask);     
            y2_s   = pts.y2_kv(mask);
            y3_s   = pts.y3_kv(mask);
            
            if(nargin >= 9)
                y2_s = y2_s + y2CartShift;
            end
            
            xl    = [(min(y1_s)-0.5) (max(y1_s)+0.5)];
            yl    = [(min(y2_s)-0.5) (max(y2_s)+0.5)]; 
            zl    = [(min(y3_s)-0.5) (max(y3_s)+0.5)]; 

            if(exist('fl_norm','var') && ~isempty(fl_norm))                
                y1_s  = [NaN;y1_s];
                y2_s  = [NaN;y2_s];        
                y3_s  = [NaN;y3_s];  
            end                                    
            
            for iSpecies=1:nSpecies

                fl_y1     = IP*flux(1:this.N1*this.N2*this.N3,iSpecies);
                fl_y2     = IP*flux(this.N1*this.N2*this.N3+1:2*this.N1*this.N2*this.N3,iSpecies);       
                fl_y3     = IP*flux(2*this.N1*this.N2*this.N3+1:end,iSpecies); 
                
                fl_y1 = fl_y1(mask,:);  fl_y2 = fl_y2(mask,:); fl_y3 = fl_y3(mask,:);
                
                %exclude close to zero fluxes                
                abs_fl    = sqrt(fl_y1.^2 + fl_y2.^2 + fl_y3.^2);
                mark_zero = (abs_fl > max(0.01,0.2*max(abs_fl)));
                fl_y1 = fl_y1(mark_zero,:);  
                fl_y2 = fl_y2(mark_zero,:);
                fl_y3 = fl_y3(mark_zero,:);

            if(exist('fl_norm','var') && ~isempty(fl_norm))
                O = ones(1,size(fl_y1,3));
                fl_y1 = [fl_norm*O;fl_y1];
                fl_y2 = [0*O;fl_y2];    
                fl_y3 = [0*O;fl_y3]; 

                y1_ss  = y1_s([true;mark_zero]);            
                y2_ss  = y2_s([true;mark_zero]);
                y3_ss  = y3_s([true;mark_zero]);
            else
                    y1_ss  = y1_s([mark_zero]);            
                    y2_ss  = y2_s([mark_zero]);
                    y3_ss  = y3_s([mark_zero]);
            end                        
            if (exist('vField3','var')) && (vField3 == true) 
                vfield3(y1_ss,y2_ss,y3_ss,scale_factor*fl_y1,scale_factor*fl_y2,scale_factor*fl_y3);
                view(3)
                camlight
            else
                if(exist('lw','var') && exist('c','var') && ~isempty(lw)) 
                    if(nSpecies == 1)
                        if (exist('scale_factor','var'))
                            quiver3(y1_ss,y2_ss,y3_ss,fl_y1,fl_y2,fl_y3,scale_factor,...
                                'LineWidth',lw,'Color',c);
                        else
                            quiver3(y1_ss,y2_ss,y3_ss,fl_y1,fl_y2,fl_y3,'LineWidth',lw,'Color',c);
                        end
                    else
                        quiver3(y1_ss,y2_ss,y3_ss,fl_y1,fl_y2,fl_y3,'LineWidth',lw,'Color',c{iSpecies});
                    end
                else
                    quiver3(y1_ss,y2_ss,y3_ss,fl_y1,fl_y2,fl_y3);
                end
            end
                hold on;
                
%                 if(nargin < 7 || ~strcmp(plain,'plain'))
%                     xlim(xl); ylim(yl); zlim(zl);                   
                    h = xlabel('$y_1$');  set(h,'Interpreter','Latex'); set(h,'fontsize',25);
                    h = ylabel('$y_2$');  set(h,'Interpreter','Latex'); set(h,'fontsize',25);
                    h = ylabel('$y_3$');  set(h,'Interpreter','Latex'); set(h,'fontsize',25);
%                     pbaspect([(xl(2)-xl(1)) (yl(2)-yl(1)) 1/2*min((xl(2)-xl(1)),(yl(2)-yl(1)))]);                
%                     if(nSpecies > 1)
%                         title(['Species ' num2str(iSpecies)]);
%                     end
                    set(gca,'fontsize',20);                        
                    set(gca,'linewidth',1.5);                                                             
%                 end
            end                            
            hold off;   
                                            
        end  
        %%%

        
        function [y1M,y2M,y3M,fl_y1,fl_y2,fl_y3,startMask1,startMask2,startMask3] = plotStreamlines(this,flux,startMask1,startMask2,startMask3,opts)
            mask    = ((this.Pts.y1_kv <= max(this.Interp.y1plot)) & ...
                               (this.Pts.y1_kv >= min(this.Interp.y1plot)) & ...
                               (this.Pts.y2_kv <= max(this.Interp.y2plot)) & ...
                               (this.Pts.y2_kv >= min(this.Interp.y2plot)) & ...
                               (this.Pts.y3_kv <= max(this.Interp.y3plot)) & ...
                               (this.Pts.y3_kv >= min(this.Interp.y3plot)));                                                           
            %PtsYCart  = GetCartPts(this,this.Pts.y1_kv,this.Pts.y2_kv);
            fl_y1  = this.Interp.InterPol*flux(1:this.N1*this.N2*this.N3);
            fl_y1  = reshape(fl_y1,this.Interp.Nplot3,this.Interp.Nplot2,this.Interp.Nplot1);
            
            fl_y2  = this.Interp.InterPol*flux(this.N1*this.N2*this.N3+1:2*this.N1*this.N2*this.N3);
            fl_y2  = reshape(fl_y2,this.Interp.Nplot3,this.Interp.Nplot2,this.Interp.Nplot1);
            
            fl_y3  = this.Interp.InterPol*flux(2*this.N1*this.N2*this.N3+1:end);
            fl_y3  = reshape(fl_y3,this.Interp.Nplot3,this.Interp.Nplot2,this.Interp.Nplot1);
            
            %yCart  = GetCartPts(this,this.Interp.pts1,this.Interp.pts2);                        
%             y1M    = reshape(this.Interp.y1plot,this.Interp.Nplot3,this.Interp.Nplot2,this.Interp.Nplot1);
%             y2M    = reshape(this.Interp.y2plot,this.Interp.Nplot3,this.Interp.Nplot2,this.Interp.Nplot1);      
%             y3M    = reshape(this.Interp.y3plot,this.Interp.Nplot3,this.Interp.Nplot2,this.Interp.Nplot1);  
            y1M = -1 + (2)*(0:(20))'/(20);
            y2M = -1 + (2)*(0:(20))'/(20);
            y3M = -1 + (2)*(0:(20))'/(20);
            [y1M,y2M,y3M] = meshgrid(y1M,y2M,y3M);
            %%%%% Hardcoded and needs change
            
%             if((nargin >= 4) && isfield(opts,'y2CartShift'))
%                 y2M = y2M + opts.y2CartShift;
%             end
            
            if((nargin == 4) || isempty(startMask1))
                startMask1 = this.Ind.bound;           
                startMask2 = this.Ind.bound;
                startMask3 = this.Ind.bound;
            elseif((nargin == 5) || isempty(startMask2))
                startMask2 = startMask1;
            end
            
            if(islogical(startMask1))
                startMask1 = this.Pts.y1_kv(mask&startMask1);
                startMask2 = this.Pts.y2_kv(mask&startMask2);
                startMask3 = this.Pts.y3_kv(mask&startMask3);
            end

            h = streamline(y1M,y2M,y3M,fl_y1,fl_y2,fl_y3,startMask1,startMask2,startMask3);                                                   
            if((nargin >= 7) && isfield(opts,'color'))
                set(h,'color',opts.color);
            end
            if((nargin >= 7) && isfield(opts,'linewidth'))
                set(h,'linewidth',opts.linewidth);
            end
        end    
        
    end
    
end

