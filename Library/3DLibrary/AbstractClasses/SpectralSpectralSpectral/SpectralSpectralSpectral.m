classdef (Abstract) SpectralSpectralSpectral < Volume
    %SPECTRALSPECTRALSPECTRAL 
    
    methods
        function this = SpectralSpectralSpectral(N1,N2,N3)            
            this@Volume(N1,N2,N3);
            
            this.Pts.x1 = ClenCurtFlip(N1-1);
            this.Pts.x2 = ClenCurtFlip(N2-1);
            this.Pts.x3 = ClenCurtFlip(N3-1);            
            
        end               
    end
    
    methods (Access = protected)
        function [I1,I2,I3] = computeInterpolationMatrices(this,interp1,interp2,interp3)
            %%% Interp1, Interp2, Interp3 must be the computational grids:
            %%% [-1,1],[-1,1],[-1,1]; 
            I1 = barychebevalMatrix(this.Pts.x1,interp1);
            I2 = barychebevalMatrix(this.Pts.x2,interp2);
            I3 = barychebevalMatrix(this.Pts.x3,interp3);
        end        
    end
        
    methods (Access = public) 
        function [Int,Trace] = ComputeIntegrationVector(this)            
            [~,dy1] = this.PhysSpace1(this.Pts.x1);
            [~,dy2] = this.PhysSpace2(this.Pts.x2);   
            [~,dy3] = this.PhysSpace3(this.Pts.x3);
            
            [~,wInt1] = ClenCurtFlip(this.N1-1);
            [~,wInt2] = ClenCurtFlip(this.N2-1);
            [~,wInt3] = ClenCurtFlip(this.N3-1);
            
            dy1(dy1 == inf) = 0;
            dy2(dy2 == inf) = 0;
            dy3(dy3 == inf) = 0;
            
            Int1 = dy1'.*wInt1;
            Int2 = dy2'.*wInt2;
            Int3 = dy3'.*wInt3;
                        
            Int      = kron(kron(Int1,Int2),Int3);
            this.Int = Int;
            
            Trace.Int1 = Int1;
            Trace.Int2 = Int2;
            Trace.Int3 = Int3;
                                    
            Trace.N1 = this.N1;
            Trace.N2 = this.N2;
            Trace.N3 = this.N3;

            this.Trace = Trace;
            
        end        
        function Diff   = ComputeDifferentiationMatrix(this)               
            if(nargin == 1)
                pts = this.Pts;                
            end
                        
            x1 = pts.x1;
            x2 = pts.x2;
            x3 = pts.x3;
            
            Diff1 = barychebdiff(x1,3);   
            Diff2 = barychebdiff(x2,3);   
            Diff3 = barychebdiff(x3,3);   

            Sel = {'Dy1' ;'Dy2' ; 'Dy3'; 'DDy1'; 'DDy2'; 'DDy3' ; 
                  'Dy1Dy2'; 'Dy1Dy3'; 'Dy2Dy3';
                  'Lap' ;'grad' ;'div';...
                  'gradDiv'; 'LapVec'}; 

            Diff = PhysicalDerivatives3(this,pts,Sel,Diff1,Diff2,Diff3,2);
            this.Diff = Diff;
        end        
        function [Interp,Interp1,Interp2,Interp3] = ComputeInterpolationMatrix(this,interp1,interp2,interp3,fullInterpGrid,saveBool)            
            
            [Interp1,Interp2,Interp3] = computeInterpolationMatrices(this,interp1,interp2,interp3);
            
            % kron of the resultant interpolation matrices
            InterPol = kron(kron(Interp1,Interp2),Interp3);
            
            y1 = this.PhysSpace1(interp1);
            y2 = this.PhysSpace2(interp2);
            y3 = this.PhysSpace3(interp3);
            
            Interp = struct('InterPol',InterPol,...
                            'Nplot1',length(interp1),...
                            'Nplot2',length(interp2),...
                            'Nplot3',length(interp3),...
                            'x1',interp1,'x2',interp2,'x3',interp3,...
                            'y1',y1,'y2',y2,'y3',y3,...
                            'N1',this.N1,'N2',this.N2,'N3',this.N3);
                                                
            if((nargin >= 5) && fullInterpGrid)
                
                ones1 = ones(size(interp1));
                ones2 = ones(size(interp2));
                ones3 = ones(size(interp3));
                
                Interp.x1plot = kron(kron(interp1,ones2),ones3);
                Interp.x2plot = kron(kron(ones1,interp2),ones3);
                Interp.x3plot = kron(kron(ones1,ones2),interp3);
                
                [Interp.y1plot,Interp.y2plot,Interp.y3plot] = PhysSpace(this,Interp.x1plot,Interp.x2plot,Interp.x3plot);
            end
            if((nargin >= 6) && saveBool)
                this.Interp = Interp;
            end                        
        end                
        
        function [y1_kv,y2_kv,y3_kv,J] = PhysSpace(this,x1,x2,x3)
            [y1_kv,dy1] = PhysSpace1(this,x1);
            [y2_kv,dy2] = PhysSpace2(this,x2);
            [y3_kv,dy3] = PhysSpace3(this,x3);
            
            if(nargout > 2)
                n        = length(x1);
                J        = zeros(n,3,3);                
                J(:,1,1) = dy1;
                J(:,2,2) = dy2;
                J(:,3,3) = dy3;
            end                        
        end        
        function [x1,x2,x3] = CompSpace(this,y1,y2,y3)
            x1 = CompSpace1(this,y1);
            x2 = CompSpace2(this,y2);
            x3 = CompSpace3(this,y3);
        end  
        
        function M_conv = ComputeConvolutionMatrix(this,f,saveBool)
             
            if(nargin(f)==1)
                useDistance = true;
            else
                useDistance = false;
            end

            N1  = this.N1;  N2  = this.N2;  N3  = this.N3;
            Pts = this.Pts;
            Int = this.Int;  % 1 x N1*N2*N3

            if(useDistance)
                fPTemp = f(GetDistance(this,Pts.y1_kv,Pts.y2_kv,Pts.y3_kv));
            else
                fPTemp = f(Pts.y1_kv,Pts.y2_kv,Pts.y3_kv);
            end
            
            fDim = size(fPTemp);
            
            nElts = prod(fDim(2:end));
            
            IntT = Int.';  % N1*N2*N3 x 1
            
            IntT = IntT(:,ones(1,nElts)); % N1*N2*N3 x nElts
            IntT = reshape(IntT,fDim);    % size(f)
            
            M_conv = zeros([N1*N2*N3,N1*N2*N3,fDim(2:end)]);
            
            Mmask = repmat({':'},[1,fDim]);
            
            for i=1:(N1*N2*N3) 
                if(useDistance)
                    fP = f(GetDistance(this,Pts.y1_kv(i) - Pts.y1_kv,...
                        Pts.y2_kv(i) - Pts.y2_kv,Pts.y3_kv(i) - Pts.y3_kv));
                else
                    fP = f(Pts.y1_kv(i) - Pts.y1_kv,...
                        Pts.y2_kv(i) - Pts.y2_kv,Pts.y3_kv(i) - Pts.y3_kv);
                end
                Mmask{1} = i;
                M_conv(Mmask{:}) = IntT.*fP;
            end
            M_conv(isnan(M_conv)) = 0;
            
            if((nargin >= 3) && islogical(saveBool) && saveBool)
                this.Conv = M_conv;
            end
            
        end        
        function Ind    = ComputeIndices(this)
            Ind      = GetIndicesBox3(this);
            this.Ind = Ind;
        end        
         
    end
    
    methods (Access = public)
            
        function PlotZeroPlane(this,iCoord,func)
            this.PlotSlice(iCoord,0,func);
            xlabel('y_1');
            ylabel('y_2');
            zlabel('y_3');
        end
        
        function PlotMidPlane(this,iCoord,func,opts)
            if(nargin < 4)
                opts = [];
            end
            yMid = ( this.(['y' num2str(iCoord) 'Min']) ...
                     + this.(['y' num2str(iCoord) 'Max']) )/2;
            
            this.PlotSlice(iCoord,yMid,func,opts);
                 
            xlabel('y_1');
            ylabel('y_2');
            zlabel('y_3');
        end
            
        function PlotMidPlanes(this,func,opts)
            if(nargin <3)
                opts = [];
            end
            for iCoord = 1:3
                this.PlotMidPlane(iCoord,func,opts);
                if ~isempty(opts)
                    caxis([opts.mi,opts.ma])
                end
                hold on
            end
        end
        

        function Movie(this,f,times,opts,saveOpts)
            
            % determine if saving pdfs
            if(nargin<=4)
                opts = struct;
                savePdf = false;
            else
                savePdf = saveOpts.savePdf;
                pdfDir = saveOpts.pdfDir;
                if(savePdf)
                    if(~exist(pdfDir,'dir'))
                        mkdir(pdfDir);
                    end
                end
            end
                
            % pdf file names
            if(savePdf)
                pdfFileNames = [];
                nDigits=ceil(log10(length(times)));
                nd=['%0' num2str(nDigits) 'd'];
            end
            
            % plotting location
            if(nargin >=5 && isfield(saveOpts,'axes'))
                axes(saveOpts.axes);
                hf = gcf;
            else
                hf = figure('Position',[50,50,800,800]);
            end
            
            % plotting function
            if(isfield(opts,'plotFn'))
                plotFn = str2func(opts.plotFn);
            else
                plotFn = @PlotMidPlanes;
            end
            
            % make plots
            for iTimes=1:length(times)        
                f_t = f(iTimes,:)';
                plotFn(this,f_t,opts);
                if(savePdf)
                    outputFile=[pdfDir num2str(iTimes,nd) '.pdf'];
                    pdfFileNames = cat(2, pdfFileNames, [' ' outputFile]);
                    save2pdf(outputFile,hf,[]);
                else
                    pause(0.1);
                end
            end
            
            % combine into movie
            if(savePdf)
                fprintf(1,'Combining pdf ... ');
                fullPdfFile=[pdfDir  'movie.pdf'];

                gs = getGS();

                gsCmd= [gs ' -dNOPAUSE -sDEVICE=pdfwrite ' ...
                          '-sOUTPUTFILE=' fullPdfFile ' -dBATCH -dQUIET ' pdfFileNames];

                system(gsCmd);
                fprintf(1,'Finished\n');
            end
            
        end
    
    end
        
    %----------------------------------------------------------------------
    
    methods (Abstract = true,Access = public)         
         [y,dy,dx,ddx,dddx,ddddx] = PhysSpace1(x);
         [y,dy,dx,ddx,dddx,ddddx] = PhysSpace2(x);
         [y,dy,dx,ddx,dddx,ddddx] = PhysSpace3(x);
         x = CompSpace1(y);
         x = CompSpace2(y);
         x = CompSpace3(y);
    end      
    
        
end


