function I  = GetIndicesBox3(this)
%************************************************************************
%I  = GetIndicesBox(x1,x2,x3)
%INPUT:
% x1 - grid points in computational space in 1st variable, x1 in [-1 1]
% x2 - grid points in computational space in 2nd variable, x2 in [-1 1]
% x3 - grid points in computational space in 3rd variable, x3 in [-1 1]
%OUTPUT: structure with ...
% - indices of 'left', 'right', 'bottom', 'top', 'front', 'back' boundaries
% - bound = includes left, right, top, bottom, front, and back boundaries
% - normal vectors: normalLeft, normalRight, normalTop, normalBottom,
%   normalFront, normalBack
% - normal = normal vectors for full boundary
% - corners 
% - indices of 'finite', 'infinite' boundaries
% - normal vectors: normalFinite, normalInfinite
%************************************************************************

        x1_kv = this.Pts.x1_kv;
        x2_kv = this.Pts.x2_kv;
        x3_kv = this.Pts.x3_kv;
        
        left    = (x1_kv == min(x1_kv));
        right   = (x1_kv == max(x1_kv)); 
        front   = (x2_kv == min(x2_kv));
        back    = (x2_kv == max(x2_kv)); 
        bottom  = (x3_kv == min(x3_kv));
        top     = (x3_kv == max(x3_kv));
        
        N1med   = floor(this.N1/2);
        N2med   = floor(this.N2/2);
        N3med   = floor(this.N3/2);
        median1 = (x1_kv == this.Pts.x1(N1med));
        median2 = (x2_kv == this.Pts.x2(N2med));
        median3 = (x3_kv == this.Pts.x3(N3med));
        
        bound   = (left | right | front| back | bottom | top);
                                      
        edges = (  (right & top) | (right & bottom) | (right & front) | (right & back) ...
                   | (left & top) | (left & bottom) | (left & front) | (left & back) ...
                   | (front & left) | (front & right) | (front &  bottom) | (front & top) ...
                   | (back & left) | (back & right) | (back &  bottom) | (back & top) ...
                   | (top & left) | (top & right) | (top & front) | (top & back) ...
                   | (bottom & left) | (bottom & right) | (bottom & front) | (bottom & back) );
        
        Z             = zeros(length(x1_kv));

        nx1Left       = Z;
        nx1Right      = Z;

        nx2Front      = Z;
        nx2Back       = Z;
        
        nx3Top        = Z;
        nx3Bottom     = Z;
         
        nx1Left(left,left)     = -speye(sum(left));
        nx1Right(right,right)  = speye(sum(right));

        nx2Front(front,front)  = -speye(sum(front));
        nx2Back(back,back)     = speye(sum(back));
        
        nx3Bottom(bottom,bottom)  = -speye(sum(bottom));
        nx3Top(top,top)           = speye(sum(top));

        %normalise at edges and corners
        nx1 = nx1Left(bound,:) + nx1Right(bound,:);
        nx2 = nx2Front(bound,:) + nx2Back(bound,:);
        nx3 = nx3Bottom(bound,:) + nx3Top(bound,:);
        
        norm = sqrt(nx1.^2 + nx2.^2 + nx3.^2);
        mask = norm>0;
        nx1(mask) = nx1(mask)./norm(mask);
        nx2(mask) = nx2(mask)./norm(mask);        
        nx3(mask) = nx3(mask)./norm(mask);  

        
        I = struct('left',left,'right',right,'front',front','back',back,'bottom',bottom,'top',top,...
            'median1',median1,'median2',median2,'median3',median3,...
            'bound',bound,...
            'normalLeft',  sparse( [nx1Left(left,:)  Z(left,:) Z(left,:)] ),...
            'normalRight', sparse( [nx1Right(right,:)  Z(right,:) Z(right,:)] ),...
            'normalFront',   sparse( [Z(front,:)  nx2Front(front,:) Z(front,:) ] ),...
            'normalBack',sparse( [Z(back,:)  nx2Back(back,:) Z(back,:) ] ),...
            'normalBottom',   sparse( [Z(bottom,:) Z(bottom,:) nx3Bottom(bottom,:)] ),...
            'normalTop',   sparse( [Z(top,:) Z(top,:) nx3Top(top,:)] ),...            
            'normal',sparse( [nx1 nx2 nx3] ),...
            'edges',edges);
        
end
    