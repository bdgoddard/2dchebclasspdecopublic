function I  = GetIndicesBoxFourier(this)
%************************************************************************
%I  = GetIndicesBox(x1,x2,x3)
%INPUT:
% x1 - grid points in computational space in 1st variable, x1 in [-1 1]
% x2 - grid points in computational space in 2nd variable, x2 in [-1 1]
% x3 - grid points in 3rd variable, x3 in [0,2pi)
%OUTPUT: structure with ...
% - indices of 'left', 'right', 'bottom', 'top' boundaries
% - bound = includes left, right, top and bottom boundaries
% - normal vectors: normalLeft,normalRight,normalTop, normalBottom
% - normal = normal vectors for full boundary
% - corners 
% - indices of 'finite', 'infinite' boundaries
% - normal vectors: normalFinite, normalInfinite
%************************************************************************

        x1_kv = this.Pts.x1_kv;
        x2_kv = this.Pts.x2_kv;
        x3_kv = this.Pts.x3_kv;
        
        y1_kv = this.Pts.y1_kv;
        y2_kv = this.Pts.y2_kv;
        y3_kv = this.Pts.y3_kv;
        
        left    = (x1_kv == min(x1_kv));
        right   = (x1_kv == max(x1_kv)); 
        bottom  = (x2_kv == min(x2_kv));
        top     = (x2_kv == max(x2_kv));
        
        N1med   = floor(this.N1/2);
        N2med   = floor(this.N2/2);
        N3med   = floor(this.N3/2);
        median1 = (x1_kv == this.Pts.x1(N1med));
        median2 = (x2_kv == this.Pts.x2(N2med));
        median3 = (x3_kv == this.Pts.x3(N3med));
        
        bound   = (left | right | bottom | top);
        
        leftFinite   = any(isfinite(y1_kv(left)));
        rightFinite  = any(isfinite(y1_kv(right)));
        topFinite    = any(isfinite(y2_kv(top)));
        bottomFinite = any(isfinite(y2_kv(bottom)));                
        
        finite1 = ( (leftFinite & left) | (rightFinite & right) );
        finite2 = ( (topFinite & top) | (bottomFinite & bottom) );        
        finite  = (finite1 | finite2);
               
        infinite = ( (~leftFinite & left) | (~rightFinite & right) ...
                   | (~topFinite & top) | (~bottomFinite & bottom) );
                              
        corners = ((right & top) | (top & left) | (left & bottom) | (bottom & right));
        
        Z             = zeros(length(x1_kv));

        nx1Left       = Z;
        nx1Right      = Z;
        
        nx2Top        = Z;
        nx2Bottom     = Z;
         
        nx1Left(left,left)     = -speye(sum(left));
        nx1Right(right,right)  = speye(sum(right));
        
        nx2Top(top,top)        = speye(sum(top));
        nx2Bottom(bottom,bottom)  = -speye(sum(bottom));
        
        normalFinite = sparse([(leftFinite*nx1Left(finite,:) + rightFinite*nx1Right(finite,:)), ...
                               (topFinite*nx2Top(finite,:) + bottomFinite*nx2Bottom(finite,:)), ...
                               Z(finite,:) ] );
                    
        normalInfinite = sparse( [((~leftFinite)*nx1Left(infinite,:) + (~rightFinite)*nx1Right(infinite,:)), ...
                        ((~topFinite)*nx2Top(infinite,:) + (~bottomFinite)*nx2Bottom(infinite,:)), ...
                        Z(infinite,:) ]);
        
        I = struct('left',left,'right',right,'bottom',bottom,'top',top,...
            'median1',median1,'median2',median2,'median3',median3,...
            'bound',bound,...
            'normalLeft',  sparse( [nx1Left(left,:)  Z(left,:) Z(left,:)] ),...
            'normalRight', sparse( [nx1Right(right,:)  Z(right,:) Z(right,:)] ),...
            'normalTop',   sparse( [Z(top,:)  nx2Top(top,:) Z(top,:) ] ),...
            'normalBottom',sparse( [Z(bottom,:)  nx2Bottom(bottom,:) Z(bottom,:) ] ),...
            'normal',sparse( [(nx1Left(bound,:)+nx1Right(bound,:)) (nx2Top(bound,:) + nx2Bottom(bound,:)) ...
                                Z(bound,:)] ),...
            'corners',corners, ...
            'finite',finite,'infinite',infinite, ...
            'normalFinite',normalFinite,'normalInfinite',normalInfinite);
        
end
    