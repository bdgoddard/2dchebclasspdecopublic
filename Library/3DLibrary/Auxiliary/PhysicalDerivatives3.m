function DiffOut = PhysicalDerivatives3(a_volume,Pts,Sel,CompDiff1,CompDiff2,CompDiff3,order)
                                                %Dx1,Dx2,DDx1,DDx2,DDDx1,DDDx2)
    n1       = length(Pts.x1);
    n2       = length(Pts.x2);
    n3       = length(Pts.x3);
    
    m        = n1*n2*n3;
    
    Diff1 = PhysicalDerivatives_1D(@a_volume.PhysSpace1,Pts.x1,CompDiff1);
    Diff2 = PhysicalDerivatives_1D(@a_volume.PhysSpace2,Pts.x2,CompDiff2);
    Diff3 = PhysicalDerivatives_1D(@a_volume.PhysSpace3,Pts.x3,CompDiff3);
        
    Diff = struct();
    
    % conversion into physical space:     
    %dy1       = sparse(diag(dx1)*Dx1);   
    %dy2       = sparse(diag(dx2)*Dx2); 
    eye1      = eye(n1);
    eye2      = eye(n2);
    eye3      = eye(n3);
    Diff.Dy1  = sparse(kron(kron(Diff1.Dy,eye2),eye3)); 
    Diff.Dy2  = sparse(kron(kron(eye1,Diff2.Dy),eye3));
    Diff.Dy3  = sparse(kron(kron(eye1,eye2),Diff3.Dy));
    
    Diff.grad = sparse([Diff.Dy1;Diff.Dy2;Diff.Dy3]);            
    Diff.div  = sparse([Diff.Dy1 Diff.Dy2 Diff.Dy3]);
    
    if (order == 1)        
        CopyFields();
        return;
    end
    
    %ddy1    = sparse(diag(ddx1)*Dx1 + (diag(dx1.^2))*DDx1);    
    %ddy2    = sparse(diag(ddx2)*Dx2 + (diag(dx2.^2))*DDx2);        
    
    Diff.DDy1     = sparse(kron(kron(Diff1.DDy,eye2),eye3));
    Diff.DDy2     = sparse(kron(kron(eye1,Diff2.DDy),eye3));          
    Diff.DDy3     = sparse(kron(kron(eye1,eye2),Diff3.DDy));
    
    Diff.Dy1Dy2   = sparse(kron(kron(Diff1.Dy,Diff2.Dy),eye3));   
    Diff.Dy1Dy3   = sparse(kron(kron(Diff1.Dy,eye2),Diff3.Dy));   
    Diff.Dy2Dy3   = sparse(kron(kron(eye1,Diff2.Dy),Diff3.Dy));   
    
    Diff.Lap      = sparse(Diff.DDy1 + Diff.DDy2 + Diff.DDy3);    
    
    Diff.gradDiv  = [Diff.DDy1   Diff.Dy1Dy2 Diff.Dy1Dy3; 
                     Diff.Dy1Dy2 Diff.DDy2   Diff.Dy2Dy3;
                     Diff.Dy1Dy3 Diff.Dy2Dy3 Diff.DDy3];
                 
    Zm = zeros(m);
    Diff.LapVec   = [Diff.Lap   Zm          Zm; 
                     Zm         Diff.Lap    Zm;
                     Zm         Zm          Diff.Lap];
       
    if (order == 2)        
        CopyFields(); 
        return;
    end
    
    %dddy1        = diag(dx1.^3)*DDDx1 + 3*diag(dx1.*ddx1)*DDx1 + diag(dddx1)*Dx1;       
    %dddy2        = diag(dx2.^3)*DDDx2 + 3*diag(dx2.*ddx2)*DDx2 + diag(dddx2)*Dx2;       
    
    Diff.DDy1Dy2 = sparse(kron(kron(Diff1.DDy,Diff2.Dy),eye3));    
    Diff.DDy1Dy3 = sparse(kron(kron(Diff1.DDy,eye2),Diff3.Dy));    
    
    Diff.Dy1DDy2 = sparse(kron(kron(Diff1.Dy,Diff2.DDy),eye3));
    Diff.Dy1DDy3 = sparse(kron(kron(Diff1.Dy,eye2),Diff3.DDy));
          
    Diff.DDy2Dy3 = sparse(kron(kron(eye1,Diff2.DDy),Diff3.Dy));    
    Diff.Dy2DDy3 = sparse(kron(kron(eye1,Diff2.Dy),Diff3.DDy));
    
    Diff.DDDy1   = sparse(kron(kron(Diff1.DDDy,eye2),eye3));
    Diff.DDDy2   = sparse(kron(kron(eye1,Diff2.DDDy),eye3));
    Diff.DDDy3   = sparse(kron(kron(eye1,eye2),Diff3.DDDy));
    
    Diff.Dy1Dy2Dy3 = kron(kron(Diff1.Dy,Diff2.Dy),Diff3.Dy);
    
    Diff.gradLap = [Diff.DDDy1   + Diff.Dy1DDy2 + Diff.Dy1DDy3 ; ...
                    Diff.DDy1Dy2 + Diff.DDDy2 + Diff.Dy2DDy3 ;...
                    Diff.Dy1DDy3 + Diff.Dy2DDy3 + Diff.DDDy3];
                
    Diff.LapDiv  = [Diff.DDDy1   + Diff.Dy1DDy2 + Diff.Dy1DDy3, ...
                    Diff.DDy1Dy2 + Diff.DDDy2   + Diff.Dy2DDy3, ...
                    Diff.DDy1Dy3 + Diff.DDy2Dy3 + Diff.DDDy3];
    
    if (order == 3)        
        CopyFields(); 
        return;
    end
    
    if (order >3)
        fprintf('Warning in PhysicalDerivatives3:\n');
        fptintf('### Diff. Matrices with (n>3) will not be computed [not implemented yet].'); 
    end
    
    function CopyFields()        
        DiffOut = struct();
        for i = 1:length(Sel)
            DiffOut.(Sel{i}) = Diff.(Sel{i});
        end                               
    end

end
