classdef Box3 < SpectralSpectralSpectral
    
    properties        
        y1Min = 0
        y2Min = 0
        y3Min = 0
        y1Max,y2Max,y3Max
    end
    
    methods        
        function this = Box3(Geometry)
            this@SpectralSpectralSpectral(Geometry.N(1),Geometry.N(2),Geometry.N(3));
                      
            this.y1Min = Geometry.y1Min;
            this.y1Max = Geometry.y1Max;
            this.y2Min = Geometry.y2Min;
            this.y2Max = Geometry.y2Max;
            this.y3Min = Geometry.y3Min;
            this.y3Max = Geometry.y3Max;            
        
            this.polar = 'cart';           
            InitializationPts(this);            
        end                         
    end
    
    methods (Access = public)
        function [y1,dy1,dx,ddx,dddx,ddddx] = PhysSpace1(this,x1)            
            [y1,dy1,dx,ddx,dddx,ddddx] = LinearMap(x1,this.y1Min,this.y1Max);
        end
        function x1 = CompSpace1(this,y1)
            x1  = InvLinearMap(y1,this.y1Min,this.y1Max);
        end
        function [y2,dy2,dx,ddx,dddx,ddddx] = PhysSpace2(this,x2)                
            [y2,dy2,dx,ddx,dddx,ddddx] = LinearMap(x2,this.y2Min,this.y2Max);
        end
        function x2 = CompSpace2(this,y2)                        
            x2  = InvLinearMap(y2,this.y2Min,this.y2Max);
        end                        
        
        function [y1,dy1,dx,ddx,dddx,ddddx] = PhysSpace3(this,x3)            
            [y1,dy1,dx,ddx,dddx,ddddx] = LinearMap(x3,this.y3Min,this.y3Max);
        end
        function x1 = CompSpace3(this,y3)
            x1  = InvLinearMap(y3,this.y3Min,this.y3Max);
        end
        

        function Ind    = ComputeIndices(this)
            Ind = ComputeIndices@SpectralSpectralSpectral(this);
        end
        
        
    end
end

