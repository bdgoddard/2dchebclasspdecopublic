classdef BoxWithOrientation < SpectralSpectralFourier
    
    properties        
        y1Min = 0
        y2Min = 0
        y1Max,y2Max               
    end
    
    methods        
        function this = BoxWithOrientation(Geometry)
            this@SpectralSpectralFourier(Geometry.N(1),Geometry.N(2),Geometry.N(3));
                      
            this.y1Min = Geometry.y1Min;
            this.y1Max = Geometry.y1Max;
            this.y2Min = Geometry.y2Min;
            this.y2Max = Geometry.y2Max;
        
            this.polar = 'cart';           
            InitializationPts(this);            
        end                         
    end
    
    methods (Access = public)
        function [y1,dy1,dx,ddx,dddx,ddddx] = PhysSpace1(this,x1)            
            [y1,dy1,dx,ddx,dddx,ddddx] = LinearMap(x1,this.y1Min,this.y1Max);
        end
        function x1 = CompSpace1(this,y1)
            x1  = InvLinearMap(y1,this.y1Min,this.y1Max);
        end
        function [y2,dy2,dx,ddx,dddx,ddddx] = PhysSpace2(this,x2)                
            [y2,dy2,dx,ddx,dddx,ddddx] = LinearMap(x2,this.y2Min,this.y2Max);
        end
        function x2 = CompSpace2(this,y2)                        
            x2  = InvLinearMap(y2,this.y2Min,this.y2Max);
        end                        
        
        % add additional indices
        function Ind    = ComputeIndices(this)
            Ind = ComputeIndices@SpectralSpectralFourier(this);
            
            thetaZero = (this.Pts.y3_kv==0);
            boundThetaZero = (thetaZero & Ind.bound);
            
            boundSpace = ComputeSpaceDensity(this,Ind.bound);
            boundSpace = logical(boundSpace);
            
            Ind.boundThetaZero = boundThetaZero;
            Ind.boundSpace = boundSpace;
            
            this.Ind = Ind;
        end
        
        
        function fSpace = ComputeSpaceDensity(this,f)
            
            fTemp = reshape(f,this.N3,(this.N1*this.N2));     
            w3 = this.Trace.Int3;
            fSpace = w3*fTemp/sum(w3); % normalisation   
            fSpace = fSpace(:);
            
        end
        
        function thetaDensity = ComputeThetaDensityInterp(this,f)
            
            interp1 = (-1:0.2:1)'; % uniform grid in space
            interp2 = (-1:0.2:1)';
            interp3 = this.Pts.x3;   % don't change theta points
            fullInterpGrid = false;
            saveBool = false;

            Interp = this.ComputeInterpolationMatrix(interp1,interp2,interp3,fullInterpGrid,saveBool);

            % interpolate onto uniform grid in space
            fInterp = Interp.InterPol*f;
            
            N1 = length(interp1);
            N2 = length(interp1);
            N3 = length(interp3);
    
            % reshape to get theta distribution at each point
            fTemp = reshape(fInterp,N3,(N1*N2));
            
            % integrate over theta for each spatial point
            w3 = this.Trace.Int3;
            fSpace = w3*fTemp/sum(w3);
            
            % replicate over theta
            fSpace = repmat(fSpace,N3,1);
            
            % normalise distribution
            rho = fTemp./fSpace;
            
            % worry about cases where you get large numerical errors
            rho(abs(fSpace)<10^-8) = NaN;
            
            thetaDensity.rho = rho;
            
            % determine grid spacing
            y1 = PhysSpace1(this,interp1);
            y2 = PhysSpace2(this,interp2);
            % and outer radius of annulus
            thetaDensity.r = min( y1(2) - y1(1), y2(2) - y2(1))/4;
            
            % determine gridpoints
            ones1 = ones(size(interp1));
            ones2 = ones(size(interp2));
            thetaDensity.y1_kv = kron(y1,ones2);
            thetaDensity.y2_kv = kron(ones1,y2);

            
        end

        
        function spaceDensity = ComputeSpaceDensityInterp(this,f)
            % integrate out over theta
            fSpace = this.ComputeSpaceDensity(f);
            
            interp1 = (-1:0.02:1)'; % uniform grid in space
            interp2 = (-1:0.02:1)';
            interp3 = 0;            % only one theta point to ensure small grid
            
            fullInterpGrid = false;
            saveBool = false;
            
            % compute interpolation matrix with only one point in theta to
            % minimise the size
            Interp12 = ComputeInterpolationMatrix(this,interp1,interp2,interp3,fullInterpGrid,saveBool);
            
            % interpolate in space
            InterPol = Interp12.InterPol12;
            y1Plot   = Interp12.y1;
            y2Plot   = Interp12.y2;
            
            fSpacePlot = InterPol*fSpace;

            % shape to correct size
            fSpacePlot = reshape(fSpacePlot,Interp12.Nplot1,Interp12.Nplot2);
            
            spaceDensity.rho = fSpacePlot;
            spaceDensity.y1_kv = y1Plot;
            spaceDensity.y2_kv = y2Plot;


        end
        
        
        function [fMin,fMax] = PlotSpaceDistribution(this,f,opts)

            if(nargin<=2)
                m1 = 11;
                m2 = 64;
            else
                m1 = opts.m(1);
                m2 = opts.m(2);
            end
            
            spaceDensity = ComputeSpaceDensityInterp(this,f);
            
            rho = spaceDensity.rho;
            y1Plot     = spaceDensity.y1_kv;
            y2Plot     = spaceDensity.y2_kv;
            
            
            % get bounds on theta distribution if not given
            getBounds = true;
            if(nargin>=3 && isfield(opts,'fMin2'))
                getBounds = false;
            end
                        
            if(getBounds)
                [fMin,fMax] = ComputeSpaceDensityBounds(this,rho);
            else
                fMin = opts.fMin2;
                fMax = opts.fMax2;
            end

            % CData for surface
            C = min(m2,(m2-1)*(rho-fMin)/(fMax-fMin)+1); 
            % Shift so it starts above the annulus colour scale
            C = C + m1;
            
            % do the plot
            h = pcolor(y1Plot,y2Plot,rho);
            set(h,'edgecolor','none');
            
            % set colour map and bar
            set(h,'CData',C);
            
            
            
        end        
        
        function [fMin,fMax] = PlotFourierDistribution(this,f,opts)            
                                    
            % interpolate and normalise theta distribution
            thetaDensity = ComputeThetaDensityInterp(this,f);
            
            rho = thetaDensity.rho;
            y1_kv = thetaDensity.y1_kv;
            y2_kv = thetaDensity.y2_kv;
            r = thetaDensity.r;
            
            % get number of colour levels
            if(nargin>=3 && isfield(opts,'m'))
                m1 = opts.m(1);
            else
                m1 = 11;
            end
            
            % get bounds on theta distribution if not given
            getBounds = true;
            if(nargin>=3 && isfield(opts,'fMin1'))
                getBounds = false;
            end
                        
            if(getBounds)
                [fMin,fMax] = ComputeThetaDensityBounds(this,rho);
            else
                fMin = opts.fMin1;
                fMax = opts.fMax1;
            end
            
            optsA.fMin = fMin;
            optsA.fMax = fMax;
            optsA.r    = r;
            optsA.m    = m1;
            
            % plot theta distribution on an annulus at each point
            plotThetaAnnulus(rho,y1_kv,y2_kv,optsA);

            % fix axes
            axis tight;
            
        end
    
        function PlotSpaceAndTheta(this,f,opts)
            m1 = 11;
            m2 = 64;
            
            m = [m1;m2];
            
            hold off
            
            opts.m = m;
            
            % do plots and get value ranges
            [cMin2,cMax2] = PlotSpaceDistribution(this,f,opts);
            hold on

            [fMin1,fMax1] = PlotFourierDistribution(this,f,opts);
            
            
            % set colour maps and bar
            colormap([gray(m1);jet(m2)])
            colorbar 
            caxis([0 (m1+m2)])
            
            % fix tick labels to be actual values
            hcb=colorbar;
            ticks = get(hcb,'YTick');

            ticks1 = ticks(ticks<=m1);
            ticks2 = ticks(ticks>m1);
            fRange1 = fMax1 - fMin1;
            cRange2 = cMax2 - cMin2;
            
            % not entirely sure about this inverse map
            ticksPhys1 = max(fMin1,(ticks1-1).*fRange1/(m1-1) + fMin1);
            ticksPhys2 = max(cMin2,(ticks2-1).*cRange2/(m2-1) + cMin2);
            
            ticksPhys = [ticksPhys1 ticksPhys2];
            
            set(hcb,'YTickLabel',ticksPhys);
            
        end
        
        function MovieSpaceAndTheta(this,f,times,opts,saveOpts)
            
            % determine if saving pdfs
            if(nargin<=4)
                savePdf = false;
            else
                savePdf = saveOpts.savePdf;
                pdfDir = saveOpts.pdfDir;
                if(savePdf)
                    if(~exist(pdfDir,'dir'))
                        mkdir(pdfDir);
                    end
                end
            end
                
            % pdf file names
            if(savePdf)
                pdfFileNames = [];
                nDigits=ceil(log10(length(times)));
                nd=['%0' num2str(nDigits) 'd'];
            end
            
            % plotting location
            if(nargin >=5 && isfield(saveOpts,'axes'))
                axes(saveOpts.axes);
                hf = gcf;
            else
                hf = figure('Position',[50,50,800,800]);
            end
            
            % make plots
            for iTimes=1:length(times)        
                f_t = f(iTimes,:)';
                PlotSpaceAndTheta(this,f_t,opts);
                if(savePdf)
                    outputFile=[pdfDir num2str(iTimes,nd) '.pdf'];
                    pdfFileNames = cat(2, pdfFileNames, [' ' outputFile]);
                    save2pdf(outputFile,hf,[]);
                else
                    pause(0.1);
                end
            end
            
            % combine into movie
            if(savePdf)
                fprintf(1,'Combining pdf ... ');
                fullPdfFile=[pdfDir  'movie.pdf'];

               switch computer
                    case {'MAC','MACI','MACI64'}			
                        gs= '/usr/local/bin/gs';
                    case {'PCWIN','PCWIN64'}
                        gs= 'gswin32c.exe';
                    otherwise
                        gs= 'gs';
                end

                gsCmd= [gs ' -dNOPAUSE -sDEVICE=pdfwrite ' ...
                          '-sOUTPUTFILE=' fullPdfFile ' -dBATCH -dQUIET ' pdfFileNames];

                system(gsCmd);
                fprintf(1,'Finished\n');
            end
            
        end
        
        function [fMin,fMax] = ComputeThetaDensityBounds(this,f)
            % number of spatial points to plot at
            NPts = size(f,2);

            geomc.R = 1;
            geomc.N = size(f,1);
            circle = Circle(geomc);

            interp = (0:0.01:1)';
            Interp = ComputeInterpolationMatrix(circle,interp);
            

            InterPol = Interp.InterPol;

            fMin = Inf;
            fMax = -Inf;
            
            for iPt = 1:NPts
                fPt = f(:,iPt);
                fPtInterp = InterPol*fPt;
                ptMin = min(min(fPtInterp));
                ptMax = max(max(fPtInterp));
                fMin = min(fMin,ptMin);
                fMax = max(fMax,ptMax);
            end

        end
        
        function [fMin,fMax] = ComputeThetaDensityBoundsTime(this,f)
            
            fMin = Inf;
            fMax = -Inf;
            
            nTimes = size(f,1);
            for iTime = 1:nTimes
                ft = f(iTime,:)';
                thetaDensity = ComputeThetaDensityInterp(this,ft);
                rho = thetaDensity.rho;
                [fMint,fMaxt] = ComputeThetaDensityBounds(this,rho);
                fMin = min(fMin,fMint);
                fMax = max(fMax,fMaxt);
            end
            
            fMin = round(fMin,4);
            fMax = round(fMax,4);
            
        end

        function [fMin,fMax] = ComputeSpaceDensityBoundsTime(this,f)
            
            fMin = Inf;
            fMax = -Inf;
            
            nTimes = size(f,1);
            for iTime = 1:nTimes
                ft = f(iTime,:)';
                spaceDensity = ComputeSpaceDensityInterp(this,ft);
                rho = spaceDensity.rho;
                [fMint,fMaxt] = ComputeSpaceDensityBounds(this,rho);
                fMin = min(fMin,fMint);
                fMax = max(fMax,fMaxt); 
            end
        end

        
        function [fMin,fMax] = ComputeSpaceDensityBounds(this,rho)
            fMin = round(min(rho(:)),4);
            fMax = round(max(rho(:)),4);
        end

        
        
    end
end

