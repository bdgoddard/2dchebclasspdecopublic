function convStruct = ConvMatrices(V2params,SpaceBox)
   
    V2 = str2func(V2params.V2);
    inputshapeParams = isfield(V2params,'shapeParams');
    
    if(inputshapeParams)
        shapeParams = V2params.shapeParams;
        % Computing Convolution Matrix
    
        if(isfield(shapeParams,'yMax'))
            if(isa(SpaceBox,'SpectralLine'))
                Conv = SpaceBox.ComputeConvolutionMatrix_Pointwise(@KernelUniversal,shapeParams); 
            elseif(isa(SpaceBox,'FiniteDifferenceLine'))
                Conv = SpaceBox.ComputeConvolutionMatrix_Pointwise(@KernelUniversal,shapeParams); 
            elseif(isa(SpaceBox,'FourierLine'))
                Conv = SpaceBox.ComputeConvolutionMatrix(@KernelUniversal,shapeParams);
            elseif(isa(SpaceBox,'FiniteDifferencePeriodicLine'))
                Conv = SpaceBox.ComputeConvolutionMatrix(@KernelUniversal,shapeParams);
            
             elseif(isa(SpaceBox,'BoxFourierFourier'))
                Conv = SpaceBox.ComputeConvolutionMatrix_Pointwise(@KernelUniversal2D,shapeParams);
            else
                error('Unknown shape for convolution');
            end
        else
        Conv = SpaceBox.ComputeConvolutionMatrix(@KernelUniversal,shapeParams); 
        
        end
    else
        Conv = SpaceBox.ComputeConvolutionMatrix(@KernelUniversal,true); 
    end
    % Computing Convolution Matrix    
    %Conv = SpaceBox.ComputeConvolutionMatrix(@KernelUniversal,true);  
    % Output Structure with Convolution Matrix
    convStruct.Conv = Conv;
    
%--------------------------------------------------------------------------
% The below determines whether the V2 function takes parameters or not.
% This needs to be done to be able to datastore the convolution
% computations...

	function z = KernelUniversal(r)                 
        if(nargin(V2)==1)
            z = V2(r);   
        else
            z = V2(r,V2params);   
        end
    end

    function z = KernelUniversal2D(r,s)                 
        if(nargin(V2)==1)
            z = V2(r,s);   
        else
            z = V2(r,s,V2params);   
        end
    end

end

% This function allows to compute convolutions and pass parameters to
% the function V2
% 'based on' FexMatrices_Meanfield.m