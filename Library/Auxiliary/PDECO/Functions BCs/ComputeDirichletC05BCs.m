function drhodtbound = ComputeDirichletC05BCs(rho, rhoflux, bound, normal, this)

drhodtbound =  rho(bound) - 0.5;

end