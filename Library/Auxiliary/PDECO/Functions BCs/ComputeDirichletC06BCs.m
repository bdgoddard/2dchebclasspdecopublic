function drhodtbound = ComputeDirichletC06BCs(rho, rhoflux, bound, normal, this)

drhodtbound =  rho(bound) - 0.6;

end