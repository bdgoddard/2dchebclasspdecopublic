function drhodtbound = ComputeDirichletConstantBCs(rho, rhoflux, bound, normal, this)
bc = this.optsPhys.optsPhysFW.Params.bc;
drhodtbound =  rho(bound) - bc;

end