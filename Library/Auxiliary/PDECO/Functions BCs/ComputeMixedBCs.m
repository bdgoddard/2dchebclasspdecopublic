function drhodtbound = ComputeMixedBCs(rho, rhoflux, bound, normal, this)
eps = this.optsPhys.Params.eps;
drhodt1(bound) = rho(bound);
drhodt2(bound) = rhoflux(bound);
drhodtbound = eps*drhodt1(bound) + drhodt2(bound);

end