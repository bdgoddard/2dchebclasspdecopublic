function drhodtbound = ComputeNeumannLRBCs(rho, rhoflux, bound, normal,Ind,u, this)

    drhodtbound = zeros(size(rho(bound)));
    
    nrhoflux = normal*rhoflux;
    %drhodtbound(bound) = nrhoflux;
    drhodtbound(1) =nrhoflux(1) ;
    drhodtbound(end) =nrhoflux(end) - u(end) + rho(end) ;
     
    
end