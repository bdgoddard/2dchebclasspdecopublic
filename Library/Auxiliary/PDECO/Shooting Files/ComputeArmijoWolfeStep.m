function sol= ComputeArmijoWolfeStep(wIn,wOut,SInt, TInt,pdeInput)
% Using the Armijo-Wolfe line search conditions to find the appropriate
% step size
% Inputs
% wIn - initial guess
% wOut - solution of system after using initial guess, wIn
% SInt,TInt, integration matrices in space and time respectively
%pdeInput - inputs for pde solver
%
%Output
% lambda, the appropriate step size
% steps, number of times while loop was executed to find lambda
tic;
disp('Using Armijo-Wolfe rule ...');
alpha =0;
beta = inf;
lambda=0.2;
delta = 0.3;
sigma = 0.5;
found = false;
steps = 0;

wInNew = wIn - alpha*(wIn - wOut);
pdeInput.wIn = wInNew;
data = ComputePDE(pdeInput);
wOutNew = data.wOut;

A_t =  norm(wInNew' - wOutNew')^2 - norm(wIn' - wOut')^2 < -delta*alpha*(norm(wIn' - wOut')^2);
W_t = -1*(wInNew'-wOutNew')*(wIn-wOut) > -sigma*( norm(wIn' - wOut')^2);

% A_t = TInt*(SInt*((wInNew' - wOutNew').^2))' - TInt*(SInt*((wIn' - wOut').^2))' < -delta*alpha*(TInt*(SInt*((wIn'-wOut').^2))');
% W_t = -1*(TInt*(SInt*((wInNew'-wOutNew').*(wIn'-wOut')))') > -sigma*( TInt*(SInt*((wIn'-wOut').^2))');

if A_t && W_t
    found = true;
end
    
 while ~found
     steps = steps+1;
     wInNew = wIn - lambda*(wIn - wOut);
     pdeInput.wIn = wInNew;
     data = ComputePDE(pdeInput);
     wOutNew = data.wOut;
     
     A_t =  norm(wInNew' - wOutNew')^2 - norm(wIn' - wOut')^2 < -delta*lambda*(norm(wIn' - wOut')^2);
     W_t = -1*(wInNew'-wOutNew')*(wIn-wOut) > -sigma*( norm(wIn' - wOut')^2);
     
%      A_t = TInt*(SInt*((wInNew' - wOutNew').^2))' - TInt*(SInt*((wIn' - wOut').^2))' < -delta*lambda*(TInt*(SInt*((wIn'-wOut').^2))');
%     W_t = -1*(TInt*(SInt*((wInNew'-wOutNew').*(wIn'-wOut')))') > -sigma*( TInt*(SInt*((wIn'-wOut').^2))');
%     
     if ~ A_t
         beta = lambda;
     elseif ~ W_t
          alpha = lambda;
     else
         found = true;
         break
     end
     
     if beta <inf
         lambda = 0.5*(alpha + beta);
     else
         lambda = 2* alpha;
     end
     
     if lambda<0.01
     lambda = 0.01;
     break
    end
 end
 toc;
 
 
 sol.awTime = toc;
 sol.lambda = lambda;
 sol.steps = steps;

end