function sol = ComputeFixedPointAlgorithm(this,Diff,Ind,Pts,rhoMask,pMask)
   disp('Solving Optimization problem with Fixed Point Method...');
   if isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FourierLine')==1 || isa(this.IDC, 'FiniteDifferenceLine')==1|| isa(this.IDC, 'FiniteDifferencePeriodicLine')==1
      PDESolver = @ComputeODOptimizationRHS1D; 
      Dy = Diff.Dy;
   elseif isa(this.IDC, 'Box') ==1|| isa(this.IDC, 'BoxFourierFourier')==1
      PDESolver = @ComputeOptimizationRHS2D;
      Grad = Diff.grad;
   else
       disp('error')
   end
 
    % Extracting parameters    
    SInt= this.IDC.Int;
    this.TDC.ComputeIntegrationVector;
    TInt=this.TDC.Int; 
    ConsTol = this.optsPhys.optsPhysOpt.OptSolver.OptTols.ConsTol;
    PDERHSStr = this.optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
    ComputeNorm=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
    times = this.TDC.Pts.y;
    lambda = this.optsPhys.optsPhysOpt.OptSolver.lambda;
    beta = this.optsPhys.optsPhysFW.Params.beta;
    n = this.optsNum.TimeArea.n;
    Conv = this.Conv;
    T0=this.optsNum.TimeArea.t0;
    TMax=this.optsNum.TimeArea.TMax;
  
    % Setting initial values for loop 
    wErr = 10^10;
    j = 0;

    % Extracting the correct initial Control term (Force/Flow & 1D/2D & Matrix/testFun file)
    if (isa(this.IDC, 'SpectralLine')==1|| isa(this.IDC, 'FiniteDifferenceLine')==1) && ((strcmp(PDERHSStr, 'AD_Flow')) || (strcmp(PDERHSStr, 'AD_Flowf')) || (strcmp(PDERHSStr, 'AD_FlowfVext')) || (strcmp(PDERHSStr, 'AD_Flowflog')))
        if isfield(this.optsPhys.optsPhysFW.DataInput,'wFlow')==0
            testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
            y=this.IDC.Pts.y;
            for iT = 1:n
            ExactSol = testFun(this,y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
            wFlow(iT,:) = ExactSol.wFlow;
            end
            wIn = wFlow; 
        else
            wIn = this.optsPhys.optsPhysFW.DataInput.wFlow; 
        end    
    elseif  isa(this.IDC, 'Box') ==1 && ((strcmp(PDERHSStr, 'AD_Flow')) || (strcmp(PDERHSStr, 'AD_Flowf')) || (strcmp(PDERHSStr, 'AD_FlowfVext')))
        if isfield(this.optsPhys.optsPhysFW.DataInput,'wFlow')==0
            testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
            for iT = 1:n
            ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
            wFlow(iT,:,:) = ExactSol.wFlow;
            end
            wIn = [wFlow(:,:,1), wFlow(:,:,2)];
        else
        wIn = this.optsPhys.optsPhysFW.DataInput.wFlow; 
        end  
    elseif  (isa(this.IDC, 'SpectralLine')==1|| isa(this.IDC, 'FiniteDifferenceLine')==1) && ((strcmp(PDERHSStr, 'AD_Force')) || (strcmp(PDERHSStr, 'AD_Forcefl')) || (strcmp(PDERHSStr, 'D_Force')))    
        if isfield(this.optsPhys.optsPhysFW.DataInput,'wForce')==0
            testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
            for iT = 1:n
            ExactSol = testFun(this,this.IDC.Pts.y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
            wForce(iT,:) = ExactSol.wForce;
            end
            wIn = wForce;
        else
            wIn = this.optsPhys.optsPhysFW.DataInput.wForce;
        end
    elseif isa(this.IDC, 'Box') ==1 && ((strcmp(PDERHSStr, 'AD_Force')) || (strcmp(PDERHSStr, 'AD_Forcefl')) || (strcmp(PDERHSStr, 'D_Force'))) 
        if ~isfield(this.optsPhys.optsPhysFW.DataInput,'wForce')
            testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
            for iT = 1:n
            ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
            wForce(iT,:) = ExactSol.wForce;
            end
            wIn = wForce;
        else
            wIn = this.optsPhys.optsPhysFW.DataInput.wForce;
        end
    elseif  (isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FourierLine')==1|| isa(this.IDC, 'FiniteDifferenceLine')==1 || isa(this.IDC, 'FiniteDifferencePeriodicLine')==1)  && ((strcmp(PDERHSStr, 'OD'))) 
        if isfield(this.optsPhys.optsPhysFW.DataInput,'rho_R_IG')==0
            testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
            for iT = 1:n
            ExactSol = testFun(this,this.IDC.Pts.y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
            rho_R_IG(iT,:) = ExactSol.rho_R_IG;
            end
            wIn = rho_R_IG;
        else
            wIn = this.optsPhys.optsPhysFW.DataInput.rho_R_IG;
        end
        
    elseif (isa(this.IDC, 'Box') || isa(this.IDC, 'BoxFourierFourier') ==1) && ((strcmp(PDERHSStr, 'OD')))
        if ~isfield(this.optsPhys.optsPhysFW.DataInput,'rho_R_IG')
            testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
            for iT = 1:n
            ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
            rho_R_IG(iT,:) = ExactSol.rho_R_IG;
            end
            wIn = rho_R_IG;
        else
            wIn = this.optsPhys.optsPhysFW.DataInput.rho_R_IG;
        end
    end
    
    % Checking whether the 'Adaptive' Option is chosen to do an adaptive
    % optimization scheme based on the consistency error.
    if isfield(this.optsPhys.optsPhysOpt.OptSolver,'AdaSolver')==1 && (isempty(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver)==0)
        Ada = 1;
        if strcmp(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver, 'ArmijoWolfeJump')
            armijocount = 0;
            armijojump = this.optsPhys.optsPhysOpt.OptSolver.AdaSolverJump;
        end
    else
        Ada=0;
    end

 %%%%%% Optimization Algorithm %%%%%%   
    count=0; %threshold number for algorithm to diverge
    tic
    while wErr > ConsTol
       
           
        % solving both PDEs on Chebyshev points
        pdeInput = struct('T0',T0, 'TMax', TMax, 'IC',[this.DataIn.rhoIC;this.DataIn.pIC],'rhoIn','na','pIn','na','Pts',Pts,'Diff',Diff,'Ind',Ind, ...
            'wIn',wIn,'muIn','na', 'this',this, 'PDESolver',PDESolver,'PDERHSStr',PDERHSStr,'rhoMask',rhoMask,'pMask',pMask,'n',n,'beta',beta);
        data = ComputePDE(pdeInput);
        rhoOut = data.rhoOut;
        pOutFlip = data.pOutFlip;
        wOut = data.wOut;
        %data = PDESolver(T0,TMax,[this.DataIn.rhoIC;this.DataIn.pIC],'na','na',Pts,Diff,Ind,wIn,'na',this);
%   
%         rhoOut = data(:,rhoMask);
%         pOut = data(:,pMask);
        
%         % reversing time in p (from backward to forward)
%         for i = 1:n
%             pOutFlip(i,:) = pOut(n-i+1,:);
%         end
%         % Evaluating Control depending on problem, using the respective
%         % gradient equation
%         if (strcmp(PDERHSStr, 'AD_Flow')) || (strcmp(PDERHSStr, 'AD_Flowf')) || (strcmp(PDERHSStr, 'AD_FlowfVext')) || (strcmp(PDERHSStr, 'AD_Flowflog')) ||...
%            (strcmp(PDERHSStr, 'AD2D_Flow')) || (strcmp(PDERHSStr, 'AD2D_Flowf')) || (strcmp(PDERHSStr, 'AD2D_FlowfVext')) 
%             if isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FiniteDifferenceLine')==1
%                 wOut1 = -(Dy*pOutFlip').*rhoOut'/beta;
%             elseif isa(this.IDC, 'Box')==1
%                 rhoOut2 = [rhoOut,rhoOut];
%                 wOut1 = -(Grad*pOutFlip').*rhoOut2'/beta;
%             end
%             wOut = wOut1';
%         elseif  (strcmp(PDERHSStr, 'AD_Force')) || (strcmp(PDERHSStr, 'AD_Forcefl')) || (strcmp(PDERHSStr, 'AD2D_Force')) || ...
%                 (strcmp(PDERHSStr, 'AD2D_Forcefl')) || (strcmp(PDERHSStr, 'D_Force')) || (strcmp(PDERHSStr, 'D2D_Force')) 
%             wOut = - pOutFlip/beta;
%         elseif  (strcmp(PDERHSStr, 'OD'))
%             if isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FourierLine')==1 || isa(this.IDC, 'FiniteDifferenceLine')==1 || isa(this.IDC, 'FiniteDifferencePeriodicLine')==1
%                 for iT=1:n
%                     wOut1(iT,:)= -(Interact2BodyAdj2(rhoOut(iT,:)',pOutFlip(iT,:)',Conv,Dy))/beta;
%                 end
%             elseif isa(this.IDC, 'Box')==1
%                 rhoOut2 = [rhoOut,rhoOut];
%                 for iT=1:n
%                     wOut1(iT,:)= -(Interact2BodyAdj2(rhoOut2(iT,:)',pOutFlip(iT,:)',Conv,Grad))/beta;
%                 end
%             end
%             wOut = wOut1;
%         end
        
        
        % Computing consistency error that determines convergence
        wErrN = ComputeNorm(wOut, wIn, SInt, TInt); 
        j= j+1;
        
        % If there is an exact solution, display errors in rho, p, 
        % otherwise just the consistency error
        if isempty(this.ExactSolution)==0
            rhoEX= this.ExactSolution.OptirhoIG;
            pEX = this.ExactSolution.OptipIG;
            rhoEX_Err = ComputeNorm(rhoOut, rhoEX, SInt, TInt); 
            pEX_Err = ComputeNorm(pOutFlip, pEX, SInt, TInt); 
            fprintf(1, '%d, %.8f, %.8f, %.8f\n', [j; wErrN; rhoEX_Err; pEX_Err]);
        else
            fprintf(1, '%d, %.8f\n', [j; wErrN]);
        end
        
        % Condition that breaks the loop if the consistency error diverges.
        
        if wErrN > wErr
            count = count+1;
            if count>10
            % output (if diverged)
                disp('diverged')
                sol.rhoNum = rhoOut(2:n,:);
                sol.pNum = pOutFlip(1:n-1,:);
                sol.wNum = wOut;
                sol.Iter = j;
                sol.ConsErr = wErrN;
                sol.converge = false;
                return  
            end
        else
            count = 0;
        end
        
        % Update scheme:
        % Mixing of solutions and update of the Control. Loop Complete.
        awTime = 0;
        steps = 0;
        if Ada == 1 
            
            if strcmp(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver, 'ArmijoWolfe')
              armijo = ComputeArmijoWolfeStep(wIn,wOut,SInt, TInt,pdeInput);
              lambda = armijo.lambda;
              steps= armijo.steps;
              awTime = armijo.awTime;
              
              %disp(strcat('Number of Iterations for Armijo-Wolfe rule is ',num2str(steps)));
            elseif strcmp(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver, 'ArmijoWolfeJump')
                armijocount = armijocount+1;
                if mod(armijocount, armijojump)== 0
                   armijo = ComputeArmijoWolfeStep(wIn,wOut,SInt, TInt,pdeInput);
                    lambda = armijo.lambda;
                    steps= armijo.steps;
                    awTime = armijo.awTime;
                    %disp(strcat('Number of Iterations for Armijo-Wolfe rule is ',num2str(steps)));  
                    
                end
            end
            
        end
        
         armijoLambda(j) = lambda ;
         armijoTime(j) = awTime;
         armijoSteps(j) = steps;
         armijowErrN(j) = wErrN;
         
         
        wIn = (1-lambda)*wIn + lambda*wOut;
        wErr = wErrN;
        
        
    end
    toc
    timeTaken = toc;
    armijo = struct('Lambda',armijoLambda,'Time',armijoTime, 'Steps', armijoSteps, 'wErr',armijowErrN);
    
   
    % Output (if not diverged!)
    sol.rhoNum = rhoOut;
    sol.pNum = pOutFlip;
    sol.wNum = wOut;
    sol.Iter = j;
    sol.ConsErr = wErrN; 
    sol.converge = true;
    sol.timeTaken = timeTaken;
    sol.armijo = armijo;
end
