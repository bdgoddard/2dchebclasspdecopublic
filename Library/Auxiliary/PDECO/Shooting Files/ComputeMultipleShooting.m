function sol = ComputeMultipleShooting(this,n,rhoIExa,pIExa,times,rhoMask,pMask,Pts,Diff,Ind)

   if isa(this.IDC, 'SpectralLine')||isa(this.IDC, 'FourierLine') ==1
      PDESolver = @ComputeOptimizationRHS1D; 
   elseif isa(this.IDC, 'Box') ==1
      PDESolver = @ComputeOptimizationRHS2D;
   else
       disp('whats IDC')
   end
 
   % Computing the solution of the PDEs at each time point i
   parfor i=1:n-1
        tMin=times(i);
        tMax = times(i+1);
        
        rhopOut = PDESolver(tMin,tMax,[rhoIExa(i,:)';pIExa(i+1,:)'],rhoIExa,pIExa,Pts,Diff,Ind,1,this);
        
        rho_err = rhopOut(end,rhoMask) - rhoIExa(i+1,:);
        p_err = rhopOut(end,pMask) - pIExa(i,:);

        rho_error(i,:)= rho_err;
        p_error(i,:)= p_err;
   end 
    
   % outputs the error at each time point for rho and p
    sol = [rho_error(:); p_error(:)];


    end
