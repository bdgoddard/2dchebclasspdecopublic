function sol = ComputePDE(pdeInput)
% solving both PDEs on Chebyshev points
        T0 = pdeInput.T0;
        TMax = pdeInput.TMax;
        rhoIn = pdeInput.rhoIn;
        pIn = pdeInput.pIn;
        Pts = pdeInput.Pts;
        Diff = pdeInput.Diff;
        Ind = pdeInput.Ind;
        wIn= pdeInput.wIn;
        muIn = pdeInput.muIn;
        this = pdeInput.this;
        PDESolver = pdeInput.PDESolver;
        PDERHSStr = pdeInput.PDERHSStr;
        rhoMask =  pdeInput.rhoMask;
        pMask =  pdeInput.pMask;
        n = pdeInput.n;
        beta = pdeInput.beta;
        
        
        Conv = this.Conv;
        
        if isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FourierLine')==1 || isa(this.IDC, 'FiniteDifferenceLine')==1|| isa(this.IDC, 'FiniteDifferencePeriodicLine')==1
            Dy = Diff.Dy;
        elseif isa(this.IDC, 'Box') ==1 || isa(this.IDC, 'BoxFourierFourier') ==1
            Grad = Diff.grad;
        else
            disp('error')
        end
        

        data = PDESolver(T0,TMax,[this.DataIn.rhoIC;this.DataIn.pIC],rhoIn,pIn,Pts,Diff,Ind,wIn,muIn,this);

            rhoOut = data(:,rhoMask);
            pOut = data(:,pMask);
        
        % reversing time in p (from backward to forward)
       
        for i = 1:n
            pOutFlip(i,:) = pOut(n-i+1,:);
        end
%         end
        % Evaluating Control depending on problem, using the respective
        % gradient equation
        % Evaluating Control depending on problem, using the respective
%         % gradient equation
        if (strcmp(PDERHSStr, 'AD_Flow')) || (strcmp(PDERHSStr, 'AD_Flowf')) || (strcmp(PDERHSStr, 'AD_FlowfVext')) || (strcmp(PDERHSStr, 'AD_Flowflog')) ||...
           (strcmp(PDERHSStr, 'AD2D_Flow')) || (strcmp(PDERHSStr, 'AD2D_Flowf')) || (strcmp(PDERHSStr, 'AD2D_FlowfVext')) 
            if isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FiniteDifferenceLine')==1
                wOut1 = -(Dy*pOutFlip').*rhoOut'/beta;
            elseif isa(this.IDC, 'Box')==1
                rhoOut2 = [rhoOut,rhoOut];
                wOut1 = -(Grad*pOutFlip').*rhoOut2'/beta;
            end
            wOut = wOut1';
        elseif  (strcmp(PDERHSStr, 'AD_Force')) || (strcmp(PDERHSStr, 'AD_Forcefl')) || (strcmp(PDERHSStr, 'AD2D_Force')) || ...
                (strcmp(PDERHSStr, 'AD2D_Forcefl')) || (strcmp(PDERHSStr, 'D_Force')) || (strcmp(PDERHSStr, 'D2D_Force')) 
            wOut = - pOutFlip/beta;
        elseif  (strcmp(PDERHSStr, 'OD'))
            if isa(this.IDC, 'SpectralLine')==1 || isa(this.IDC, 'FourierLine')==1 || isa(this.IDC, 'FiniteDifferenceLine')==1 || isa(this.IDC, 'FiniteDifferencePeriodicLine')==1
                for iT=1:n
                    wOut1(iT,:)= -(Interact2BodyAdj2(rhoOut(iT,:)',pOutFlip(iT,:)',Conv,Dy))/beta;
                end
            elseif isa(this.IDC, 'Box')==1 || isa(this.IDC, 'BoxFourierFourier')==1 
                
                for iT=1:n
                    wOut1(iT,:)= -(Interact2BodyAdj2OD2D(rhoOut(iT,:)',pOutFlip(iT,:)',Conv,Grad))/beta;
                end
            end
            wOut = wOut1;
        end

    sol.rhoOut = rhoOut;
    sol.pOut = pOut;
    sol.pOutFlip = pOutFlip;
    sol.wOut = wOut;


end