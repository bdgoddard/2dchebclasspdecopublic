function optimalsol = ComputePicardShooting(this,n,rhop_IC,rhoExa,pExa,times,rhoMask,pMask,Pts,Diff,Ind)
    disp('Solving Optimization problem with Picard Solver...');
   
    if isa(this.IDC, 'SpectralLine')==1
        PDESolver = @ComputeOptimizationRHS1D; 
    elseif isa(this.IDC, 'Box') ==1
        PDESolver = @ComputeOptimizationRHS2D;
    else
        print('error')
    end

    % Extracting Parameters
    SInt= this.IDC.Int;
    this.TDC.ComputeIntegrationVector;
    TInt=this.TDC.Int; 
    ConsTol=this.optsPhys.optsPhysOpt.OptSolver.OptTols.ConsTol;
    ComputeNorm=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
    lambda = this.optsPhys.optsPhysOpt.OptSolver.lambda;
    
    rhoIC(2:n,:) = rhop_IC(:,rhoMask);
    pIC(1:n-1,:) = rhop_IC(:,pMask);
    rhoIC(1,:) = this.DataIn.rhoIC;
    pIC(n,:) =this.DataIn.pIC;
     
    % Initial loop values
    rhop_Err= 10^3;
    j=0;
  
    rhoIn  = rhoExa;
    rhoOut = rhoExa; 
    pIn    = pExa;
    pOut   = pExa;

    % Checking if the 'Adaptive' option is chosen (to do an adaptive
    % updating method based on the error in solution)
    if isfield(this.optsPhys.optsPhysOpt.OptSolver,'AdaSolverStr')==1 && (isempty(this.optsPhys.optsPhysOpt.OptSolver.AdaSolverStr)==0)
        Ada = 1;
        AdaSolver = str2func(this.optsPhys.optsPhysOpt.OptSolver.AdaSolverStr); 
    else
        Ada = 0;
    end
    
%%% The optimization algorithm: %%%%

    while rhop_Err > ConsTol && j<5000 % hard coded limit of iterations at 5000. can be changed
        
        % lambda update, if adaptive/ otherwise is fixed
        if Ada == 1                       
            lambda = AdaSolver(rhop_Err,lambda);
        end
        
        % mixing scheme and updating of input variables
        rhoIn =  (1-lambda)*rhoIn + lambda*rhoOut;
        pIn =    (1-lambda)*pIn + lambda*pOut ;
                
        % solving the PDEs on each time point i
        parfor i=1:n-1
            tMin=times(i);
            tMax = times(i+1);
            
            rhopOut = PDESolver(tMin,tMax,[rhoIn(i,:)';pIn(i+1,:)'],rhoIn,pIn,Pts,Diff,Ind,1,this);
                       
            rhoOut(i+1,:) = rhopOut(end,rhoMask);
            pOut(i,:) = rhopOut(end,pMask);

        end 
 
        % Computing consistency errors (for convergence).
        rho_Err = ComputeNorm(rhoOut, rhoIn, SInt, TInt); 
        p_Err = ComputeNorm(pOut, pIn, SInt, TInt); 
        rhop_Err= rho_Err + p_Err;
         
        j=j+1;
        
        % If exact solutions exist, display exact errors as well, otherwise
        % just display the consistency error.
        if isempty(this.ExactSolution)==0
            rhoEX= this.ExactSolution.OptirhoIG;
            pEX = this.ExactSolution.OptipIG;
            rhoEX_Err = ComputeNorm(rhoOut, rhoEX, SInt, TInt); 
            pEX_Err = ComputeNorm(pOut, pEX, SInt, TInt); 
            fprintf(1, '%d, %.8f, %.8f, %.8f\n', [j; rhop_Err; rhoEX_Err; pEX_Err]);
        else
            fprintf(1, '%d, %.8f\n', [j; rhop_Err]);
        end
        
    end
 %%% output number of iterations and solution at all points except rhoIC, pIC       
    opt1 = [rhoIn(2:n,:),pIn(1:n-1,:)];
    optimalsol.opt1 = opt1;
    optimalsol.optj = j;
                        
 end
