
function res = CostFunctional(this,bet)
    rho = this.rho;
    rhoHat = this.g;
    w = this.w;

    geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];
    nx = 20; 
    n = 11; % necessary to achieve 1e-10 acc.
    geom.N = [nx;nx]; 

    aBox = Box(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc
    aBox.ComputeInterpolationMatrix((-1:0.01:1)',(-1:0.01:1)',true,true);
    
    ageom.yMin = 0;
    ageom.yMax = 1;
    ageom.N = n;
    aLine = SpectralLine(ageom);

    TInt = aLine.ComputeIntegrationVector;
    SInt = aBox.Int;

    J1 = TInt*(SInt*((rho - rhoHat).^2))'; 

    if length(SInt) == length(w) 
        dProdw = w.^2;
    else
         for i = 1:n
             wt = w(:,i);
             dProdw(i,:)= wt(1:end/2).*wt(1:end/2) + wt(end/2+1:end).*wt(end/2+1:end);
         end
         dProdw = dProdw';
    end
    J2 = TInt*((SInt*(dProdw))');

    Jfun = (1/2)*J1 + (bet/2)*J2;
    
    res = Jfun;
%     res.Jfun = Jfun;
%     res.J1 = J1;
%     res.J2 = J2;
    
end