
function res = CostFunctional3D(this,bet)
    rho = this.rho;
    rhoHat = this.g;
    w = this.w;

    geom.y1Min = -1; geom.y1Max = 1; 
    geom.y2Min = -1; geom.y2Max = 1; 
    geom.y3Min = -1; geom.y3Max = 1; 

    nx = 20;    
    n = 11;
    geom.N = [nx;nx;nx]; 

    aBox = Box3(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc
    
    ageom.yMin = 0;
    ageom.yMax = 1;
    ageom.N = n;
    aLine = SpectralLine(ageom);

    TInt = aLine.ComputeIntegrationVector;
    SInt = aBox.Int;
    
    J1 = TInt*(SInt*((rho - rhoHat).^2))'; 

    if length(SInt) == length(w) 
        dProdw = w.^2;
    else
         for i = 1:n
             wt = w(:,i);
             dProdw(i,:)= wt(1:end/3).*wt(1:end/3) + wt(end/3+1:2*end/3).*wt(end/3+1:2*end/3)...
                 + wt(2*end/3+1:end).*wt(2*end/3+1:end);
         end
         dProdw = dProdw';
    end
    J2 = TInt*((SInt*(dProdw))'); 

    Jfun = (1/2)*J1 + (bet/2)*J2;
    
    res.Jfun = Jfun;
    res.J1 = J1;
    res.J2 = J2;
    
end