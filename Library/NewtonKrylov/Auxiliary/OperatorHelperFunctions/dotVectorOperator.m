function vdotOp = dotVectorOperator(v,Op)

    v_1 = v(1:end/2); v_2 = v(end/2 + 1:end);
    
    Op_1 = Op(1:end/2,:); Op_2 = Op(end/2 + 1:end,:);
    
    vdotOp = spdiags(v_1,0,length(v_1),length(v_1))*Op_1 ...
             + spdiags(v_2,0,length(v_2),length(v_2))*Op_2;

end