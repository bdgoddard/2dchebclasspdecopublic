function vdotOp = dotVectorOperator3(v,Op)

    v_1 = v(1:end/3); 
    v_2 = v(end/3 + 1:2*end/3);
    v_3 = v(2*end/3 + 1:end);
    
    Op_1 = Op(1:end/3,:); 
    Op_2 = Op(end/3 + 1:2*end/3,:);
    Op_3 = Op(2*end/3 + 1:end,:);
    
    vdotOp = spdiags(v_1,0,length(v_1),length(v_1))*Op_1 ...
             + spdiags(v_2,0,length(v_2),length(v_2))*Op_2 ...
             + spdiags(v_3,0,length(v_3),length(v_3))*Op_3;

end