function Fulldot = dotVectorOperatorMS(this,v,Op)

   vdotOp = zeros(length(this.Pts.y1_kv), length(this.Pts.y1_kv));
   ptsCountVec = 0;
   ptsCountVecm = 0;
   Fulldot = [];
   N1 = zeros(this.nShapes,1);
   N2 = zeros(this.nShapes,1);
   for iShape = 1:this.nShapes
        N1(iShape) = this.Shapes(iShape).Shape.Pts.N1;
        N2(iShape) = this.Shapes(iShape).Shape.Pts.N2;
   end
   Mi = N1.*N2;
   this.M = sum(Mi);
   
   for iShape = 1:this.nShapes

        mask = false(this.M,1);
        mask(ptsCountVecm + 1 : ptsCountVecm + Mi(iShape)) = true;
        ptsCountVecm = ptsCountVecm + Mi(iShape);
        maskVec = false(2*this.M,1);
        maskVec(ptsCountVec + 1 : ptsCountVec + 2*Mi(iShape)) = true;
        ptsCountVec = ptsCountVec + 2*Mi(iShape);
       
        v1 = v(maskVec);
        
        v1p = v1(1:end/2);
        v2p = v1(end/2+1:end);
        
        Op_1 = Op(maskVec,mask);
        Op1p = Op_1(1:end/2,:);
        Op2p = Op_1(end/2+1:end,:);

        vdotOp = spdiags(v1p,0,length(v1p),length(v1p))*Op1p ...
         + spdiags(v2p,0,length(v2p),length(v2p))*Op2p;
        Fulldot = blkdiag(Fulldot,vdotOp);
   end

end
