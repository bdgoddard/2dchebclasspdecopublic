function v1dotv2 = dotVectors(v1,v2)

    v1_1 = v1(1:end/2); v1_2 = v1(end/2 + 1:end);
    v2_1 = v2(1:end/2); v2_2 = v2(end/2 + 1:end);

    v1dotv2 = v1_1 .* v2_1 + v1_2 .* v2_2;
    
end