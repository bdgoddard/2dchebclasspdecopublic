function dprod = dotVectorsMS(this,u,v)
        N1 = zeros(this.nShapes,1);
        N2 = zeros(this.nShapes,1);
        for iShape = 1:this.nShapes
            N1(iShape) = this.Shapes(iShape).Shape.Pts.N1;
            N2(iShape) = this.Shapes(iShape).Shape.Pts.N2;
        end
        Mi = N1.*N2;
        M = sum(Mi);
        dprod = zeros(M, 1);
        ptsCountVec = 0;
        ptsCountVecm = 0;
        for iShape = 1:this.nShapes     
            mask = false(this.M,1);
            mask(ptsCountVecm + 1 : ptsCountVecm + Mi(iShape)) = true;
            ptsCountVecm = ptsCountVecm + Mi(iShape);
            maskVec = false(2*this.M,1);
            maskVec(ptsCountVec + 1 : ptsCountVec + 2*Mi(iShape)) = true;
            ptsCountVec = ptsCountVec + 2*Mi(iShape);
            
            um = u(maskVec);
            vm = v(maskVec);
            u1p = um(1:end/2);
            u2p = um(end/2+1:end);
            v1p = vm(1:end/2);
            v2p = vm(end/2+1:end);
            dprod(mask) = (u1p.*v1p) + (u2p.*v2p);
        end
     
end

%    N1 = zeros(2,1);
%         N2 = zeros(2,1);
%         for iShape = 1:2
%             N1(iShape) = 14;
%             N2(iShape) = 14;
%         end
%         Mi = N1.*N2;
%         dprod = zeros(2*196, 1);
%         ptsCountVec = 0;
%         ptsCountVecm = 0;
%         for iShape = 1:2      
%             mask = false(196,1);
%             mask(ptsCountVecm + 1 : ptsCountVecm + Mi(iShape)) = true;
%             ptsCountVecm = ptsCountVecm + Mi(iShape);
%             maskVec = false(2*196,1);
%             maskVec(ptsCountVec + 1 : ptsCountVec + 2*Mi(iShape)) = true;
%             ptsCountVec = ptsCountVec + 2*Mi(iShape);
%             
%             um = u(maskVec);
%             vm = v(maskVec);
%             u1p = um(1:end/2);
%             u2p = um(end/2+1:end);
%             v1p = vm(1:end/2);
%             v2p = vm(end/2+1:end);
%             dprod(mask) = (u1p.*v1p) + (u2p.*v2p);
%         end
%   