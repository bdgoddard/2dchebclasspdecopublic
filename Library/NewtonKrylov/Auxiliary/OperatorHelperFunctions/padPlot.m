function padPlot(IDC,f)

N1 = IDC.N1; N2 = IDC.N2;
bound = IDC.Ind.bound;

fPlot = zeros(N1*N2,1);
fPlot(~bound) = f;

IDC.plot(fPlot);

end