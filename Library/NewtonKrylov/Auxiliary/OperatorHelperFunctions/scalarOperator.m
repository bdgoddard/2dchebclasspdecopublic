function S = scalarOperator(s)
    S = sparse(diag(s));
end