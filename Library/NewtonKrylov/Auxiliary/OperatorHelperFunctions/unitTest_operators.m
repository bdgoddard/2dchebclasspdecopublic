geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];

nx = 12;       % interior grid pts in space
geom.N = [nx+2;nx+2]; % pad and we'll delete the boundary ones later

aBox = Box(geom); % make a Box object
aBox.ComputeAll;  % compute differentiation, integration, etc

x1 = aBox.Pts.y1_kv; x2 = aBox.Pts.y2_kv; % points from box

bound = aBox.Ind.bound;
x1(bound) = []; x2(bound) = []; % kill the boundary points

Lfull = aBox.Diff.Lap;   % Laplacian 
L = Lfull(~bound,~bound);

% derivatives in x1 and x2
Dx1full = aBox.Diff.Dy1; Dx2full = aBox.Diff.Dy2;
Dx1 = Dx1full(~bound,~bound); Dx2 = Dx2full(~bound,~bound);
grad = [Dx1;Dx2];

v1 = [2*cos(pi*x1/2); cos(pi*x2/2)];
v2 = [sin(pi*x1/2); cos(pi*x2/2)];

v1dotv2 = 2*cos(pi*x1/2).*sin(pi*x1/2) + cos(pi*x2/2).*cos(pi*x2/2);
test = dotVectors(v1,v2);
max(abs(test-v1dotv2))

f = cos(pi*x1/2).*cos(pi*x2/2);
gradf = -pi/2*[sin(pi*x1/2).*cos(pi*x2/2);cos(pi*x1/2).*sin(pi*x2/2)];
v = [cos(pi*x1/2);cos(pi*x2/2)];

vgradf = -pi/2*( sin(pi*x1/2).*cos(pi*x2/2).*cos(pi*x1/2) ...
                 + cos(pi*x1/2).*sin(pi*x2/2).*cos(pi*x2/2) );

test = dotVectorOperator(v,grad)*f;
max(abs(test - vgradf))
