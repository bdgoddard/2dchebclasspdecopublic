function g = GaussianNK(x)

    g = exp(-x.^2);

end