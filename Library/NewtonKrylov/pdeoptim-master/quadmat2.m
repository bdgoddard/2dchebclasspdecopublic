function C_new = quadmat2(a,b,x,d)
% [C,w] = quadmat2(a,b,x,d)
% Returns the collocation matrix C for cumulative integration at
% the points x on [a,b]. The barycentric blending parameter is d.
% If f is a vector of function values f(x), then g = C*f should
% be an approximation to g(x) with g = cumsum(f).
%
% Uses the CUMSUM command of Chebfun. For more details about Chebfun see
%
%   T. A. Driscoll, N. Hale, and L. N. Trefethen, editors, Chebfun Guide,
%   Pafnuty Publications, Oxford, 2014.
%
% For details about QUADMAT2 and rational deferred correction, see
%
%   S. G\"{u}ttel and G. Klein: "Efficient high-order rational integration
%   and deferred correction with equispaced data." Electronic Transactions
%   on Numerical Analysis 41, pp. 443-464, 2014.

geom.yMin = a; geom.yMax = b;
geom.N = length(x);

fullLine = SpectralLine(geom);

n = geom.N;

% n = length(x) - 1;
% 
% w = weights(n,d,x);
% 
% C = zeros(n+1,n+1);
% 
% maxlen = 0;
% 
% if n == 0
%     C = 1;
%     return
% end
% 
% for k = 1:ceil((n+1)/2)
%     fx = 0*x; fx(k) = 1; % k-th Lagrange function
%     c = chebfun(@(xx) bcinterpol(w,x,fx,xx.'),[x(1),x(end)]); % ! overriding adaptive choice for nr of pts
%     maxlen = max(maxlen,length(c));
%     c = cumsum(c); c = c(:);
%     C(:,k) = c(x) - c.vals(1);
% end
% for k = ceil((n+1)/2)+1:n+1  % uses symmetry of the Lagrange function, see KB, BIT
%     C(:,k) = C(end,n+1-k+1) - C(end:-1:1,n+1-k+1);
% end


C_new = zeros(n,n);

if n == 1
    C_new = 1;
    return
end

geom.N = 20;
geom.yMin = a;

for k = 2:n
    geom.yMax = x(k);
    intLine = SpectralLine(geom);
    Int = intLine.ComputeIntegrationVector;
    IntPts = intLine.Pts.y;
    Interp = fullLine.ComputeInterpolationMatrixPhys(IntPts);
    InterPol = Interp.InterPol;
    C_new(k,:) = Int*InterPol;
end


end
