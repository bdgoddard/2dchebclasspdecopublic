function data= Compute_Forward_Dynamics(this)

     disp('Solving Forward problem ...');
     tic;
     optsPhysFW=this.optsPhys.optsPhysFW;
     optsNum=this.optsNum;  
     
    %---------------------------------------------------------------------%
    % Extracting Parameters                                                     %
    %---------------------------------------------------------------------% 
    beta=optsPhysFW.Params.beta; 
    
    AbsTol=optsNum.Tols.AbsTol;
    RelTol=optsNum.Tols.RelTol;

    aLine = this.IDC;  
    PlotArea= optsNum.PlotArea;
    [Pts,Diff,SInt,Ind,~] = aLine.ComputeAll(PlotArea); 
    this.optsPhys.optsPhysFW.Params.SInt = SInt;    
    InterpSpace = aLine.InterpolationPlot(PlotArea,true);
    Dy=Diff.Dy;
    DDy = Diff.DDy;
    y=Pts.y;
    bound=Ind.bound;
    normal = Ind.normal;
    N = this.optsNum.PhysArea.N;
    
   
    
    n=optsNum.TimeArea.n;
    TMax=optsNum.TimeArea.TMax;
    InterLine= this.TDC;
    times = InterLine.Pts.y;
    InterLine.ComputeIntegrationVector;
    
    Conv = this.Conv;
     
    % Here is where we combine the input string that indicates the RHS to
    % be solved with the standardized prefix, to get the RHS function we need
    % to call
    PDERHSStr = optsPhysFW.ProbSpecs.PDERHSStr;
    PDErhsStr = strcat("ComputeFW",PDERHSStr);
    ComputeFWPDE = str2func(PDErhsStr);
    
    ComputeNorm=str2func(optsPhysFW.ProbSpecs.ComputeNormStr);
    testFun=str2func(optsPhysFW.DataInput.testFun);
    BCFun=str2func(optsPhysFW.ProbSpecs.BCFunStr);
  
    % Extracting needed data
    if strcmp(PDERHSStr, 'AD_Flowflog') || (strcmp(PDERHSStr, 'D_Forcelog'))
       rho_ic = log(this.DataIn.rhoIC);
    else
       rho_ic = this.DataIn.rhoIC;
    end
    
    %flag for opinion dynamics problems
    inputOD = isfield(optsPhysFW.DataInput,'OD');
    if(inputOD)
        inputODValue = optsPhysFW.DataInput.OD;
    end
    %rescaling mass of initial condition to M_rho
    if(inputOD)
        C = 1/(SInt*rho_ic);
        M_rho = this.optsPhys.optsPhysFW.Params.M_rho;
        rho_ic = M_rho*C*rho_ic;
        this.DataIn.rhoIC = rho_ic;
        
    end
  
    %----------------------------------------------------------------------
    % Preparing RHS inputs
    %----------------------------------------------------------------------
        
    % Inputs for RHS if variables given as matrices rather than analytic
    % functions
    inputForce = isfield(optsPhysFW.DataInput,'Force');
    if(inputForce)
        ForceIn = optsPhysFW.DataInput.Force;
    end
    inputFlow = isfield(optsPhysFW.DataInput,'Flow');
    if(inputFlow)
        FlowIn = optsPhysFW.DataInput.Flow;
    end
    inputwForce = isfield(optsPhysFW.DataInput,'wForce');
    if(inputwForce)
        wForceIn = optsPhysFW.DataInput.wForce;
    end
    inputwFlow = isfield(optsPhysFW.DataInput,'wFlow');
    if(inputwFlow)
        wFlowIn = optsPhysFW.DataInput.wFlow;
    end
    
    inputVext = isfield(optsPhysFW.DataInput,'Vext');
    if(inputVext)
        VextIn = optsPhysFW.DataInput.Vext;
    end
    inputrho_R = isfield(optsPhysFW.DataInput,'rho_R');
    
    if(inputOD)
        M = this.optsPhys.optsPhysFW.Params.M; %parameter for scaling rho_R
        if(inputrho_R)        
            rho_RIn = optsPhysFW.DataInput.rho_R;
        elseif (inputODValue) 
            %scaling non-time dependent control rho_R in opinion dynamics problem            
            SolPts =testFun(this,y,times(1),TMax,beta); 
            rho_RIn = SolPts.rho_R;         
            C_R = 1/(SInt*rho_RIn);
            rho_RIn = M*C_R*rho_RIn;
                
        end

    end
    
    % General inputs for RHS that don't change in ODE solve
    % (differentiation matrices and parameters)
        RHSInput.Dy = Dy;
        RHSInput.DDy = DDy;
        RHSInput.gamma = optsPhysFW.Params.gamma;
        RHSInput.D0 = optsPhysFW.Params.D0;

    
    %---------------------------------------------------------------------%
    % Solve the PDE                                                       %
    %---------------------------------------------------------------------%
    %outTimes = 0:1/50:1;
    mM  = ones(size(y));
    mM(bound)   = 0;    
    opts = odeset('RelTol',RelTol,'AbsTol',AbsTol,'Mass',diag(mM));
    
    [outTimes, rho_t] = ode15s(@drho_dt,times,rho_ic,opts);   
  
    wForceCheb = zeros(n,N);
    wFlowCheb = zeros(n,N);
    rho_RCheb = zeros(n,N);
    if(inputOD)
        if (inputODValue)
        for iT = 1:n
            
            rho_RCheb(iT,:) = rho_RIn;
        end
        elseif(inputrho_R)
            
            rho_RCheb = rho_RIn;
        else
            for iT = 1:n
            SolChebPts =testFun(this,y,times(iT),TMax,beta);
            rho_RCheb(iT,:) = SolChebPts.rho_R;
            end
        end
         
         
    else
        for iT = 1:n
            SolChebPts =testFun(this,y,times(iT),TMax,beta); 
            wForceCheb(iT,:) = SolChebPts.wForce;
            wFlowCheb(iT,:) = SolChebPts.wFlow;
        end
    end
  
    %---------------------------------------------------------------------%
    % Output                                                              %
    %---------------------------------------------------------------------%    
    % Outputting the correct 'control value' (if the forward problem is run
    % as an initial guess and we want to compute cost functionals).
    if strcmp(PDERHSStr, 'D_Force') ||  strcmp(PDERHSStr, 'AD_Force')|| strcmp(PDERHSStr, 'AD_Forcefl') ||(strcmp(PDERHSStr, 'D_Forcelog'))|| strcmp(PDERHSStr, 'D_Force_Boundary')
        if  isfield(optsPhysFW.DataInput,'wForce')
          FWCost = optsPhysFW.DataInput.wForce; 
        else
          FWCost = wForceCheb;  
        end 
    elseif  strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext')|| strcmp(PDERHSStr, 'AD_Flowflog') 
        if  isfield(optsPhysFW.DataInput,'wFlow')
          FWCost = optsPhysFW.DataInput.wFlow; 
        else
          FWCost = wFlowCheb; 
        end
    elseif strcmp(PDERHSStr, 'OD')
         if  isfield(optsPhysFW.DataInput,'rho_R')
          FWCost = optsPhysFW.DataInput.rho_R; 
        else
          FWCost = rho_RCheb; 
        end
    end
    
   elapsedtime = toc;
   
%Storing results
    data.rho_t=rho_t;
    data.wFW=FWCost;
    data.CompTimeFW = elapsedtime;
    data.SInt = SInt;
    data.All=this;

    


    %----------------------------------------------------------------------
    % RHS of ODE
    %----------------------------------------------------------------------

    function drhodt = drho_dt(t,rho)
        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(t));
        Interp=InterpT.InterPol;   

        % Extracting Variables 
        
        % if no input matrix for a variable was given, we need the testFun
        % file at the correct time points
        if ~(inputForce) || ~(inputFlow) || ~(inputwForce) || ~(inputwFlow)...
                || ~(inputVext) || ~(inputrho_R)
        testSol = testFun(this,y,t,TMax,beta);
        end

        % if a variable input is given as matrix, we need to interpolate
        % onto the correct time, otherwise extract the value from the
        % testFun file
        if(inputOD)
            if(inputForce)
            Force = Interp*ForceIn;
            else
            Force = testSol.Force;
            end
            if(inputrho_R)                
                rho_R = Interp*rho_RIn;
                rho_R = rho_R';
            elseif(inputODValue)
                rho_R = rho_RIn; 
            else
                rho_R = testSol.rho_R;
            end
           
        else
            if(inputForce)
                Force = Interp*ForceIn;
            else
                Force = testSol.Force;
            end
            if(inputFlow)
                Flow = Interp*FlowIn;
            else
                Flow = testSol.Flow;
            end
            if(inputwForce)
                wForce = Interp*wForceIn;
            else
                wForce = testSol.wForce;
            end
            if(inputwFlow)
                wFlowI = Interp*wFlowIn;
                wFlow = wFlowI';
            else
                wFlow = testSol.wFlow;
            end
            if(inputVext)
                Vext = Interp*VextIn;
            else
                Vext = testSol.Vext;
            end
            gradVext = Dy*Vext; 
        end
         
        
        % Computing Convolution term
        ConvV2FW = Interact2BodyFW(rho,Conv);
        if(inputOD)
            
            ConvV2FW_R = Interact2BodyFW_R(rho,rho_R,Conv);
        end

     % Computing RHS
        
        % Inputs that change during the ODE solve are stored in a structure
        % to hand to the RHS file
        if(inputOD)
            RHSInput.ConvV2FW_R = ConvV2FW_R;
            RHSInput.rho = rho;
            RHSInput.ConvV2FW = ConvV2FW; 
            RHSInput.Force = Force;
        else
            RHSInput.rho = rho;
            RHSInput.ConvV2FW = ConvV2FW;        
            RHSInput.Flow = Flow;
            RHSInput.wFlow = wFlow;
            RHSInput.wForce = wForce;
            RHSInput.Force = Force;
            RHSInput.gradVext = gradVext;
        
        end
        
        RHS = ComputeFWPDE(this, RHSInput);
        rhoflux = RHS.rhoflux;
        drhodt = RHS.drhodt;
        
        % Computing BCs
        if(strcmp(optsPhysFW.ProbSpecs.BCFunStr, 'ComputeNeumannLRBCs'))
            
           drhodt(bound) = BCFun(rho, rhoflux, bound, normal,Ind,wForce, this);
        else
        drhodt(bound) = BCFun(rho, rhoflux, bound, normal, this);
        end
    end


end

