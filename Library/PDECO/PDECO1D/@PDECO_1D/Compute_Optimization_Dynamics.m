function sol = Compute_Optimization_Dynamics(this)
    tic;
    optsNum=this.optsNum;
    optsPhys=this.optsPhys;
    
    %---------------------------------------------------------------------%
    % Extracting Parameters                                               %
    %---------------------------------------------------------------------%
    PDERHSStr = optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
    SolverFlag = optsPhys.optsPhysOpt.OptSolver.SolverFlag;
    ComputeNorm=str2func(optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
    
    FunTol = optsPhys.optsPhysOpt.OptSolver.OptTols.FunTol;
    OptiTol = optsPhys.optsPhysOpt.OptSolver.OptTols.OptiTol;
    StepTol = optsPhys.optsPhysOpt.OptSolver.OptTols.StepTol;
    
    N = optsNum.PhysArea.N;
    n = optsNum.TimeArea.n;
    beta = optsPhys.optsPhysFW.Params.beta;

    aLine = this.IDC;
    optsNum.aLine = aLine;  
    PlotArea = optsNum.PlotArea;

    [Pts,Diff,SInt,Ind,~] = aLine.ComputeAll(PlotArea); 
    
    rhoMask = 1:N;
    pMask = N+1:2*N;   
    Dy = Diff.Dy;
    Conv = this.Conv;
    
    %----------------------------------------------------------------------
    % Time Discretization; Chebyshev times, Interpolation Line
    %----------------------------------------------------------------------
    InterLine= this.TDC;
    times = InterLine.Pts.y;   
    %----------------------------------------------------------------------
    % Extracting Initial guesses
    %----------------------------------------------------------------------
    rhoExa = zeros(n,N);
    pExa = zeros(n,N);
    
    if strcmp(SolverFlag, 'fsolve') || strcmp(SolverFlag, 'Picard')
    rhoExa(1,:) = this.DataIn.rhoIC;
    rhoExa(2:n, :) = this.DataIn.OptirhoIG(2:n, :);
    pExa(1:n-1, :) = this.DataIn.OptipIG(1:n-1, :);
    pExa(n,:) = this.DataIn.pIC;
    solExa = [rhoExa(2:n, :) pExa(1:n-1, :)];
    end
   %-----------------------------------------------------------------------
   % Depending on the solver choice we need to extract information differently
   %-----------------------------------------------------------------------
    
    if strcmp(SolverFlag, 'fsolve')
        disp('Solving Optimization problem with fsolve ...');
        options = optimoptions('fsolve','FunctionTolerance',FunTol,'OptimalityTolerance',OptiTol,'StepTolerance',StepTol,'Display','iter');
        %data =fsolve(@error,solExa,options);
        [data,~,exitflag,output] = fsolve(@error,solExa,options);
        Iterations = output.iterations;
    elseif strcmp(SolverFlag, 'Picard')
        
        dataAll = ComputePicardShooting(this,n,solExa,rhoExa,pExa,times,rhoMask,pMask,Pts,Diff,Ind);
        data = dataAll.opt1; 
        Iterations = dataAll.optj;
        
    elseif strcmp(SolverFlag, 'FixPt') 
        dataAll = ComputeFixedPointAlgorithm(this,Diff,Ind,Pts,rhoMask,pMask);
        Iterations = dataAll.Iter;
        converge = dataAll.converge;
        timeTaken = dataAll.timeTaken;
        data = [dataAll.rhoNum, dataAll.pNum];
        armijo = dataAll.armijo;
        
        
   elseif strcmp(SolverFlag, 'FixPtActiveSet') 
        dataAll = ComputeFixedPoint_ActiveSetAlgorithm(this,Diff,Ind,Pts,rhoMask,pMask);
        Iterations = dataAll.Iter;
        converge = dataAll.converge;
        data = [dataAll.rhoNum, dataAll.pNum];
        if strcmp(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver, 'ArmijoWolfe')||strcmp(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver, 'ArmijoWolfeJump')
            armijo = dataAll.armijo;
        end
    end
    
   %-----------------------------------------------------------------------
   % Output
   %-----------------------------------------------------------------------

    if strcmp(SolverFlag, 'FixPtActiveSet') || strcmp(SolverFlag, 'FixPt')
        rhoNum = data(:,rhoMask);
        pNum = data(:,pMask);
    else
        rhoNum(1,:) = this.DataIn.rhoIC;
        pNum(n,:) = this.DataIn.pIC;
        rhoNum (2:n,:)= data(:,rhoMask);
        pNum(1:n-1,:) = data(:,pMask);
    end

    % Evaluating Gradient Equation to output correct control term.
    if strcmp(SolverFlag, 'FixPtActiveSet') 
        Control = dataAll.wNum;
        Control = Control';
    elseif strcmp(PDERHSStr, 'D_Force') || strcmp(PDERHSStr, 'AD_Force')|| strcmp(PDERHSStr, 'AD_Forcefl') 
      Control =-pNum'/beta; 
    elseif  strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext') || (strcmp(PDERHSStr, 'AD_Flowflog'))
      Control =-((Dy*pNum').*rhoNum')/beta;  
    elseif strcmp(PDERHSStr, 'OD')
        Control = zeros(n,N);
        for k=1:n
              Control(k,:)= -(Interact2BodyAdj2(rhoNum(k,:)',pNum(k,:)',Conv,Dy))/beta;
        end
        Control = Control';
    end

    elapsedtime = toc;
    
    sol.rhoNum=rhoNum;
    sol.pNum=pNum;
    sol.Control =Control;
    sol.Iterations = Iterations;
    sol.CompTimeOpti = elapsedtime;
    if  strcmp(SolverFlag, 'FixPt') || strcmp(SolverFlag, 'FixPtActiveSet')
    sol.converge = converge;    
    sol.timeTaken = timeTaken;
    sol.armijo = armijo;
     
    end
    if strcmp(SolverFlag, 'fsolve')
        sol.output = output;
        sol.exitflag = exitflag;
    end
    sol.All=this;
   
   % function for fsolve to minimize
   function sol = error(initial_Sol)
   
       rhoIExa = zeros(n,N);
       pIExa = zeros(n,N);
       rhoIExa(2:n,:) = initial_Sol(:,rhoMask);
       pIExa(1:n-1,:) = initial_Sol(:,pMask);
       rhoIExa(1,:) = rhoExa(1,:);
       pIExa(n,:) =pExa(n,:);
   
       sol = ComputeMultipleShooting(this,n,rhoIExa,pIExa,times,rhoMask,pMask,Pts,Diff,Ind);

   end

end