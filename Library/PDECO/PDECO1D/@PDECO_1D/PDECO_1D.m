classdef PDECO_1D < Computation
    properties (Access = public)
       Conv = []
       ForwardResult = []
       OptimizationResult = []
       ExactSolution = []
       DataIn = []
       TDC = []
       Errors=[]        
    end
    
    methods (Access = public)
        
        function this  = PDECO_1D(configuration)
            if (nargin == 0)
                configuration = [];
            end
            this@Computation(configuration);
            Preprocess(this);
            PreprocessTime(this);
        end
        
        function Preprocess(this)
            Preprocess@Computation(this);
            disp('Preprocess is working');
        end
        
        function PreprocessTime(this)
            disp('PreprocessTime is working');
            geom.yMin = this.optsNum.TimeArea.t0; 
            geom.yMax = this.optsNum.TimeArea.TMax; 
            geom.N = this.optsNum.TimeArea.n;
            this.TDC= SpectralLine(geom);
        end
        
        function getExactSolution(this)  
            % Computes the exact solution from the input test funtion file
            % and stores them in a structure
            % only does this if specifically asked to; otherwise the code
            % assumes you don't have an EXACT solution but just some inputs
            % in your test file
            
                %flag for opinion dynamics problems
                inputOD = isfield(this.optsPhys.optsPhysFW.DataInput,'OD');
                
                times = this.TDC.Pts.y;
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                rhoIC = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                OptirhoIG = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                pIC = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                OptipIG = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                rhoTarget = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                if(inputOD)
                    rho_R = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                    rho_R_IG = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                    Force = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                else
                    wForce = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                    Vext = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                    Force = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                    wFlow = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                    Flow = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
                end
                for iT = 1:this.optsNum.TimeArea.n
                    ExactSol = testFun(this,this.IDC.Pts.y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                    rhoIC(iT,:) = ExactSol.rhoIC;
                    OptirhoIG(iT,:) = ExactSol.OptirhoIG;
                    pIC(iT,:) = ExactSol.pIC;
                    OptipIG(iT,:) = ExactSol.OptipIG;
                    rhoTarget(iT,:) = ExactSol.rhoTarget;
                    if(inputOD)
                        rho_R(iT,:) = ExactSol.rho_R;
                        rho_R_IG(iT,:) = ExactSol.rho_R_IG;
                        Force(iT,:) = ExactSol.Force;
                    else
                        wForce(iT,:) = ExactSol.wForce;
                        Vext(iT,:) = ExactSol.Vext;
                        Force(iT,:) = ExactSol.Force;
                        wFlow(iT,:) = ExactSol.wFlow;
                        Flow(iT,:) = ExactSol.Flow;
                    end
                end
                ExaSol.rhoIC=rhoIC;
                ExaSol.OptirhoIG=OptirhoIG;
                ExaSol.pIC=pIC;
                ExaSol.OptipIG = OptipIG;
                ExaSol.rhoTarget=rhoTarget;
                if(inputOD)
                    ExaSol.rho_R = rho_R;
                    ExaSol.rho_R_IG = rho_R_IG;
                    ExaSol.Force=Force;
                else
                    ExaSol.wForce=wForce;
                    ExaSol.Vext=Vext;
                    ExaSol.Force=Force;
                    ExaSol.wFlow=wFlow;
                    ExaSol.Flow=Flow;
                end
                this.ExactSolution = ExaSol;
        end   
        
        function getForwardIC(this)
            % Getting the initial condition for the forward problem, either
            % from the testFun file or as a vector
            disp('getForwardIC');
            if  ~isfield(this.optsPhys.optsPhysFW.DataInput,'rhoIC')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                ExactFWSol = testFun(this, this.IDC.Pts.y,0,this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                this.DataIn.rhoIC = ExactFWSol.rhoIC;        
            else
                this.DataIn.rhoIC = this.optsPhys.optsPhysFW.DataInput.rhoIC;
            end   
        end
        function getBackwardIC(this)
            % getting the final time condition for the adjoint. In this
            % case only possible from testFun file -- could be extended to
            % take direct input
            disp('getBackwardIC');
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                ExactBWSol = testFun(this,this.IDC.Pts.y,this.optsNum.TimeArea.TMax,this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                this.DataIn.pIC = ExactBWSol.pIC;
        end
        
        function ForwardDynamics(this)
            % Computes forward dynamics/ checks if all needed inputs are
            % there/else computes them
            if  ~isfield(this.optsPhys.optsPhysFW.DataInput,'rhoIC')   
                getForwardIC(this)
            else 
                this.DataIn.rhoIC = this.optsPhys.optsPhysFW.DataInput.rhoIC;
            end

            if isempty(this.Conv)
                % Compute convolutions
                getConvMatrix(this)
            end
            if isfield(this.optsPhys.optsPhysFW.DataInput, 'DataRecompFW') && ((this.optsPhys.optsPhysFW.DataInput.DataRecompFW == true))
                DataRecompFW = true;
            else
                DataRecompFW = false;
            end
            % here we check if the code should ignore datastorage (in case
            % you want to check an answer but not delete your results
            %(two separate flags - one for the
            % convolution, one for the forward problem))
            if isfield(this.optsPhys.optsPhysFW.DataInput, 'DataRecompConv')
            this.optsPhys.optsPhysFW.DataInput = rmfield(this.optsPhys.optsPhysFW.DataInput,'DataRecompConv');
            end
            if isfield(this.optsPhys.optsPhysFW.DataInput, 'DataRecompFW')
            this.optsPhys.optsPhysFW.DataInput = rmfield(this.optsPhys.optsPhysFW.DataInput,'DataRecompFW');
            end
            opts.optsNum = this.optsNum;
            opts.optsPhysFW = this.optsPhys.optsPhysFW;
            data = DataStorage(['ForwardData' filesep class(this.IDC) filesep this.optsPhys.optsPhysFW.DataInput.testFun], @Forward_Dynamics, opts, this,[],[],[],DataRecompFW);        
            this.ForwardResult = data.ForwardResult;
        end

        
        function getOptimizationIG(this)
            % getting the optimization inital guess. Only needed for Picard
            % and fsolve/ not for fixed point method
            OptirhoIG = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
            OptipIG = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
            times = this.TDC.Pts.y;
            % checking where to get rhoIG from (either testFun file, a
            % given input matrix or from running the forward problem)
            if ~isfield(this.optsPhys.optsPhysOpt.OptDataInput,'OptirhoIG')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                for iT = 1:this.optsNum.TimeArea.n
                ExactSol = testFun(this,this.IDC.Pts.y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                OptirhoIG(iT,:) = ExactSol.OptirhoIG;
                end
                this.DataIn.OptirhoIG = OptirhoIG;
            elseif (strcmp(this.optsPhys.optsPhysOpt.OptDataInput.OptirhoIG, 'FWrho')) 
                if  isempty(this.ForwardResult)
                ForwardDynamics(this)       
                end
                this.DataIn.OptirhoIG = this.ForwardResult.rho_t;
            else
                this.DataIn.OptirhoIG = this.optsPhys.optsPhysOpt.OptDataInput.OptirhoIG;
            end
            % pIG can be calculated from the testFun file or is given as
            % input matrix
            if ~isfield(this.optsPhys.optsPhysOpt.OptDataInput,'OptipIG')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                for iT = 1:this.optsNum.TimeArea.n
                ExactSol = testFun(this,this.IDC.Pts.y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                OptipIG(iT,:) = ExactSol.OptipIG;
                this.DataIn.OptipIG = OptipIG;
                end
            else
                this.DataIn.OptipIG = this.optsPhys.optsPhysOpt.OptDataInput.OptipIG;
            end
        end
        
        function getRhoTarget(this)
            % getting rhoHat, either form testFun file or from input matrix
            rhoTarget = zeros(this.optsNum.TimeArea.n,this.optsNum.PhysArea.N);
            times = this.TDC.Pts.y;
            if ~isfield(this.optsPhys.optsPhysOpt.OptDataInput,'rhoTarget')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                for iT = 1:this.optsNum.TimeArea.n
                ExactSol = testFun(this,this.IDC.Pts.y,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                rhoTarget(iT,:) = ExactSol.rhoTarget;
                end
                this.DataIn.rhoTarget = rhoTarget;
            elseif(strcmp(this.optsPhys.optsPhysOpt.OptDataInput.rhoTarget, 'FWrho'))
                if  isempty(this.ForwardResult)
                    ForwardDynamics(this)       
                end
                this.DataIn.rhoTarget = this.ForwardResult.rho_t;   
            else
                this.DataIn.rhoTarget = this.optsPhys.optsPhysOpt.OptDataInput.rhoTarget;
            end
        end
        
        function getConvMatrix(this)
           %computes convolution matrix
           disp('getConvMatrix');
           this.optsNum.V2Num.Pts= this.IDC.Pts;
           %check whether to ignore data storage or not
           if isfield(this.optsPhys.optsPhysFW.DataInput, 'DataRecompConv') && ((this.optsPhys.optsPhysFW.DataInput.DataRecompConv== true))
                DataRecompConv = true;
           else
                DataRecompConv = false;
           end
           ConvStruct = DataStorage(['ConvData' filesep class(this.IDC) filesep this.optsPhys.optsPhysFW.DataInput.testFun],@ConvMatrices,this.optsNum.V2Num,this.IDC,[],[],[], DataRecompConv);     
           Conv1= ConvStruct.Conv;
           this.Conv=Conv1;
        end
        

        function OptimizationDynamics(this)
            % computing the optimization
            % checks first whether the inputs are there.
            if ~isfield(this.DataIn,'rhoIC') 
                getForwardIC(this)  
            elseif isfield(this.DataIn,'rhoIC') && isempty(this.DataIn.rhoIC)
                getForwardIC(this)  
            end
            getBackwardIC(this)
            
            if strcmp(this.optsPhys.optsPhysOpt.OptSolver.SolverFlag, 'fsolve') || strcmp(this.optsPhys.optsPhysOpt.OptSolver.SolverFlag, 'Picard')
            getOptimizationIG(this)
            end
            
            getRhoTarget(this)

            if isempty(this.Conv)
                getConvMatrix(this)
            end
            opts.optsNum = this.optsNum;
            opts.optsPhys = this.optsPhys;
            
            % checks whether to ignore data storage results
            if isfield(this.optsPhys.optsPhysOpt.OptDataInput, 'DataRecompOpt') && ((this.optsPhys.optsPhysOpt.OptDataInput.DataRecompOpt == true))
                DataRecompOpt = true;
            else
                DataRecompOpt = false;
            end
            opts.optsNum = this.optsNum;
            opts.optsPhys = this.optsPhys;
            this.optsPhys.optsPhysOpt.OptDataInput = rmfield(this.optsPhys.optsPhysOpt.OptDataInput,'DataRecompOpt');
            data = DataStorage(['OptimizationData' filesep class(this.IDC) filesep this.optsPhys.optsPhysFW.DataInput.testFun], @Optimization_Dynamics, opts, this,[],[],[],DataRecompOpt);        
            
            this.OptimizationResult = data.OptimizationResult;
        end
        
  
        function ComputeForwardError(this)
            if isempty(this.ForwardResult)
                ForwardDynamics(this)
            end
            this.TDC.ComputeIntegrationVector;
            % Compute the cost functional (and each individual term of it)
            % for the forward problem
            FWError.J1FW = ComputeJ1(this.ForwardResult.rho_t, this.DataIn.rhoTarget, this.IDC.Int, this.TDC.Int);
            FWError.J2FW = ComputeJ2(this.ForwardResult.wFW, this.IDC.Int, this.TDC.Int);
            FWError.JFW = ComputeCostFunctional(this.ForwardResult.rho_t, this.DataIn.rhoTarget, this.ForwardResult.wFW, ...
                                                this.optsPhys.optsPhysFW.Params.beta, this.IDC.Int, this.TDC.Int);

            if ~isempty(this.ExactSolution)
                % if an exact solution exists, compute exact errors
                % (depends on whether you ask to 'getExactSolution' or not
                ComputeNorm=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
                FWError.rhoExactErr = ComputeNorm(this.ForwardResult.rho_t, this.ExactSolution.rhoIC, this.IDC.Int, this.TDC.Int);
            end
            this.Errors.ForwardError = FWError;
        end
        
        function ComputeOptimizationError(this)
            if isempty(this.OptimizationResult)
                OptimizationDynamics(this)
            end
            this.TDC.ComputeIntegrationVector;
            % computes cost functionals and each term of it for the
            % optimized result
            OptiError.J1Opti = ComputeJ1(this.OptimizationResult.rhoNum, this.DataIn.rhoTarget,  this.IDC.Int, this.TDC.Int);
            OptiError.J2Opti = ComputeJ2(this.OptimizationResult.Control', this.IDC.Int, this.TDC.Int);
            OptiError.JOpti = ComputeCostFunctional(this.OptimizationResult.rhoNum, this.DataIn.rhoTarget,...
                                                    this.OptimizationResult.Control', this.optsPhys.optsPhysFW.Params.beta, this.IDC.Int, this.TDC.Int);
            OptiError.All=this;
            if ~isempty(this.ExactSolution)
                % computing exact errors if the exact solution exists
                ComputeNorm=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
                OptiError.rhoExactErr = ComputeNorm(this.OptimizationResult.rhoNum, this.ExactSolution.OptirhoIG, this.IDC.Int, this.TDC.Int);
                OptiError.pExactErr = ComputeNorm(this.OptimizationResult.pNum, this.ExactSolution.OptipIG,  this.IDC.Int, this.TDC.Int);
                
                % for the control it depends on the control type, so need
                % the if statement here
                PDERHSStr = this.optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
                if strcmp(PDERHSStr, 'D_Force') || strcmp(PDERHSStr, 'AD_Force')|| strcmp(PDERHSStr, 'AD_Forcefl') 
                    OptiError.wExactErr = ComputeNorm(this.OptimizationResult.Control', this.ExactSolution.wForce,  this.IDC.Int, this.TDC.Int); 
                elseif  strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext') || (strcmp(PDERHSStr, 'AD_Flowflog'))
                    OptiError.wExactErr = ComputeNorm(this.OptimizationResult.Control', this.ExactSolution.wFlow,  this.IDC.Int, this.TDC.Int);
                elseif  strcmp(PDERHSStr, 'OD')
                    OptiError.wExactErr = ComputeNorm(this.OptimizationResult.Control', this.ExactSolution.rho_R_IG,  this.IDC.Int, this.TDC.Int);
                end                
            end
            this.Errors.OptimizationError = OptiError;
        end
        
        function ComputeErrors(this)
            % computing both errors
            ComputeForwardError(this);
            ComputeOptimizationError(this);
        end
         
        function plotResults(this,optsPlot)
            % plotting specified results according to the inputs of the
            % plotting function
            plotResult(this,optsPlot);
        end
         
%         function plotForwardResult(this)
%             rhoFF = this.ForwardResult.rho_t;
%             fhFW = figure();
%             for iT = 1:this.optsNum.TimeArea.n  
%                 rhoF=rhoFF(iT,:)';       
%                 this.IDC.plot(rhoF);
%                 pause(0.1);
%                 xlim([-1,1]);
%                 title('Forward rho');
%             end 
%          end
%         
%         
%          function plotForwardControl(this)
%             wFF= this.ForwardResult.FWCost;
%             fhFWC = figure();
%             for iT = 1:this.optsNum.TimeArea.n  
%                 wF=wFF(iT,:)';       
%                 this.IDC.plot(wF);
%                 pause(0.1);
%                 xlim([-1,1]);
%                 title('Forward Cost');
%             end 
%          end
%         
%          function plotOptimizationResult(this)
%             rhoEE = this.OptimizationResult.rhoNum;
%             fhOPT = figure();
%             for iT = 1:this.optsNum.TimeArea.n  
%                 rhoE=rhoEE(iT,:)';       
%                 this.IDC.plot(rhoE);
%                 pause(0.1);
%                 xlim([-1,1]);
%                 title('Optimal rho');
%             end 
%          end
%         
%         
%          function plotOptimizationControl(this)
%             wEE= this.OptimizationResult.Control';
%             fhOPTC = figure();
%             for iT = 1:this.optsNum.TimeArea.n  
%                 wE=wEE(iT,:)';       
%                 this.IDC.plot(wE);
%                 pause(0.1);
%                 xlim([-1,1]);
%                 title('Optimal Cost');
%             end 
%          end
%         
        function ComputeAll(this)
%            getExactSolution(this)
            getForwardIC(this)
            getOptimizationIG(this)
            getRhoTarget(this)
            getConvMatrix(this)
            ForwardDynamics(this)
            OptimizationDynamics(this)
%            plotResults(this,this.optsPlot)
%             plotForwardResult(this)
%             plotOptimizationResult(this)
%             plotForwardControl(this)
%             plotOptimizationControl(this)
            ComputeErrors(this)
        end
    
    end

end