function Norm = ComputeL1Norm(MNew, MOld, SInt, TInt)

Norm = TInt*(SInt*abs(MNew-MOld)')';

end