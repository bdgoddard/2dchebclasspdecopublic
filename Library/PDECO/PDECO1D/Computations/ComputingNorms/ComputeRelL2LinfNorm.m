function RelNorm = ComputeRelL2LinfNorm(MNew, MOld, SInt, TInt)
MDiff = abs(MNew - MOld);

L2MDiff = (SInt*(MDiff.^2)').^(1/2);
L2MOld = (SInt*((MOld+10^-10).^2)').^(1/2);

% Take either relative or absolute error in space.
L2Rel = L2MDiff./L2MOld;
L2Err = min(L2Rel,L2MDiff);

RelNorm = max(L2Err); 
end