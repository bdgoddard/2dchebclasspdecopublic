function RelNorm = ComputeRelPWNorm(MNew, MOld, SInt, TInt)
MDiff = MNew - MOld;

NormDiff = max(abs(MDiff));
NormOld = max(abs(MOld));

mask1 = NormOld <10^-10;
mask2 = NormDiff < 10^-10;
NormOld(mask1)=0;
NormDiff(mask1)=0;
NormOld(mask2)=0;
NormDiff(mask2)=0;

RelNorm = max(NormDiff)/max(NormOld +10^-10);
end