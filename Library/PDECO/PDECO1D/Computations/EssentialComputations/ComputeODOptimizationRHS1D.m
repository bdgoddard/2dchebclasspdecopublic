 function rhop_t = ComputeODOptimizationRHS1D(T0,Tf,IC,rhoExa,pExa,Pts,Diff,Ind,wIn,muIn,this)
    
    optsNum=this.optsNum;
    optsPhys=this.optsPhys;
    %---------------------------------------------------------------------%
    % Extracting Parameters                                                     %
    %---------------------------------------------------------------------%
    SolverFlag=optsPhys.optsPhysOpt.OptSolver.SolverFlag;
    target = this.DataIn.rhoTarget;

    AbsTol=optsNum.Tols.AbsTol;
    RelTol=optsNum.Tols.RelTol;  

    beta=optsPhys.optsPhysFW.Params.beta;
    gamma=optsPhys.optsPhysFW.Params.gamma;
    D0=optsPhys.optsPhysFW.Params.D0;
    N=optsNum.PhysArea.N;
    n=optsNum.TimeArea.n;
    
    t0=optsNum.TimeArea.t0;
    TMax=optsNum.TimeArea.TMax;
    
    % Combining the PDE RHS indicator string with the correct prefix to get
    % the RHS functions for rho and p named by the naming convention
    PDERHSStr = optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
    PDErhsStr = strcat("ComputeFW",PDERHSStr);
    ComputeOptrhoPDE = str2func(PDErhsStr);
    PDEpStr = strcat("ComputeOptp",PDERHSStr);
    ComputeOptpPDE = str2func(PDEpStr);
    
    % here it is checked whether we give a different boundary condition to
    % p (from rho). This can happen for example for non-zero Dirichlet
    % boundary conditions
    BCFun=str2func(optsPhys.optsPhysFW.ProbSpecs.BCFunStr);
    
    if isfield(optsPhys.optsPhysOpt.OptSolver,'BCFunOptStr') && ~isempty(optsPhys.optsPhysOpt.OptSolver.BCFunOptStr)
        BCFunp=str2func(optsPhys.optsPhysOpt.OptSolver.BCFunOptStr);
    else
        BCFunp = BCFun;
    end
    testFunStr=this.optsPhys.optsPhysFW.DataInput.testFun;
    testFun=str2func(testFunStr);

    y=Pts.y; 
    Dy=Diff.Dy;
    DDy=Diff.DDy; 
    bound=Ind.bound;
    normal = Ind.normal;
    rhoMask = 1:N;
    pMask = N+1:2*N;     
    
    InterLine= this.TDC;
    times = InterLine.Pts.y;
    
    %----------------------------------------------------------------------
    % Computing Convolution Matrix and Datastoring it
    %----------------------------------------------------------------------
    Conv = this.Conv;
    
    %----------------------------------------------------------------------
    % Extracting Variables
    %----------------------------------------------------------------------

    Flowflog = strcmp(this.optsPhys.optsPhysFW.ProbSpecs.PDERHSStr, 'AD_Flowflog');
     
    FixPtActiveSet = (strcmp(SolverFlag, 'FixPtActiveSet')); 
    FixPt = (strcmp(SolverFlag, 'FixPt')); 
    Picard = (strcmp(SolverFlag, 'Picard'));
    fsolve = (strcmp(SolverFlag, 'fsolve'));
    inputForce = isfield(this.optsPhys.optsPhysFW.DataInput,'Force');
    if(inputForce)
        ForceIn = this.optsPhys.optsPhysFW.DataInput.Force;
    end
    inputFlow = isfield(this.optsPhys.optsPhysFW.DataInput,'Flow');
    if(inputFlow)
        FlowIn = this.optsPhys.optsPhysFW.DataInput.Flow;
    end
    inputVext = isfield(this.optsPhys.optsPhysFW.DataInput,'Vext');
    if(inputVext)
        VextIn = this.optsPhys.optsPhysFW.DataInput.Vext;
    end
    inputrho_R = isfield(this.optsPhys.optsPhysFW.DataInput,'rho_R');
    if(inputrho_R)
        rho_RIn = this.optsPhys.optsPhysFW.DataInput.rho_R;
    end
    
    
    %flag for opinion dynamics problems
    inputOD = isfield(this.optsPhys.optsPhysFW.DataInput,'OD');
    if(inputOD)
        inputODValue = optsPhys.optsPhysFW.DataInput.OD;
    end
    
    % General inputs for rho RHS that don't change in ODE solve (rho)
    % (differentiation matrices and parameters)
        rhoRHSInput.Dy = Dy;
        rhoRHSInput.DDy = DDy;
        rhoRHSInput.gamma = gamma;
        rhoRHSInput.D0 = D0;
    % General inputs for RHS that don't change in ODE solve (p)
    % (differentiation matrices and parameters)
        pRHSInput.Dy = Dy;
        pRHSInput.DDy = DDy;
        pRHSInput.gamma = gamma;
        pRHSInput.D0 = D0;
    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------
    tMax = Tf;
    t0 = T0;
    
    %----------------------------------------------------------------------
    % Find initial condition
    %----------------------------------------------------------------------
    if strcmp(this.optsPhys.optsPhysFW.ProbSpecs.PDERHSStr, 'AD_Flowflog')
        rho0 = log(IC(rhoMask));
    else
        rho0 = IC(rhoMask);
    end
        p0 = IC(pMask);
    
        SInt = this.IDC.Int;
        
    
    
    if(inputOD)
        if ~isfield(this.optsPhys.optsPhysOpt.OptDataInput,'OptirhoIG')
        C = 1/(SInt*rho0);
        M_rho = this.optsPhys.optsPhysFW.Params.M_rho;
        rho0 = M_rho*C*rho0;
        
        
        
        end
    end
    
    %scaling non-time dependent target in opinion dynamics problem
    if(inputOD)
        if isfield(this.optsPhys.optsPhysFW.DataInput,'scaletarget')
            if this.optsPhys.optsPhysFW.DataInput.scaletarget
                M_tar = this.optsPhys.optsPhysFW.DataInput.M_tar;
            C_hat = 1/(SInt*target(1,:)');
            for iT = 1:n
                target(iT,:) = M_tar*C_hat* target(iT,:);
            end
            end  
        end
    end
    %---------------------------------------------------------------------%
    % Solve the PDE                                                       %
    %---------------------------------------------------------------------%
     if  (strcmp(SolverFlag, 'FixPt') || strcmp(SolverFlag, 'ActiveSet')|| strcmp(SolverFlag, 'FixPtActiveSet'))
        outTimes = times; % the fixed point algorithm calculates the solution at the chebyshev points
     else
         outTimes = t0:(tMax-t0)/2:tMax; % each multiple shooting step only computes 3 time points (and first and last are extracted)
     end
    
    mM  = ones(size(y));
    mM(bound)   = 0; 

    opts = odeset('RelTol',RelTol,'AbsTol',AbsTol,'Mass',diag(mM));
    [outTimes,rho_t] = ode15s(@drho_dt,outTimes,rho0,opts);
    
    [outTimes,p_t] = ode15s(@dp_dt,outTimes,p0,opts);
  
    
     rhop_t =  [rho_t, p_t];

    %----------------------------------------------------------------------
    % RHS of ODE
    %----------------------------------------------------------------------


     function drhodt = drho_dt(t,rho)
        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(t));
        InterpFW=InterpT.InterPol;
        
        % Setting up control input dependent on the solver
        if  (FixPt)
            % Control is directly given as input for this solver
            wInI = InterpFW*wIn;
            wForceCurrent = wInI';
            wFlowCurrent = wInI';
            rho_RCurrent = wInI';
        
        elseif (FixPtActiveSet)
            % Control is directly given as input for this solver
            wInI = InterpFW*wIn;
            wForceCurrent = wInI';
            wFlowCurrent = wInI';
            rho_RCurrent = wInI';
            muInI = InterpFW*muIn;
            muInCurrent = muInI';
            
        elseif (Picard) || (fsolve)
            % Control is found by interpolating rhoIG, pIG and evaluating the
            % gradient eqation
            p1=InterpFW*pExa;
            pCurrent=p1';
            rho1=InterpFW*rhoExa;
            rhoCurrent=rho1';
            % note: each gradient equation that differs from the below needs 
            % it's unique name (even if it corresponds to the same control type)
            wForceCurrent = - pCurrent/beta; 
            wFlowCurrent = -(Dy*pCurrent).*rhoCurrent/beta;
            rho_RCurrent = -(Interact2BodyAdj2(rhoCurrent,pCurrent,Conv,Dy))/beta;
        end

        % Extracting Variables, checking whether inputs are in matrix form
        % or supplied through the testFun file
        if ~(inputForce) || ~(inputFlow) || ~(inputVext) || ~(inputrho_R)
        testSol=testFun(this,y,t,TMax,beta); % forward time
        end
        
        % Setting up active sets for active set method
        if(FixPtActiveSet)
            rhoA = testSol.rhoAIG;
            rhoB = testSol.rhoBIG;
            if(inputOD)
                maskA_MinusCurrent = (rho_RCurrent - muInCurrent)<rhoA;
                maskA_PlusCurrent = (rho_RCurrent - muInCurrent)>rhoB;
                maskA_ICurrent = ~(or(maskA_MinusCurrent , maskA_PlusCurrent)); 
            else
               maskA_MinusCurrent = (wForceCurrent - muInCurrent)<rhoA;
                maskA_PlusCurrent = (wForceCurrent - muInCurrent)>rhoB;
                maskA_ICurrent = ~(or(maskA_MinusCurrent , maskA_PlusCurrent)); 
            end
            
        end
      
        
        if(inputOD)
            if(inputForce)
                Force = InterpFW*ForceIn;
            else
                Force = testSol.Force;
            end
        else
            if(inputForce)
                Force = InterpFW*ForceIn;
            else
                Force = testSol.Force;
            end
            if(inputFlow)
                Flow = InterpFW*FlowIn;
            else
                Flow = testSol.Flow;
            end
            if(inputVext)
                Vext = InterpFW*VextIn;
            else
                Vext = testSol.Vext;
            end

            gradVext = Dy*Vext; 
        end
        
        % Computing Convolution term
        ConvV2FW = Interact2BodyFW(rho,Conv);
        if(inputOD)
             if(FixPtActiveSet)
                 ConvV2FW_R_Plus = Interact2BodyFW_R(rho,rhoB,Conv);
                 ConvV2FW_R_Minus = Interact2BodyFW_R(rho,rhoA,Conv);
                 ConvV2FW_R = Interact2BodyFW_R(rho,rho_RCurrent,Conv);
             else
            ConvV2FW_R = Interact2BodyFW_R(rho,rho_RCurrent,Conv);
             end
        end
        
        % Computing RHS

        % Inputs to RHS that change during the ODE solve 
        if(inputOD)
            rhoRHSInput.ConvV2FW_R = ConvV2FW_R;
            rhoRHSInput.rho = rho;
            rhoRHSInput.ConvV2FW = ConvV2FW; 
            rhoRHSInput.Force = Force;
        else
            rhoRHSInput.rho = rho;
            rhoRHSInput.ConvV2FW = ConvV2FW;
            rhoRHSInput.Flow = Flow;
            rhoRHSInput.wFlow = wFlowCurrent;
            rhoRHSInput.wForce = wForceCurrent;
            rhoRHSInput.Force = Force;
            rhoRHSInput.gradVext = gradVext;
        end
        
        if(FixPtActiveSet)
            rhoRHSInput.rhoA = rhoA;
            rhoRHSInput.rhoB = rhoB;
            rhoRHSInput.maskA_Minus = maskA_MinusCurrent;
            rhoRHSInput.maskA_Plus = maskA_PlusCurrent;
            rhoRHSInput.maskA_I = maskA_ICurrent;
            %rhoRHSInput.solver = 'ActiveSet';
            if(inputOD)
                rhoRHSInput.ConvV2FW_R_Plus = ConvV2FW_R_Plus;
                rhoRHSInput.ConvV2FW_R_Minus = ConvV2FW_R_Minus;
            end
        end
        
        
        rhoRHS = ComputeOptrhoPDE(this, rhoRHSInput);
        rhoflux = rhoRHS.rhoflux;
        drhodt = rhoRHS.drhodt;

        % Computing BCs 
        
        drhodt(bound) = BCFun(rho, rhoflux, bound, normal, this);
        
     end
 
  function dpdt = dp_dt(t,p)
        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(tMax+t0-t));
        InterpBW=InterpT.InterPol;

        % Setting up control input dependent on the solver
        % Setting up rho input dependent on the solver
        if  (FixPt)
                % Taking rho from solving rho PDE as input.                 
                rho2=InterpBW*rho_t; 
                rhoLater=rho2';
                % Taking Control as direct input (if flow control - else its a waste).
                wInIBw = InterpBW*wIn;
                wFlowLater = wInIBw';
                wForceLater = wInIBw';
                rho_RLater =  wInIBw';               
                rhoHat2= InterpBW*target;
                
        elseif  (FixPtActiveSet)
                % Taking rho as direct  input.                
                rho2=InterpBW*rho_t; 
                rhoLater=rho2';                
                % Taking Control as direct input (if flow control - else its a waste).
                wInIBw = InterpBW*wIn;
                wFlowLater = wInIBw';
                wForceLater = wInIBw';
                rho_RLater =  wInIBw';               
                rhoHat2= InterpBW*target;
                muInIBw = InterpBW*muIn;
                muInLater = muInIBw';
        elseif (Picard) || (fsolve)
            % Taking rho, p from rhoIG, pIG and interpolate.
            rho2=InterpBW*rhoExa;
            rhoHat2= InterpBW*target;
            p2=InterpBW*pExa;
            rhoLater=rho2';
            pLater = p2';
            wFlowLater = -(Dy*pLater).*rhoLater/beta;
            rho_RLater = -(Interact2BodyAdj2(rhoLater,p,Conv,Dy))/beta;
        end
     
        % Extracting Variables
        rhoHat = rhoHat2';
   
        if ~(inputForce) || ~(inputFlow) || ~(inputVext) || ~(inputrho_R)
        testSolBw=testFun(this,y,tMax-t+ t0,TMax,beta); %backward time 
        end
        
        
         % Setting up active sets for active set method
        if(FixPtActiveSet)
            rhoA = testSolBw.rhoAIG;
            rhoB = testSolBw.rhoBIG;
            if(inputOD)
                maskA_MinusLater = (rho_RLater - muInLater)<rhoA;
                maskA_PlusLater = (rho_RLater - muInLater)>rhoB;
                maskA_ILater = ~(or(maskA_MinusLater , maskA_PlusLater)); 
            else
               maskA_MinusLater = (wForceLater - muInLater)<rhoA;
                maskA_PlusLater = (wForceLater - muInLater)>rhoB;
                maskA_ILater = ~(or(maskA_MinusLater , maskA_PlusLater)); 
            end
            
        end
        
        
        if~(inputOD)
            
            if(inputFlow)
                FlowBw = InterpBW*FlowIn;
            else
                FlowBw = testSolBw.Flow;
            end
            
            if(inputVext)
                VextBw = InterpBW*VextIn;
            else
                VextBw = testSolBw.Vext;
            end
            gradVextBw = Dy*VextBw;
        else
            
            
        end
        
        % Evaluating Convolution terms
        ConvV2BW1 = Interact2BodyAdj1(rhoLater,Conv);        
        ConvV2BW2 = Interact2BodyAdj2(rhoLater,p,Conv,Dy);
        if (inputOD)
            if(FixPtActiveSet)
                ConvV2BW_R_Plus = Interact2BodyAdj1(rhoB,Conv);
                ConvV2BW_R_Minus = Interact2BodyAdj1(rhoA,Conv);
                ConvV2BW_R = Interact2BodyAdj1(rho_RLater,Conv);
            else
                ConvV2BW_R = Interact2BodyAdj1(rho_RLater,Conv);
            end
        end

        % Computing RHS
        % Inputs that change during the ODE solve
        if(inputOD)
            pRHSInput.p = p;
            pRHSInput.rhoLater = rhoLater;
            pRHSInput.rhoHat = rhoHat;
            pRHSInput.ConvV2BW1 = ConvV2BW1;
            pRHSInput.ConvV2BW2 = ConvV2BW2;
            pRHSInput.ConvV2BW_R = ConvV2BW_R;
        else
            pRHSInput.p = p;
            pRHSInput.rhoLater = rhoLater;
            pRHSInput.rhoHat = rhoHat;
            pRHSInput.ConvV2BW1 = ConvV2BW1;
            pRHSInput.ConvV2BW2 = ConvV2BW2;
            pRHSInput.FlowBw = FlowBw;
            pRHSInput.wFlowLater = wFlowLater;
            pRHSInput.wForceLater = wForceLater;
            pRHSInput.gradVextBw = gradVextBw;
        end
        
        if(FixPtActiveSet)
            rhoRHSInput.rhoA = rhoA;
            rhoRHSInput.rhoB = rhoB;
            rhoRHSInput.maskA_Minus = maskA_MinusLater;
            rhoRHSInput.maskA_Plus = maskA_PlusLater;
            rhoRHSInput.maskA_I = maskA_ILater;
            %rhoRHSInput.solver = 'ActiveSet';
            if(inputOD)
                rhoRHSInput.ConvV2BW_R_Plus = ConvV2BW_R_Plus;
                rhoRHSInput.ConvV2BW_R_Minus = ConvV2BW_R_Minus;
            end
        end
        
        pRHS = ComputeOptpPDE(this,pRHSInput);
        
        dpdt = pRHS.dpdt;
        pflux = pRHS.pflux;

        % Computing BCs (may be the same or different from rho BCs)     
        dpdt(bound) = BCFunp(p, pflux, bound, normal, this);
  end
 end
