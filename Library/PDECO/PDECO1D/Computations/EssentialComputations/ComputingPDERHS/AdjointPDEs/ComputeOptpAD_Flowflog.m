function dpdt = ComputeOptpAD_Flowflog(p, Dy, DDy, D0, gamma, ConvV2BW1, ConvV2BW2, rhoLater, rhoHat, FlowBw, wFlowLater, gradVextBw, this, otheropts)

    dpdt =  D0*DDy*p + rhoLater - rhoHat + wFlowLater.*(Dy*p) - (gamma*ConvV2BW1).*(Dy*p) + gamma*ConvV2BW2;    

end