function output = ComputeOptpAD_Forcefl(this,pRHSInput)
    Dy = pRHSInput.Dy;
    DDy = pRHSInput.DDy;
    gamma = pRHSInput.gamma;
    D0 = pRHSInput.D0;
    p = pRHSInput.p;
    rhoLater = pRHSInput.rhoLater;
    rhoHat = pRHSInput.rhoHat;
    ConvV2BW1 = pRHSInput.ConvV2BW1;
    ConvV2BW2 = pRHSInput.ConvV2BW2;
    gradVextBw = pRHSInput.gradVextBw;
    FlowBw = pRHSInput.FlowBw;
    
    dpdt =  D0*DDy*p + rhoLater - rhoHat  - gradVextBw.* (Dy*p)+ FlowBw.*(Dy*p) - (gamma*ConvV2BW1).*(Dy*p) + gamma*ConvV2BW2; 
    pflux = Dy*p;

    output.dpdt = dpdt;
    output.pflux = pflux;
end