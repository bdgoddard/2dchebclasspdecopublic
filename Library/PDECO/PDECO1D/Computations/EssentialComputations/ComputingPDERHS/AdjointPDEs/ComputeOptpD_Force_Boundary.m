function output = ComputeOptpD_Force_Boundary(this,pRHSInput)
    Dy = pRHSInput.Dy;
    DDy = pRHSInput.DDy;
    gamma = pRHSInput.gamma;
    D0 = pRHSInput.D0;
    p = pRHSInput.p;
    rhoLater = pRHSInput.rhoLater;
    rhoHat = pRHSInput.rhoHat;
    ConvV2BW1 = pRHSInput.ConvV2BW1;
    ConvV2BW2 = pRHSInput.ConvV2BW2;
    
    dpdt =  D0*DDy*p - rhoLater + rhoHat ;
    pflux = Dy*p;

    output.dpdt = dpdt;
    output.pflux = pflux;
end