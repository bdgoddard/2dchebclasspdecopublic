function output = ComputeFWAD_Flow(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    wFlow = RHSInput.wFlow;
    Dy = RHSInput.Dy;
    gamma = RHSInput.gamma;
    D0 = RHSInput.D0;
    
    rhoflux =  -(D0*Dy*rho - rho.*wFlow  + gamma*ConvV2FW);   
    drhodt  = -Dy*rhoflux;   

    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end