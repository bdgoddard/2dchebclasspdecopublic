function output = ComputeFWAD_FlowfVext(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    wFlow = RHSInput.wFlow;
    Force = RHSInput.Force;
    Dy = RHSInput.Dy;
    gamma = RHSInput.gamma;
    D0 = RHSInput.D0;
    gradVext = RHSInput.gradVext;

    rhoflux =  -(D0*Dy*rho + rho.*gradVext - (rho.*wFlow) + gamma*ConvV2FW);        
    drhodt  = -Dy*rhoflux + Force ;

    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end