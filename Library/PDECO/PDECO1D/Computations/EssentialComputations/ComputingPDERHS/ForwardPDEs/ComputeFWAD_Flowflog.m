function output = ComputeFWAD_Flowflog(h, Dy, DDy, D0, gamma, ConvV2FW, Flow, wFlow, Force, wForce, gradVext, this, otheropts)

    drhodt  = D0*DDy*h + (Dy*h).^2 - Dy*wFlow - (Dy*h).*wFlow + Force.*exp(-h);
    rhoflux = exp(h).*(D0*Dy*h - wFlow);

    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end