function output = ComputeFWAD_Force_AS(this,RHSInput)
    rho = RHSInput.rho;
    rhoA = RHSInput.rhoA;
    ConvV2FW = RHSInput.ConvV2FW;
    wForce = RHSInput.wForce;
    Force = RHSInput.Force;
    Dy = RHSInput.Dy;
    gamma = RHSInput.gamma;
    D0 = RHSInput.D0;
    gradVext = RHSInput.gradVext;
    maskA_Minus = RHSInput.maskA_Minus;
    maskA_I = RHSInput.maskA_I;
    
    rhoflux =  -(D0*Dy*rho + rho.*gradVext  + gamma*ConvV2FW);       
    drhodt  = -Dy*rhoflux +  Force ;
    drhodt(maskA_I)  =  drhodt(maskA_I)+ wForce(maskA_I) ;
    drhodt(maskA_Minus)  =  drhodt(maskA_Minus)+ rhoA(maskA_Minus) ;
    
    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end