function output = ComputeFWAD_Forcefl(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    wForce = RHSInput.wForce;
    Force = RHSInput.Force;
    Flow = RHSInput.Flow;
    Dy = RHSInput.Dy;
    gamma = RHSInput.gamma;
    D0 = RHSInput.D0;
    gradVext = RHSInput.gradVext;
    
    rhoflux = -(D0*Dy*rho + rho.*gradVext - (rho.*Flow) + gamma*ConvV2FW);      
    drhodt  = -Dy*rhoflux + wForce + Force;

    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end