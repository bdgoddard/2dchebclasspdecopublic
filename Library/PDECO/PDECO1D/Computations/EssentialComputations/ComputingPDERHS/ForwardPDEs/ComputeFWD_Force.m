function output = ComputeFWD_Force(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    wForce = RHSInput.wForce;
    Dy = RHSInput.Dy;
    gamma = RHSInput.gamma;
    D0 = RHSInput.D0;
    Force = RHSInput.Force;
    
    if isfield(RHSInput,'solver')
        rhoA = RHSInput.rhoA;
        rhoB = RHSInput.rhoB;
        maskA_Minus = RHSInput.maskA_Minus;
        maskA_Plus = RHSInput.maskA_Plus;
        maskA_I = RHSInput.maskA_I;
    
        rhoflux =  -(D0*Dy*rho + gamma*ConvV2FW);      
        drhodt  = -Dy*rhoflux + Force ;
        drhodt(maskA_I)  =  drhodt(maskA_I)+ wForce(maskA_I) ;
        drhodt(maskA_Minus)  =  drhodt(maskA_Minus)+ rhoA(maskA_Minus) ;
        drhodt(maskA_Plus)  =  drhodt(maskA_Plus)+ rhoB(maskA_Plus) ;
    else
    
    rhoflux =  -(D0*Dy*rho + gamma*ConvV2FW);       
    drhodt  = -Dy*rhoflux + wForce + Force ;
    end
    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end