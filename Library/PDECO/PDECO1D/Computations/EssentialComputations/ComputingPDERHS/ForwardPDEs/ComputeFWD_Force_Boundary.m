function output = ComputeFWD_Force_Boundary(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    wForce = RHSInput.wForce;
    Dy = RHSInput.Dy;
    gamma = RHSInput.gamma;
    D0 = RHSInput.D0;
    
    
    
    rhoflux =  -(D0*Dy*rho);       
    drhodt  = -Dy*rhoflux ;
    
    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end