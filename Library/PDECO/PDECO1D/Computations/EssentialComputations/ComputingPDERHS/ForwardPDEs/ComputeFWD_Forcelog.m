function output = ComputeFWD_Forcelog(h, Dy, DDy, D0, gamma, ConvV2FW, Flow, wFlow, Force, wForce, gradVext, this, otheropts)

    drhodt  = D0*DDy*h + (Dy*h).^2 + wForce.*exp(-h);
    rhoflux = exp(h).*(D0*Dy*h );

    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end