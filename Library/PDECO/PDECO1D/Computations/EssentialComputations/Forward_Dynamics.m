function data = Forward_Dynamics(opts,aPDECO_1D)
 
    ForwardSol= aPDECO_1D.Compute_Forward_Dynamics(); 
    data.ForwardResult=ForwardSol;
    
end

