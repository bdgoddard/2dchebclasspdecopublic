     function g = ComputeGradGaussian(y,V2params)
        alpha= V2params.alpha;
        g= -2*y.*exp(- y.^2) * alpha;
    end