
    function f = Interact2BodyFW(rho,Conv)
        f = rho.*(Conv* rho);
        
    end