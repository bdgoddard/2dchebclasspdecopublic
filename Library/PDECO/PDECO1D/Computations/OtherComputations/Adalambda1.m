function lambda = Adalambda1(rhop_Err,lambda)
if rhop_Err > 1
    lambda = 0.1;
elseif rhop_Err > 10^-1
    lambda = 0.2;
elseif rhop_Err > 10^-5
    lambda = 1;
else 
    lambda = 10;
% elseif rhop_Err > 10^-3 
%     lambda = 0.5;
% elseif rhop_Err > 10^-5
%     lambda = 0.7;

end