function Jfun = ComputeCostFunctional(rho, rhoHat, w, beta, SInt, TInt)
J1 = TInt*(SInt*((rho - rhoHat)'.^2))'; 
J2 = TInt*(SInt*(w.^2)')'; 

Jfun = (1/2)*J1 + (beta/2)*J2;
end