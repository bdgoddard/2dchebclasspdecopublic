function J1 = ComputeJ1(rho, rhoHat, SInt, TInt)

J1 = TInt*(SInt*(((rho - rhoHat).^2)'))'; 
end