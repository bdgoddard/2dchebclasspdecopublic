function J2 = ComputeJ2(w, SInt, TInt)

J2 = TInt*(SInt*(w.^2)')'; 
end