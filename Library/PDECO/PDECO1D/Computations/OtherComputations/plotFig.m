function plotFig(this,Var,optsP,varOpts, plotTimes,i,j,k)
    % Interpolates onto the plotting times
    InterpT = this.TDC.ComputeInterpolationMatrix(this.TDC.CompSpace(plotTimes'));
    Interp=InterpT.InterPol; 
    IVar = Interp*Var;
    varnumber = isfield(varOpts,'number');
    if varnumber
        IVar2= varOpts.Var2;
    end
    
    subplot(i,j,k);
    lgd = legend('Location','northeastoutside','interpreter','latex');
    
   for iT = 1:length(plotTimes)  
        %Specifying plotting options
        optsP.plain = true;
        optsP.linestyle = '-';
        optsP.linewidth = 1;
        iStr = num2str(iT);
        optsP.displayname = strcat("t","_{",iStr,"} = ",num2str(plotTimes(iT)));

        hold on
        this.IDC.plot(IVar(iT,:)',optsP);
        if varnumber
            this.IDC.plot(IVar2(iT,:)',optsP);
        end
        %xlim([-1,1]);
        if isfield(varOpts,'yLimit')
            ylim(varOpts.yLimit);
        end
        ylabel(varOpts.yLabel,'interpreter','latex')
        title(varOpts.titleName,'interpreter','latex');
        ax = gca;
        ax.TitleFontSizeMultiplier = 1;
        ax.FontSize = 10;
        set(ax,'TickLabelInterpreter','latex')
    end

 end 