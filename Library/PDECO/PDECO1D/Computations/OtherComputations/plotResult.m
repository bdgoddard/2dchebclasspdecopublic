function plotResult(this,optsPlot)
 
% Extracting Plotting Times
if isfield(optsPlot,'plotTimes')
    plotTimes = optsPlot.plotTimes;
else
    TMax=this.optsNum.TimeArea.TMax;
    t0=this.optsNum.TimeArea.t0;
    plotTimes = linspace(t0,TMax,10);
end
% flag for limits on y-axis
inputyLimit = isfield(optsPlot,'yLimit');


% Checking how many variables are there to plot (up to 8) and set positions
% on subplot accordingly
% VarStr is the input structure taking either flags to indicate which variable to plot and/or a matrix to plot
VarStr = optsPlot.VarStr;
if numel(fieldnames(VarStr)) == 1
    i = 1;
    j = 1;
    hf = figure();
elseif numel(fieldnames(VarStr)) == 2
    i = 1;
    j = 2;
    hf = figure('Position',[50,50,1200,300]);
elseif numel(fieldnames(VarStr)) == 4 || numel(fieldnames(VarStr)) == 3
    i = 2;
    j = 2;
    hf = figure('Position',[50,50,1000,700]);
elseif numel(fieldnames(VarStr)) == 5 || numel(fieldnames(VarStr)) == 6
    i = 2;
    j = 3;
    hf = figure('Position',[1,1,3000,450]);
elseif numel(fieldnames(VarStr)) == 7 || numel(fieldnames(VarStr)) == 8
    i = 3;
    j = 3;
    hf = figure('Position',[1,1,2000,800]);
end

% Specifying for each variable which title and yLabel to use and giving it
% the location on the subplot
k = 0;
    %hf = figure(10);
    colorOrder = linspecer(length(plotTimes),'sequential');
    set(hf,'defaultAxesColorOrder',colorOrder);
if isfield (VarStr, 'rhoIC')==1
    varOpts.titleName = 'Initial Condition for $\rho$';
    varOpts.yLabel = '$\rho$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    for it = 1:this.optsNum.TimeArea.n
    Var(it,:) = this.DataIn.rhoIC;
    end
    k = VarStr.rhoIC;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
    
    
end
if isfield (VarStr, 'OptirhoIG')==1
    varOpts.titleName = 'Initial Guess for $\rho$';
    varOpts.yLabel = '$\rho$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.DataIn.OptirhoIG;
    k = VarStr.OptirhoIG;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'pIC')==1
    varOpts.titleName = 'Initial Condition for $p$';
    varOpts.yLabel = '$p$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    for it = 1:this.optsNum.TimeArea.n
    Var(it,:) = this.DataIn.pIC;
    end
    k = VarStr.pIC;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'OptipIG')==1
    varOpts.titleName = 'Initial Guess for $p$';
    varOpts.yLabel = '$p$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.DataIn.OptipIG;
    k = VarStr.OptipIG;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'rhoFW')==1
    varOpts.titleName = '$\rho_{uc}$';%'Forward $\rho$';
    varOpts.yLabel = '$\rho$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.ForwardResult.rho_t;
    k = VarStr.rhoFW;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'rhoFWwFW')==1
    varOpts.titleName = '$\rho_{uc}$ and forward $w$';
    varOpts.yLabel = '$\rho$';
    varOpts.number = 2;
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.ForwardResult.rho_t;
    varOpts.Var2 = this.ForwardResult.wFW;
    k = VarStr.rhoFWwFW;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
    
    
end
if isfield (VarStr, 'rhoOpt')==1
    varOpts.titleName = '$\rho_{c}$';%'Optimal $\rho$';
    varOpts.yLabel = '$\rho$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.OptimizationResult.rhoNum;
    k = VarStr.rhoOpt;    
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'pOpt')==1
    varOpts.titleName = 'Optimal $p$';
    varOpts.yLabel = '$p$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.OptimizationResult.pNum;
    k = VarStr.pOpt;    
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'wFW')==1
     Var = this.ForwardResult.wFW;
     mass = this.ForwardResult.SInt *Var(2,:)';
    varOpts.titleName = strcat('Forward Control, mass = ', num2str(mass));
    varOpts.yLabel = '$w$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
   
    k = VarStr.wFW;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'wOpt')==1
    varOpts.titleName = 'Optimal Control ';
    varOpts.yLabel = '$w$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.OptimizationResult.Control';
    k = VarStr.wOpt;    
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'rhoHat')==1
    varOpts.titleName = '$\hat\rho$';
    varOpts.yLabel = '$\rho$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = this.DataIn.rhoTarget;
    k = VarStr.rhoHat;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end


if isfield (VarStr, 'Force')==1     
    if isfield(this.optsPhys.DataInput,'Force')==1
       Var = this.optsPhys.DataInput.Force;
    else
       testFun = str2func(this.optsPhys.DataInput.testFun);
       times = this.TDC.Pts.y;
       for l = 1:length(times)
            ExactSol = testFun(this,this.IDC.Pts.y,times(l),this.optsNum.TimeArea.TMax,this.optsPhys.Params.beta);
            Var(l,:) = ExactSol.Force;
       end
    end   
    varOpts.titleName = 'Force';
    varOpts.yLabel = '$f$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    k = VarStr.Force;  
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'Flow') ==1 
    if isfield(this.optsPhys.DataInput,'Flow')==1
       Var = this.optsPhys.DataInput.Flow;
    else
       testFun = str2func(this.optsPhys.DataInput.testFun);
       times = this.TDC.Pts.y;
       for l = 1:length(times)
            ExactSol = testFun(this,this.IDC.Pts.y,times(l),this.optsNum.TimeArea.TMax,this.optsPhys.Params.beta);
            Var(l,:) = ExactSol.Flow;
       end
    end 
    varOpts.titleName = 'Flow';
    varOpts.yLabel = '$fl$';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    k = VarStr.Flow;  
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end
if isfield (VarStr, 'Vext')==1 
    if isfield(this.optsPhys.DataInput,'Vext')
        Var = this.optsPhys.DataInput.Vext;
    else
       testFun = str2func(this.optsPhys.DataInput.testFun);
       times = this.TDC.Pts.y;
       for l = 1:length(times)
            ExactSol = testFun(this,this.IDC.Pts.y,times(l),this.optsNum.TimeArea.TMax,this.optsPhys.Params.beta);
            Var(l,:) = ExactSol.Vext;
       end
    end 
    varOpts.titleName = 'Vext';
    varOpts.yLabel = 'Vext';
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    k = VarStr.Vext;   
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end

% In case there is a matrix input to be plotted
% the field 'Vec' takes the input matrix, the field 'Vec2' takes the
% position k on the plot.
if isfield(VarStr,'Vec')==1
    varOpts.titleName = optsPlot.VecOpts.titleName;
    varOpts.yLabel = optsPlot.VecOpts.yLabel;
    if (inputyLimit)
        varOpts.yLimit = optsPlot.yLimit;
    end
    Var = VarStr.Vec;
    k = VarStr.Vec2;  
    plotFig(this,Var,[],varOpts, plotTimes,i,j,k)
end


  end