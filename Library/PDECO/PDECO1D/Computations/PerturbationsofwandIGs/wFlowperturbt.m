    function wFlow= wFlowperturbt(N,yMin,yMax,plotN,beta,n,t0,TMax,testFunStr,other)
    
    PhysArea = struct('shape','SpectralLine','N',N,'yMin',yMin,'yMax',yMax);
    PlotArea = struct('N',plotN,'yMin',yMin,'yMax',yMax); 
    shapeClass = str2func(PhysArea.shape);
    aLine = shapeClass(PhysArea);
    
    [Pts,Diff,Int,Ind,~] = aLine.ComputeAll(PlotArea); 
    y=Pts.y;    
    
    geom.yMin = t0; 
    geom.yMax = TMax; 
    geom.N = n;
    InterLine= SpectralLine(geom);
    times = InterLine.Pts.y;
    testFun=str2func(testFunStr);

    for iT=1:n
        t=times(iT);  
        exactSol=testFun(other,y,t,TMax,beta); 
        wFlowExa(iT,:)=exactSol.wFlow; 
    end

        wFlowPert = zeros(n,N);

       al= 0.7;
       tm= -0.01;
       
    % Perturbation function in time
    for iT = 1:n
        t = times(iT);
        ft(iT) = (((exp(-al/(t-tm))/(1*(exp(-al/(t-tm)) + exp(-al/(1-t-tm))))))*((exp(al/(t-tm))/(1*(exp(al/(t-tm)) + exp(al/(1-t-tm))))))/2);
    end
     ft2 =ft/(max(abs(ft)));
    % Perturbing w   
    for iT = 1 : n
        wFlowPert(iT, :) = wFlowExa(iT, :)*(1 + 0.1*ft2(iT));
    end
    
    wFlow=wFlowPert;
    end

    
    
    
    