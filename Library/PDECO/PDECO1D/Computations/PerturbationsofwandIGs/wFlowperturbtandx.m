    function wFlow= wFlowperturbtandx(N,yMin,yMax,plotN,beta,n,t0,TMax,testFunStr,other)

    PhysArea = struct('shape','SpectralLine','N',N,'yMin',yMin,'yMax',yMax);
    PlotArea = struct('N',plotN,'yMin',yMin,'yMax',yMax); 
    shapeClass = str2func(PhysArea.shape);
    aLine = shapeClass(PhysArea);
    
    [Pts,Diff,Int,Ind,~] = aLine.ComputeAll(PlotArea); 
    y=Pts.y;    
    
    geom.yMin = t0; 
    geom.yMax = TMax; 
    geom.N = n;
    InterLine= SpectralLine(geom);
    times = InterLine.Pts.y;
    
    testFun=str2func(testFunStr);
    
    for iT=1:n
        t=times(iT);  
        exactSol=testFun(other,y,t,TMax,beta); 
        wFlowExa(iT,:)=exactSol.wFlow; 
    end

       wFlowPert = zeros(n,N);
        
       al= 0.7;
       tm= -0.01;
       
       % computing perturbation in time
       for iT = 1:n
           t = times(iT);
           ft(iT) = (((exp(-al/(t-tm))/(1*(exp(-al/(t-tm)) + exp(-al/(1-t-tm))))))*((exp(al/(t-tm))/(1*(exp(al/(t-tm)) + exp(al/(1-t-tm))))))/2);
       end
       ft2 =ft/(max(abs(ft)));
       
       % computing perturbation in space
       for iy = 1:N
           yi = y(iy);
           fy(iy) = (((exp(-al/(yi-tm+1))/(1*(exp(-al/(yi-tm+1)) + exp(-al/(1-yi-tm))))))*((exp(al/(yi-tm+1))/(1*(exp(al/(yi-tm+1)) + exp(al/(1-yi-tm))))))/2);
       end
       fy2=fy/(max(abs(fy)));
       
       % perturbing w
        for iy = 1:N
            for iT = 1 : n
                wFlowPert(iT, iy) = wFlowExa(iT, iy)*(1 + 0.1*ft2(iT)*fy2(iy));
            end
        end
        
%         figure(1)
%         subplot(1,2,1)
%         plot(y,fy2)
%         title('Perturbation function $\tilde h(x)$','interpreter','latex')
%         xlabel('$x$','interpreter','latex')
%         ylabel('$\tilde h(x)$','interpreter','latex')
%         ax = gca;
%         ax.TitleFontSizeMultiplier = 1;
%         ax.FontSize = 10;
%         set(ax,'TickLabelInterpreter','latex')
%         subplot(1,2,2)
%         plot(times, ft2)
%         title('Perturbation function $\tilde g(t)$','interpreter','latex')
%         %xlim([-1,1]);
%         xlabel('$t$','interpreter','latex')
%         ylabel('$\tilde g(t)$','interpreter','latex')
%         ax = gca;
%         ax.TitleFontSizeMultiplier = 1;
%         ax.FontSize = 10;
%         set(ax,'TickLabelInterpreter','latex')
%         
%         
        
        
    
    wFlow=wFlowPert;
    end

    
    
    
    