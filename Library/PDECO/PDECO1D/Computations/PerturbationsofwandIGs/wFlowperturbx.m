    function wFlow= wFlowperturbx(N,yMin,yMax,plotN,beta,n,t0,TMax,testFunStr)
    
    PhysArea = struct('shape','SpectralLine','N',N,'yMin',yMin,'yMax',yMax);
    PlotArea = struct('N',plotN,'yMin',yMin,'yMax',yMax); 
    shapeClass = str2func(PhysArea.shape);
    aLine = shapeClass(PhysArea);
    
    [Pts,Diff,Int,Ind,~] = aLine.ComputeAll(PlotArea); 
    y=Pts.y;    
    
    geom.yMin = t0; 
    geom.yMax = TMax; 
    geom.N = n;
    InterLine= SpectralLine(geom);
    times = InterLine.Pts.y;
    testFun=str2func(testFunStr);

    for iT=1:n
        t=times(iT);  
        exactSol=testFun([],y,t,TMax,beta); 
        wFlowExa(iT,:)=exactSol.wFlowExact; 
    end

        wFlowPert = zeros(n,N);

       al= 0.7;
       tm= -0.01;
       
    % Perturbation function in time
    for iy = 1:N
         yi = y(iy);
         fy(iy) = (((exp(-al/(yi-tm+1))/(1*(exp(-al/(yi-tm+1)) + exp(-al/(1-yi-tm))))))*((exp(al/(yi-tm+1))/(1*(exp(al/(yi-tm+1)) + exp(al/(1-yi-tm))))))/2);
    end
     fy2=fy/(max(abs(fy)));
    % Perturbing w   
    for iY = 1 : N
        wFlowPert(:,iY) = wFlowExa(:,iY)*(1 + 0.1*fy2(iY));
    end
    
    wFlow=wFlowPert;
    end

    
    
    
    