function data= Compute_Forward_Dynamics2D(this)

     disp('Solving Forward problem ...');
     tic;
     optsPhysFW=this.optsPhys.optsPhysFW;
     optsNum=this.optsNum;  
     
    %---------------------------------------------------------------------%
    % Extracting Parameters                                                     %
    %---------------------------------------------------------------------%
    beta=optsPhysFW.Params.beta;
    gamma=optsPhysFW.Params.gamma;
    D0 = optsPhysFW.Params.D0;
    other = optsPhysFW.Params.other;
    AbsTol=optsNum.Tols.AbsTol;
    RelTol=optsNum.Tols.RelTol;
     
    %time
    n=optsNum.TimeArea.n;
    TMax=optsNum.TimeArea.TMax;
    
    SpaceBox = this.IDC;
    [Pts,Diff,Int,Ind] = SpaceBox.ComputeAll(); 
   
    y1_kv=SpaceBox.Pts.y1_kv;
    y2_kv=SpaceBox.Pts.y2_kv;
    
    Lap= Diff.Lap;
    Grad =Diff.grad;
    Div = Diff.div;
    bound = Ind.bound;
    normal = Ind.normal;
   
    InterLine= this.TDC;
    times = InterLine.Pts.y;
    InterLine.ComputeIntegrationVector;
    
    Conv = this.Conv;
    
    % taking the string specifying the PDE problem and merging it into the
    % function name (given the naming convention)
    PDERHSStr = optsPhysFW.ProbSpecs.PDERHSStr;
    PDErhsStr = strcat("ComputeFW2D",PDERHSStr);
    ComputeFW2DPDE = str2func(PDErhsStr);
    
    BCFun=str2func(optsPhysFW.ProbSpecs.BCFunStr);
    ComputeNorm=str2func(optsPhysFW.ProbSpecs.ComputeNormStr);
    testFun=str2func(optsPhysFW.DataInput.testFun);
  
    % Extracting needed data
    rho_ic = this.DataIn.rhoIC;
    
    %flag for opinion dynamics problems
    inputOD = isfield(optsPhysFW.DataInput,'OD');
    if(inputOD)
        inputODValue = optsPhysFW.DataInput.OD;
    end
    %rescaling mass of initial condition to 1
    if(inputOD)
        M_rho = this.optsPhys.optsPhysFW.Params.M_rho; %parameter for scaling rho_R
        C = 1/(Int*rho_ic);
        rho_ic = M_rho*C*rho_ic;
        this.DataIn.rhoIC = rho_ic;
        
    end
  
    %----------------------------------------------------------------------
    % Input for ODE RHS
    %----------------------------------------------------------------------
    
    inputForce = isfield(optsPhysFW.DataInput,'Force');
    if(inputForce)
        ForceIn = optsPhysFW.DataInput.Force;
    end
    inputFlow = isfield(optsPhysFW.DataInput,'Flow');
    if(inputFlow)
        FlowInVec = optsPhysFW.DataInput.Flow;
    end
    inputwForce = isfield(optsPhysFW.DataInput,'wForce');
    if(inputwForce)
        wForceIn = optsPhysFW.DataInput.wForce;
    end
    inputwFlow = isfield(optsPhysFW.DataInput,'wFlow');
    if(inputwFlow)
        wFlowInVec = optsPhysFW.DataInput.wFlow;
    end
    
    inputVext = isfield(optsPhysFW.DataInput,'Vext');
    if(inputVext)
        VextIn = optsPhysFW.DataInput.Vext;
    end
    
    inputrho_R = isfield(optsPhysFW.DataInput,'rho_R');
    if(inputrho_R)
        rho_RIn = optsPhysFW.DataInput.rho_R;
    end
    
    if(inputOD)
    if(inputODValue)
        M = this.optsPhys.optsPhysFW.Params.M; %parameter for scaling rho_R
        %scaling non-time dependent control rho_R in opinion dynamics problem 
            
         for i=1:n       
             SolPts =testFun(this,y1_kv, y2_kv,times(i),TMax,beta);
             rho_RIn(i,:) = SolPts.rho_R';       
             C_R = 1/(Int*rho_RIn(i,:));
             rho_RIn(i,:) = M*C_R*rho_RIn(1,:); 
                    
        end
     end

    end
    
    % RHS input that does not change in loop
        RHSInput.Div = Div;
        RHSInput.Grad = Grad;
        RHSInput.Lap = Lap;
        RHSInput.D0 = D0;
        RHSInput.gamma = gamma;

    
    %---------------------------------------------------------------------%
    % Solve the PDE                                                       %
    %---------------------------------------------------------------------%
    %outTimes = 0:1/50:1;
    mM  = ones(size(y1_kv));
    mM(bound)   = 0;    
    opts = odeset('RelTol',RelTol,'AbsTol',AbsTol,'Mass',diag(mM));
    
    [outTimes, rho_t] = ode15s(@drho_dt,times,rho_ic,opts);   
  
    
    
    wForceCheb = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
    rho_RCheb = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
    z1 = zeros(this.optsNum.TimeArea.n,length(this.IDC.Pts.y1_kv));
    z2(:,:,1) = z1;
    z2(:,:,2) = z1;
    wFlowCheb = z2;
    
    if ~strcmp(PDERHSStr, 'OD')
    for iT = 1:n
        SolChebPts =testFun(this,y1_kv, y2_kv,times(iT),TMax,beta); 
        wForceCheb(iT,:) = SolChebPts.wForce;
        wFlowCheb(iT,:,:) = SolChebPts.wFlow;
    end
    else
        for iT = 1:n
        SolChebPts =testFun(this,y1_kv, y2_kv,times(iT),TMax,beta); 
        rho_RCheb(iT,:) = SolChebPts.rho_R;
        end
        
    end
    
  % Outputting the correct 'control'.
    if strcmp(PDERHSStr, 'D_Force') ||  strcmp(PDERHSStr, 'AD_Force')|| strcmp(PDERHSStr, 'AD_Forcefl') 
        if  isfield(this.optsPhys.optsPhysFW.DataInput,'wForce')
          FWCost = this.DataIn.wForce; 
        else
          FWCost = wForceCheb;  
        end 
    elseif  strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext') 
        if  isfield(this.DataIn,'wFlow')
           w = this.DataIn.wFlow;            
           FWCost = [w(:,:,1),w(:,:,2)];
        else
           w = wFlowCheb; 
           FWCost = [w(:,:,1),w(:,:,2)];
        end
     elseif  strcmp(PDERHSStr, 'OD') 
        if  isfield(this.DataIn,'rho_R')      
           FWCost = this.DataIn.rho_R;
        elseif(inputODValue)      
                FWCost = rho_RIn;
        else
            FWCost = rho_RCheb;
        end                 
            
     else
            
           w = rho_RCheb; 
           FWCost = [w(:,:,1),w(:,:,2)];
        
    end

    %%%% outputs
    elapsedtime = toc;
    
    data.rho_t=rho_t;
    data.FWCost=FWCost;
    data.CompTimeFW = elapsedtime;
    data.All=this;

    
    %----------------------------------------------------------------------
    % RHS of ODE
    %----------------------------------------------------------------------

    function drhodt = drho_dt(t,rho)
        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(t));
        Interp=InterpT.InterPol;   
        
        rho2= [rho;rho];
        
        % Extract Variables
        if ~(inputForce) || ~(inputFlow) || ~(inputwForce) || ~(inputwFlow)...
                || ~(inputVext)
        testSol = testFun(this,y1_kv,y2_kv,t,TMax,beta);
        end
        % choosing variables depending on whether they are given directly
        % or computed via the testFun file
        
        if(inputOD)
            if(inputForce)
            Force = Interp*ForceIn;
            else
            Force = testSol.Force;
            end
            if(inputrho_R)                
                rho_R = Interp*rho_RIn;
                rho_R = rho_R';
            elseif(inputODValue)
                rho_R = rho_RIn; 
            else
                rho_R = testSol.rho_R;
                
            end
           
        else
        if(inputForce)
            Force = Interp*ForceIn;
        else
            Force = testSol.Force;
        end
         
        if(inputFlow)
            Flow= (Interp*FlowInVec(:,:))';
        else
            FlowVec = testSol.Flow;
            Flow= [FlowVec(:,1);FlowVec(:,2)];
        end
        if(inputwForce)
            wForce = Interp*wForceIn;
        else
            wForce = testSol.wForce;
        end
        if(inputwFlow)
            wFlow= (Interp*wFlowInVec(:,:))';
        else
            wFlowVec = testSol.wFlow;
            wFlow= [wFlowVec(:,1);wFlowVec(:,2)];
        end
        if(inputVext)
            Vext = Interp*VextIn;
        else
            Vext = testSol.Vext;
        end
        gradVext = Grad*Vext; 
        end
        
        % Compute Convolution term
        
        if(inputOD)
            ConvV2FW = Interact2BodyFW2D(rho,Conv);
            ConvV2FW_R = Interact2BodyFW_R2D(rho,rho_R,Conv);
        else
            ConvV2FW = Interact2BodyFW2D(rho,rho2, Conv,Grad);
        end
 

        % Computing RHS 
        % Inputs for RHS that change in ODE solve
        if(inputOD)
            RHSInput.ConvV2FW_R = ConvV2FW_R;
            RHSInput.rho = rho;
            RHSInput.rho2 = rho2;
            RHSInput.ConvV2FW = ConvV2FW; 
            RHSInput.Force = Force;
        else
        RHSInput.rho = rho;
        RHSInput.rho2 = rho2;
        RHSInput.ConvV2FW = ConvV2FW;
        RHSInput.Flow = Flow;
        RHSInput.wFlow = wFlow;
        RHSInput.Force = Force;
        RHSInput.wForce = wForce;
        RHSInput.gradVext = gradVext;
        end
        
        
        RHS = ComputeFW2DPDE(this,RHSInput);
        rhoflux = RHS.rhoflux;
        drhodt = RHS.drhodt;
        
        % Computing BCs
        drhodt(bound) = BCFun(rho, rhoflux, bound, normal, this);
    end
end

