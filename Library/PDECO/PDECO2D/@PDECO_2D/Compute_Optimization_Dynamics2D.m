function sol = Compute_Optimization_Dynamics2D(this)
    tstart = tic;
    optsNum=this.optsNum;
    optsPhys=this.optsPhys;
    
    %---------------------------------------------------------------------%
    % Extracting Parameters                                                     %
    %---------------------------------------------------------------------%
    PDERHSStr = optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
    SolverFlag = optsPhys.optsPhysOpt.OptSolver.SolverFlag;
    ComputeNorm=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
    
    FunTol = optsPhys.optsPhysOpt.OptSolver.OptTols.FunTol;
    OptiTol = optsPhys.optsPhysOpt.OptSolver.OptTols.OptiTol;
    StepTol = optsPhys.optsPhysOpt.OptSolver.OptTols.StepTol;
    
    N1=this.IDC.N1;
    N2=this.IDC.N2;
    
    n = optsNum.TimeArea.n;
    beta = optsPhys.optsPhysFW.Params.beta;
    
    Conv = this.Conv;

    SpaceBox = this.IDC;
    [Pts,Diff,Int,Ind] = SpaceBox.ComputeAll();    
    Grad = Diff.grad;
    y1_kv=Pts.y1_kv;
    y2_kv=Pts.y2_kv; 
    N=length(y1_kv);
    
    rhoMask = 1:length(y1_kv);
    pMask = length(y1_kv)+1:2*length(y1_kv);   

    
    %----------------------------------------------------------------------
    % Time Discretization; Chebyshev times, Interpolation Line
    %----------------------------------------------------------------------
    InterLine= this.TDC;
    times = InterLine.Pts.y;
     
    %----------------------------------------------------------------------
    % Extracting Initial guesses
    %----------------------------------------------------------------------

    if strcmp(SolverFlag, 'fsolve') || strcmp(SolverFlag, 'Picard')
    rhoExa = zeros(n,N);
    pExa = zeros(n,N);
    rhoExa(1,:) = this.DataIn.rhoIC;
    rhoExa(2:n, :) = this.DataIn.OptirhoIG(2:n, :);
    pExa(1:n-1, :) = this.DataIn.OptipIG(1:n-1, :);
    pExa(n,:) = this.DataIn.pIC;
    solExa = [rhoExa(2:n, :) pExa(1:n-1, :)];
    end
   %-----------------------------------------------------------------------
   % Extracting information given different solvers
   %-----------------------------------------------------------------------

    if strcmp(SolverFlag, 'fsolve')
        disp('Solving Optimization problem with fsolve ...');
        options = optimoptions('fsolve','FunctionTolerance',FunTol,'OptimalityTolerance',OptiTol,'StepTolerance',StepTol,'Display','iter');
        [data,~,exitflag,output] = fsolve(@error,solExa,options);
        Iterations = output.iterations;
    elseif strcmp(SolverFlag, 'Picard')
        
        dataAll = ComputePicardShooting(this,n,solExa,rhoExa,pExa,times,rhoMask,pMask,Pts,Diff,Ind);
        data = dataAll.opt1; 
        Iterations = dataAll.optj;
        
    elseif strcmp(SolverFlag, 'FixPt') 
        
        dataAll = ComputeFixedPointAlgorithm(this,Diff,Ind,Pts,rhoMask,pMask);
        Iterations = dataAll.Iter;
        diverged = dataAll.converge;
        data = [dataAll.rhoNum, dataAll.pNum];
    end
    
   %-----------------------------------------------------------------------
   % Output
   %-----------------------------------------------------------------------

    
    
    rhoNum (:,:)= data(:,rhoMask); %   rhoNum (2:n,:)
    pNum(:,:) = data(:,pMask); %pNum(1:n-1,:)
    rhoNum(1,:) = this.DataIn.rhoIC;
    pNum(n,:) = this.DataIn.pIC;

    % Computing the control, given the PDE problem.
    if strcmp(PDERHSStr, 'D_Force') || strcmp(PDERHSStr, 'AD_Force')|| strcmp(PDERHSStr, 'AD_Forcefl') 
      Control =-pNum/beta; 
    elseif  strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext')
      rhoNum2= [rhoNum,rhoNum];
      Control2 =-(((Grad*pNum').*rhoNum2'))/beta; 
      Control = Control2';
    elseif strcmp(PDERHSStr, 'OD')
        
        w = zeros(n,N);
        for iT=1:n
            w(iT,:)= -(Interact2BodyAdj22D(rhoNum(iT,:)',pNum(iT,:)',Conv,Grad))/beta;
        end
        Control = w;
    end

    elapsedtime = toc(tstart);
    
    sol.rhoNum=rhoNum;
    sol.pNum=pNum;
    sol.Control =Control;
    sol.Iterations = Iterations;
    sol.CompTimeOpti = elapsedtime;
    if strcmp(SolverFlag, 'FixPt')
        sol.diverged = diverged;
    end
    if strcmp(this.optsPhys.optsPhysOpt.OptSolver.AdaSolver, 'ArmijoWolfe')
        sol.armijo = dataAll.armijo;
    end
    if strcmp(SolverFlag, 'fsolve')
        sol.output = output;
        sol.exitflag = exitflag;
    end
    sol.All=this;
   
    % error function for fsolve to minimize
    function sol = error(initial_Sol)
       
       rhoIExa = zeros(n,N);
       pIExa = zeros(n,N);
       rhoIExa(2:n,:) = initial_Sol(:,rhoMask);
       pIExa(1:n-1,:) = initial_Sol(:,pMask);
       rhoIExa(1,:) = rhoExa(1,:);
       pIExa(n,:) =pExa(n,:);
   
       sol = ComputeMultipleShooting(this,n,rhoIExa,pIExa,times,rhoMask,pMask,Pts,Diff,Ind);

    end

end