classdef PDECO_2D < Computation
    properties (Access = public)
       Conv = []
       ForwardResult = []
       OptimizationResult = []
       ExactSolution = []
       DataIn = []
       TDC = []
       Errors=[]        
    end
    
    methods (Access = public)
        
        function this  = PDECO_2D(configuration)
            if (nargin == 0)
                configuration = [];
            end
            this@Computation(configuration);
            Preprocess(this);
            PreprocessTime(this);
        end
        
        function Preprocess(this)
            Preprocess@Computation(this);
            disp('Preprocess is working');
        end
        
        function PreprocessTime(this)
            disp('PreprocessTime is working');
            geom.yMin = this.optsNum.TimeArea.t0; 
            geom.yMax = this.optsNum.TimeArea.TMax; 
            geom.N = this.optsNum.TimeArea.n;
            this.TDC= SpectralLine(geom);
        end
        
        function getExactSolution(this)  
            % computing exact solution from testFun file, if specifically
            % called. stores result in structure
            
            %flag for opinion dynamics problems
                inputOD = isfield(this.optsPhys.optsPhysFW.DataInput,'OD');
                
                times = this.TDC.Pts.y;
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                rhoIC = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                OptirhoIG = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                pIC = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                OptipIG = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                rhoTarget = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                if(inputOD)
                    rho_R = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                    rho_R_IG = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                    Force = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                else
                wForce = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                Vext = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                Force = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
                z1 = zeros(this.optsNum.TimeArea.n,length(this.IDC.Pts.y1_kv));
                z2(:,:,1) = z1;
                z2(:,:,2) = z1;
                wFlow = z2;
                Flow = z2;
                end
                for iT = 1:this.optsNum.TimeArea.n
                    ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                    rhoIC(iT,:) = ExactSol.rhoIC;
                    OptirhoIG(iT,:) = ExactSol.OptirhoIG;
                    pIC(iT,:) = ExactSol.pIC;
                    OptipIG(iT,:) = ExactSol.OptipIG;
                    rhoTarget(iT,:) = ExactSol.rhoTarget;
                    if(inputOD)
                        rho_R(iT,:) = ExactSol.rho_R;
                        rho_R_IG(iT,:) = ExactSol.rho_R_IG;
                        Force(iT,:) = ExactSol.Force;
                    else
                    wForce(iT,:) = ExactSol.wForce;
                    Vext(iT,:) = ExactSol.Vext;
                    Force(iT,:) = ExactSol.Force;
                    wFlow(iT,:,:) = ExactSol.wFlow;
                    Flow(iT,:,:) = ExactSol.Flow;
                    end
                end
                
                ExaSol.rhoIC=rhoIC;
                ExaSol.OptirhoIG=OptirhoIG;
                ExaSol.pIC=pIC;
                ExaSol.OptipIG = OptipIG;
                ExaSol.rhoTarget=rhoTarget;
                if(inputOD)
                    ExaSol.rho_R = rho_R;
                    ExaSol.rho_R_IG = rho_R_IG;
                    ExaSol.Force=Force;
                else
                ExaSol.wForce=wForce;
                ExaSol.Vext=Vext;
                ExaSol.Force=Force;
                ExaSol.wFlow=wFlow;
                end
                this.ExactSolution = ExaSol;
        end   
        
        function getForwardIC(this)
            % get forward IC from testFun or as vector directly supplied
            disp('getForwardIC');
            if  ~isfield(this.optsPhys.optsPhysFW.DataInput,'rhoIC')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                ExactFWSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,0,this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                this.DataIn.rhoIC = ExactFWSol.rhoIC;        
            else
                this.DataIn.rhoIC = this.optsPhys.DataInput.rhoIC;
            end   
        end
        function getBackwardIC(this)
            % get final time condition for p either via testFun or as input
            % vector
            disp('getBackwardIC');
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                ExactFWSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,this.optsNum.TimeArea.TMax,this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                this.DataIn.pIC = ExactFWSol.pIC;
        end
        
        function ForwardDynamics(this)
            % computing the forward problem and all needed inputs
            if  ~isfield(this.optsPhys.optsPhysFW.DataInput,'rhoIC')  
                getForwardIC(this)
            else 
                this.DataIn.rhoIC = this.optsPhys.optsPhysFW.DataInput.rhoIC;
            end
            if isempty(this.Conv)
                getConvMatrix(this)
            end
            % check whether to search for stored results or recompute
            if  isfield(this.optsPhys.optsPhysFW.DataInput, 'DataRecompFW') && ((this.optsPhys.optsPhysFW.DataInput.DataRecompFW)==true)
                DataRecompFW = true;
            else
                DataRecompFW = false;
            end
            this.optsPhys.optsPhysFW.DataInput = rmfield(this.optsPhys.optsPhysFW.DataInput,'DataRecompConv');
            this.optsPhys.optsPhysFW.DataInput = rmfield(this.optsPhys.optsPhysFW.DataInput,'DataRecompFW');
            opts.optsNum = this.optsNum;
            opts.optsPhysFW = this.optsPhys.optsPhysFW;
            data = DataStorage(['ForwardData' filesep class(this.IDC) filesep this.optsPhys.optsPhysFW.DataInput.testFun], @Forward_Dynamics2D, opts, this,[],[],[],DataRecompFW);        
            this.ForwardResult = data.ForwardResult;
        end

        
        function getOptimizationIG(this)
            % computing the initial guess for the optimization if using
            % flsove or Picard
            OptirhoIG = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
            OptipIG = zeros(this.optsNum.TimeArea.n, length(this.IDC.Pts.y1_kv));
            times = this.TDC.Pts.y;
            % the inital guess for rho can be from testFun, a matrix input
            % or from the forward problem
            if ~isfield(this.optsPhys.optsPhysFW.DataInput,'OptirhoIG')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                for iT = 1:this.optsNum.TimeArea.n
                ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                OptirhoIG(iT,:) = ExactSol.OptirhoIG;
                end
                this.DataIn.OptirhoIG = OptirhoIG;
            elseif (strcmp(this.optsPhys.optsPhysFW.DataInput.OptirhoIG, 'FWrho')) 
                if  isempty(this.ForwardResult)==1
                ForwardDynamics(this)       
                end
                this.DataIn.OptirhoIG = this.ForwardResult.rho_t;
            else
                this.DataIn.OptirhoIG = this.optsPhys.optsPhysFW.DataInput.OptirhoIG;
            end
            % the guess for p can be from testFun or from an input matrix
            if ~isfield(this.optsPhys.optsPhysFW.DataInput,'OptipIG')
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                for iT = 1:this.optsNum.TimeArea.n
                ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                OptipIG(iT,:) = ExactSol.OptipIG;
                this.DataIn.OptipIG = OptipIG;
                end
            else
                this.DataIn.OptipIG = this.optsPhys.optsPhysFW.DataInput.OptipIG;
            end
        end
        
        function getRhoTarget(this)
            % rhoHat is found via the testFun or supplied as input matrix
            times = this.TDC.Pts.y;
            if isfield(this.optsPhys.optsPhysFW.DataInput,'rhoTarget')==0
                testFun=str2func(this.optsPhys.optsPhysFW.DataInput.testFun);
                for iT = 1:this.optsNum.TimeArea.n
                ExactSol = testFun(this,this.IDC.Pts.y1_kv,this.IDC.Pts.y2_kv,times(iT),this.optsNum.TimeArea.TMax,this.optsPhys.optsPhysFW.Params.beta);
                rhoTarget(iT,:) = ExactSol.rhoTarget;
                end
                this.DataIn.rhoTarget = rhoTarget;
            else
                this.DataIn.rhoTarget = this.optsPhys.optsPhysFW.DataInput.rhoTarget;
            end
        end
        
        function getConvMatrix(this)
            % computing convolution
           disp('getConv')
           this.optsNum.V2Num.Pts= this.IDC.Pts;
           % checking whether to search for existing results or not
           if isfield(this.optsPhys.optsPhysFW.DataInput, 'DataRecompConv') && ((this.optsPhys.optsPhysFW.DataInput.DataRecompConv)==true)
                DataRecompConv = true;
           else
                DataRecompConv = false;
           end
           ConvStruct = DataStorage(['ConvData' filesep class(this.IDC) filesep this.optsPhys.optsPhysFW.DataInput.testFun],@ConvMatrices,this.optsNum.V2Num,this.IDC,[],[],[],DataRecompConv);
          
           Conv1= ConvStruct.Conv;
           this.Conv=Conv1;
        end
        

        function OptimizationDynamics(this)
            % compute optimization problem and needed inputs
            if ~isfield(this.DataIn,'rhoIC') 
                getForwardIC(this)  
            elseif isfield(this.DataIn,'rhoIC') && isempty(this.DataIn.rhoIC) 
                getForwardIC(this)  
            end
            if ~isfield(this.DataIn,'pIC') 
               getBackwardIC(this)   
            end
            
            if strcmp(this.optsPhys.optsPhysOpt.OptSolver.SolverFlag, 'fsolve') || strcmp(this.optsPhys.optsPhysOpt.OptSolver.SolverFlag, 'Picard')
            getOptimizationIG(this)
            end
            getRhoTarget(this)

            if isempty(this.Conv)==1
                getConvMatrix(this)
            end
            % check whether to search for stored results or recompute
            if isfield(this.optsPhys.optsPhysOpt.OptDataInput, 'DataRecompOpt') && ((this.optsPhys.optsPhysOpt.OptDataInput.DataRecompOpt)==true)
                DataRecompOpt = true;
            else
                DataRecompOpt = false;
            end
            opts.optsNum = this.optsNum;
            opts.optsPhys = this.optsPhys;
            this.optsPhys.optsPhysOpt.OptDataInput = rmfield(this.optsPhys.optsPhysOpt.OptDataInput,'DataRecompOpt');         
            data = DataStorage(['OptimizationData' filesep class(this.IDC) filesep this.optsPhys.optsPhysFW.DataInput.testFun], @Optimization_Dynamics2D, opts, this,[],[],[],DataRecompOpt);         
            
            this.OptimizationResult = data.OptimizationResult;
        end
        
  
        function ComputeForwardError(this)
            if isempty(this.ForwardResult)==1
                ForwardDynamics2D(this)
            end
            this.TDC.ComputeIntegrationVector;
            % compute cost functional of the forward problem (and both
            % terms individually)
            FWError.J1FW2D = ComputeJ12D(this.ForwardResult.rho_t, this.DataIn.rhoTarget, this.IDC.Int, this.TDC.Int);
            FWError.J2FW2D = ComputeJ22D(this.ForwardResult.FWCost, this.IDC.Int, this.TDC.Int);
            FWError.JFW2D = ComputeCostFunctional2D(this.ForwardResult.rho_t, this.DataIn.rhoTarget, this.ForwardResult.FWCost, ...
                                                this.optsPhys.optsPhysFW.Params.beta, this.IDC.Int, this.TDC.Int);

            if ~isempty(this.ExactSolution)
                % compute exact error if exact result exists
                ComputeNorm2D=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
                FWError.rhoExactErr2D = ComputeNorm2D(this.ForwardResult.rho_t, this.ExactSolution.rhoIC, this.IDC.Int, this.TDC.Int);
            end
            this.Errors.ForwardError = FWError;
        end
        
        function ComputeOptimizationError(this)
            if isempty(this.OptimizationResult)
                OptimizationDynamics2D(this)
            end
            this.TDC.ComputeIntegrationVector;
            % compute cost functional for optimal solution (and both terms
            % that go into the cost functional)
            OptiError.J1Opti2D = ComputeJ12D(this.OptimizationResult.rhoNum, this.DataIn.rhoTarget,  this.IDC.Int, this.TDC.Int);
            OptiError.J2Opti2D = ComputeJ22D(this.OptimizationResult.Control, this.IDC.Int, this.TDC.Int);
            OptiError.JOpti2D = ComputeCostFunctional2D(this.OptimizationResult.rhoNum, this.DataIn.rhoTarget,...
                                                    this.OptimizationResult.Control, this.optsPhys.optsPhysFW.Params.beta, this.IDC.Int, this.TDC.Int);
            OptiError.All=this;
            if ~isempty(this.ExactSolution)
                % compute exact errors if exact solution exists
                PDERHSStr = this.optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
                if strcmp(PDERHSStr, 'D_Force') ||  strcmp(PDERHSStr, 'AD_Force')|| strcmp(PDERHSStr, 'AD_Forcefl') || strcmp(PDERHSStr, 'OD') 
                    Exw = this.ExactSolution.wForce;
                elseif  strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext') 
                    wVec = this.ExactSolution.wFlow;
                    Exw = [wVec(:,:,1),wVec(:,:,2)];
                 
                end
                ComputeNorm2D=str2func(this.optsPhys.optsPhysFW.ProbSpecs.ComputeNormStr);
                OptiError.rhoExactErr2D = ComputeNorm2D(this.OptimizationResult.rhoNum, this.ExactSolution.OptirhoIG, this.IDC.Int, this.TDC.Int);
                OptiError.pExactErr2D = ComputeNorm2D(this.OptimizationResult.pNum, this.ExactSolution.OptipIG,  this.IDC.Int, this.TDC.Int);
                OptiError.wExactErr2D = ComputeNorm2D(this.OptimizationResult.Control, Exw,  this.IDC.Int, this.TDC.Int);
            end
            this.Errors.OptimizationError = OptiError;
        end
        
        function ComputeErrors(this)
            % compute all errors
            ComputeForwardError(this);
            ComputeOptimizationError(this);
        end
         
      
        function ComputeAll(this)
            getExactSolution(this)
            getForwardIC(this)
            getOptimizationIG(this)
            getRhoTarget(this)
            getConvMatrix(this)
            ForwardDynamics(this)
            OptimizationDynamics(this)
            ComputeErrors(this)
        end
    
    end

end