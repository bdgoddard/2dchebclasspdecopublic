function Norm = ComputeL1Norm2D(MNew, MOld, SInt, TInt)
if length(SInt') == length(MNew) 
    SInt1 = SInt;
else
    SInt1 = [SInt,SInt];
end
Norm = TInt*(SInt1*abs(MNew-MOld)')';

end