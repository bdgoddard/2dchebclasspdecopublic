 function rhop_t = ComputeOptimizationRHS2D(T0,Tf,IC,rhoExa,pExa,Pts,Diff,Ind,wIn,na,this)
    
    optsNum=this.optsNum;
    optsPhys=this.optsPhys;
    %---------------------------------------------------------------------%
    % Extracting Parameters                                                     %
    %---------------------------------------------------------------------%
    SolverFlag = optsPhys.optsPhysOpt.OptSolver.SolverFlag;
    target = this.DataIn.rhoTarget;

    AbsTol=optsNum.Tols.AbsTol;
    RelTol=optsNum.Tols.RelTol;  

    beta=optsPhys.optsPhysFW.Params.beta;
    gamma=optsPhys.optsPhysFW.Params.gamma;
    D0 = optsPhys.optsPhysFW.Params.D0;
    other = optsPhys.optsPhysFW.Params.other;

    N1=this.IDC.N1;
    N2=this.IDC.N2;
    
    t0=optsNum.TimeArea.t0;
    TMax=optsNum.TimeArea.TMax;
    
    % creating the function name for the RHS of both PDEs given the
    % indicator string for the problem and given that the function is
    % called after naming convention below.
    PDERHSStr = optsPhys.optsPhysFW.ProbSpecs.PDERHSStr;
    PDErhsStr = strcat("ComputeFW2D",PDERHSStr);
    ComputeOptrho2DPDE = str2func(PDErhsStr);
    PDErhspStr = strcat("ComputeOptp2D",PDERHSStr);
    ComputeOptp2DPDE = str2func(PDErhspStr);
    
    BCFun=str2func(optsPhys.optsPhysFW.ProbSpecs.BCFunStr);
    testFunStr=this.optsPhys.optsPhysFW.DataInput.testFun;
    testFun=str2func(testFunStr);
    
    SpaceBox = this.IDC;
    y1_kv=SpaceBox.Pts.y1_kv;
    y2_kv=SpaceBox.Pts.y2_kv;
    
    Lap= Diff.Lap;
    Grad = Diff.grad;
    Div = Diff.div;
    bound = Ind.bound;
    normal = Ind.normal;
             
    rhoMask = 1:length(y1_kv);
    pMask = (length(y1_kv)+1):(2*length(y1_kv));     
    
    InterLine= this.TDC;
    times = InterLine.Pts.y;
    
    %----------------------------------------------------------------------
    % Computing Convolution Matrix and Datastoring it
    %----------------------------------------------------------------------
    Conv = this.Conv;
    
    %----------------------------------------------------------------------
    % Extracting Variables for ODE RHS
    %----------------------------------------------------------------------
    FixPt = (strcmp(SolverFlag, 'FixPt')); 
    Picard = (strcmp(SolverFlag, 'Picard')); 
    inputForce = isfield(this.optsPhys.optsPhysFW.DataInput,'Force');
    if(inputForce)
        ForceIn = this.optsPhys.optsPhysFW.DataInput.Force;
    end
    inputFlow = isfield(this.optsPhys.optsPhysFW.DataInput,'Flow');
    if(inputFlow)
        FlowIn = this.optsPhys.optsPhysFW.DataInput.Flow;
    end
    inputVext = isfield(this.optsPhys.optsPhysFW.DataInput,'Vext');
    if(inputVext)
        VextIn = this.optsPhys.optsPhysFW.DataInput.Vext;
    end
    
    inputrho_R = isfield(this.optsPhys.optsPhysFW.DataInput,'rho_R');
    if(inputrho_R)
        rho_RIn = this.optsPhys.optsPhysFW.DataInput.rho_R;
    end
    % RHS input that does not change in loop (rho)
        rhoRHSInput.Div = Div;
        rhoRHSInput.Grad = Grad;
        rhoRHSInput.Lap = Lap;
        rhoRHSInput.D0 = D0;
        rhoRHSInput.gamma = gamma;
    % RHS input that does not change in loop (p)
        pRHSInput.Div = Div;
        pRHSInput.Grad = Grad;
        pRHSInput.Lap = Lap;
        pRHSInput.D0 = D0;
        pRHSInput.gamma = gamma;
    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------
    tMax = Tf;
    t0 = T0;
    
    %----------------------------------------------------------------------
    % Find initial condition
    %----------------------------------------------------------------------
    rho0 = IC(rhoMask);
    p0 = IC(pMask);
    
    Int = this.IDC.Int;
    
    %flag for opinion dynamics problems
    inputOD = isfield(optsPhys.optsPhysFW.DataInput,'OD');
    
    %rescaling mass of initial condition to 1
    if(inputOD)
        if ~isfield(this.optsPhys.optsPhysOpt.OptDataInput,'OptirhoIG')
        M_rho = this.optsPhys.optsPhysFW.Params.M_rho; %parameter for scaling rho_R
        C = 1/(Int*rho0);
        rho0 = M_rho*C*rho0;
        this.DataIn.rhoIC = rho0;
        end
    end
    
     %scaling non-time dependent target in opinion dynamics problem
    if(inputOD)
        if isfield(this.optsPhys.optsPhysFW.DataInput,'scaletarget')
            if this.optsPhys.optsPhysFW.DataInput.scaletarget
                M_tar = this.optsPhys.optsPhysFW.DataInput.M_tar;
            C_hat = 1/(SInt*target(1,:)');
            for iT = 1:n
                target(iT,:) = M_tar*C_hat* target(iT,:);
            end
            end  
        end
    end
    
    
    
    %---------------------------------------------------------------------%
    % Solve the PDE                                                       %
    %---------------------------------------------------------------------%
    if  (strcmp(SolverFlag, 'FixPt'))
        outTimes = times;
    else
        outTimes = t0:(tMax-t0)/50:tMax;
    end
    
    mM  = ones(size(y1_kv));
    mM(bound) = 0; 

    opts = odeset('RelTol',RelTol,'AbsTol',AbsTol,'Mass',diag(mM));
    [outTimes,rho_t] = ode15s(@drho_dt,outTimes,rho0,opts);
  
    [outTimes,p_t] = ode15s(@dp_dt,outTimes,p0,opts);
  
    rhop_t =  [rho_t, p_t];
    
    %----------------------------------------------------------------------
    % RHS of ODE
    %----------------------------------------------------------------------

     function drhodt = drho_dt(t,rho)

        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(t));
        InterpFW=InterpT.InterPol;
        rho2= [rho;rho];
        
       % Getting the Control depending on the solver. 
        if  (FixPt)            
             wInI = InterpFW*wIn;
             testSol=testFun(this,y1_kv,y2_kv,t,TMax,beta);
             if strcmp(PDERHSStr, 'AD_Flow') || strcmp(PDERHSStr, 'AD_Flowf') || strcmp(PDERHSStr, 'AD_FlowfVext')
                wForceCurrent = testSol.wForce;
                wFlowCurrent = wInI';   
             elseif ((strcmp(PDERHSStr, 'AD_Force')) || (strcmp(PDERHSStr, 'AD_Forcefl')) || (strcmp(PDERHSStr, 'D_Force')))
                wForceCurrent = wInI';
                wFlowCurrent = testSol.wFlow;
             elseif strcmp(PDERHSStr, 'OD') 
                rho_RCurrent = testSol.rho_R_IG;
             end
             
        elseif (Picard)
            p1=InterpFW*pExa;
            pCurrent=p1';
            rho1=InterpFW*rhoExa;
            rhoCurrent=rho1';
            rhoCurrent2= [rhoCurrent;rhoCurrent];
            % note: if the gradient equation in a new problem is different,
            % you need to create a new variable with a new name here!
            wForceCurrent = - pCurrent/beta;
            wFlowCurrent = -(Grad*pCurrent).*rhoCurrent2/beta;
        end

        % Extracting Variables
        if ~(inputForce) || ~(inputFlow) || ~(inputVext) || ~(inputrho_R)
        testSol=testFun(this,y1_kv,y2_kv,t,TMax,beta); % forward time
        end
        
        if(inputOD)
        if(inputForce)
            Force = InterpFW*ForceIn;
        else
            Force = testSol.Force;
        end
        else
        if (inputForce)
            Force = InterpFW*ForceIn;
        else
            Force = testSol.Force;
        end
        
        if(inputFlow)
            Flow = InterpFW*FlowIn;
        else
            Flow = testSol.Flow;
        end
        if(inputVext)
            Vext = InterpFW*VextIn;
        else
            Vext = testSol.Vext;
        end
        gradVext = Grad*Vext;
        end
          
        % Computing Convolution
        
        if(inputOD)
            ConvV2FW = Interact2BodyFW2D(rho,Conv);
            ConvV2FW_R = Interact2BodyFW_R2D(rho,rho_RCurrent,Conv);
        else
           ConvV2FW = Interact2BodyFW2D(rho,rho2, Conv,Grad);
        end
        
        % Computing RHS
        % Inputs for RHS that change in ODE solve
        if(inputOD)
            rhoRHSInput.ConvV2FW_R = ConvV2FW_R;
            rhoRHSInput.rho = rho;
            rhoRHSInput.ConvV2FW = ConvV2FW; 
            rhoRHSInput.Force = Force;
        else
        rhoRHSInput.rho = rho;
        rhoRHSInput.rho2 = rho2;
        rhoRHSInput.ConvV2FW = ConvV2FW;
        rhoRHSInput.Flow = Flow;
        rhoRHSInput.wFlow = wFlowCurrent;
        rhoRHSInput.Force = Force;
        rhoRHSInput.wForce = wForceCurrent;
        rhoRHSInput.gradVext = gradVext;
        end
        
%         rhs = -Div*(-(D0*Grad*rho + rho2.*gradVext - (rho2.*wFlowCurrent) + gamma*Grad* ConvV2FW)) + Force;
%         lhs = (1/4)*beta^(1/2)*exp(t)*(cos(pi*y1_kv)+1).*(cos(pi*y2_kv)+1);
%         diff = max(abs(lhs-rhs))
        rhoRHS = ComputeOptrho2DPDE(this,rhoRHSInput);
        rhoflux = rhoRHS.rhoflux;
        drhodt = rhoRHS.drhodt;
        
        % Computing BCs
        drhodt(bound) = BCFun(rho, rhoflux, bound, normal, this);
        
  
     end

    function dpdt = dp_dt(t,p)

        InterpT = InterLine.ComputeInterpolationMatrix(InterLine.CompSpace(tMax+t0-t));
        InterpBW=InterpT.InterPol;
    
        % Getting the control, rho and p depending on the solver.
        if  (FixPt)
            rhoL=InterpBW*rho_t; 
            rhoLater=rhoL';
            rhoLater2=[rhoLater;rhoLater];
            wInIBw = InterpBW*wIn;
            wFlowLater = wInIBw';
            rho_RLater = wInIBw';
        elseif (Picard)
            rhoL=InterpBW*rhoExa;
            pL = InterpBW*pExa;
            rhoLater=rhoL';
            rhoLater2=[rhoLater;rhoLater];
            % create a new gradient equation variable here if you have a
            % different gradient equation
            wFlowLater = -(Grad*pL').*rhoLater2/beta;
            rho_RLater = -(Interact2BodyAdj2(rhoLater,p,Conv,Dy))/beta;
        end
        
        rhoHat2= InterpBW*target; % interpolate given rhoHat
        rhoHat = rhoHat2';
        gradp=Grad*p;
        
        % Extracting Variables
        if ~(inputForce) || ~(inputFlow) || ~(inputVext) || ~(inputrho_R)
        testSolBw=testFun(this,y1_kv,y2_kv,tMax-t+ t0,TMax,beta); %backward time 
        end
        
        if(~inputOD)
        
        if(inputFlow)
            FlowBw = InterpBW*FlowIn;
        else
            FlowBw = testSolBw.Flow;
        end
        if(inputVext)
            VextBw = InterpBW*VextIn;
        else
            VextBw = testSolBw.Vext;
        end
        gradVextBw = Grad*VextBw; 
        end
        
        % Computing Convolution terms
        
        if (inputOD)
             ConvV2BW1 = Interact2BodyAdj12D(rhoLater,Conv);
                ConvV2BW_R = Interact2BodyAdj12D(rho_RLater,Conv);
                 ConvV2BW22D = Interact2BodyAdj2OD2D(rhoLater,p,Conv,Grad);
        else
            ConvV2BW1 = Interact2BodyAdj1(rhoLater,Conv);
            ConvV2BW22D = Interact2BodyAdj22D(rhoLater,p,Conv,Grad);
        end

        % Computing RHS
        % Inputs that change during the ODE solve
        if(inputOD)
            pRHSInput.p = p;            
            pRHSInput.gradp = gradp;
            pRHSInput.rhoLater = rhoLater;
            pRHSInput.rhoHat = rhoHat;
            pRHSInput.ConvV2BW1 = ConvV2BW1;
            pRHSInput.ConvV2BW22D = ConvV2BW22D;
            pRHSInput.ConvV2BW_R = ConvV2BW_R;
        else
        pRHSInput.p = p;
        pRHSInput.gradp = gradp;
        pRHSInput.rhoLater = rhoLater;
        pRHSInput.rhoHat = rhoHat;
        pRHSInput.ConvV2BW1 = ConvV2BW1;
        pRHSInput.ConvV2BW22D = ConvV2BW22D;
        pRHSInput.wFlowLater = wFlowLater;
        pRHSInput.gradVextBw = gradVextBw;
        pRHSInput.FlowBw = FlowBw;
        end
        
        pRHS = ComputeOptp2DPDE(this,pRHSInput);
        dpdt = pRHS.dpdt;
        pflux = pRHS.pflux;
     
        % Computing BCs 

        dpdt(bound) = BCFun(p, pflux, bound, normal, this);

    end
    end
