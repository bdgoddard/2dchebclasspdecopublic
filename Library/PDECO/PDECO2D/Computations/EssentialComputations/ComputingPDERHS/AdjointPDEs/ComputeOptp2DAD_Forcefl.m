function output = ComputeOptp2DAD_Forcefl(this,pRHSInput)
    p = pRHSInput.p;
    gradp = pRHSInput.gradp;
    rhoLater = pRHSInput.rhoLater;
    rhoHat = pRHSInput.rhoHat;
    ConvV2BW1 = pRHSInput.ConvV2BW1;
    ConvV2BW22D = pRHSInput.ConvV2BW22D;
    gradVextBw = pRHSInput.gradVextBw;
    FlowBw = pRHSInput.FlowBw;
    Grad = pRHSInput.Grad;
    Lap = pRHSInput.Lap;
    D0 = pRHSInput.D0;
    gamma = pRHSInput.gamma;
    
    dpdt =  D0*Lap*p + rhoLater - rhoHat  - dotProduct(gradVextBw,gradp) + dotProduct(FlowBw,gradp) ...
            - gamma*dotProduct(Grad*ConvV2BW1,gradp) + gamma*ConvV2BW22D;        
    pflux = Grad*p;
   
    output.dpdt = dpdt;
    output.pflux = pflux;
end