function output = ComputeOptp2DOD(this,pRHSInput)
    
    gamma = pRHSInput.gamma;
    p = pRHSInput.p;
    gradp = pRHSInput.gradp;
    rhoLater = pRHSInput.rhoLater;
    rhoHat = pRHSInput.rhoHat;
    ConvV2BW1 = pRHSInput.ConvV2BW1;
    ConvV2BW22D = pRHSInput.ConvV2BW22D;
    ConvV2BW_R = pRHSInput.ConvV2BW_R;
    sigma = this.optsPhys.optsPhysFW.Params.sigma;
    Grad = pRHSInput.Grad;
    Lap = pRHSInput.Lap;
    
    
    
    
        if isfield(this.optsPhys.optsPhysFW.Params,'kappa')
            kappa = this.optsPhys.optsPhysFW.Params.kappa;
        else 
            kappa=1;
        end

            dpdt =  kappa*(sigma^2/2)*Lap*p + kappa*rhoLater - kappa*rhoHat  - kappa*gamma*dotProduct(ConvV2BW_R,gradp)- kappa*dotProduct(ConvV2BW1,gradp) + kappa*ConvV2BW22D;
        
    
    pflux = Grad*p;

    output.dpdt = dpdt;
    output.pflux = pflux;
    
    
end