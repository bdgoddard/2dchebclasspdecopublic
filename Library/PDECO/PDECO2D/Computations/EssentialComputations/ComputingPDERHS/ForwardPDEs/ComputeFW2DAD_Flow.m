function RHS = ComputeFW2DAD_Flow(this,RHSInput)
    rho = RHSInput.rho;
    rho2 = RHSInput.rho2;
    ConvV2FW = RHSInput.ConvV2FW;
    wFlow = RHSInput.wFlow;
    Div = RHSInput.Div;
    Grad = RHSInput.Grad;
    D0 = RHSInput.D0;
    gamma = RHSInput.gamma;
    
    rhoflux =  -(D0*Grad*rho - rho2.*wFlow  + gamma*ConvV2FW);   
    drhodt  = -Div*rhoflux;  

    RHS.rhoflux = rhoflux;
    RHS.drhodt = drhodt;
end