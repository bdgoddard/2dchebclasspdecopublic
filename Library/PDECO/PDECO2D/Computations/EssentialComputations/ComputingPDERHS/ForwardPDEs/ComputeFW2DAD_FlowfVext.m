function RHS = ComputeFW2DAD_FlowfVext(this,RHSInput)
    rho = RHSInput.rho;
    rho2 = RHSInput.rho2;
    ConvV2FW = RHSInput.ConvV2FW;
    wFlow = RHSInput.wFlow;
    Force = RHSInput.Force;
    wForce = RHSInput.wForce;
    gradVext = RHSInput.gradVext;
    Div = RHSInput.Div;
    Grad = RHSInput.Grad;
    D0 = RHSInput.D0;
    gamma = RHSInput.gamma;
    
    rhoflux =  -(D0*Grad*rho + rho2.*gradVext - (rho2.*wFlow) + gamma*ConvV2FW);   
    
    drhodt  = -Div*rhoflux + wForce + Force ;  

    RHS.rhoflux = rhoflux;
    RHS.drhodt = drhodt;
end