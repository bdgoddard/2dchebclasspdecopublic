function RHS = ComputeFW2DAD_Forcefl(this,RHSInput)
    rho = RHSInput.rho;
    rho2 = RHSInput.rho2;
    ConvV2FW = RHSInput.ConvV2FW;
    Flow = RHSInput.Flow;
    Force = RHSInput.Force;
    wForce = RHSInput.wForce;
    gradVext = RHSInput.gradVext;
    Div = RHSInput.Div;
    Grad = RHSInput.Grad;
    D0 = RHSInput.D0;
    gamma = RHSInput.gamma;
    
    rhoflux =  -(D0*Grad*rho + rho2.*gradVext - (rho2.*Flow) + gamma*ConvV2FW);      
    drhodt  = -Div*rhoflux + wForce + Force;  

    RHS.rhoflux = rhoflux;
    RHS.drhodt = drhodt;
end