function RHS = ComputeFW2DD_Force(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    wForce = RHSInput.wForce;
    Div = RHSInput.Div;
    Grad = RHSInput.Grad;
    D0 = RHSInput.D0;
    gamma = RHSInput.gamma;
    
    rhoflux =  -(D0*Grad*rho + gamma*Grad*ConvV2FW);       
    drhodt  = -Div*rhoflux + wForce ;

    RHS.rhoflux = rhoflux;
    RHS.drhodt = drhodt;
end