function output = ComputeFW2DOD(this,RHSInput)
    rho = RHSInput.rho;
    ConvV2FW = RHSInput.ConvV2FW;
    ConvV2FW_R = RHSInput.ConvV2FW_R;
    Force = RHSInput.Force;
    gamma = RHSInput.gamma;
    sigma = this.optsPhys.optsPhysFW.Params.sigma;
    Int = this.IDC.Int;
    Div = RHSInput.Div;
    Grad = RHSInput.Grad;
  
     if isfield(this.optsPhys.optsPhysFW.Params,'kappa')
        kappa = this.optsPhys.optsPhysFW.Params.kappa;
     else
         kappa =1;
     end
        rhoflux =  -(kappa*(sigma^2/2)*Grad*rho + kappa*gamma*ConvV2FW_R  + kappa*ConvV2FW ); 
    
        drhodt  = -Div*rhoflux + kappa*Force ;
        
    
    output.rhoflux = rhoflux;
    output.drhodt = drhodt;
end