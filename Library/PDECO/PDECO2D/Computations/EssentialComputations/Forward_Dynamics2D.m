function data = Forward_Dynamics2D(opts,aPDECO_2D)
 
    ForwardSol= aPDECO_2D.Compute_Forward_Dynamics2D(); 
    data.ForwardResult=ForwardSol;
    
end

