     function g = ComputeGaussian2D(y,V2params)
        alpha= V2params.alpha;
        
        g= exp(- y.^2) * alpha; 

    end