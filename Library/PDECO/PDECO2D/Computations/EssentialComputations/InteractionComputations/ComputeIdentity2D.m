   function g = ComputeIdentity2D(y1, y2,V2params)
        alpha= V2params.alpha;
        g= alpha*[y1 y2] ;
    end