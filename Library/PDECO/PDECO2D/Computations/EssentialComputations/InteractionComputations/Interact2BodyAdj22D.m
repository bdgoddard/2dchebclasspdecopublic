    function l = Interact2BodyAdj22D(rho,p,Conv,Grad)
      
        Grad1=Grad(1:end/2,:);
        Grad2=Grad(end/2+1:end,:);

        Conv1 = Conv*((Grad1*p).*rho);
        Conv2 = Conv*((Grad2*p).*rho);

        l= Grad1*Conv1 + Grad2*Conv2;
      
    end
       
        