    function l = Interact2BodyAdj2OD2D(rho,p,Conv,Grad)
      
        Grad1=Grad(1:end/2,:);
        Grad2=Grad(end/2+1:end,:);
        conv1 = Conv(:,:,1);
        conv2 = Conv(:,:,2);

        Conv1 = conv1*((Grad1*p).*rho);
        Conv2 = conv2*((Grad2*p).*rho);

        l= Conv1 + Conv2;
      
    end
       
        