
function f = Interact2BodyFW2D(rho,rho2, Conv,Grad)
 
    f = rho2.*(Grad*(Conv*rho));       

end