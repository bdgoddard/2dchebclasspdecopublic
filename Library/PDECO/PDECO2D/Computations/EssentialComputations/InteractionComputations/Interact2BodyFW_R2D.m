
    function f = Interact2BodyFW_R2D(rho,rho_R,Conv)
        f =[ rho.*(Conv(:,:,1)* rho_R);  rho.*(Conv(:,:,2)* rho_R)];
        
    end