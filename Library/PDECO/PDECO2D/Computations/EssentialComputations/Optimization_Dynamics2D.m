 
function data = Optimization_Dynamics2D(opts,aPDECO_2D)
    
       OptiSol= aPDECO_2D.Compute_Optimization_Dynamics2D();     
       data.OptimizationResult=OptiSol;

end

