        function dprod= dotProduct(u,v)
        dprod= u(1:end/2).*v(1:end/2) + u(end/2+1:end).*v(end/2+1:end);
        end