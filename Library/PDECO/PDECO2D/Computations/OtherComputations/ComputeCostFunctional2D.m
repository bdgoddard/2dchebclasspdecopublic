function Jfun = ComputeCostFunctional2D(rho, rhoHat, w, beta, SInt, TInt)
if length(SInt) == length(w) 
    SInt1 = SInt;
else
    SInt1 = [SInt,SInt];
end
J2 = TInt*((SInt1*((w.^2)'))'); 
J1 = TInt*(SInt*((rho - rhoHat)'.^2))'; 
 
Jfun = (1/2)*J1 + (beta/2)*J2;
end