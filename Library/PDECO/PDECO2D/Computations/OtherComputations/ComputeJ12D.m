function J1 = ComputeJ12D(rho, rhoHat, SInt, TInt)
if length(SInt') == length(rho) 
    SInt1 = SInt;
else
    SInt1 = [SInt,SInt];
end
J1 = TInt*(SInt1*((rho - rhoHat).^2)')'; 
end