function J2 = ComputeJ22D(w, SInt, TInt)
if length(SInt) == length(w) 
    SInt1 = SInt;
else
    SInt1 = [SInt,SInt];
end

J2 = TInt*((SInt1*((w.^2)'))'); 
end