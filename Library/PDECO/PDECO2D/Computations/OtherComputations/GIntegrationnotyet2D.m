function IntA = GIntegration(A,yMin,yMax)
% y = this.IDC.Pts.y;
% times=this.TDC.Pts.y;
nT = length(A(:,1));
nY = length(A(1,:));

geom1.yMin = yMin;
geom1.yMax = yMax;
geom1.N = nY;
bigLine = SpectralLine(geom1);
yBig = bigLine.Pts.y;

for iT = 1:nT
At = A(iT,:);
for iy = 1 : nY    
    geom2.yMin = -1;
    geom2.yMax = yBig(iy);
    geom2.N = nY;
    smallLine = SpectralLine(geom2);
    sy = smallLine.Pts.y;
    
    InterpT = bigLine.ComputeInterpolationMatrix(bigLine.CompSpace(sy));
    Interp=InterpT.InterPol;

    Asy = Interp*At';  

    Int = smallLine.ComputeIntegrationVector;
    Afint = Int*Asy;
    Afiny(iy) = Afint;
end
Afin(iT,:) = Afiny;
end
IntA = Afin;
end