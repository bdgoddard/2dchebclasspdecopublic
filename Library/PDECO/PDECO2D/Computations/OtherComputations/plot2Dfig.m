function fig = plot2Dfig(Var, ti, funtitle, zLabelStr, output, vec1)

    VarI = Var(ti,:)';
    output.IDC.plot(VarI);
    title(funtitle,'interpreter', 'latex')
    %zlim([0,1])
    zlim(vec1)
    xlim([-1,1])
    ylim([-1,1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel(zLabelStr,'interpreter', 'latex')
    h = light;
    shading interp
    lighting gouraud
    
end