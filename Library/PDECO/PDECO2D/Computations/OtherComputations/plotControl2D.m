
%output1a = TestInteractProb5PDECO2D(1,10^-1);
output1a = TestInteractProb5PDECO2D(-1,10^-1);
Control = output1a.OptimizationResult.Control;    

t1 = 2;
t2 = 5;
t3 = 9;

% funtitle = 'Optimal Control $\vec{w}$, $\gamma = 1$';
 funtitle = 'Optimal Control $\vec{w}$, $\gamma = -1$';
% funtitle = 'Optimal Control $\vec{w}$, $\gamma = 0$';
PlotArea.NFlux = 30;
PlotArea.y1Min = -1;
PlotArea.y1Max = 1;
PlotArea.y2Min = -1;
PlotArea.y2Max = 1;
output1a.IDC.InterpolationPlotFlux(PlotArea)
flnorm = max(max(abs(output1a.OptimizationResult.Control)));

hf = figure() 
subplot(1,3,1)
plotControl2Dfig(Control, t1, flnorm, funtitle, output1a)

subplot(1,3,2) 
plotControl2Dfig(Control, t2, flnorm, funtitle, output1a)

subplot(1,3,3) 
plotControl2Dfig(Control, t3, flnorm, funtitle, output1a) 
      