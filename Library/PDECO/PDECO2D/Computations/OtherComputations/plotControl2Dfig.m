function fig = plotControl2Dfig(Control, ti, flnorm, funtitle, output)

    fl = Control(ti,:)';
    output.IDC.plotFlux(fl,[],flnorm,1.2,'k',false,0);
    %output.IDC.plotStreamlines(fl,[],[],[]);
    pause(0.2)
    
    title(funtitle,'interpreter', 'latex')
    %zlim([0,1])
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')


end