function output = plotResults2D()
output1a = TestInteractProb5PDECO2D(-1,10^-3);
Var = output1a.OptimizationResult.rhoNum;  
Control = output1a.OptimizationResult.Control;
Vara1 = output1a.ExactSolution.rhoTarget;
Vara2 = output1a.ForwardResult.rho_t;
SInt = output1a.IDC.Int;
%SInt*(output1a.OptimizationResult.rhoNum')

output1a.Errors.ForwardError
output1a.Errors.OptimizationError

output = output1a;


t1 = 4; %2;
t2 = 10; %5;
t3 = 18; %9;


funtitlea1 = 'Uncontrolled State $\rho$, $t=0.5$';
funtitlea2 = 'Uncontrolled State $\rho$, $t=0.9$';
funtitlea3 = 'Desired State $\widehat \rho$, $t=0.9$';
funtitle1 = 'Optimal State $\rho$, $t=0.2$';
funtitle2 = 'Optimal State $\rho$, $t=0.5$';
funtitle3 = 'Optimal State $\rho$, $t=0.9$';
funtitleC1 = 'Optimal Control $\vec{w}$, $t=0.2$';
funtitleC2 = 'Optimal Control $\vec{w}$, $t=0.5$';
funtitleC3 = 'Optimal Control $\vec{w}$, $t=0.9$';

vec1 = [0,1];%[0,0.5];%[0,1];%[0,3.2];
vec2 = [0,1];%[0,0.5];%[0,1];
vec3 = [0,1];%[0,0.5];%[0,1];%[0,0.8];


% hfa = figure() ;
zLabelStr = '$\rho$';
zLabelStra1 = '$\widehat \rho$';


s1a = subplot(1,3,1);
plot2Dfig(Vara2, t2, funtitlea1, zLabelStr, output1a, vec2)
set(s1a,'FontSize',15);

s2a = subplot(1,3,2) ;
plot2Dfig(Vara2, t3, funtitlea2, zLabelStr, output1a, vec2)
set(s2a,'FontSize',15);

s3a = subplot(1,3,3) ;
plot2Dfig(Vara1, t3, funtitlea3, zLabelStra1, output1a, vec1)
set(s3a,'FontSize',15);   


hf = figure() ;
s1 = subplot(2,3,1);
plot2Dfig(Var, t1, funtitle1, zLabelStr, output1a, vec3)
set(s1,'FontSize',15);

s2 = subplot(2,3,2) ;
plot2Dfig(Var, t2, funtitle2, zLabelStr, output1a, vec3)
set(s2,'FontSize',15);

s3 = subplot(2,3,3) ;
plot2Dfig(Var, t3, funtitle3, zLabelStr, output1a, vec3)
set(s3,'FontSize',15);      



PlotArea.NFlux = 10;
PlotArea.y1Min = -1;
PlotArea.y1Max = 1;
PlotArea.y2Min = -1;
PlotArea.y2Max = 1;
output1a.IDC.InterpolationPlotFlux(PlotArea)
flnorm = max(max(abs(output1a.OptimizationResult.Control)));

s4 = subplot(2,3,4);
plotControl2Dfig(Control, t1, flnorm, funtitleC1, output1a)
set(s4,'FontSize',15); 

s5 = subplot(2,3,5) ;
plotControl2Dfig(Control, t2, flnorm, funtitleC2, output1a)
set(s5,'FontSize',15);     

s6 = subplot(2,3,6) ;
plotControl2Dfig(Control, t3, flnorm, funtitleC3, output1a) 
set(s6,'FontSize',15);     



end

      