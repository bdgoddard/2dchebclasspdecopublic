function FlowControlPlot(R1,nx,opts,fileName)
    t1 = 3;
    t2 = 6;
    t3 = 9;
    funtitle1 = 'Optimal State $\rho$, $t=0.1$';
    funtitle2 = 'Optimal State $\rho$, $t=0.5$';
    funtitle3 = 'Optimal State $\rho$, $t=0.9$';
    funtitleV = 'External Potential $V_{ext}$';
    funtitleV1 = 'External Potential $V_{ext}$, $t=0.1$';
    funtitleV2 = 'External Potential $V_{ext}$, $t=0.5$';
    funtitleV3 = 'External Potential $V_{ext}$, $t=0.9$';
    funtitleC1 = 'Optimal Control $\vec{w}$, $t=0.1$';
    funtitleC2 = 'Optimal Control $\vec{w}$, $t=0.5$';
    funtitleC3 = 'Optimal Control $\vec{w}$, $t=0.9$'; 
    zLabelStr = '$\rho$';
    zLabelStrV = '$V_{ext}$';
    %% set up domain
    geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];
    geom.N = [nx;nx]; 
    aBox = Box(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc
    aBox.ComputeInterpolationMatrix((-1:0.01:1)',(-1:0.01:1)',true,true);
    
    Vext = R1.Vout;
    
    hf = figure('Position',[1,1,1600,400]);
    
    colormap(parula(30))
   
        
    %Plot control
    PlotArea.NFlux = 10;
    PlotArea.y1Min = -1;
    PlotArea.y1Max = 1;
    PlotArea.y2Min = -1;
    PlotArea.y2Max = 1;
    aBox.InterpolationPlotFlux(PlotArea)
    %flnorm1 = max(max(abs(R1.w)));
    
   
    ax1 = subplot(1,4,1); 
    ax2 = copyobj(ax1,hf);
    
    axes(ax1)
    fl1 = R1.w(:,t1);
    aBox.plotFlux(fl1,[],opts.flnorm,1.2,'k',false,0);
    title(funtitleC1,'interpreter', 'latex')
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax1,'FontSize',15);  
    axes(ax2)
    aBox.plot(Vext(:,t1),'contour');
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax2,'FontSize',15); 
    
    colormap(ax2,'pink')
    
    linkaxes([ax1,ax2])
    ax2.Visible = 'off';
    ax2.XTick = [];
    ax2.YTick = [];
    
    ax1a = subplot(1,4,2); 
    ax2a = copyobj(ax1a,hf);
    axes(ax1a)
    fl2 = R1.w(:,t2);
    aBox.plotFlux(fl2,[],opts.flnorm,1.2,'k',false,0);
    title(funtitleC2,'interpreter', 'latex')
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax1a,'FontSize',15);  
    axes(ax2a)
    aBox.plot(Vext(:,t2),'contour');
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax2a,'FontSize',15); 
    colormap(ax2a,'pink')
    
    linkaxes([ax1a,ax2a])
    ax2a.Visible = 'off';
    ax2a.XTick = [];
    ax2a.YTick = [];
    
    ax1b = subplot(1,4,3); 
    ax2b = copyobj(ax1b,hf);
    axes(ax1b)
    fl2 = R1.w(:,t3);
    aBox.plotFlux(fl2,[],opts.flnorm,1.2,'k',false,0);
    title(funtitleC3,'interpreter', 'latex')
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax1b,'FontSize',15);  
    axes(ax2b)
    aBox.plot(Vext(:,t3),'contour');
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax2b,'FontSize',15); 
   
    colormap(ax2b,'pink')
    
    linkaxes([ax1b,ax2b])
    ax2b.Visible = 'off';
    ax2b.XTick = [];
    ax2b.YTick = [];
    
    ax1c = subplot(1,4,4); 
    ax2c = copyobj(ax1c,hf);
    colormap(ax1c,'parula')
    colormap(ax2c,'pink')
    
    linkaxes([ax1c,ax2c])
    ax2c.Visible = 'off';
    ax2c.XTick = [];
    ax2c.YTick = [];

    cb = colorbar(ax2c,'location','west','Ticks',[0, 0.5, 1],...
                     'TickLabels',{round(opts.Vi,2),round(opts.Ve,2),round(opts.Va,2)},'TickLabelInterpreter','latex','FontSize',13);
    cbar_handle = findobj(cb,'tag','Colorbar');
    set(cbar_handle, 'YAxisLocation','right')
    axis off       
%            
    save2png(fileName,gcf,300)

end