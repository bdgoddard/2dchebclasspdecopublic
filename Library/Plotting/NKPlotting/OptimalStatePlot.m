function OptimalStatePlot(R1,nx,opts,fileName)
    t1 = 3;
    t2 = 6;
    t3 = 9;
    funtitle1 = 'Optimal State $\rho$, $t=0.1$';
    funtitle2 = 'Optimal State $\rho$, $t=0.5$';
    funtitle3 = 'Optimal State $\rho$, $t=0.9$';
    zLabelStr = '$\rho$';
    %% set up domain
    geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];
    geom.N = [nx;nx]; 
    aBox = Box(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc
    aBox.ComputeInterpolationMatrix((-1:0.01:1)',(-1:0.01:1)',true,true);
    
    Vext = R1.Vout;
    % Plot rho
    hf = figure('Position',[1,1,1600,400]);
    
    
    s1a = subplot(1,4,1);
    aBox.plot(R1.rho(:,t1),'color');
    colormap(parula(30))
    caxis([opts.mi,opts.ma])
    title(funtitle1,'interpreter', 'latex')
    xlim([-1,1])
    ylim([-1,1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel(zLabelStr,'interpreter', 'latex')
    set(s1a,'FontSize',15);
    axes1a = axes('Parent',hf,'Position',[10 420 400 400]);
    
    s2a = subplot(1,4,2);
    aBox.plot(R1.rho(:,t2),'color');
    colormap(parula(30))
    caxis([opts.mi,opts.ma])
    title(funtitle2,'interpreter', 'latex')
    xlim([-1,1])
    ylim([-1,1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel(zLabelStr,'interpreter', 'latex')
    set(s2a,'FontSize',15);
    axes2a = axes('Parent',hf,'Position',[440 420 400 400]);
    
    
    s3a = subplot(1,4,3);
    aBox.plot(R1.rho(:,t3),'color');
    colormap(parula(30))
    caxis([opts.mi,opts.ma])
    title(funtitle3,'interpreter', 'latex')
    xlim([-1,1])
    ylim([-1,1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel(zLabelStr,'interpreter', 'latex')
    set(s3a,'FontSize',15);   
    axes3a = axes('Parent',hf,'Position',[880 420 400 400]);
    
    subplot(1,4,4)

    ca = colorbar('location','west','Ticks',[0, 0.5, 1],...
                     'TickLabels',{round(opts.mi,2),round(opts.me,2),round(opts.ma,2)},'TickLabelInterpreter','latex','FontSize',13);
        axis off   
  
    save2png(fileName,gcf,300)

end