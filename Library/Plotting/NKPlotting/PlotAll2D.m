function PlotAll2D()

optsa.beta = 10^-3;
optsa.kappa = -1;
optsb.beta = 10^-3;
optsb.kappa = 0;
optsc.beta = 10^-3;
optsc.kappa = 1;


SCDkn1 = DataStorage(['NewtonKrylovData' filesep 'nSCD'] ,@SCDExample,optsa,[]); 
SCDk0 = DataStorage(['NewtonKrylovData' filesep 'nSCD'] ,@SCDExample,optsb,[]); 
SCDk1 = DataStorage(['NewtonKrylovData' filesep 'nSCD'] ,@SCDExample,optsc,[]); 
opts1.ma = max([max(max((SCDkn1.rho))),max(max((SCDk0.rho))), max(max((SCDk1.rho)))]);
opts1.mi = min([min(min((SCDkn1.rho))),min(min((SCDk0.rho))), min(min((SCDk1.rho)))]);
opts1.me = (opts1.ma + opts1.mi)/2;
opts1.mac = max([max(max((SCDkn1.w))),max(max((SCDk0.w))), max(max((SCDk1.w)))]);
opts1.mic = min([min(min((SCDkn1.w))),min(min((SCDk0.w))), min(min((SCDk1.w)))]);
opts1.mec = (opts1.mac + opts1.mic)/2;
opts1.Va = max([max(max((SCDkn1.Vout))),max(max((SCDk0.Vout))), max(max((SCDk1.Vout)))]);
opts1.Vi = min([min(min((SCDkn1.Vout))),min(min((SCDk0.Vout))), min(min((SCDk1.Vout)))]);
opts1.Ve = (opts1.Va + opts1.Vi)/2;
 
OptimalStatePlot(SCDkn1,20,opts1,'SCDkn1.png')
OptimalStatePlot(SCDk0,20,opts1,'SCDk0.png')
OptimalStatePlot(SCDk1,20,opts1,'SCDk1.png')
SourceControlPlot(SCDkn1,20,opts1,'SCDkn1c.png')
SourceControlPlot(SCDk0,20,opts1,'SCDk0c.png')
SourceControlPlot(SCDk1,20,opts1,'SCDk1c.png')


SCNkn1 = DataStorage(['NewtonKrylovData' filesep 'nSCN'] ,@SCNExample,optsa,[]); 
SCNk0 = DataStorage(['NewtonKrylovData' filesep 'nSCN'] ,@SCNExample,optsb,[]); 
SCNk1 = DataStorage(['NewtonKrylovData' filesep 'nSCN'] ,@SCNExample,optsc,[]); 
opts2.ma = max([max(max((SCNkn1.rho))),max(max((SCNk0.rho))), max(max((SCNk1.rho)))]);
opts2.mi = min([min(min((SCNkn1.rho))),min(min((SCNk0.rho))), min(min((SCNk1.rho)))]);
opts2.me = (opts2.ma + opts2.mi)/2;
opts2.mac = max([max(max((SCNkn1.w))),max(max((SCNk0.w))), max(max((SCNk1.w)))]);
opts2.mic = min([min(min((SCNkn1.w))),min(min((SCNk0.w))), min(min((SCNk1.w)))]);
opts2.mec = (opts2.mac + opts2.mic)/2;
opts2.Va = max([max(max((SCNkn1.Vout))),max(max((SCNk0.Vout))), max(max((SCNk1.Vout)))]);
opts2.Vi = min([min(min((SCNkn1.Vout))),min(min((SCNk0.Vout))), min(min((SCNk1.Vout)))]);
opts2.Ve = (opts2.Va + opts2.Vi)/2;
 
OptimalStatePlot(SCNkn1,20,opts2,'SCNkn1.png')
OptimalStatePlot(SCNk0,20,opts2,'SCNk0.png')
OptimalStatePlot(SCNk1,20,opts2,'SCNk1.png')
SourceControlPlot(SCNkn1,20,opts2,'SCNkn1c.png')
SourceControlPlot(SCNk0,20,opts2,'SCNk0c.png')
SourceControlPlot(SCNk1,20,opts2,'SCNk1c.png')
% return

FCNkn1 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,optsa,[]); 
FCNk0 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,optsb,[]); 
FCNk1 = DataStorage(['NewtonKrylovData' filesep 'nFCN'] ,@FCNExample,optsc,[]); 
opts3.ma = max([max(max(FCNkn1.rho)),max(max(FCNk0.rho)), max(max(FCNk1.rho))]);
opts3.mi = min([min(min(FCNkn1.rho)),min(min(FCNk0.rho)), min(min(FCNk1.rho))]);
opts3.me = (opts3.ma + opts3.mi)/2;
opts3.flnorm = max([max(max(abs(FCNkn1.w))),max(max(abs(FCNk0.w))), max(max(abs(FCNk1.w)))]);
opts3.Va = max([max(max(FCNkn1.Vout)),max(max(FCNk0.Vout)), max(max(FCNk1.Vout))]);
opts3.Vi = min([min(min(FCNkn1.Vout)),min(min(FCNk0.Vout)), min(min(FCNk1.Vout))]);
opts3.Ve = (opts3.Va + opts3.Vi)/2;

OptimalStatePlot(FCNkn1,20,opts3,'FCNkn1.png')
OptimalStatePlot(FCNk0,20,opts3,'FCNk0.png')
OptimalStatePlot(FCNk1,20,opts3,'FCNk1.png')
FlowControlPlot(FCNkn1,20,opts3,'FCNkn1c.png')
FlowControlPlot(FCNk0,20,opts3,'FCNk0c.png')
FlowControlPlot(FCNk1,20,opts3,'FCNk1c.png')

FCDkn1 = DataStorage(['NewtonKrylovData' filesep 'nFCD'] ,@FCDExample,optsa,[]); 
FCDk0 = DataStorage(['NewtonKrylovData' filesep 'nFCD'] ,@FCDExample,optsb,[]); 
FCDk1 = DataStorage(['NewtonKrylovData' filesep 'nFCD'] ,@FCDExample,optsc,[]); 
opts4.ma = max([max(max(FCDkn1.rho)),max(max(FCDk0.rho)), max(max(FCDk1.rho))]);
opts4.mi = min([min(min(FCDkn1.rho)),min(min(FCDk0.rho)), min(min(FCDk1.rho))]);
opts4.me = (opts4.ma + opts4.mi)/2;
opts4.flnorm = max([max(max(abs(FCDkn1.w))),max(max(abs(FCDk0.w))), max(max(abs(FCDk1.w)))]);
opts4.Va = max([max(max(FCDkn1.Vout)),max(max(FCDk0.Vout)), max(max(FCDk1.Vout))]);
opts4.Vi = min([min(min(FCDkn1.Vout)),min(min(FCDk0.Vout)), min(min(FCDk1.Vout))]);
opts4.Ve = (opts4.Va + opts4.Vi)/2;

OptimalStatePlot(FCDkn1,20,opts4,'FCDkn1.png')
OptimalStatePlot(FCDk0,20,opts4,'FCDk0.png')
OptimalStatePlot(FCDk1,20,opts4,'FCDk1.png')
FlowControlPlot(FCDkn1,20,opts4,'FCDkn1c.png')
FlowControlPlot(FCDk0,20,opts4,'FCDk0c.png')
FlowControlPlot(FCDk1,20,opts4,'FCDk1c.png')


end