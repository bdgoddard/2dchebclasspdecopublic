function Plotting3DControl(w, flnorm, scale,vField3, flmax, flmin, fileName)
% wmin = -2.0709
% wmax = 2.1438
    t1 = 3;
    t2 = 6;
    t3 = 9;
    
    funtitleC1 = 'Optimal Control $\vec{w}$, $t=0.1$';
    funtitleC2 = 'Optimal Control $\vec{w}$, $t=0.5$';
    funtitleC3 = 'Optimal Control $\vec{w}$, $t=0.9$'; 
    
    geom.y1Min = -1; geom.y1Max = 1; 
    geom.y2Min = -1; geom.y2Max = 1; 
    geom.y3Min = -1; geom.y3Max = 1; 

    nx = 20;    
    geom.N = [nx;nx;nx]; 

    aBox = Box3(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc

    aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);

    %Plot control
    PlotArea.NFlux = 5;
    PlotArea.y1Min = -1;
    PlotArea.y1Max = 1;
    PlotArea.y2Min = -1;
    PlotArea.y2Max = 1;
    PlotArea.y3Min = -1;
    PlotArea.y3Max = 1;
    aBox.InterpolationPlotFlux(PlotArea)

    gh = figure('Position',[1,1,1400,320]);
    sf1 = subplot(1,4,1);
    fl1 = w(:,t1);
    aBox.plotFlux(fl1,[],flnorm,1,'k', scale,vField3);
    title(funtitleC1,'interpreter', 'latex')
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')
    set(sf1,'FontSize',15);  
    %axes5 = axes('Parent',hf,'Position',[10 10 400 400]);
    
    sf2 = subplot(1,4,2);
    fl2 = w(:,t2);
    aBox.plotFlux(fl2,[],flnorm,1,'k', scale,vField3);
    title(funtitleC2,'interpreter', 'latex')
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')
    set(sf2,'FontSize',15);  
    %axes6 = axes('Parent',hf,'Position',[440 10 400 400]);
    
    sf3 = subplot(1,4,3);
    fl3 = w(:,t3);
    aBox.plotFlux(fl3,[],flnorm,1,'k', scale,vField3);
    title(funtitleC3,'interpreter', 'latex')
    xlim([-1.1,1.1])
    ylim([-1.1,1.1])
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')
    set(sf3,'FontSize',15);  
    %axes7 = axes('Parent',hf,'Position',[880 10 400 400]);
    
    
    sf4 = subplot(1,4,4);
    
    flme = (flmin + flmax)/2;
    cb = colorbar('location','west','Ticks',[0, 0.5, 1],...
                     'TickLabels',{round(flmin,2),round(flme,2),round(flmax,2)},'TickLabelInterpreter','latex','FontSize',13);
    axis off    
    save2png(fileName,gcf,300)
    
end