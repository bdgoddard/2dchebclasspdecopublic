function Plotting3DVext(Vext)
    t1 = 2;
    t2 = 3;
    t3 = 9;


    funtitle1 = '$V_{ext}$';
    
    geom.y1Min = -1; geom.y1Max = 1; 
    geom.y2Min = -1; geom.y2Max = 1; 
    geom.y3Min = -1; geom.y3Max = 1; 

    nx = 20;    
    geom.N = [nx;nx;nx]; 

    aBox = Box3(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc

    aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);

    hh = figure();
    %colormap(parula(50))
    
    aBox.PlotMidPlanes(Vext)
    title(funtitle1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')

    colorbar
%     ca = colorbar('Ticks',[0, 0.5, 1],...
%                          'TickLabels',{round(mit1,2),round(met1,2),round(mat1,2)},'TickLabelInterpreter','latex','FontSize',13);



end