function Plotting3Drho(Res,fileName)
    t1 = 2;
    t2 = 3;
    t3 = 9;


    funtitle1 = 'Optimal State $\rho$, $t=0.1$';
    funtitle2 = 'Optimal State $\rho$, $t=0.2$';
    funtitle3 = 'Optimal State $\rho$, $t=0.9$';
    
    
    geom.y1Min = -1; geom.y1Max = 1; 
    geom.y2Min = -1; geom.y2Max = 1; 
    geom.y3Min = -1; geom.y3Max = 1; 

    nx = 20;    
    geom.N = [nx;nx;nx]; 

    aBox = Box3(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc

    aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);

    hh = figure('Position',[1,1,1400,320]);
    rho = Res.rho;
    %colormap(parula(50))
    opts1.ma = 0.1383;
    opts1.mi = 0.1125;

    s1a = subplot(1,3,1);
    aBox.PlotMidPlanes(rho(:,t1),opts1)
    title(funtitle1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')
    set(s1a,'FontSize',15);
    colorbar
%     ca = colorbar('Ticks',[0, 0.5, 1],...
%                          'TickLabels',{round(mit1,2),round(met1,2),round(mat1,2)},'TickLabelInterpreter','latex','FontSize',13);

    opts2.ma = 0.1506;
    opts2.mi = 0.0997;
    
    s2a = subplot(1,3,2);
    aBox.PlotMidPlanes(rho(:,t2),opts2)
    title(funtitle2,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')
    set(s2a,'FontSize',15);
    colorbar
%     ca = colorbar('Ticks',[0, 0.5, 1],...
%                          'TickLabels',{round(mit2,2),round(met2,2),round(mat2,2)},'TickLabelInterpreter','latex','FontSize',13);

    opts3.ma = 0.3938;
    opts3.mi = 0.0559;

    s3a = subplot(1,3,3);
    aBox.PlotMidPlanes(rho(:,t3),opts3)
    title(funtitle3,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    zlabel('$x_3$','interpreter', 'latex')
    set(s3a,'FontSize',15);
    colorbar
%     ca = colorbar('Ticks',[0, 0.5, 1],...
%                          'TickLabels',{round(mit3,2),round(met3,2),round(mat3,2)},'TickLabelInterpreter','latex','FontSize',13);

    save2png(fileName,gcf,300)
end