function Plotting3DwNorm(w)


    geom.y1Min = -1; geom.y1Max = 1; 
    geom.y2Min = -1; geom.y2Max = 1; 
    geom.y3Min = -1; geom.y3Max = 1; 

    nx = 20;    
    n = 11;
    geom.N = [nx;nx;nx]; 

    aBox = Box3(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc
    aBox.ComputeInterpolationMatrix((-1:0.1:1)',(-1:0.1:1)',(-1:0.1:1)',true,true);
figure()

for i = 1:11
     wt = w(:,i);
     dProdw(i,:)= wt(1:end/3).*wt(1:end/3) + wt(end/3+1:2*end/3).*wt(end/3+1:2*end/3)...
         + wt(2*end/3+1:end).*wt(2*end/3+1:end);
     w2Norm(i,:) = sqrt(dProdw(i,:));
end
 
w2Norm = w2Norm';
%colormap(parula(50))
mi = min(min(w2Norm))
ma = max(max(w2Norm))
me = (ma + mi)/2
opts.ma = ma;
opts.mi = mi;
subplot(2,3,1)
aBox.PlotMidPlanes(w2Norm(:,1),opts)
subplot(2,3,2)
aBox.PlotMidPlanes(w2Norm(:,3),opts)
subplot(2,3,3)
aBox.PlotMidPlanes(w2Norm(:,5),opts)
subplot(2,3,4)
aBox.PlotMidPlanes(w2Norm(:,7),opts)
subplot(2,3,5)
aBox.PlotMidPlanes(w2Norm(:,9),opts)
subplot(2,3,6)

ca = colorbar('location','eastoutside','Ticks',[0, 0.5, 1],...
                     'TickLabels',{mi,me,ma},'TickLabelInterpreter','latex','FontSize',13);
% ca = colorbar('location','eastoutside','Ticks',[0, 0.5, 1],...
%                      'TickLabels',{round(mi,2),round(me,2),round(ma,2)},'TickLabelInterpreter','latex','FontSize',13);
axis off

end