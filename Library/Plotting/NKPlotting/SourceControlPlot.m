function SourceControlPlot(R1,nx,opts,fileName)
    t1 = 3;
    t2 = 6;
    t3 = 9;
    funtitleC1 = 'Optimal Control $\vec{w}$, $t=0.1$';
    funtitleC2 = 'Optimal Control $\vec{w}$, $t=0.5$';
    funtitleC3 = 'Optimal Control $\vec{w}$, $t=0.9$'; 
    %% set up domain
    geom.y1Min = -1; geom.y1Max = 1; geom.y2Min = -1; geom.y2Max = 1; % [-1,1] x [-1,1];
    geom.N = [nx;nx]; 
    aBox = Box(geom); % make a Box object
    aBox.ComputeAll;  % compute differentiation, integration, etc
    aBox.ComputeInterpolationMatrix((-1:0.01:1)',(-1:0.01:1)',true,true);
    
    Vext = R1.Vout;
    % Plot rho
    hf = figure('Position',[1,1,1600,400]);
    colormap(parula(30))
  
    ax1 = subplot(1,4,1); 
    ax2 = copyobj(ax1,hf);
    
    axes(ax1)
    aBox.plot(R1.w(:,t1),'color');
    caxis([opts.mic,opts.mac])
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax1,'FontSize',15);  
    axes(ax2)
    aBox.plot(Vext(:,t1),'contour');
    caxis([opts.Vi,opts.Va])
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax2,'FontSize',15); 
    
    colormap(ax1,'parula')
    colormap(ax2,'pink')
    
    linkaxes([ax1,ax2])
    ax2.Visible = 'off';
    ax2.XTick = [];
    ax2.YTick = [];
    

    ax1a = subplot(1,4,2); 
    ax2a = copyobj(ax1a,hf);
    axes(ax1a)
    aBox.plot(R1.w(:,t2),'color');
    caxis([opts.mic,opts.mac])
    title(funtitleC2,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax1a,'FontSize',15);  
    axes(ax2a)
    aBox.plot(Vext(:,t2),'contour');
    caxis([opts.Vi,opts.Va])
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax2a,'FontSize',15); 
    
    colormap(ax1a,'parula')
    colormap(ax2a,'pink')
    
    linkaxes([ax1a,ax2a])
    ax2a.Visible = 'off';
    ax2a.XTick = [];
    ax2a.YTick = [];
    
    ax1b = subplot(1,4,3); 
    ax2b = copyobj(ax1b,hf);
    axes(ax1b)
    aBox.plot(R1.w(:,t3),'color');
    caxis([opts.mic,opts.mac])
    title(funtitleC3,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax1b,'FontSize',15);  
    axes(ax2b)
    aBox.plot(Vext(:,t3),'contour');
    caxis([opts.Vi,opts.Va])
    title(funtitleC1,'interpreter', 'latex')
    xlabel('$x_1$','interpreter', 'latex')
    ylabel('$x_2$','interpreter', 'latex')
    set(ax2b,'FontSize',15); 
    
    colormap(ax1b,'parula')
    colormap(ax2b,'pink')
    
    linkaxes([ax1b,ax2b])
    ax2b.Visible = 'off';
    ax2b.XTick = [];
    ax2b.YTick = [];
    
    ax1c = subplot(1,4,4); 
    ax2c = copyobj(ax1c,hf);
    colormap(ax1c,'parula')
    colormap(ax2c,'pink')
    
    linkaxes([ax1c,ax2c])
    ax2c.Visible = 'off';
    ax2c.XTick = [];
    ax2c.YTick = [];
    ca = colorbar(ax1c,'location','west','Ticks',[0, 0.5, 1],...
                     'TickLabels',{round(opts.mic,2),round(opts.mec,2),round(opts.mac,2)},'TickLabelInterpreter','latex','FontSize',13);
    
    cb = colorbar(ax2c,'location','east','Ticks',[0, 0.5, 1],...
                     'TickLabels',{round(opts.Vi,2),round(opts.Ve,2),round(opts.Va,2)},'TickLabelInterpreter','latex','FontSize',13);
           
    cbar_handle = findobj(cb,'tag','Colorbar');
    v1 = get(cbar_handle,'Position');
    v1(1) = 0.81;
    set(cbar_handle,'Position',v1)
   % set(cbar_handle, 'YAxisLocation','right')
    axis off   
      
    save2png(fileName,gcf,300)

end