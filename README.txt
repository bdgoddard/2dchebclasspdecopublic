# README #

2DChebClassPDECO is an extension to the existing 2DChebClass library developed by Midred Aduamoah, Ben Goddard, John Pearson and Jonna Roden.  It is designed to tackle PDE-constrained optimisation problems for non-linear, non-local forward problems in a variety of 1D and 2D geometries.

2DChebClass is a code Andreas Nold and Ben Goddard developed as a project for DFT and DDFT computations in 2012 at Imperial College London. Since then, it evolved into a library of classes and functions to solve 1D and 2D DFT and DDFT problems.  Its use is not restricted to DFT and DDFT - it can be used to solve a wide range of (integro)-PDE systems in various 1D and 2D geometries.


### How do I run tests? ###

All examples using the Newton-Krylov solver can be found in the subdirectories Examples/NewtonKrylovExamples2D and Examples/NewtonKrylovExamples3D.
Examples using the fixed-point solver can be found in the subdirectories Examples/FixedPointExamples2D. 
Individual examples can be run and figures and tables from the paper can be reproduced, using the files in these subdirectories. 


Run any file in the subdirectories of Examples/PDECOCodeExamples.

### How do I write my own examples? ###

See the guidance in PDECO Input Options.pdf

### Change Data Folder ###
 
Please open "AddPaths.m" in the main folder. Define via "dirData" a folder where the computational results should be saved. 

### Matlab-Versions

The code currently runs using Matlab versions from Matlab2016a to Matlab2020a.

### Contact ###

Ben Goddard
b.goddard@ed.ac.uk
